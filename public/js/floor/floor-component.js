Vue.component('floors', {

    props: [
        'buttoncreate',
        'namespace',
        'architecture_id',
        'house_zone_placeholder'
    ],

    template: `
        <div class="floor-root-element">
             
            <div class="row pull-right">
            
                <div class="col-lg-12">
                
                    <button class="btn btn-primary" @click="createFloor">+ {{ buttoncreate }}</button>
                
                </div>
            
            </div>
            
            <div class="row">
            
                 <div v-for="floor in floors">
                 
                   <floor-item :floor="floor"></floor-item>
            
                </div>
            
            </div>
        
        </div>
    `,

    data: function() {

        return {

            floors: [],

            house_zone: []

        }
    },

    created: function() {

        this.getHouseZone();

        this.getFloors();
    },

    mounted: function() {

    },

    methods: {
        getFloors: function() {
            var self = this;
            var options = {
                params: {
                    architecture_id: self.architecture_id
                }
            };

            this.$http.get("/api/floors", options).then(
                function(response) {

                    self.floors = response.body.floors;

                }, function(errors) {


                }
            );
        },

        getHouseZone: function () {
            var self = this;

            this.$http.get("/api/house_zone").then(
                function(response) {

                    self.house_zone = response.body.house_zone;

                }, function(errors) { }
            );
        },

        createFloor: function() {

            var self = this;

            var options = {
                    params: {
                        architecture_id: self.architecture_id
                    }
                };

                this.$http.get("/api/floor/create/", options).then(function(response) {

                self.floors.push(response.body);

                }, function(errors) { }
            );
        },

    },

});

Vue.component('floor-item', {

    props: ['floor'],

    template: `
        <div>
             <div class="col-lg-12">
                
                <form :action="formAction" class="form-horizontal" enctype="multipart/form-data" method="POST">
    
                    <div class="ibox float-e-margins animated fadeInDown">
                        
                        <div class="ibox-title">
                            
                            <h5>Этаж номер {{ floor.number }}</h5>
                            
                            <div class="ibox-tools">
                            
                                <a class="collapse-link">
                            
                                    <i class="fa fa-chevron-up"></i>
                            
                                </a>
                            
                            </div>
                            
                        </div> 
                        
                        <div class="ibox-content">
                            
                            <div class="row">
                            
                                <div class="col-lg-6">
                                
                                    <input :id="getImageId" type="file" name="image" v-image_uploader>
                                
                                    <input type="hidden" name="number" :value="getFloorNumber"/>
                                
                                </div>
                                
                                <div class="col-lg-6">
                                
                                    <div class="row m-t-xl">
                                    
                                        <div class="form-group m-b-n">
                                            
                                            <div class="col-lg-12 b-m-n">
                                            
                                                <div class="input-group">
                                            
                                                    <input type="text" :value="getFloorArea" name="area" class="form-control area-input">
                                            
                                                    <span class="input-group-addon">
                                            
                                                        m<sup><small>2</small></sup>
                                            
                                                    </span>
                                            
                                                </div>
                                            
                                            </div>
                                        
                                        </div>
                                
                                    </div>
                                    
                                    <div class="hr-line-dashed m-l-n"></div>
                                    
                                    <div class="row">
                                    
                                        <div class="col-lg-12">
                                        
                                            <div class="form-group">
                                        
                                                <select :data-placeholder="getHousePlaceHolder" class="chosen-select house_zone_list" v-chosen :data-index="getFloorNumber">
                                                    
                                                    <option value="" disable></option>
                                                    
                                                    <option v-for="zone in getHouseZone" :value="zone.id">{{zone.name}}</option>
                                                
                                                </select>
                                        
                                            </div>
                                        
                                        </div>
                                    
                                    </div> 
                                    
                                </div>
                                
                                <div class="row p-lg">
                                
                                    <div class="col-lg-12">
                                    
                                        <div v-for="house_zone_item in house_zone_items" :id="getHouseZoneElementId" class="house_zone_input_list">
                                            
                                            <house_zone_list_item :houseZoneData="house_zone_item"></house_zone_list_item>
                                            
                                        </div>
                                    
                                    </div>
                                    
                                </div>
        
                            </div>
                        
                        </div>
        
                    </div>
                
                </form>
       
            </div>
                  
        </div>
    `,

    data: function() {

        return {

            house_zone_items: this.getZoneItems
        }
    },

    computed: {

        formAction: function() {

            return "/"+this.$parent.namespace+"/floors/"+this.$parent.architecture_id+"/edit";
        },

        getFloorNumber: function() {

            return this.floor.number;
        },

        getImageId: function() {

            return 'image_'+this.floor.id;
        },

        getHouseZoneElementId: function() {

            return 'houseZoneElement_'+this.floor.id;
        },

        getFloorArea: function() {

            return this.floor.area;
        },

        getHousePlaceHolder: function() {

            return this.$parent.house_zone_placeholder;
        },

        getHouseZone: function() {

            return this.$parent.house_zone;
        },

        getZoneItems: function() {
            // console.log(store.getters.getFloorHouseZoneItems(this.getFloorNumber));
            return store.getters.getFloorHouseZoneItems(this.getFloorNumber);
        }
    },

    methods: {
        /*Your methods lives here*/
        // ...mapGetters('mFloors', [
        //
        //     'getFloorHouseZoneItems'
        // ])

    }

});

Vue.component('house_zone_list_item', {

    props: ['houseZoneData'],

    template: `
        <div class="form-group animated fadeInLeft">
        
            <div class="col-lg-12">
               
               <input type="hidden" class="form-control" name="floor[house_zone_id][]" :value="houseZoneData.zoneID"> 
            
               <label class="col-lg-4 control-label">{{houseZoneData.zoneName}}</label>
               
               <div class="col-lg-6">
                
                    <input type="text" :placeholder="houseZoneData.zoneName" class="form-control" name="floor[house_zone_value][]">
                     
               </div>
                
                <div class="col-lg-2 m-l-n">
                
                    <button class="btn btn-danger btn-circle btn-outline btn-delete" type="button">
                    
                        <i class="fa fa-trash-o"></i>
                        
                    </button>
                
                </div>
                
            </div>
        
        </div>
    `
});

Vue.directive('chosen', {

    props: ['parentEl'],

    twoWay: true,

    inserted: function (el) {
        $(el).chosen({
            inherit_select_classes: true,
            width: '100%',
            disable_search_threshold: 999
        }).change(function(){
            var zoneName = $(this).find('option:selected').text();
            var zoneID = $(this).find('option:selected').val();
            var index  = $(this).attr('data-index');
            // console.log(store.dispatch(pushItem({'zoneName':zoneName, 'zoneID':zoneID, 'floor':index}));
            store.dispatch('mFloors/pushItem',{'zoneName':zoneName, 'zoneID':zoneID, 'floor':index});

        });
    }
});

Vue.directive('image_uploader', {
    twoWay: true,
    inserted: function (el) {
        var boxHTML = '<div class="fileuploader-items">' +
            '<ul class="fileuploader-items-list">' +
            '<li class="fileuploader-thumbnails-input" style="width: 100%;">'+
            '   <div class="fileuploader-thumbnails-input-inner">' +
            '      <img src="/img/common/placeholder.jpg" style="max-width: 100%;max-height: 100%">'+
            '      <span>+</span>'+
            '   </div>' +
            '</li>' +
            '</ul>' +
            '</div>';
        var itemHTML = '<li class="fileuploader-item" style="width: 100%;">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '<div class="progress-holder">${progressBar}</div>' +
            '</div>' +
            '</li>';
        var item2HTML = '<li class="fileuploader-item" style="width: 100%;">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '</div>' +
            '</li>';

        $(el).fileuploader({
            listInput: null,
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: ' ',
            theme: 'thumbnails',
            enableApi: true,
            addMore: false,
            //files:files,
            thumbnails: {
                box: boxHTML,
                item: itemHTML,
                item2: item2HTML,
                startImageRenderer: true,
                canvasImage: false,
                removeConfirmation: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    if(api.getFiles().length >= api.getOptions().limit) {
                        plusInput.hide();
                    }

                    plusInput.insertAfter(item.html);


                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        setTimeout(function() {
                            html.remove();

                            if(api.getFiles().length - 1 < api.getOptions().limit) {
                                plusInput.show();
                            }
                        }, 100);
                    });

                    $.ajax({
                        url:'/api/image_remove',
                        data:{id: inputEl.attr('data-id')},
                        daType: 'json',
                        success: function(response) {

                        },
                        fail:(function() {
                            alert( "error" );
                        })
                    })
                }
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = $.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            }
        });
    }
});

var app = new Vue({

    el: '#floors_container',

    store,

    components: { store }

});
