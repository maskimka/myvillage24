$(document).ready(function(){
    var boxHTML = '<div class="fileuploader-items">' +
        '<ul class="fileuploader-items-list">' +
        '<li class="fileuploader-thumbnails-input" style="width: 100%;">'+
        '   <div class="fileuploader-thumbnails-input-inner">' +
        '      <img src="/img/common/placeholder.jpg" style="max-width: 100%;max-height: 100%">'+
        '      <span>+</span>'+
        '   </div>' +
        '</li>' +
        '</ul>' +
        '</div>';
    var itemHTML = '<li class="fileuploader-item" style="width: 100%;">' +
        '<div class="fileuploader-item-inner">' +
        '<div class="thumbnail-holder">${image}</div>' +
        '<div class="actions-holder">' +
        '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
        '</div>' +
        '<div class="progress-holder">${progressBar}</div>' +
        '</div>' +
        '</li>';
    var item2HTML = '<li class="fileuploader-item" style="width: 100%;">' +
        '<div class="fileuploader-item-inner">' +
        '<div class="thumbnail-holder">${image}</div>' +
        '<div class="actions-holder">' +
        '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
        '</div>' +
        '</div>' +
        '</li>';
    
// ==========================================
// one image upload
// ==========================================
    $('.simple_floor_image').each(function(){
        var files = [];
        var image = $(this).attr('data-content');
        if (typeof image !== 'undefined') {

            var fileElement = {};
            fileElement.name = image.split('/').pop();
            fileElement.type = 'image/jpeg';
            fileElement.file = image;

            files.push(fileElement);
        }

        $(this).fileuploader({
            listInput: null,
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: ' ',
            theme: 'thumbnails',
            enableApi: true,
            addMore: false,
            files:files,
            thumbnails: {
                box: boxHTML,
                item: itemHTML,
                item2: item2HTML,
                startImageRenderer: true,
                canvasImage: false,
                removeConfirmation: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    if(api.getFiles().length >= api.getOptions().limit) {
                        plusInput.hide();
                    }

                    plusInput.insertAfter(item.html);


                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        setTimeout(function() {
                            html.remove();

                            if(api.getFiles().length - 1 < api.getOptions().limit) {
                                plusInput.show();
                            }
                        }, 100);
                    });

                }
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = $.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            }
        });
    });

    $("#addFloor").click(function(e){

        e.preventDefault();

        $.ajax({
            url: '/api/floor/create/',
            data:{'architecture_id':1},
            dataType: 'json',
            success: function(response){
                
            },
            error: function(error){
                alert('Exception happen');
            }
        })
    });
});
