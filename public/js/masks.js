$(document).ready(function() {

    if(typeof masks[user_country] !== "undefined") {
        $('#landCadastralNumber').inputmask(masks[user_country].cadastral);
    }

    if(typeof masks[user_country] !== "undefined") {
        $('.mobile-mask').inputmask(masks[user_country].mobile);
    } else {
        $('.mobile-mask').inputmask('+999 99 99 99 99');
    }

    if(typeof masks[user_country] !== "undefined") {
        $('.phone-mask').inputmask(masks[user_country].phone);
    } else {
        $('.phone-mask').inputmask('+999 99 99 99 99');
    }

    $(".decimal-mask").inputmask({
        alias:"decimal",
        integerDigits:11,
        digits:2,
        allowMinus:false,
        digitsOptional: false,
        placeholder: "0",
        rightAlign: false
    });

    $(".date-mask").inputmask("9999-99-99", {placeholder: 'YYYY-MM-DD' });

    $(".email-mask").inputmask({
        alias:"email"
    });
});