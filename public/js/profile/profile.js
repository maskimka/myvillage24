$(document).ready(function(){
// ======================================================
//                  PROFILE EDIT FORM
// ======================================================
    var wizard_form =
        $("#wizard-form").steps({
            bodyTag: "fieldset",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                // Always allow going backward even if the current step contains invalid fields!
                if (currentIndex > newIndex)
                {
                    return true;
                }

                var form = $(this);

                // Clean up if user went backward before
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }

                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Start validation; Prevent going forward if false
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex)
            {

            },
            onFinishing: function (event, currentIndex)
            {
                var form = $(this);

                // Disable validation on fields that are disabled.
                // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                form.validate().settings.ignore = ":disabled";

                // Start validation; Prevent form submission if false
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                var form = $(this);

                // Submit form input
                form.submit();
            }
        });

        wizard_form.validate({
        errorPlacement: function (error, element)
        {
            element.closest('.input-group').before(error);
        },
        ignore: ":hidden:not(.chosen-select)",
        rules: {
            first_name:{
                required: true,
                 minlength: 3
            },
            last_name:{
                required: true,
                 minlength: 3
            },
            birthday:{
                //required: true,
                date: true
            },
            password:{
                required:false,
                 //strong_pwd:true
            },
            confirmed: {
                equalTo: "#password"
            },
            country_id:{
                required:true,
                min: 1,
            },
            zip:{
                number: true
            },
            email:{
                required: true,
                email: true
            },
            terms:{
                required: true
            }
        }
    });
// ======================================================
//                  END PROFILE EDIT FORM
// ======================================================
    var imageCroppedCoords;
    var avatarContainer;
    /*change avatar*/
    var inputFile = $('#file-input').on("change", function(e){
         $('#imagecrop').modal('show');

         var $image = $(".image-crop > img");
         avatarContainer = $(this).find('img');
         imageCroppedCoords = $(this).next('input');
         
         $($image).cropper({
             aspectRatio: 1 / 1,
             preview: ".img-preview",
             minWidth: 500,
             minHeight: 500,
             multiple: false,
             done: saveCropParam
         });

         if (window.FileReader) {
             var fileReader = new FileReader(),
                 files = this.files,
                 file;

             if (!files.length) {
                 return;
             }

             file = files[0];

             if (/^image\/\w+$/.test(file.type)) {
                 fileReader.readAsDataURL(file);
                 fileReader.onload = function (e) {
                     $(this).val("");
                     $image.cropper("reset", true).cropper("replace", this.result);
                     $(avatarContainer).prop('src', e.target.result);
                 };
             } else {
                 showMessage("Please choose an image file.");
             }
         }

         $('.imagecrop-btn').click(function(){
             $('#avatarForm').submit();
             $('#imagecrop').modal('hide');
         });

         $('.imagecrop-close-btn').click(function(){
             $(inputFile).val('');
         });

         $(".image_preview.filled").closest(".image-upload").on("mouseenter", function(){
             $(this).find('.file_upload_buttons').fadeIn("500");
         });

         $(".image_preview.filled").closest(".image-upload").on("mouseleave", function(){
             $(this).find('.file_upload_buttons').fadeOut("500");
         });

         $(".file_upload_buttons.close").on("click",function(){
             //alert(1);
         });

         function saveCropParam(data){
             $(imageCroppedCoords).val(JSON.stringify(data));
         }

    });

    $('.input-group.birthday').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'yyyy-mm-dd',
        startView: 2
    });

    toastr.options = {
        "closeButton": true,
        "progressBar": true,
    };

    /* Request update role */
    $("#getRoleRealtorBtn").on('click', function() {

        var self = this;
        var success = false;
        var ladda = $('#getRoleRealtorBtn').ladda();
        ladda.ladda('start');
        var roleId   = $("#roleRealtor > input[name='role']").val();

        $.ajax({
            url: '/' + namespace_path + '/profile/role/realtor',
            type: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {roleId: roleId},
            success: function(response) {
                success = true;
                var message = "Роль риелтора была присвоена";
                window.location.reload();
            },
            error: function(e) {
                var message = "Не получилось присвоить роль";
                setTimeout(function() {
                    toastr.error(message, 'Ошибка!');
                },1000)
            },
            complete: function() {
                $('a.close').trigger('click');
                setTimeout(function() {
                    ladda.ladda('stop');
                    if(success) {
                        $(self).html('Риелтор');
                        $(self).prop('disabled', true);
                    }
                },1000)
            }
        });
    });

    $("#roleArchitect").validate({
        errorPlacement: function (error, element)
        {
            if (element.get(0).name == 'docs') {
                element.closest('.form-group').before(error);
            }
        },
        rules: {
            docs:{
                required: true,
                extension: "rar|zip",
                filesize: 7
            }
        },
        submitHandler: function(form, e) {

            e.preventDefault();
            var self = $('#getRoleArchitectBtn');
            var success = false;
            var ladda = $(self).ladda();
            ladda.ladda('start');

            var data = new FormData();
            var role   = form.role.value;
            var files  = $("#userDocs")[0].files[0];

            data.append('roleId', role);
            data.append('archive', files);

            $.ajax({
                url: '/' + namespace_path + '/profile/role/architect',
                type: 'post',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data,
                success: function(response) {
                    success = true;
                    var message = "Роль архитектора была присвоена";
                    if ($.isEmptyObject(response)) {
                        setTimeout(function() {
                            toastr.success(message, 'Успешно!');
                        },1000);
                    } else {
                        if (response.archive !== undefined) {
                            $('#userDocs').closest('.form-group').before('<label class="error">' + response.archive[0] + '</label>');
                        }
                        message = "Не получилось присвоить роль";
                        setTimeout(function() {
                            toastr.error(message, 'Ошибка!');
                        },1000)
                    }

                },
                error: function(e) {
                    var message = "Не получилось присвоить роль";
                    setTimeout(function() {
                        toastr.error(message, 'Ошибка!');
                    },1000)
                },
                complete: function() {
                    $('a.close').trigger('click');
                    setTimeout(function() {
                        ladda.ladda('stop');if(success) {
                            $(self).html('Архитектор');
                            $(self).prop('disabled', true);
                        }
                    },1000)
                }
            });
        }
    });

    $("#destroyUser").click(function() {
        var url = $(this).data("action");
        swal({
                title: Lang.get('messages.text_confirm_delete'),
                text: null,
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: Lang.get('messages.button_delete'),
                cancelButtonText: Lang.get('messages.button_cancel'),
                showCancelButton: true,
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true,
            },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {_method:"DELETE"},
                        dataType:'json',
                        success: function(){
                            swal({
                                title: Lang.get('messages.text_delete_success'),
                                text: null,
                                type: "success",
                                confirmButtonColor: "#1AB394",
                            },function(){
                               window.location.reload();
                            });
                        },
                        error: function(){
                            swal({
                                title: Lang.get('messages.text_error_occurred'),
                                text: null,
                                type: "error",
                            });
                        }
                    })
                } else {
                    swal({
                        title: Lang.get('messages.text_cancel_success'),
                        text: null,
                        type: "error",
                        //confirmButtonColor: "#1AB394",
                    });
                }
            });
    });
});
