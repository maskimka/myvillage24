$(document).ready(function(){
    $('.footable').footable();
// ======================================================
//              CREATE EDIT LAND FORM
// ======================================================
    var wizard_village_form =
        $("#wizard_village_form").steps({
            bodyTag: "fieldset",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                // Always allow going backward even if the current step contains invalid fields!
                if (currentIndex > newIndex)
                {
                    return true;
                }

                var form = $(this);

                // Clean up if user went backward before
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }

                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Start validation; Prevent going forward if false
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex)
            {

            },
            onFinishing: function (event, currentIndex)
            {
                var form = $(this);

                // Disable validation on fields that are disabled.
                // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                form.validate().settings.ignore = ":disabled";

                // Start validation; Prevent form submission if false
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                var form = $(this);
                // Submit form input
                form.submit();
            }
        });

    wizard_village_form.validate({
        errorPlacement: function (error, element)
        {
            element.closest('.input-group').before(error);
            element.closest('.image-upload').before(error);
        },
        ignore: ":hidden:not(.chosen-select)",
        rules: {
            area:{
                number: true
            },
            from_city:{
                required: true,
                number: true
            },
            purpose_id: {
                number: true
            },
            description:{
                minlength: 100
            },
            state: {
                nonNumeric: true
            },
            price: {
                number:true
            },
            region: {
                nonNumeric: true
            },
            city: {
                nonNumeric: true
            },
            country_id:{
                number:true
            },
            lat: {
                required:true,
                number:true
            },
            lng: {
                required:true,
                number:true
            },
            //land_video: {
            //    url: true
            //}
        }
    });
// ======================================================
//            END CREATE EDIT LAND FORM
// ======================================================

    $('input[name="general_area"]').on("blur",function(){
        var value = (parseFloat($(this).val()))?parseFloat($(this).val()):0;
        var text = "";
            text += value;
            text += "&nbsp;m<sup><small>2</small></sup>";

        $('#top-general_area').html(text);
    });

    $('.table-foo').on('after.ft.paging', function(e, ft) {

        $('.switch').change(function() {

            var self = this;
            var village_id = $(this).closest('tr').data('content');
            var flag = $(self).find("input[type=checkbox]").prop("checked")?1:0;

            toastr.options = {
                "closeButton": true,
                "progressBar": true,
            };

            $.ajax({
                type: 'post',
                url: '/' + namespace_path + '/village/activate',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {village_id: village_id, activate: flag},
                success: function(response) {

                    var message = (flag) ? "Активирован" : "Деактивирован";
                    toastr.success(message, 'Успешно!');

                },
                error: function(e) {

                    toastr.error("Изменения не сохранились",'Error!');
                    setTimeout(function () {
                        $(self).find("input[type=checkbox]").prop("checked", !flag);
                    }, 200);
                }
            });
        });
    });
});
