$(document).ready(function() {

    $('.table-foo').on('after.ft.paging', function(e, ft) {

        var previous;

        $('.chosen-no-search').on('chosen:ready', function(e, params) {
            var control = $(this).next().find('a');
            $(control).css('color', $(this).css('color'));

        }).chosen(
            {
                disable_search: true,
                width: "170px"
            }
        ).on('chosen:showing_dropdown', function(e, params){

            previous = $(this).find('option:selected').val();

        }).change(function(e, params) {

            var self = this;
            var pto_request_id = $(this).data('ptoRequest');
            var status_id = params.selected;

            toastr.options = {
                "closeButton": true,
                "progressBar": true,
            };

            $.ajax({
                url: '/' + namespace_path + '/pto_request/change_status',
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {pto_request_id: pto_request_id, status_id: status_id},
                success: function(response) {

                    var control = $(self).next().find('a');
                    $(control).css('color', $(self).find('option:selected').data('color'));

                    var message = "Request's status was modified";
                    toastr.success(message, 'Success!');
                },
                error: function(e) {
                    toastr.error("Could not modify request's status",'Error!');

                    $(self).val(previous);
                    setTimeout(function() {
                        $(self).trigger("chosen:updated");
                    }, 500);

                }
            });
        });
    });
});