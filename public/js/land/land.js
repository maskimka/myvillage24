$(document).ready(function(){

    //$('.footable').footable();

// ======================================================
//              CREATE EDIT LAND FORM
// ======================================================
    var wizard_land_form =
        $("#wizard_land_form").steps({
            bodyTag: "fieldset",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                // Always allow going backward even if the current step contains invalid fields!
                if (currentIndex > newIndex)
                {
                    return true;
                }

                var form = $(this);

                // Clean up if user went backward before
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }

                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Start validation; Prevent going forward if false
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex)
            {
                if (priorIndex == 0) {


                    setTimeout(function(){

                        initialize();


                        if ($('#mapLat').val() == "" || $('#mapLng').val() == "") {

                            marker.setPosition(countries[userCountry].center);
                        } else {

                            marker.setPosition({
                                lat: parseFloat($('#mapLat').val()),
                                lng: parseFloat($('#mapLng').val())
                            });
                        }

                        map.setCenter(marker.getPosition());

                    }, 300);
                }
            },
            onFinishing: function (event, currentIndex)
            {
                var form = $(this);

                // Disable validation on fields that are disabled.
                // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                form.validate().settings.ignore = ":disabled";

                // Start validation; Prevent form submission if false
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                var form = $(this);
                // Submit form input
                form.submit();
            }
        });

         wizard_land_form.validate({
             errorPlacement: function (error, element)
             {
                 element.closest('.input-group').before(error);
                 element.closest('.image-upload').before(error);
             },
             ignore: ":hidden:not(.chosen-select)",
             rules: {
                 area:{
                     number: true
                 },
                 from_city:{
                     required: true,
                     number: true
                 },
                 purpose_id: {
                    number: true
                 },
                 description:{
                     minlength: 100
                 },
                 //state: {
                 //    nonNumeric: true
                 //},
                 price: {
                     number:true
                 },
                 region: {
                     number: true
                 },
                 district: {
                     number: true
                 },
                 city: {
                     number: true
                 },
                 country_id:{
                     number:true
                 },
                 lat: {
                     required:true,
                     number:true
                 },
                 lng: {
                     required:true,
                     number:true
                 },
                 //land_video: {
                 //    url: true
                 //},
             }
         });
// ======================================================
//            END CREATE EDIT LAND FORM
// ======================================================

// ======================================================
//            4 Image uploader
// ======================================================
    $('.simple_image_complement').each(function(){
        var boxHTML = '<div class="fileuploader-items">' +
            '<ul class="fileuploader-items-list">' +
            '<li class="fileuploader-thumbnails-input" style="width: 100%;">'+
            '   <div class="fileuploader-thumbnails-input-inner">' +
            '      <img src="/img/common/placeholder.jpg" style="max-width: 100%;max-height: 100%">'+
            '      <span>+</span>'+
            '   </div>' +
            '</li>' +
            '</ul>' +
            '</div>';
        var itemHTML = '<li class="fileuploader-item" style="width: 100%;">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '<div class="progress-holder">${progressBar}</div>' +
            '</div>' +
            '</li>';
        var item2HTML = '<li class="fileuploader-item" style="width: 100%;">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '</div>' +
            '</li>';
        var files = [];

        var image = $(this).attr('data-content');
        
        if (typeof image !== 'undefined') {
            var fileElement = {};
            fileElement.name = image.split("/").pop();
            fileElement.type = 'image/jpeg';
            fileElement.file = image;

            files.push(fileElement);
        }

        $(this).fileuploader({
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: ' ',
            theme: 'thumbnails',
            enableApi: true,
            addMore: false,
            files:files,
            thumbnails: {
                box: boxHTML,
                item: itemHTML,
                item2: item2HTML,
                startImageRenderer: true,
                canvasImage: false,
                removeConfirmation: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    if(api.getFiles().length >= api.getOptions().limit) {
                        plusInput.hide();
                    }

                    plusInput.insertAfter(item.html);


                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        setTimeout(function() {
                            html.remove();

                            if(api.getFiles().length - 1 < api.getOptions().limit) {
                                plusInput.show();
                            }
                        }, 100);
                    });
                }
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = $.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            }
        }); 
    });
    
// ======================================================
//            END 4 Image uploader
// ======================================================

// ======================================================
//            Complement Part
// ======================================================
    $("#landParties").on("change", function(){
        var parts = $(this).find("option:selected").val();
        var container = $(this).closest('.form-group').find(".parts-container");

        $(container).html('');

        for(var i = 0; i < parts; i++) {
            var name = (i+1)+"-"+(i+2);

            if (i == (parts-1)) {
                name = (i+1) + "-1";
            }

            var html = "";
                html += "<div class='col-sm-6'>";
                html += "<div class='form-group'>";
                html += "<div class='input-group'>";
                html += "<input class='form-control' type='text' name='parties["+name+"]' placeholder='"+name+"'>";
                html += "<span class='input-group-addon'>m<sup><small>2</small></sup></span>";
                html += "</div>";
                html += "</div>";
                html += "</div>";

                $(container).append(html);
        }

        $(".parts-container input.form-control").each(function(){
            $(this).rules('add',{
                required:true,
                number: true,
                range:[0.50,99999.99]
            })
        });
    });

    /*FORM*/

    $("#complementland_form").validate({
        errorPlacement: function (error, element)
        {
            element.closest('.input-group').before(error);
            element.closest('.image-upload').before(error);
        },
        rules:{
            imageSizeMap:{
                extension: 'png|jpeg|jpg',

            },
            imageCadastralMap:{
                extension: 'png|jpeg|jpg'
            }
        }
    });

    $(".parts-container input.form-control").each(function(){
        $(this).rules('add',{
            required:true,
            number: true,
            range:[0.50,99999.99]
        })
    });

    /*END FORM*/

    $('.table-foo').on('after.ft.paging', function(e, ft) {

        var previous;
        $('.chosen-no-search').chosen(
            {
                disable_search: true,
                width: "182px"
            }
        ).on('chosen:showing_dropdown', function(e, params){

            previous = $(this).find('option:selected').val();

        }).change(function(e, params) {

            var self = this;
            var land_id = $(this).data('land');
            var status_id = params.selected;

            toastr.options = {
                "closeButton": true,
                "progressBar": true,
            };

            $.ajax({
                url: '/' + namespace_path + '/land/change_sale_status',
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {land_id: land_id, status_id: status_id},
                success: function(response) {
                    var message = "Land's sale status was modified";
                    toastr.success(message, 'Success!');
                },
                error: function(e) {
                    toastr.error("Could not modify land's sale status",'Error!');

                    $(self).val(previous);
                    setTimeout(function() {
                        $(self).trigger("chosen:updated");
                    }, 500);

                }
            });
        });

        /* Switcher */

        $('.switch').change(function() {

            var self = this;
            var land_id = $(this).closest('tr').data('content');
            var flag = $(self).find("input[type=checkbox]").prop("checked")?1:0;

            toastr.options = {
                "closeButton": true,
                "progressBar": true,
            };

            $.ajax({
                type: 'post',
                url: '/' + namespace_path + '/land/activate',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {land_id: land_id, activate: flag},
                success: function(response) {

                    var message = (flag) ? "Land was activated" : "Land was deactivated";
                    toastr.success(message, 'Success!');

                },
                error: function(e) {

                    toastr.error("Could not save modification",'Error!');
                    setTimeout(function () {
                        $(self).find("input[type=checkbox]").prop("checked", !flag);
                    }, 200);
                }
            });
        });
        /* End switch*/
    });
});