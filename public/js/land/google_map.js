var map;
var marker;
var userCountry = $("#userCountryIso").val();
var countryRestrict = {'country': userCountry};

function initialize() {

    var latlng = new google.maps.LatLng(0, 0);

    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: latlng,
        zoom: countries[userCountry].map_zoom
    });

    marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
    });

    var input = document.getElementById('searchInput');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var geocoder = new google.maps.Geocoder();
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    autocomplete.setTypes(["(regions)"]);
    //autocomplete.setComponentRestrictions({'country': ['ru', 'md', 'ro', 'ua', 'de', 'es', 'ch', 'at']});

    var infowindow = new google.maps.InfoWindow();
    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        bindDataToForm(place,place.geometry.location.lat(),place.geometry.location.lng());
        infowindow.setContent(place.formatted_address);
        infowindow.open(map, marker);

    });

    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    bindDataToForm(results[0],marker.getPosition().lat(),marker.getPosition().lng());
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });
    });
}
function bindDataToForm(address, lat, lng) {

    clearAutocompleteForm();

    var administrative = '';
    var sublocality = '';

    $.each(address.address_components, function(index, item) {

        if (item.types[0] == 'country') {
            document.getElementById('country').value = item.long_name;
            document.getElementById('country_code').value = item.short_name.toLowerCase();
        }
        if (item.types[0].slice(0, 14) == 'administrative') {
            administrative += item.long_name + ' ';
        }
        if (item.types[0] == 'locality') {
            document.getElementById('locality').value = item.long_name;
        }
        if (item.types[0].slice(0, 11) == 'sublocality' || item.types[0] == 'neighborhood') {
            sublocality += item.long_name + ' ';
        }
        if ((item.types[0] == 'route' || item.types[0] == 'street_address') && item.long_name !== 'Unnamed Road') {
            document.getElementById('street').value = item.long_name;
        }
        if (item.types[0] == 'street_number') {
            document.getElementById('street_number').value = item.long_name;
        }
    });

    document.getElementById('administrative').value = administrative.trim();
    document.getElementById('sublocality').value = sublocality.trim();

    document.getElementById('mapLat').value = lat;
    document.getElementById('mapLng').value = lng;
}

function clearAutocompleteForm() {

    document.getElementById('country').value        = "";
    document.getElementById('locality').value       = "";
    document.getElementById('administrative').value = "";
    document.getElementById('sublocality').value    = "";
    document.getElementById('street').value         = "";
    document.getElementById('street_number').value  = "";

    document.getElementById('mapLat').value = "";
    document.getElementById('mapLng').value = "";
}

google.maps.event.addDomListener(window, 'load', initialize);


