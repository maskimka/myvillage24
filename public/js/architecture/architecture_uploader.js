$(document).ready(function(){
// ==========================================
// multiple image main gallery upload
// ==========================================
    var files = [];
    var imagesJson = $('#architecture_images_here').val();
    var architecture_images;

    if(typeof imagesJson !== 'undefined') {

        architecture_images = $.parseJSON(imagesJson);
        $.each(architecture_images, function(index, item){
            var fileElement = {};
            fileElement.name = item.name;
            fileElement.type = 'image/jpeg';
            fileElement.file = '/img/uploads/architecture/thumbnails/'+item.name;

            files.push(fileElement);
        });
        $('#architecture_images_here').remove();
    }

    $('input[name="images[]"]').fileuploader({
            limit: 9,
            fileMaxSize: 2,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: ' ',
            theme: 'thumbnails',
            enableApi: true,
            addMore: true,
            files: files,
            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input" style="width: 33%">'+
                    '   <div class="fileuploader-thumbnails-input-inner">' +
                     '      <img src="/img/common/placeholder.jpg" style="max-width: 100%;max-height: 100%">'+
                     '      <span>+</span>'+
                    '   </div>' +
                    '</li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item" style="width: 33%">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="thumbnail-holder">${image}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                    '</div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item" style="width: 33%">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="thumbnail-holder">${image}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                    '</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                removeConfirmation: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    // remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input');

                    plusInput.insertAfter(item.html);

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                }
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = $.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            }
        });

// ==========================================
// multiple image facade gallery upload
// ==========================================
    var files = [];

    var imagesJson = $('#facade_images_here').val();
    var facade_images_here;

    if(typeof imagesJson !== 'undefined') {

        facade_images_here = $.parseJSON(imagesJson);
        $.each(facade_images_here, function(index, item){
            var fileElement = {};
            fileElement.name = item.name;
            fileElement.type = 'image/jpeg';
            fileElement.file = '/img/uploads/architecture/thumbnails/'+item.name;

            files.push(fileElement);
        });
        $('#facade_images_here').remove();
    }

    $('input[name="facade_gallery[]"]').fileuploader({
        limit: 10,
        fileMaxSize: 2,
        extensions: ['jpg', 'jpeg', 'png'],
        changeInput: ' ',
        theme: 'thumbnails',
        enableApi: true,
        addMore: true,
        files: files,
        thumbnails: {
            box: '<div class="fileuploader-items">' +
            '<ul class="fileuploader-items-list">' +
            '<li class="fileuploader-thumbnails-input" style="width: 33%">'+
            '   <div class="fileuploader-thumbnails-input-inner">' +
            '      <img src="/img/common/placeholder.jpg" style="max-width: 100%;max-height: 100%">'+
            '      <span>+</span>'+
            '   </div>' +
            '</li>' +
            '</ul>' +
            '</div>',
            item: '<li class="fileuploader-item" style="width: 33%">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '<div class="progress-holder">${progressBar}</div>' +
            '</div>' +
            '</li>',
            item2: '<li class="fileuploader-item" style="width: 33%">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '</div>' +
            '</li>',
            startImageRenderer: true,
            canvasImage: false,
            removeConfirmation: false,
            _selectors: {
                list: '.fileuploader-items-list',
                item: '.fileuploader-item',
                start: '.fileuploader-action-start',
                retry: '.fileuploader-action-retry',
                // remove: '.fileuploader-action-remove'
            },
            onItemShow: function(item, listEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input');

                plusInput.insertAfter(item.html);

                if(item.format == 'image') {
                    item.html.find('.fileuploader-item-icon').hide();
                }
            }
        },
        afterRender: function(listEl, parentEl, newInputEl, inputEl) {
            var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                api = $.fileuploader.getInstance(inputEl.get(0));

            plusInput.on('click', function() {
                api.open();
            });
        }
    });


// ==========================================
// multiple image interior gallery upload
// ==========================================
    var files = [];

    var imagesJson = $('#interior_images_here').val();
    var interior_images_here;

    if(typeof imagesJson !== 'undefined') {

        interior_images_here = $.parseJSON(imagesJson);
        $.each(interior_images_here, function(index, item){
            var fileElement = {};
            fileElement.name = item.name;
            fileElement.type = 'image/jpeg';
            fileElement.file = '/img/uploads/architecture/thumbnails/'+item.name;

            files.push(fileElement);
        });
        $('#interior_images_here').remove();
    }

    $('input[name="interior_gallery[]"]').fileuploader({
        limit: 10,
        fileMaxSize: 2,
        extensions: ['jpg', 'jpeg', 'png'],
        changeInput: ' ',
        theme: 'thumbnails',
        enableApi: true,
        addMore: true,
        files: files,
        thumbnails: {
            box: '<div class="fileuploader-items">' +
            '<ul class="fileuploader-items-list">' +
            '<li class="fileuploader-thumbnails-input" style="width: 33%">'+
            '   <div class="fileuploader-thumbnails-input-inner">' +
            '      <img src="/img/common/placeholder.jpg" style="max-width: 100%;max-height: 100%">'+
            '      <span>+</span>'+
            '   </div>' +
            '</li>' +
            '</ul>' +
            '</div>',
            item: '<li class="fileuploader-item" style="width: 33%">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '<div class="progress-holder">${progressBar}</div>' +
            '</div>' +
            '</li>',
            item2: '<li class="fileuploader-item" style="width: 33%">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '</div>' +
            '</li>',
            startImageRenderer: true,
            canvasImage: false,
            removeConfirmation: false,
            _selectors: {
                list: '.fileuploader-items-list',
                item: '.fileuploader-item',
                start: '.fileuploader-action-start',
                retry: '.fileuploader-action-retry',
                // remove: '.fileuploader-action-remove'
            },
            onItemShow: function(item, listEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input');

                plusInput.insertAfter(item.html);

                if(item.format == 'image') {
                    item.html.find('.fileuploader-item-icon').hide();
                }
            }
        },
        afterRender: function(listEl, parentEl, newInputEl, inputEl) {
            var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                api = $.fileuploader.getInstance(inputEl.get(0));

            plusInput.on('click', function() {
                api.open();
            });
        }
    });


// ==========================================
// one image upload
// ==========================================
    var boxHTML = '<div class="fileuploader-items">' +
        '<ul class="fileuploader-items-list">' +
        '<li class="fileuploader-thumbnails-input" style="width: 100%;">'+
        '   <div class="fileuploader-thumbnails-input-inner">' +
        '      <img src="/img/common/placeholder.jpg" style="max-width: 100%;max-height: 100%">'+
        '      <span>+</span>'+
        '   </div>' +
        '</li>' +
        '</ul>' +
        '</div>';
    var itemHTML = '<li class="fileuploader-item" style="width: 100%;">' +
        '<div class="fileuploader-item-inner">' +
        '<div class="thumbnail-holder">${image}</div>' +
        '<div class="actions-holder">' +
        '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
        '</div>' +
        '<div class="progress-holder">${progressBar}</div>' +
        '</div>' +
        '</li>';
    var item2HTML = '<li class="fileuploader-item" style="width: 100%;">' +
        '<div class="fileuploader-item-inner">' +
        '<div class="thumbnail-holder">${image}</div>' +
        '<div class="actions-holder">' +
        '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
        '</div>' +
        '</div>' +
        '</li>';

    $('.simple_image').each(function(){
        var files = [];
        var image = $(this).attr('data-content');
        if (typeof image !== 'undefined') {
            var fileElement = {};
            fileElement.name = image.split("/").pop();
            fileElement.type = 'image/jpeg';
            fileElement.file = image;

            files.push(fileElement);
        }

        $(this).fileuploader({
            // listInput: null,
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: ' ',
            theme: 'thumbnails',
            enableApi: true,
            addMore: false,
            files:files,
            thumbnails: {
                box: boxHTML,
                item: itemHTML,
                item2: item2HTML,
                startImageRenderer: true,
                canvasImage: false,
                removeConfirmation: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    if(api.getFiles().length >= api.getOptions().limit) {
                        plusInput.hide();
                    }

                    plusInput.insertAfter(item.html);


                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        setTimeout(function() {
                            html.remove();

                            if(api.getFiles().length - 1 < api.getOptions().limit) {
                                plusInput.show();
                            }
                        }, 100);
                    });
                }
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = $.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            }
        });
    });
});
