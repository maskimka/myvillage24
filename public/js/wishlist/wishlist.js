$(document).ready(function() {

    $('.table-foo').on('after.ft.paging', function(e, ft) {

        $('.parent-checkbox').on('change', function() {

            $('.child-checkbox').prop('checked', $(this).is(':checked'));
        });

        $('.child-checkbox').on('change', function() {

            var checkParent = true;

            $(".child-checkbox").each(function() {

                if(!$(this).is(':checked')) {
                    checkParent = false;
                    return false;
                }
            });

            $('.parent-checkbox').prop('checked', checkParent);

        });
    });

    $('#wishlistOrderModal').on('show.bs.modal', function(e) {

        var articles = $('input[name="products[]"]:checked');

        $('#articlesCount').html(articles.length);

        $(articles).each(function(key, item) {

            var element = '';

            if ($(item).data('type') == 'Land') {
                element = "<input type='hidden' name='models[lands][]' value='" + $(item).val() + "'>";
            }

            if ($(item).data('type') == 'Architecture') {
                element = "<input type='hidden' name='models[architecture][]' value='" + $(item).val() + "'>";
            }

            if ($(item).data('type') == 'Building') {
                element = "<input type='hidden' name='models[buildings][]' value='" + $(item).val() + "'>";
            }

            $('#ptoRequestForm').append(element);
        });

        if (!articles.length) {

            e.preventDefault();

            swal({
                title: "Внимание",
                text: "Добавьте пожалуйста минимум 1 артикль",
                type: "warning",
                confirmButtonColor: "#1AB394",
                confirmButtonText: "Продолжить",
                closeOnConfirm: true,
            });
        }
    });
});