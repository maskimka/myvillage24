$(document).ready(function(){

    /*COMMON*/
    $('.logout').on('click', function(){
        $(this).closest('form.logout_form').submit();
        return false;
    });

    $('#lang_change').change(function(){
        var locale = $(this).val();
        var _token = $("input[name='_token']").val();

        $.ajax({
            url: "/language",
            type: "POST",
            data: {locale:locale, _token:_token},
            dataType:'json',
            success: function(response){

            },
            error: function(response){

            },
            complete: function(){
                window.location.reload(true);
            }
        })
    });
    
/*Chosen*/
    $('.chosen-select').chosen({width:"100%"});

    $('.chosen-select-nosearch').chosen({
        width:"100%",
        disable_search_threshold: 50
    });

/*iCheck*/
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

/*dataPicker*/
    $('.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
/*footable*/

    var denyEdit = ['/admin/order/', '/admin/pto_request/', '/admin/wishlist/'];
    var denyDelete = ['/admin/seo/land/', '/admin/seo/architecture/', '/admin/seo/building/'];

    var pathname = window.location.pathname + '/';
    $('.table-foo').footable({
        editing: {
            enabled: true,
            alwaysShow: true,
            allowAdd: false,
            allowEdit: !denyEdit.includes(pathname),
            allowDelete: !denyDelete.includes(pathname),
            editRow: function(row) {
                var element = $(row.$el).get(0);
                var modelId = $(element).data('content');
                var lang = $(element).data('lang');
                var params = (lang != undefined)?'?lang='+lang:'';
                window.location.href = pathname + modelId+"/edit"+params;
            },
            deleteRow: function(row) {
                var element = $(row.$el).get(0);
                var modelId = $(element).data('content');
                var modelType = $(element).data('type');
                var _token = $("input[name='_token']").val();
                swal({
                        title: Lang.get('messages.text_confirm_delete'),
                        text: null,
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: Lang.get('messages.button_delete'),
                        cancelButtonText: Lang.get('messages.button_cancel'),
                        showCancelButton: true,
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        showLoaderOnConfirm: true,
                    },
                    function(isConfirm){
                        if (isConfirm) {
                            $.ajax({
                                url: pathname + modelId,
                                type: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {_method:"DELETE", type: modelType, id: modelId},
                                dataType:'json',
                                success: function(response){
                                    swal({
                                        title: Lang.get('messages.text_delete_success'),
                                        text: null,
                                        type: "success",
                                        confirmButtonColor: "#1AB394",
                                    },function(){
                                        row.delete();
                                    });
                                },
                                error: function(){
                                    swal({
                                        title: Lang.get('messages.text_error_occurred'),
                                        text: null,
                                        type: "error",
                                        //confirmButtonColor: "#1AB394",
                                    });
                                }
                            })
                        } else {
                            swal({
                                title: Lang.get('messages.text_cancel_success'),
                                text: null,
                                type: "error",
                                //confirmButtonColor: "#1AB394",
                            });
                        }
                    });
            }
        }
    });

    $('.table-foo').on('postdraw.ft.table', function(e, ft){
        $('.ibox-content').removeClass('sk-loading');
        $(this).css('display', 'table');
    });

    $('.table-foo').on('ready.ft.table', function(e, ft){
        ft.raise('after.ft.paging');
    });

    /*modals*/
    $('#modalChoseLang').on('show.bs.modal', function(e) {
        let element = e.relatedTarget;

        $('#createRegistration').on('click', function() {
            let model = $(element).data('model');
            let action = 'create';
            let lang = $(this).data('redirectLang');

            if(e.relatedTarget.id) {
                action = e.relatedTarget.id +'/edit';
            }
            console.log($('#locale').find('option:selected').val());
            window.location.href = location.origin + "/admin/" + lang +"/" + model + '/' + action + "?lang=" + $('#locale').find('option:selected').val();
        });
    });

    /* Settings */
    $('#countryIso').on('click', function(e) {

        //e.preventDefault();
        var iso2 = $(this).val();
        if (iso2 == 0) {

            $('#settingsContent').addClass('hidden');
            $('#addConfig').prop('disabled', true);
        } else {

            $('#addConfig').removeAttr('disabled');

            $.ajax({
                url: '/admin/getArchConfigByCountry',
                type: 'post',
                data: {iso2: iso2},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {

                    var html = '';

                    response.forEach(function(item) {
                        html += '<div class="col-md-4"><div class="form-group">';
                        html += '<label>'+ Lang.get('messages.'+item.key) +'</label>';
                        html += '<input type="text" class="form-control" value="'+ item.value +'" name="'+ item.key +'">';
                        html += '</div></div>';
                    })

                    $('#settingsContent').html(html);
                    var button = '<div class="col-md-12 text-center"><button class="btn b-btn btn-primary">'+ Lang.get('messages.button_send') +'</button></div>';
                    $('#settingsContent').append(button);
                    $('input[name="country_iso2"]').val(iso2);
                    $('#settingsContent').removeClass('hidden');
                },
                error: function() {

                }
            });

        }
    });

});
