$(document).ready(function() {

    $('.table-foo').on('after.ft.paging', function(e, ft) {

        var previous;

        $('.chosen-no-search').on('chosen:ready', function(e, params) {
            var control = $(this).next().find('a');
            $(control).css('color', $(this).css('color'));

        }).chosen(
            {
                disable_search: true,
                width: "170px"
            }
        ).on('chosen:showing_dropdown', function(e, params){

            previous = $(this).find('option:selected').val();

        }).change(function(e, params) {

            var self = this;
            var order_id = $(this).data('order');
            var status_id = params.selected;

            toastr.options = {
                "closeButton": true,
                "progressBar": true,
            };

            $.ajax({
                url: '/' + namespace_path + '/order/change_status',
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {order_id: order_id, status_id: status_id},
                success: function(response) {

                    var control = $(self).next().find('a');
                    $(control).css('color', $(self).find('option:selected').data('color'));

                    var message = "Order's sale status was modified";
                    toastr.success(message, 'Success!');
                },
                error: function(e) {
                    toastr.error("Could not modify order's sale status",'Error!');

                    $(self).val(previous);
                    setTimeout(function() {
                        $(self).trigger("chosen:updated");
                    }, 500);

                }
            });
        });
    });


    //var path = '/' + namespace_path + '/order/';
    //
    //$(".delete-button").on("click", function() {
    //
    //    var currentRow = $(this).closest("tr");
    //    var order_id = $(currentRow).attr("data-content");
    //    var _token = $("input[name='_token']").val();
    //
    //    swal({
    //            title: text_confirm_delete,
    //            text: null,
    //            type: "warning",
    //            confirmButtonColor: "#DD6B55",
    //            confirmButtonText: button_delete,
    //            cancelButtonText: button_cancel,
    //            showCancelButton: true,
    //            closeOnConfirm: false,
    //            closeOnCancel: false,
    //            showLoaderOnConfirm: true,
    //        },
    //        function(isConfirm){
    //            if (isConfirm) {
    //                $.ajax({
    //                    url: path+order_id,
    //                    type: "POST",
    //                    headers: {
    //                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                    },
    //                    data: {_method:"DELETE"},
    //                    dataType:'json',
    //                    success: function(response){
    //                        swal({
    //                            title: text_delete_success,
    //                            text: null,
    //                            type: "success",
    //                            confirmButtonColor: "#1AB394",
    //                        },function(){
    //                            var nextRow = $(currentRow).next();
    //                            if ($(nextRow).hasClass('footable-row-detail')) {
    //                                $(nextRow).fadeOut();
    //                            }
    //                            $(currentRow).remove();
    //                        });
    //                    },
    //                    error: function(){
    //                        swal({
    //                            title: text_error_occurred,
    //                            text: null,
    //                            type: "error",
    //                            //confirmButtonColor: "#1AB394",
    //                        });
    //                    }
    //                })
    //            } else {
    //                swal({
    //                    title: text_cancel_success,
    //                    text: null,
    //                    type: "error",
    //                    //confirmButtonColor: "#1AB394",
    //                });
    //            }
    //        });
    //});
});