$(document).ready(function() {

    $('#registrationUserForm').validate({
        rules: {
            first_name: {
                required: true,
                maxlength: 150
            },
            last_name: {
                required: true,
                maxlength:150
            },
            email: {
                required: true,
                email: true,
                maxlength: 150
            },
            password: {
                required: true,
                // strong_pwd: true
            },
            password_confirm: {
                equalTo: "#password"
            },
            gender: {
                required: true
            },
            country_id: {
                required: true
            }
        },
        messages: {
            fisrt_name: {
                required: "The first name field is required. "
            },
            password: {
                 //strong_pwd: "Weak password."
            }
        },
        errorPlacement: function(error, element) {
            error.insertBefore(element);
        },
        errorElement: "label",
    });

    $('#loginForm').validate({
        rules: {
            email: {
                required: true,
                email: true,
                maxlength: 150
            },
            password: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            error.insertBefore(element);
        },
        errorElement: "label",
    });

    $('input[name=terms]').on('ifChanged', function(event) {
        if (event.target.checked) {
            $('button[type="submit"]').removeAttr("disabled");
        } else {
            $('button[type="submit"]').attr("disabled", "disabled");
        }
    });
});