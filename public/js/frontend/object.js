(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = function () {function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ObjectPage = function () {
    function ObjectPage() {
        _classCallCheck(this, ObjectPage);

        var that = this;

        that.map = true;
        that.object = window.data_object;

        that.initHandlers();

        $('.b-object-thumbs__item-link').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });

        $('.b-object-thumbs__item-link-construction').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });

        $('.b-object-thumbs__item-link-facade').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });

        $('.b-object-thumbs__item-img-plan').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });

        $('.b-object-thumb__list').each(function () {
            $(this).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        });


        //$('.js-product-carousel').slick({
        //    slidesToShow: 5,
        //    slidesToScroll: 1,
        //    dots: true,
        //    arrows: true,
        //    prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
        //    nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>'
        //});
        //$('.b-object-carousel').magnificPopup({
        //    type: 'image',
        //    delegate: '.b-object-carousel__slide-img',
        //    gallery: {
        //        enabled: true,
        //        tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
        //    }
        //});

        $('.view-mode-link').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var that = this;
            var mode = $(that).data('cards');
            $('.view-mode-link').removeClass('active')
            $(that).addClass('active');
            if(mode == 'grid') {
                $('.b-grid-cards').removeClass('b-grid-1').addClass('b-grid-4').find('.b-card').addClass('b-grid-column');
            }
            if(mode == 'row') {
                $('.b-grid-cards').removeClass('b-grid-4').addClass('b-grid-1').find('.b-card').removeClass('b-grid-column');
            }
        });
    }

    _createClass(ObjectPage, [{
        key: 'initHandlers',
        value: function initHandlers() {
            var that = this;
        }
    }, {
        key: 'initMap',
        value: function initMap() {
            var that = this;

            var coords = {
                lat: parseFloat(that.object.lat),
                lng: parseFloat(that.object.lng)
                    //lat: 47.023380, lng: 28.849181
            },
                styles = [{ 'stylers': [{
                    'lightness': 10,
                    'saturation': 60
                }] }];

            that.map = new google.maps.Map(document.getElementById('js-object-map'), {
                zoom: 14,
                center: coords,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            //that.map.setOptions({styles: styles});

            var current_marker = new google.maps.Marker({
                position: coords,
                map: that.map,
                icon: '/img/pin_active.png'
            });
        }
    }]);

    return ObjectPage;
}();

window._ObjectPage = new ObjectPage();

},{}]},{},[1]);

//# sourceMappingURL=object.js.map
