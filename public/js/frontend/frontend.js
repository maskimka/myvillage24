$(document).ready(function() {

    //================= Modals ===============
    $('.video-play').on('click', function() {
        $('#modalMainVideo').modal('show');
    });

    $('#mapView, #mobile-map').on('click', function() {
        $('#modalMap').modal('show');
    });

    $('.land-map').on('click', function() {
        $('#modalLandMap').modal('show');
    });

    $('#modalMap').on('hidden.bs.modal', function () {
        $('.b-object-modal-card').removeClass('is-showing');
    });

    $('#modalMainVideo').on('hidden.bs.modal', function () {
        $("#modalMainVideo iframe").attr("src", $("#modalMainVideo iframe").attr("src"));
    });

    $('.land-video-play').on('click', function() {
        $('#modalLandVideo').modal('show');
    });

    $('#modalLandVideo').on('hidden.bs.modal', function () {
        $("#modalLandVideo iframe").attr("src", $("#modalLandVideo iframe").attr("src"));
    });

    $('.home-architecture-block .price i').on('mouseover', function() {
        $('#priceMeaning').modal('show');
    });

    //============================================

    /*Chosen*/
    //$('.chosen-select').chosen({width:"100%"});

    $('.b-objects-map__form_arrow').on('click', function() {

        if ($('.optional-filters').css('display') == 'none') {
            $(".optional-filters").fadeIn(600);
        } else {
            $(".optional-filters").fadeOut();
        }
    });

    //$('#moreInfrastructure, #moreCommunications').on('click', function() {
    //
    //    if ($('.panel-options-1').css('height') == "150px") {
    //
    //        $('.panel-options-1').animate({
    //            height: "100%"
    //        }, 100);
    //
    //        $('#moreInfrastructure').find('span').html('Скрыть');
    //        $('#moreInfrastructure').find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    //
    //        $('#moreCommunications').find('span').html('Скрыть');
    //        $('#moreCommunications').find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    //    } else {
    //
    //        $('.panel-options-1').animate({
    //            height: "150px"
    //        }, 100);
    //
    //        $('#moreInfrastructure').find('span').html('Еще');
    //        $('#moreInfrastructure').find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
    //
    //        $('#moreCommunications').find('span').html('Еще');
    //        $('#moreCommunications').find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
    //    }
    //});

    //$('#moreTransport').on('click', function() {
    //
    //    if ($('.panel-options-2').css('height') == "150px") {
    //
    //        $('.panel-options-2').animate({
    //            height: "100%"
    //        }, 100);
    //
    //        $(this).find('span').html('Скрыть');
    //        $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    //    } else {
    //
    //        $('.panel-options-2').animate({
    //            height: "150px"
    //        }, 100);
    //
    //        $(this).find('span').html('Еще');
    //        $(this).find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
    //    }
    //});

    //$('#moreLandSizeMap').on('click', function() {
    //
    //    if ($('.map-size-list').css('height') == "150px") {
    //
    //        $('.map-size-list').animate({
    //            height: "100%"
    //        }, 100);
    //
    //        $(this).find('span').html('Скрыть');
    //        $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    //    } else {
    //
    //        $('.map-size-list').animate({
    //            height: "150px"
    //        }, 100);
    //
    //        $(this).find('span').html('Еще');
    //        $(this).find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
    //    }
    //});

    //$("a[href='#top']").click(function() {
    //    $("html, body").animate({ scrollTop: 0 }, "slow");
    //    return false;
    //});

    //var position = $(window).scrollTop();
    //
    //$(window).scroll(function(e) {
    //
    //    var scroll = $(window).scrollTop();
    //    var height = $(this).height();
    //
    //    if (scroll > 100) {
    //        $("a[href='#top']").css('display', 'block');
    //    } else {
    //        $("a[href='#top']").css('display', 'none');
    //    }
    //
    //    if (height - scroll < 1500 && scroll > position) {
    //        $("a[href='#top']").css('bottom', '350px');
    //    } else {
    //        $("a[href='#top']").css('bottom', '100px');
    //    }
    //
    //});
});
