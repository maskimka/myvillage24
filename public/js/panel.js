$(document).ready(function(){

    /*COMMON*/
    $('.logout').on('click', function(){
        $(this).closest('form.logout_form').submit();
        return false;
    });

    $('#lang_change').change(function(){
        var locale = $(this).val();
        var _token = $("input[name='_token']").val();

        $.ajax({
            url: "/language",
            type: "POST",
            data: {locale:locale, _token:_token},
            dataType:'json',
            success: function(response){

            },
            error: function(response){

            },
            complete: function(){
                window.location.reload(true);
            }
        })
    });
    
    /*Chosen*/
   $('.chosen-select').chosen({width:"100%"});

    $('.chosen-select-nosearch').chosen({
        width:"100%",
        disable_search_threshold: 50
    });

    /*footable*/

    var denyEdit = ['/panel/order/'];
    var denyDelete = ['/panel/seo/land/', '/panel/seo/architecture/', '/panel/seo/building/'];

    var pathname = window.location.pathname + '/';
    $('.table-foo').footable({
        editing: {
            enabled: true,
            alwaysShow: true,
            allowAdd: false,
            allowEdit: !denyEdit.includes(pathname),
            allowDelete: !denyDelete.includes(pathname),
            editRow: function(row) {
                var element = $(row.$el).get(0);
                var modelId = $(element).data('content');
                var lang = $(element).data('lang');
                var params = (lang != undefined)?'?lang='+lang:'';
                window.location.href = pathname + modelId+"/edit"+params;
            },
            deleteRow: function(row) {
                var element = $(row.$el).get(0);
                var modelId = $(element).data('content');
                var _token = $("input[name='_token']").val();
                swal({
                        title: Lang.get('messages.text_confirm_delete'),
                        text: null,
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: Lang.get('messages.button_delete'),
                        cancelButtonText: Lang.get('messages.button_cancel'),
                        showCancelButton: true,
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        showLoaderOnConfirm: true,
                    },
                    function(isConfirm){
                        if (isConfirm) {
                            $.ajax({
                                url: pathname + modelId,
                                type: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {_method:"DELETE"},
                                dataType:'json',
                                success: function(response){
                                    swal({
                                        title: Lang.get('messages.text_delete_success'),
                                        text: null,
                                        type: "success",
                                        confirmButtonColor: "#1AB394",
                                    },function(){
                                        var nextRow = $(element).next();
                                        if ($(nextRow).hasClass('footable-detail-row')) {
                                            $(nextRow).fadeOut();
                                        }
                                        $(element).remove();
                                    });
                                },
                                error: function(){
                                    swal({
                                        title: Lang.get('messages.text_error_occurred'),
                                        text: null,
                                        type: "error",
                                        //confirmButtonColor: "#1AB394",
                                    });
                                }
                            })
                        } else {
                            swal({
                                title: Lang.get('messages.text_cancel_success'),
                                text: null,
                                type: "error",
                                //confirmButtonColor: "#1AB394",
                            });
                        }
                    });
            }
        }
    });

    $('.table-foo').on('postdraw.ft.table', function(e, ft){
        $('.ibox-content').removeClass('sk-loading');
        $(this).css('display', 'table');
    });

    $('.table-foo').on('ready.ft.table', function(e, ft){
        ft.raise('after.ft.paging');
    });

    /*iCheck*/
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    /*dataPicker*/
    $('.input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    /*modals*/
    $('#modalChoseLang').on('show.bs.modal', function(e) {
        let element = e.relatedTarget;

        $('#createRegistration').on('click', function() {
            let model = $(element).data('model');
            let action = 'create';
            let lang = $(this).data('redirectLang');

            if(e.relatedTarget.id) {
                action = e.relatedTarget.id +'/edit';
            }
            window.location.href = location.origin + "/panel/" + lang +"/" + model +"/" + action + "?lang=" + $('#locale').find('option:selected').val();
        });
    });

});
