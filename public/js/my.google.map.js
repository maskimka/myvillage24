// When the window has finished loading google map
google.maps.event.addDomListener(window, 'load', initAutocomplete);

var map, autocomplete, places, marker, geocoder;;
var userCountry = $("#userCountryIso").val();
var countryRestrict = {'country': userCountry};
var componentForm = {
    street_number: 'short_name',
    route: 'short_name',
    locality: 'short_name',
    administrative_area_level_1: 'short_name',

};
var countries = {
    /*UA*/
    'UA': {
        center: {lat: 49.29020169510767, lng: 32.33482337500004},
        zoom: 5
    },
    /*RU*/
    'RU': {
        center: {lat: 55.751244, lng: 37.618423},
        zoom: 4
    },
    /*ES*/
    'ES': {
        center: {lat: 40.5, lng: -3.7},
        zoom: 5
    },
    /*MD*/
    'MD': {
        center: {lat: 47.00367, lng: 28.907089000000042},
        zoom: 6
    }
};

function initAutocomplete() {

    var mapOptions = {
        zoom: countries[userCountry].zoom,
        center: new google.maps.LatLng( 0, 0 ),
        // Style for Google Maps
        styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}],
        mapTypeId: 'roadmap'
    };
    
    geocoder = new google.maps.Geocoder();

    // Create the Google Map using elements
    var mapCanvas = document.getElementById('map-canvas');

    map = new google.maps.Map(mapCanvas, mapOptions);

    marker = new google.maps.Marker({
        //position: countries[userCountry].center,
        map: map,
        draggable: true
    });


    marker.addListener('position_changed', function(){
        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();

        $('#mapLat').val(lat);
        $('#mapLng').val(lng);
        //codeLatLng(lat, lng);
    });

    // Create the autocomplete object, restricting the search to geographical
    // location types.
    //autocomplete = new google.maps.places.Autocomplete(
    //    /** @type {!HTMLInputElement} */(document.getElementById('searchBox')),
    //        {
    //            types: ['geocode'],
    //            componentRestrictions: countryRestrict
    //        }
    //);
    //
    //places = new google.maps.places.PlacesService(map);
    //// When the user selects an address from the dropdown, populate the address
    //// fields in the form.
    //autocomplete.addListener('place_changed', fillInAddress);


    // Add a DOM event listener to react when the user selects a country.
    $("#landCountry").on("change",function(){
        setAutocompleteCountry($(this).find("option:selected").attr('data-content'));
        clearFields();
    });

    $('#mapLat, #mapLng').on("keyup", function(){
        setTimeout(function(){

            var lat = parseFloat($("#mapLat").val());
            var lng = parseFloat($("#mapLng").val());

            marker.setPosition({lat:lat, lng:lng});

            map.setCenter(marker.getPosition());

            map.setZoom(6);
        }, 500);
    });
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    //completeAddressFields(place);

    $('#mapLat').val(lat);
    $('#mapLng').val(lng);

    //set position on map
    marker.setPosition({lat:lat, lng:lng});
    map.setCenter(marker.getPosition());
    map.setZoom(13);
}

// Set the country restriction based on user input.
// Also center and zoom the map on the given country.
function setAutocompleteCountry(countryIso) {

    autocomplete.setComponentRestrictions({'country': countryIso});
    // map.setCenter(countries[countryIso].center);
    map.setZoom(countries[countryIso].zoom);

    marker.setPosition(countries[countryIso].center);

    map.setCenter(marker.getPosition());

}

function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

function codeLatLng(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({
        'latLng': latlng
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                clearFields();
                $('#searchBox').val(results[0].formatted_address);
               // completeAddressFields(results[0]);
                if($('#locality').val() == "") {
                    var city = results[0].formatted_address.split(',');
                    $('#locality' == "").val(city);
                }
            }
        }
    });
}

function completeAddressFields(address) {
    for (var i = 0; i < address.address_components.length; i++) {
        var addressType = address.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = address.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }
    //duplicate region
    $('#landRegion').val($('#administrative_area_level_1').val());
}

function clearFields() {
    $('#searchBox').val("");
    $("#locality").val("");
    $("#administrative_area_level_1").val("");
    $("#landRegion").val("");
    $("#street").val("");
    $("#street_number").val("");
}