/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

eval("var Ibox = (function (superclass) {\n    function Ibox () {\n        superclass.apply(this, arguments);\n    }\n\n    if ( superclass ) Ibox.__proto__ = superclass;\n    Ibox.prototype = Object.create( superclass && superclass.prototype );\n    Ibox.prototype.constructor = Ibox;\n\n    Ibox.prototype.render = function render () {\n        return (\n            React.createElement( 'div', { className: \"ibox float-e-margins animated fadeInRight\" },\n                React.createElement( 'div', { className: \"ibox-title\" },\n                    React.createElement( 'h5', null, \"Этаж номер 1\" )\n                ),\n                React.createElement( 'div', { className: \"ibox-content\" },\n                    React.createElement( 'div', { className: \"row\" },\n                        React.createElement( 'div', { className: \"col-lg-6\" },\n                            React.createElement( 'input', { type: \"file\", name: \"floors[][image]\", className: \"simple_image\" })\n                        ),\n                        React.createElement( 'div', { className: \"col-lg-6\" },\n                            React.createElement( FloorArea, null ),\n                            React.createElement( 'div', { className: \"hr-line-dashed m-l-n\" }),\n                            React.createElement( HouseZone, null )\n                        )\n                    )\n                )\n            )\n        )\n    };\n\n    return Ibox;\n}(React.Component));\n\nvar FloorArea = (function (superclass) {\n    function FloorArea () {\n        superclass.apply(this, arguments);\n    }\n\n    if ( superclass ) FloorArea.__proto__ = superclass;\n    FloorArea.prototype = Object.create( superclass && superclass.prototype );\n    FloorArea.prototype.constructor = FloorArea;\n\n    FloorArea.prototype.render = function render () {\n        return (\n            React.createElement( 'div', { className: \"row m-t-xl\" },\n                React.createElement( 'div', { className: \"form-group m-b-n\" },\n                    React.createElement( 'div', { className: \"col-lg-12 b-m-n\" },\n                        React.createElement( 'div', { className: \"input-group\" },\n                            React.createElement( 'input', { type: \"text\", name: \"floors[][area]\", className: \"form-control\", placeholder: \"Площадь\" }),\n                            React.createElement( 'span', { className: \"input-group-addon\" }, \"m\", React.createElement( 'sup', null, React.createElement( 'small', null, \"2\" ) )\n                            )\n                        )\n                    )\n                )\n            )\n        )\n    };\n\n    return FloorArea;\n}(React.Component));\n\nvar HouseZone = (function (superclass) {\n    function HouseZone () {\n        superclass.apply(this, arguments);\n    }\n\n    if ( superclass ) HouseZone.__proto__ = superclass;\n    HouseZone.prototype = Object.create( superclass && superclass.prototype );\n    HouseZone.prototype.constructor = HouseZone;\n\n    HouseZone.prototype.render = function render () {\n        return (\n            React.createElement( 'div', { className: \"row\" },\n                React.createElement( 'div', { className: \"col-lg-12\" },\n                    React.createElement( 'div', { className: \"form-group\" },\n                        React.createElement( 'select', { 'data-placeholder': \"Зоны\", className: \"chosen-select house_zone_list\" },\n                            React.createElement( 'option', { value: \"\", disable: true })\n                        )\n                    )\n                )\n            )\n        )\n    };\n\n    return HouseZone;\n}(React.Component));\n\nReactDOM.render(\n    React.createElement( Ibox, null ),\n    document.getElementById('floors_container')\n);//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NvbXBvbmVudHMvZmxvb3JzLmpzPzRhMDciXSwic291cmNlc0NvbnRlbnQiOlsiY2xhc3MgSWJveCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgcmVuZGVyICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaWJveCBmbG9hdC1lLW1hcmdpbnMgYW5pbWF0ZWQgZmFkZUluUmlnaHRcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImlib3gtdGl0bGVcIj5cbiAgICAgICAgICAgICAgICAgICAgPGg1PtCt0YLQsNC2INC90L7QvNC10YAgMTwvaDU+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpYm94LWNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLWxnLTZcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImZpbGVcIiBuYW1lPVwiZmxvb3JzW11baW1hZ2VdXCIgY2xhc3NOYW1lPVwic2ltcGxlX2ltYWdlXCIvPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1sZy02XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZsb29yQXJlYSAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaHItbGluZS1kYXNoZWQgbS1sLW5cIj48L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SG91c2Vab25lIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKVxuICAgIH1cbn1cblxuY2xhc3MgRmxvb3JBcmVhIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICByZW5kZXIgKCkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbS10LXhsXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwIG0tYi1uXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLWxnLTEyIGItbS1uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImlucHV0LWdyb3VwXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgbmFtZT1cImZsb29yc1tdW2FyZWFdXCIgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCIgcGxhY2Vob2xkZXI9XCLQn9C70L7RidCw0LTRjFwiLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJpbnB1dC1ncm91cC1hZGRvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtPHN1cD48c21hbGw+Mjwvc21hbGw+PC9zdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIClcbiAgICB9XG59XG5cbmNsYXNzIEhvdXNlWm9uZSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgcmVuZGVyICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbGctMTJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c2VsZWN0IGRhdGEtcGxhY2Vob2xkZXI9XCLQl9C+0L3Ri1wiIGNsYXNzTmFtZT1cImNob3Nlbi1zZWxlY3QgaG91c2Vfem9uZV9saXN0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIlwiIGRpc2FibGU+PC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L3NlbGVjdD5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKVxuICAgIH1cbn1cblxuUmVhY3RET00ucmVuZGVyKFxuICAgIDxJYm94Lz4sXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2Zsb29yc19jb250YWluZXInKVxuKTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gcmVzb3VyY2VzL2Fzc2V0cy9qcy9jb21wb25lbnRzL2Zsb29ycy5qcyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTs7Ozs7Ozs7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUF0QkE7QUFDQTtBQXVCQTtBQUFBOzs7Ozs7OztBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQWpCQTtBQUNBO0FBa0JBO0FBQUE7Ozs7Ozs7O0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFkQTtBQUNBO0FBZUE7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==");

/***/ }
/******/ ]);