$(document).ready(function() {

    var wizard_user_form =
        $("#wizard_user_form").steps({
            bodyTag: "fieldset",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                // Always allow going backward even if the current step contains invalid fields!
                if (currentIndex > newIndex)
                {
                    return true;
                }

                var form = $(this);

                // Clean up if user went backward before
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }

                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Start validation; Prevent going forward if false
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex)
            {

            },
            onFinishing: function (event, currentIndex)
            {
                var form = $(this);

                // Disable validation on fields that are disabled.
                // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                form.validate().settings.ignore = ":disabled";

                // Start validation; Prevent form submission if false
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                var form = $(this);
                // Submit form input
                form.submit();
            }
        });

    wizard_user_form.validate({
        errorPlacement: function (error, element)
        {
            element.closest('.input-group').before(error);
            element.closest('.image-upload').before(error);
        },
        ignore: ":hidden:not(.chosen-select)",
        rules: {
            first_name:{
                required: true
            },
            last_name:{
                required: true
            },
            email:{
                required: true,
                email: true
            },
        }
    });

    $('.table-foo').on('after.ft.paging', function(e, ft) {

        var previous;

        $('.chosen-no-search').chosen(
            {
                disable_search: true,
                width: "250px"
            }
        ).on('chosen:showing_dropdown', function(e, params){

            previous = $(this).val();

        }).change(function(e, params) {

            var self = this;
            var user_id = $(this).data('user');
            var roles = $(this).val();

            toastr.options = {
                "closeButton": true,
                "progressBar": true,
            };

            $.ajax({
                url: '/admin/users/updateRole',
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {user_id: user_id, roles: roles},
                success: function(response) {
                    var message = "User's roles were modified";
                    toastr.success(message, 'Success!');
                },
                error: function(e) {
                    toastr.error("Could not modify user's roles",'Error!');

                    $(self).val(previous);
                    setTimeout(function() {
                        $(self).trigger("chosen:updated");
                    }, 500);

                }
            });
        });
    });

    //$(".delete-button").on("click", function(){
    //
    //    var currentRow = $(this).closest("tr");
    //    var user_id = $(currentRow).data("content");
    //    var _token = $("input[name='_token']").val();
    //    swal({
    //            title: text_confirm_delete,
    //            text: null,
    //            type: "warning",
    //            confirmButtonColor: "#DD6B55",
    //            confirmButtonText: button_delete,
    //            cancelButtonText: button_cancel,
    //            showCancelButton: true,
    //            closeOnConfirm: false,
    //            closeOnCancel: false,
    //            showLoaderOnConfirm: true,
    //        },
    //        function(isConfirm){
    //            if (isConfirm) {
    //                $.ajax({
    //                    url: '/admin/users/' + user_id,
    //                    type: "POST",
    //                    headers: {
    //                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                    },
    //                    data: {_method:"DELETE"},
    //                    dataType:'json',
    //                    success: function(response){
    //                        swal({
    //                            title: text_delete_success,
    //                            text: null,
    //                            type: "success",
    //                            confirmButtonColor: "#1AB394",
    //                        },function(){
    //                            var nextRow = $(currentRow).next();
    //                            if ($(nextRow).hasClass('footable-row-detail')) {
    //                                $(nextRow).fadeOut();
    //                            }
    //                            $(currentRow).remove();
    //                        });
    //                    },
    //                    error: function(){
    //                        swal({
    //                            title: text_error_occurred,
    //                            text: null,
    //                            type: "error",
    //                            //confirmButtonColor: "#1AB394",
    //                        });
    //                    }
    //                })
    //            } else {
    //                swal({
    //                    title: text_cancel_success,
    //                    text: null,
    //                    type: "error",
    //                    //confirmButtonColor: "#1AB394",
    //                });
    //            }
    //        });
    //});
    //
    //$(".edit-button").on("click", function(){
    //    window.location.href = '/admin/users/' + $(this).closest("tr").data("content") + "/edit";
    //});
    //
    //$('.footable').footable();

    $('input[name="password"]').on('mouseover', function() {

        $(this).attr('type', 'text');
    }).on('mouseleave', function() {

        $(this).attr('type', 'password');
    });


    $('#generatePassword').on('click', function() {
        $('input[name="password"]').val(randString($(this)));
    });

    function randString(id){
        var dataSet = $(id).attr('data-character-set').split(',');
        var possible = '';
        if($.inArray('a-z', dataSet) >= 0){
            possible += 'abcdefghijklmnopqrstuvwxyz';
        }
        if($.inArray('A-Z', dataSet) >= 0){
            possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        if($.inArray('0-9', dataSet) >= 0){
            possible += '0123456789';
        }
        if($.inArray('#', dataSet) >= 0){
            possible += '![]{}()%&*$#^<>~@|';
        }
        var text = '';
        for(var i=0; i < $(id).attr('data-size'); i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
});