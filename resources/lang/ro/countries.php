<?php

return [
    'Austria' => 'Austria',
    'Germany' => 'Germania',
    'Moldova' => 'Moldova',
    'Romain' => 'România',
    'Russian' => 'Rusia',
    'Spain' => 'Spania',
    'Switzerland' => 'Elveția',
    'Ukraine' => 'Ukraina'
];