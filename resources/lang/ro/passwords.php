<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Parolele trebuie să conțină cel puțin șase simboluri și să corespundă confirmării.',
    'reset' => 'Parola Dvs a fost resetata!',
    'sent' => 'Link-ul de resetare a parolei a fost trimis pe e-mail Dvs!',
    'token' => 'Acest token pentru resetarea parolei este nevalid.',
    'user' => " Nu a fost găsit nici un utilizator cu această adresă de e-mail.",

];
