<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Acest câmp trebuie să fie acceptat.',
    'active_url'           => 'Acest câmp nu este o adresă URL validă.',
    'after'                => 'Acest câmp trebuie să fie o dată mai mare decât :date.',
    'alpha'                => 'Acest câmp poate conține doar litere.',
    'alpha_dash'           => 'Acest câmp poate conține doar caractere alfanumerice și cratime.',
    'alpha_num'            => 'Acest câmp poate conține doar caractere alfanumerice.',
    'array'                => 'Acest câmp trebuie să fie o matrice.',
    'before'               => 'Acest câmp trebuie să fie o dată mai mică decât :date.',
    'between'              => [
        'numeric' => 'Acest câmp trebuie să fie între :min și :max.',
        'file'    => 'Acest câmp trebuie să fie între :min și :max kilobyți.',
        'string'  => 'Acest câmp trebuie să fie între :min și :max caractere.',
        'array'   => 'Acest câmp trebuie să fie între :min și :max itemi.',
    ],
    'boolean'              => 'Acest câmp trebuie să fie adevărat sau fals.',
    'confirmed'            => 'Confirmarea nu se potrivește.',
    'date'                 => 'Acest câmp nu este o dată validă.',
    'date_format'          => 'Acest câmp nu se potrivește formatului :format.',
    'different'            => 'Acest câmp și :other trebuie să fie diferite.',
    'digits'               => 'Acest câmp trebuie să conțină :digits cifre.',
    'digits_between'       => 'Acest câmp trebuie să fie între :min și :max cifre.',
    'dimensions'           => 'Dimensiunea imaginii este nevalidă.',
    'distinct'             => 'Acest câmp trebuie sa fie unic.',
    'email'                => 'Adresă de e-mail invalidă.',
    'exists'               => 'Valoarea selectată nu există.',
    'file'                 => 'Acest câmp trebuie să fie fișier.',
    'filled'               => 'Acest câmp este obligatoriu.',
    'image'                => 'Acest câmp trebuie sa fie imagine.',
    'in'                   => 'Valoarea selectată este invalidă.',
    'in_array'             => 'Această valoare nu se conține în :other.',
    'integer'              => 'Valoarea trebuie să fie un număr întreg.',
    'ip'                   => 'IP adresă invalidă.',
    'json'                 => 'Acest câmp trebuie să fie JSON valid.',
    'max'                  => [
        'numeric' => 'Valoarea nu poate depăși :max.',
        'file'    => 'Fișierul nu poate depăși :max kilobyți.',
        'string'  => 'Șirul nu poate depăși :max caractere.',
        'array'   => 'Tabelul nu poate depași :max itemi.',
    ],
    'mimes'                => 'Sunt permise fișiere de tipul: :values.',
    'mimetypes'            => 'Sunt permise fișiere de tipul: :values.',
    'min'                  => [
        'numeric' => 'Valoarea nu poate fi mai mică de :min.',
        'file'    => 'Fișierul nu poate fi mai mic de :min kilobyți.',
        'string'  => 'Șirul trebuie să conțină cel puțin :min caractere.',
        'array'   => 'Tabelul trebuie să conțină cel putțin :min itemi.',
    ],
    'not_in'               => 'Valoarea selectată este invalidă.',
    'numeric'              => 'Acest câmp trebuie sa fie număr.',
    'present'              => 'Acest câmp trebuie sa fie număr present.',
    'regex'                => 'Format invalid.',
    'required'             => 'Acest câmp este obligatoriu.',
    'required_if'          => 'Acest câmp este obligatoriu dacă :other este :value.',
    'required_unless'      => 'Acest câmp este obligatoriu cu excepția când :other este în :values.',
    'required_with'        => 'Acest câmp este obligatoriu dacă :values este present.',
    'required_with_all'    => 'Acest câmp este obligatoriu dacă :values este present.',
    'required_without'     => 'Acest câmp este obligatoriu dacă :values nu este present.',
    'required_without_all' => 'Acest câmp este obligatoriu dacă nici unul din :values nu este present.',
    'same'                 => 'Acest câmp și :other trebuie sa fie la fel.',
    'size'                 => [
        'numeric' => 'Numărul trebuie sa fie :size.',
        'file'    => 'Fișierul trebuie să fie :size kilobyți.',
        'string'  => 'Șitul trebuie să conțină :size caractere.',
        'array'   => 'Tabelul trebuie să conțină :size itemi.',
    ],
    'string'               => 'Acest câmp trebuie să fie șir.',
    'timezone'             => 'Fus orar invalid.',
    'unique'               => 'Această valoare există deja.',
    'uploaded'             => 'Încarcarea a eșuat.',
    'url'                  => 'Adresă URL invalidă.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'password' => [
            'required' => 'Câmpul parolă este obligatoriu',
            'min' => 'Parola trebuie să conțină minimum 8 simboluri',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

    'name_required' => 'Vă rugăm, introduceți numele',
    'phone_required' => 'Câmpul telefon este obligatoriu',
    'phone_minlength' => 'Vă rugăm, introduceți minimum 6 simboluri',

];
