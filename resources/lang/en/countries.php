<?php

return [
    'Austria' => 'Austria',
    'Germany' => 'Germany',
    'Moldova' => 'Moldavia',
    'Romain' => 'Romania',
    'Russian' => 'Russia',
    'Spain' => 'Spain',
    'Switzerland' => 'Switzerland',
    'Ukraine' => 'Ukraine'
];