<?php

return [
    'Austria' => 'Австрия',
    'Germany' => 'Германия',
    'Moldova' => 'Молдова',
    'Romain' => 'Румыния',
    'Russian' => 'Россия',
    'Spain' => 'Испания',
    'Switzerland' => 'Швейцария',
    'Ukraine' => 'Украина'
];