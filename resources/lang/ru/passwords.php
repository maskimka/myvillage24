<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать не менее 6 символов и соответствовать ранее набранному паролю.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Мы выслали на вашу электронную почту ссылку для сброса пароля!',
    'token' => 'Код сброса пароля недействителен!',
    'user' => "Пользователь с указанной электронной почтой не существует.",

];
