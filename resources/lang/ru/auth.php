<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Введенные учетные данные не соответствуют ни одному зарегистрированному пользователю.',
    'throttle' => 'Слишком много попыток входа. Попробуйте еще раз через :seconds секунд.',

];
