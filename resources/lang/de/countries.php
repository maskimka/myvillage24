<?php

return [
    'Austria' => 'Österreich',
    'Germany' => 'Deutschland',
    'Moldova' => 'Moldawien',
    'Romain' => 'Rumänien',
    'Russian' => 'Russland',
    'Spain' => 'Spanien',
    'Switzerland' => 'Schweiz',
    'Ukraine' => 'Ukraine'
];