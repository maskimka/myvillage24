<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('title', trans('messages.label_favorite', [], null, $adminLang))

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}" />
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.favorite">{{trans('messages.label_favorite', [], null, $adminLang)}}</h2>
            {!! Breadcrumbs::render('wishlist_backend', $namespace, $adminLang) !!}
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        <table id="userList" class="table table-foo" data-sorting="true" data-filtering="true">
                            <thead>
                                <tr>
                                    <th width="10%" data-sortable="false">
                                        <div class="checkbox checkbox-success">
                                            <input type="checkbox" class="parent-checkbox">
                                            <label>
                                                &nbsp;<b data-i18n="admin.select_all">
                                                    {{trans('messages.select_all', [], null, $adminLang)}}
                                                </b>
                                            </label>
                                        </div>
                                    </th>
                                    <th data-i18n="admin.image" data-sortable="false">
                                        {{trans('messages.image', [], null, $adminLang)}}
                                    </th>
                                    <th data-i18n="admin.article">
                                        {{trans('messages.label_article', [], null, $adminLang)}}
                                    </th>
                                    <th data-i18n="admin.type">
                                        {{trans('messages.type', [], null, $adminLang)}}
                                    </th>
                                    <th data-i18n="admin.area">
                                        {{trans('messages.label_area', [], null, $adminLang)}}
                                    </th>
                                    <th data-i18n="admin.price">
                                        {{trans('messages.label_price', [], null, $adminLang)}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($wishlist as $key => $item)
                                    <tr data-type="{{ $item->product_type }}" data-content="{{ $item->product_id }}">
                                        <td>
                                            <div class="checkbox checkbox-success">
                                                <input type="checkbox" class="child-checkbox"
                                                       data-type="{{ $item->product_type }}" name="products[]"
                                                       value="{{ $item->product_id }}">
                                                <label>
                                                    &nbsp;
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="img-land">
                                                @if(isset($item['products']->images[0]))
                                                    <img class="img-thumbnail" src="{{asset(config('custom.'
                                                    .strtoupper($item->product_type).'_IMAGES_PATH')."/thumbnails/"
                                                    .$item['products']->images[0]->name)}}" alt="image">
                                                @else
                                                    <img class="img-thumbnail"
                                                         src="{{asset("/img/common/placeholder.jpg")}}" alt="image">
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            {{ $item['products']->article }}
                                        </td>
                                        <td>
                                            {{ $item->product_type }}
                                        </td>
                                        <td>
                                            @if($item['products']->area)
                                                {{ $item['products']->area }} <span data-i18n="admin.are">{{trans('messages.are', [], null, $adminLang)}}</span>
                                            @endif
                                            @if($item['products']->general_area)
                                                {{ $item['products']->general_area }} m<sup>2</sup>
                                            @endif
                                        </td>
                                        <td>
                                            @if( $item->product_type === 'Architecture')
                                                {{ $item['products']->full_low_price}} €
                                            @else
                                                {{ $item['products']->price or $item['products']->building_price }} €
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn b-btn btn-primary" data-toggle="modal" data-target="#wishlistOrderModal">
                                    {{trans('messages.label_send_request', [], null, $adminLang)}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modals.wishlist_order')
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('/js/wishlist/wishlist.js') !!}" type="text/javascript"></script>
@endsection