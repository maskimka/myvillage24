<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>
<?php $dataLang = (request()->lang)?:config('app.fallback_locale')?>

@extends($extend_path)

@section('title', trans('messages.label_add_architect_project', [], null, $adminLang))

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/uploader/jquery.fileuploader.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/plugins/uploader/jquery.fileuploader-thumb.css') !!}" />
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.add_architect_project">{{ trans('messages.label_add_architect_project', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('projects_edit_backend', $namespace, $architecture->id, $adminLang) !!}
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInDown">
        <div class="row">
            <div class="wrapper wrapper-content animated fadeInDown col-lg-8">
                <div class="ibox-title">
                    <h5 data-i18n="[html]admin.info_project">
                        {!! trans('messages.info_project', [], null, $adminLang) !!}
                    </h5>
                </div>
                <div class="ibox-content">
                    <form id="wizard_architecture_form"  action="/{{ $namespace }}/architecture/{{ $architecture->id }}"
                          class="form-horizontal" enctype="multipart/form-data" method="POST" data-locale="{{ $adminLang }}">
                        @include('errors.error_list')
                        <h1 data-i18n="admin.info">{{ trans('messages.info', [], null, $adminLang) }}</h1>
                        <fieldset>
                            <div class="">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <h2 data-i18n="admin.project_title">{{ trans('messages.label_project_title', [], null, $adminLang) }}:</h2>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group ">
                                            <input data-i18n="[placeholder]admin.project_title" type="text" name="title"
                                                   class="form-control text-capitalize" value="{{ $architecture->title }}"
                                                   placeholder="{{ trans('messages.label_project_title', [], null, $adminLang) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <h2 id="top-general_area">{{ $architecture->general_area or 0 }}  m<sup><small>2</small></sup></h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <h2 data-i18n="admin.project_complectation">
                                            {{ trans('messages.label_project_complectation', [], null, $adminLang) }}:
                                        </h2>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <select data-i18n="[placeholder]admin.project_complectation"
                                                    data-placeholder="{{ trans('messages.label_project_complectation', [], null, $adminLang) }}"
                                                    multiple name="aac_kit[]" class="chosen-select">
                                                @foreach($aac_kit as $item)
                                                    <option data-i18n="admin.{{ $item->slug }}" value="{{ $item->id }}"
                                                            @if(in_array($item->id, $acc_selected)) selected @endif>
                                                        {{ trans('messages.'.$item->slug, [], null, $adminLang) }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span class="help-block m-b-none">&nbsp;</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label data-i18n="admin.article" class="col-lg-4 control-label">
                                                {{ trans('messages.label_article', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <h2>{{ $architecture->article }}</h2>
                                                <input data-i18n="[placeholder]admin.article" type="hidden" disabled
                                                       placeholder="{{ trans('messages.label_article', [], null, $adminLang) }}"
                                                       class="form-control" value="{{ $architecture->article }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label data-i18n="admin.general_area" class="col-lg-4 control-label">
                                                {{ trans('messages.label_general_area', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.general_area" type="text" name="general_area"
                                                       class="form-control decimal-mask" value="{{ $architecture->general_area }}"
                                                       placeholder="{{ trans('messages.label_general_area', [], null, $adminLang) }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.living_area" class="col-lg-4 control-label">
                                                {{ trans('messages.label_living_area', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.living_area" type="text" name="living_area"
                                                       placeholder="{{ trans('messages.label_living_area', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask" value="{{ $architecture->living_area }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.floor_area" class="col-lg-4 control-label">
                                                {{ trans('messages.label_floor_area', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.floor_area" type="text" name="floors_area"
                                                       placeholder="{{ trans('messages.label_floor_area', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask" value="{{ $architecture->floors_area }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.roof_area" class="col-lg-4 control-label">
                                                {{ trans('messages.label_roof_area', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.roof_area" type="text" name="roof_area"
                                                       placeholder="{{ trans('messages.label_roof_area', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask" value="{{ $architecture->roof_area }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.builtup_area" class="col-lg-4 control-label">
                                                {{ trans('messages.label_builtup_area', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.builtup_area" type="text" name="builtup_area"
                                                       placeholder="{{ trans('messages.label_builtup_area', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask" value="{{ $architecture->builtup_area }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.ceilling_height" class="col-lg-4 control-label">
                                                {{ trans('messages.label_ceilling_height', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.ceilling_height" type="text" name="celling_height"
                                                       placeholder="{{ trans('messages.label_ceiling_height', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask" value="{{ $architecture->celling_height }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.width" class="col-lg-4 control-label">
                                                {{ trans('messages.width', [], null, $adminLang) }}:</label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.width" type="text" name="width"
                                                       placeholder="{{ trans('messages.width', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask" value="{{ $architecture->width }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.length" class="col-lg-4 control-label">
                                                {{ trans('messages.length', [], null, $adminLang) }}:</label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.length" type="text" name="length"
                                                       placeholder="{{ trans('messages.length', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask" value="{{ $architecture->length }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label data-i18n="admin.room_number" class="col-lg-4 control-label">
                                                {{ trans('messages.label_room_number', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.room_number" type="text" name="rooms_number"
                                                       placeholder="{{ trans('messages.label_room_number', [], null, $adminLang) }}"
                                                       class="form-control" value="{{ $architecture->rooms_number }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.restrooms_number" class="col-lg-4 control-label">
                                                {{ trans('messages.label_restrooms_number', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.restrooms_number" type="text" name="bathrooms_number"
                                                       placeholder="{{ trans('messages.label_restrooms_number', [], null, $adminLang) }}"
                                                       class="form-control" value="{{ $architecture->bathrooms_number or 0 }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.garages_number" class="col-lg-4 control-label">
                                                {{ trans('messages.label_garages_number', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.garages_number" type="text" name="garages_number"
                                                       placeholder="{{ trans('messages.label_garages_number', [], null, $adminLang) }}"
                                                       class="form-control" value="{{ $architecture->garages_number or 0 }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.floors" class="col-lg-4 control-label">
                                                {{ trans('messages.label_floors', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.floors" type="text" name="floors_number"
                                                       placeholder="{{ trans('messages.label_floors', [], null, $adminLang) }}"
                                                       class="form-control" value="{{ $architecture->floors_number or 0 }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.building_type" class="col-lg-4 control-label">
                                                {{ trans('messages.label_building_type', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <select name="building_type_id" id="building_type" class="form-control">
                                                    @foreach($building_types as $type)
                                                        <option data-i18n="admin.{{ $type->slug }}" value="{{ $type->id }}"
                                                                {{($architecture->building_type_id == $type->id)?"selected":''}}>
                                                            {{ trans('messages.'.$type->slug, [], null, $adminLang) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.arch_gray_price" class="col-lg-4 control-label">
                                                {{ trans('messages.arch_gray_price', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.price" type="text" name="building_price"
                                                       placeholder="{{ trans('messages.label_price', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask"
                                                       value="{{ ($architecture->building_price > 0)?$architecture->building_price:config('custom.BUILDING_PRICE') }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.arch_gray_facade_price" class="col-lg-4 control-label">
                                                {{ trans('messages.arch_gray_facade_price', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input data-i18n="[placeholder]admin.price" type="text" name="key_price"
                                                       placeholder="{{ trans('messages.label_price', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask"
                                                       value="{{ ($architecture->key_price > 0)?$architecture->key_price:config('custom.BUILDING_FACADE_PRICE') }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">
                                                {{ trans('messages.label_first_payment', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" name="first_payment"
                                                       placeholder="{{ trans('messages.label_first_payment', [], null, $adminLang) }}"
                                                       class="form-control decimal-mask"
                                                       value="{{ $architecture->first_payment }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <h1 data-i18n="admin.technology_and_design">
                            {{ trans('messages.label_technology_and_design', [], null, $adminLang) }}
                        </h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <h2 data-i18n="admin.technology_and_design">
                                                {{ trans('messages.label_technology_and_design', [], null, $adminLang) }}
                                                <span><img src="/img/common/lang/{{ $dataLang }}.png" alt=""></span>
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="col-lg-6"></div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label data-i18n="admin.exterior_walls" class="col-lg-1 control-label">
                                            {{ trans('messages.label_exterior_walls', [], null, $adminLang) }}:
                                        </label>
                                        <div class="col-lg-11">
                                            <textarea data-i18n="[placeholder]admin.exterior_walls" class="form-control"
                                                      rows="3" name="exterior_walls" style="resize: vertical;"
                                                      placeholder="{{ trans('messages.label_exterior_walls', [], null, $adminLang) }}">
                                                {{ isset($architecture->translation[$dataLang]->exterior_walls)?$architecture->translation[$dataLang]->exterior_walls:'' }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label data-i18n="admin.overlappings" class="col-lg-1 control-label">
                                            {{ trans('messages.label_overlappings', [], null, $adminLang) }}:
                                        </label>
                                        <div class="col-lg-11">
                                            <textarea data-i18n="[placeholder]admin.overlappings" class="form-control"
                                                      rows="3" name="overlappings" style="resize: vertical;"
                                                      placeholder="{{ trans('messages.label_overlappings', [], null, $adminLang) }}">
                                                {{ isset($architecture->translation[$dataLang]->overlappings)?$architecture->translation[$dataLang]->overlappings:'' }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label data-i18n="admin.housetop" class="col-lg-1 control-label">
                                            {{ trans('messages.label_housetop', [], null, $adminLang) }}:
                                        </label>
                                        <div class="col-lg-11">
                                            <textarea data-i18n="[placeholder]admin.housetop" class="form-control"
                                                      rows="3" name="roof" style="resize: vertical;"
                                                      placeholder="{{ trans('messages.label_housetop', [], null, $adminLang) }}">
                                                {{ isset($architecture->translation[$dataLang]->roof)?$architecture->translation[$dataLang]->roof:'' }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label data-i18n="admin.boiler" class="col-lg-1 control-label">
                                            {{ trans('messages.label_boiler', [], null, $adminLang) }}:
                                        </label>
                                        <div class="col-lg-11">
                                            <textarea data-i18n="[placeholder]admin.boiler" class="form-control" rows="3"
                                                      name="boiler" style="resize: vertical;"
                                                      placeholder="{{ trans('messages.label_boiler', [], null, $adminLang) }}">
                                                {{ isset($architecture->translation[$dataLang]->boiler)?$architecture->translation[$dataLang]->boiler:'' }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label data-i18n="admin.description" class="col-lg-1 control-label">
                                            {{ trans('messages.label_description', [], null, $adminLang) }}:
                                        </label>
                                        @if ($errors->has('description'))
                                            <label class="error">{{ $errors->first('description') }}</label>
                                        @endif
                                        <div class="col-lg-11">
                                            <textarea data-i18n="[placeholder]admin.description" class="form-control"
                                                      rows="7" name="description" style="resize: vertical;"
                                                      placeholder="{{ trans('messages.label_description', [], null, $adminLang) }}">
                                                {{ isset($architecture->translation[$dataLang]->description)?$architecture->translation[$dataLang]->description:'' }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <h1 data-i18n="admin.gallery">{{ trans('messages.label_gallery', [], null, $adminLang) }}</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="file" name="images[]">
                                    <input id="architecture_images_here" type="hidden" value="{{ $architecture->images }}">
                                </div>
                            </div>
                        </fieldset>
                        <h1 data-i18n="admin.general_plan_and_section">
                            {{ trans('messages.label_general_plan_and_section', [], null, $adminLang) }}
                        </h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <h2 data-i18n="admin.general_plan">
                                                {{ trans('messages.label_general_plan', [], null, $adminLang) }}
                                            </h2>
                                            <input type="file" class="simple_image" name="general_plan"
                                                   @if(!empty($architecture->general_plan)) data-content="{{'/'.config('custom.ARCHITECTURE_IMAGES_PATH').'original/'.$architecture->general_plan}}" @endif>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <h2 data-i18n="admin.house_section">
                                                {{ trans('messages.label_house_section', [], null, $adminLang) }}
                                            </h2>
                                            <input type="file" class="simple_image" name="house_section"
                                                   @if(!empty($architecture->house_section)) data-content="{{'/'.config('custom.ARCHITECTURE_IMAGES_PATH').'original/'.$architecture->house_section}}" @endif>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <input type="hidden" name="data_lang" value="{{$dataLang}}">
                        {{ method_field('PUT') }}
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </form>
                </div>
            </div>
            <div id="floors_container" class="col-lg-4">
                <floors
                        buttoncreate="{{ trans('messages.label_add_floor', [], null, $adminLang) }}"
                        namespace="{{ $namespace }}"
                        architecture_id="{{ $architecture->id }}"
                        house_zone_placeholder="{{ trans('messages.label_add_house_zone', [], null, $adminLang) }}"
                        f_key="architecture_id"
                        locale="{{ $adminLang }}"
                ></floors>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInDown m-b-n">
                            <div class="ibox m-b-n">
                                <div class="ibox-title">
                                    <h5 data-i18n="[html]admin.facades_images">
                                        {!! trans('messages.facades_images', [], null, $adminLang) !!}
                                    </h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content" style="display:none">
                                    <form action="/{{ $namespace }}/architecture/{{ $architecture->id }}/save_galleries"
                                          class="form-horizontal architecture-gallery" enctype="multipart/form-data" method="POST">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <h2 data-i18n="admin.facade">{{ trans('messages.label_facade', [], null, $adminLang) }}</h2>
                                                    <input type="file" name="facade_gallery[]">
                                                    <input type="hidden" id="facade_images_here" value="{{ $architecture->facadeGalleryImages()->get() }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-lg-offset-5">
                                                <button class="btn btn-w-m btn-primary" type="submit">
                                                    {{ trans('messages.button_send', [], null, $adminLang) }}
                                                </button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInDown m-b-n">
                            <div class="ibox m-b-n">
                                <div class="ibox-title">
                                    <h5 data-i18n="[html]admin.design_images">
                                        {!! trans('messages.design_images', [], null, $adminLang) !!}
                                    </h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content" style="display:none">
                                    <form action="/{{ $namespace }}/architecture/{{ $architecture->id }}/save_galleries"
                                          class="form-horizontal architecture-gallery" enctype="multipart/form-data" method="POST">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <h2 data-i18n="admin.catalog_interior_design">
                                                        {{ trans('messages.catalog_interior_design', [], null, $adminLang) }}
                                                    </h2>
                                                    <input type="file" name="interior_gallery[]">
                                                    <input type="hidden" id="interior_images_here"
                                                           value="{{ $architecture->interiorGalleryImages()->get() }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-lg-offset-5">
                                                <button data-i18n="admin.button_send" class="btn btn-w-m btn-primary" type="submit">
                                                    {{ trans('messages.button_send', [], null, $adminLang) }}
                                                </button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <attached-lands
                                :lands="{{($architecture->lands()->get())}}"
                                namespace="{{ $namespace }}"
                                model="architecture"
                                :model_id="{{$architecture->id}}"
                                locale="{{ $adminLang }}"
                        ></attached-lands>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
