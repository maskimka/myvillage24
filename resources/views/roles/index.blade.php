<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('styles')
    @parent
@stop

@section('title', trans('messages.menu_roles', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.menu_roles">{{trans('messages.menu_roles', [], null, $adminLang)}}</h2>
            {!! Breadcrumbs::render('roles_backend', $namespace, $adminLang) !!}
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <table id="userList" class="table table-foo" data-sorting="true" data-filtering="true" data-paging="true">
                            <thead>
                            <tr>
                                <th data-i18n="admin.no">
                                    {{trans('messages.no', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.role">
                                    {{trans('messages.label_role', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.description" data-breakpoints="xs sm">
                                    {{trans('messages.label_description', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.created" data-breakpoints="xs sm md">
                                    {{trans('messages.label_created', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.updated" data-breakpoints="xs sm md">
                                    {{trans('messages.label_updated', [], null, $adminLang)}}
                                </th>
                                {{--<th data-breakpoints="">--}}
                                    {{--@lang('messages.label_actions')--}}
                                {{--</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $key => $role)
                                <tr data-content="{{ $role->id }}">
                                    <td>
                                        {{ $key + 1 }}
                                    </td>
                                    <td>
                                        @if($role->slug)
                                            <span data-i18n="admin.{{ $role->slug }}">
                                                {{trans('messages.'.$role->slug, [], null, $adminLang)}}
                                            </span>
                                        @else
                                            {{ $role->name }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $role->description }}
                                    </td>
                                    <td>
                                        {{ $role->created_at }}
                                    </td>
                                    <td>
                                        {{ $role->updated_at }}
                                    </td>
                                    <td>
                                        {{--<button class="btn btn-primary permissions-button btn-circle-dim">--}}
                                            {{--<i class="fa fa-key"></i>--}}
                                        {{--</button>--}}
                                        <button class="btn btn-info edit-button btn-circle-dim">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button class="btn btn-danger delete-button btn-circle-dim">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        if(!!window.performance && window.performance.navigation.type === 2)
        {
            window.location.reload();
        }
    </script>
@endsection