<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('title', trans('messages.label_permissions', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.permissions">{{ trans('messages.label_permissions', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('role_create_backend', $namespace, $adminLang) !!}
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @include('errors.error_list')
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <form id="" action="/{{$namespace}}/roles" method="POST">
                            <h2 data-i18n="admin.permissions">
                                {{trans('messages.label_permissions', [], null, $adminLang)}}
                            </h2>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input data-i18n="[placeholder]admin.name" type="text" name="name"
                                               placeholder="{{trans('messages.label_name', [], null, $adminLang)}}"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input data-i18n="[placeholder]admin.slug" type="text" name="slug"
                                               placeholder="{{trans('messages.label_slug', [], null, $adminLang)}}"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input data-i18n="[placeholder]admin.description" type="text" name="description"
                                               placeholder="{{trans('messages.label_description', [], null, $adminLang)}}"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea data-i18n="[placeholder]admin.permissions" name="permissions"
                                                  placeholder="{{trans('messages.label_permissions', [], null, $adminLang)}}"
                                                  class="form-control">

                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row form-group">--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.label_model')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.label_all')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.label_view')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.label_create')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.label_update')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.label_delete')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<label>@lang('messages.label_others')</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row form-group i-checks">--}}
                                {{--<div class="col-md-1 font-bold">--}}
                                    {{--<label>@lang('messages.menu_land')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="landAll" value="/admin/land/*" class="parent-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="land[/admin/land]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="land[/admin/land/create]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="land[/admin/land/edit]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="land[/admin/land/destroy]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6 nopadding-left">--}}
                                    {{--<div class="col-md-3">--}}
                                        {{--<input id="land_status" type="checkbox" name="land[/admin/land/change_sale_status]" value="true" class="child-checkbox other">--}}
                                        {{--<label for="land_status">@lang('messages.label_modify_status')</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-3">--}}
                                        {{--<input id="land_moderate" type="checkbox" name="land[/admin/land/{id}/complement]" value="true" class="child-checkbox other">--}}
                                        {{--<label for="land_moderate">@lang('messages.label_moderate')</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<input id="land_activate" type="checkbox" name="land[/admin/land/activate]" value="true" class="child-checkbox other">--}}
                                        {{--<label for="land_activate">@lang('messages.label_activation') / @lang('messages.label_deactivation')</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row form-group i-checks">--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.menu_architecture')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="architectureAll" value="/admin/architecture/*" class="parent-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="architecture[/admin/architecture]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="architecture[/admin/architecture/create]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="architecture[/admin/architecture/edit]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="architecture[/admin/architecture/destroy]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6 nopadding-left">--}}
                                    {{--<div class="col-md-3">--}}
                                    {{--<input id="architecture_moderate" type="checkbox" name="architecture[/admin/architecture/{id}/complement]" value="true" class="child-checkbox other">--}}
                                    {{--<label for="architecture_moderate">@lang('messages.label_moderate')</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-5">--}}
                                        {{--<input id="architecture_activate" type="checkbox" name="architecture[/admin/architecture/activate]" value="true" class="child-checkbox other">--}}
                                        {{--<label for="architecture_activate">@lang('messages.label_activation') / @lang('messages.label_deactivation')</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row form-group i-checks">--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.menu_building')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="buildingAll" value="/admin/building/*" class="parent-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="building[/admin/building]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="building[/admin/building/create]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="building[/admin/building/edit]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="building[/admin/building/destroy]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6 nopadding-left">--}}
                                    {{--<div class="col-md-5">--}}
                                        {{--<input id="building_activate" type="checkbox" name="building[/admin/building/activate]" value="true" class="child-checkbox other">--}}
                                        {{--<label for="building_activate">@lang('messages.label_activation') / @lang('messages.label_deactivation')</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row form-group i-checks">--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.menu_users')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="usersAll" value="/admin/users/*" class="parent-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="users[/admin/users]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="users[/admin/users/create]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="users[/admin/users/edit]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="users[/admin/users/destroy]" value="true" class="child-checkbox">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6 nopadding-left">--}}
                                    {{--<div class="col-md-3">--}}
                                        {{--<input id="users_role" type="checkbox" name="users[/admin/users/updateRole]" value="true" class="child-checkbox other">--}}
                                        {{--<label for="users_role">@lang('messages.label_change_role')</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-3">--}}
                                        {{--<input id="users_reset_password" type="checkbox" name="users[/admin/users/resetPassword]" value="true" class="child-checkbox other">--}}
                                        {{--<label for="users_reset_password">@lang('messages.label_reset_password')</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row form-group i-checks">--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label>@lang('messages.menu_roles')</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="roleAll" value="/admin/roles/*" class="parent-checkbox" checked disabled>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="role[/admin/roles]" value="true" class="child-checkbox" checked disabled>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="role[/admin/roles/create]" value="true" class="child-checkbox" checked disabled>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="role[/admin/roles/edit]" value="true" class="child-checkbox" checked disabled>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<input type="checkbox" name="role[/admin/roles/destroy]" value="true" class="child-checkbox" checked disabled>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <button data-i18n="admin.save" class="btn btn-w-m btn-primary pull-right" type="submit">
                                        {{trans('messages.label_save', [], null, $adminLang)}}
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/inputmask/jquery.inputmask.js') !!}" type="text/javascript"></script>
@stop