@push('content_floors')
    @foreach($floors as $key => $item)
        <form action="/{{$namespace}}/floors/{{$item->id}}/edit" class="form-horizontal" enctype="multipart/form-data" method="POST">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        {{trans('messages.label_floor', ['number'=>$item->number])}}
                    </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="file" name="image" class="simple_floor_image" @if(isset($item->images()->first()->name))data-content="{{url(config('custom.FLOOR_IMAGES_PATH').'original/'.$item->images()->first()->name)}}" data-id="{{$item->images()->first()->id}}"@endif>
                            <input type="hidden" name="number" value="{{$item->number}}"/>
                        </div>
                        <div class="col-lg-6">
                            <div class="row m-t-xl">
                                <div class="form-group m-b-n">
                                    <div class="col-lg-12 b-m-n">
                                        <div class="input-group">
                                            <input type="text" value="{{$item->area or '0.00'}}" name="area" class="form-control area-input" placeholder="@lang('messages.label_area')">
                                            <span class="input-group-addon">
                                                m<sup><small>2</small></sup>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed m-l-n"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select data-placeholder="@lang('messages.label_add_house_zone')" data-index="{{$key}}" class="chosen-select house_zone_list" style="display: none;">
                                            <option value="" disable></option>
                                            @foreach($house_zone as $zone)
                                                <option value="{{$zone->id}}">{{$zone->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row p-lg">
                            <div class="col-lg-12">
                                <div class="house_zone_input_list"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ method_field('PUT') }}
            <input type="hidden" name="_token" value="{{csrf_token()}}">
        </form>
    @endforeach
@endpush
@section('scripts')
    @parent
    <script src="{!! asset('js/floor/floor_uploader.js') !!}" type="text/javascript"></script>
@stop