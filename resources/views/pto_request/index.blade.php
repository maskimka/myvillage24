<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('styles')
    @parent
@stop

@section('title', trans('messages.label_users_list', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.pto_requests">{{trans('messages.pto_requests', [], null, $adminLang)}}</h2>
            {!! Breadcrumbs::render('pto_request_backend', $namespace, $adminLang) !!}
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        <table id="userList" class="table table-foo" data-sorting="true" data-filtering="true" data-paging="true">
                            <thead>
                            <tr>
                                <th data-i18n="admin.no" data-breakpoints="xs">
                                    {{trans('messages.no', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.name">
                                    {{trans('messages.label_name', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.email">
                                    {{trans('messages.label_email', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.phone">
                                    {{trans('messages.label_phone', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.comments" data-breakpoints="xs sm" width="30%">
                                    {{trans('messages.label_comments', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.article" data-breakpoints="xs">
                                    {{trans('messages.label_article', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.created" data-breakpoints="xs sm md">
                                    {{trans('messages.label_created', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.status" data-breakpoints="xs">
                                    {{trans('messages.label_status', [], null, $adminLang)}}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($requests as $key => $ptoRequest)
                                <tr data-content="{{$ptoRequest->id}}">
                                    <td>
                                        {{ $key + 1}}
                                    </td>
                                    <td>
                                        {{ $ptoRequest->name }}
                                    </td>
                                    <td>
                                        {{ $ptoRequest->email }}
                                    </td>
                                    <td>
                                        {{ $ptoRequest->phone }}
                                    </td>
                                    <td>
                                        {{ $ptoRequest->comment }}
                                    </td>
                                    <td>
                                        @if(count($ptoRequest->lands) > 0)
                                            <p>[
                                                @foreach($ptoRequest->lands as $land)
                                                    {{ $land->article.', ' }}
                                                @endforeach
                                            ]</p>
                                        @endif
                                        @if(count($ptoRequest->architecture) > 0)
                                            <p>[
                                                @foreach($ptoRequest->architecture as $architecture)
                                                    {{ $architecture->article.', ' }}
                                                @endforeach
                                            ]</p>
                                        @endif
                                        @if(count($ptoRequest->buildings) > 0)
                                            <p>[
                                                @foreach($ptoRequest->buildings as $buildings)
                                                    {{ $buildings->article.', ' }}
                                                @endforeach
                                            ]</p>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $ptoRequest->created_at }}
                                    </td>
                                    <td>
                                        <select name="status_id" class="chosen-no-search form-control"
                                                data-pto-request="{{ $ptoRequest->id }}" style="color: {{ $ptoRequest->status->color }}">
                                            @foreach($statuses as $status)
                                                <option data-i18n="admin.{{ $status->slug }}" data-color="{{ $status->color }}"
                                                        {{ ($status->id == $ptoRequest->status_id)?'selected':'' }} value="{{ $status->id }}">
                                                    {{trans('messages.'.$status->slug, [], null, $adminLang)}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection