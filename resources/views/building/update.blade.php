<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>
<?php $dataLang = (request()->lang)?:config('app.fallback_locale')?>

@extends($extend_path)

@section('title', trans('messages.menu_buildings', [], null, $adminLang))

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/uploader/jquery.fileuploader.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/plugins/uploader/jquery.fileuploader-thumb.css') !!}" />
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.add_building">{{ trans('messages.add_building', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('buildings_edit_backend', $namespace, $building->id, $adminLang) !!}
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInDown">
        <div class="row">
            <div class="wrapper wrapper-content animated fadeInDown col-lg-8">
                <div class="ibox m-b-n">
                    <div class="ibox-title">
                        <h5 data-i18n="[html]admin.info_project">
                            {!! trans('messages.info_project', [], null, $adminLang) !!}
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form id="wizard_building_form"  action="/{{ $namespace }}/building/{{ $building->id }}"
                              class="form-horizontal" enctype="multipart/form-data" method="POST">
                            @include('errors.error_list')
                            <h1 data-i18n="admin.info">{{ trans('messages.info', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <h2>
                                                <span data-i18n="admin.project_title"></span>
                                                {{ trans('messages.label_project_title', [], null, $adminLang) }}:
                                                <span><img src="/img/common/lang/{{ $dataLang }}.png" alt=""></span>
                                            </h2>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group ">
                                                <input data-i18n="[placeholder]admin.project_title" type="text"
                                                       name="title" class="form-control text-capitalize"
                                                       placeholder="{{ trans('messages.label_project_title', [], null, $adminLang) }}"
                                                       value="{{ isset($building->translation[$dataLang]->title)?$building->translation[$dataLang]->title:'' }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <h2 id="top-general_area">{{ $building->general_area or 0 }}  m<sup><small>2</small></sup></h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <h2 data-i18n="admin.project_complectation">
                                                {{ trans('messages.label_project_complectation', [], null, $adminLang) }}:
                                            </h2>
                                        </div>
                                        <div class="col-lg-6">
                                            </br>
                                            <div class="form-group">
                                                <select data-i18n="[placeholder]admin.project_complectation"  multiple name="aac_kit[]" class="chosen-select"
                                                        data-placeholder="{{ trans('messages.label_project_complectation', [], null, $adminLang) }}">
                                                    @foreach($aac_kit as $item)
                                                        <option data-i18n="admin.{{ $item->slug }}" value="{{ $item->id }}"
                                                                @if(in_array($item->id, $acc_selected)) selected @endif>
                                                            {{ trans('messages.'.$item->slug, [], null, $adminLang) }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block m-b-none">&nbsp;</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label data-i18n="admin.article" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_article', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <h2>{{ $building->article }}</h2>
                                                    <input data-i18n="[placeholder]admin.article" type="hidden" disabled
                                                           placeholder="{{ trans('messages.label_article', [], null, $adminLang) }}"
                                                           class="form-control" value="{{ $building->article }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label data-i18n="admin.general_area" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_general_area', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.general_area" type="text" name="general_area"
                                                           placeholder="{{ trans('messages.label_general_area', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->general_area }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.living_area" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_living_area', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.living_area" type="text" name="living_area"
                                                           placeholder="{{ trans('messages.label_living_area', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->living_area }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.floor_area" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_floor_area', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.floor_area" type="text" name="floors_area"
                                                           placeholder="{{ trans('messages.label_floor_area', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->floors_area }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.roof_area" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_roof_area', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.roof_area" type="text" name="roof_area"
                                                           placeholder="{{ trans('messages.label_roof_area', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->roof_area }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.builtup_area" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_builtup_area', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.builtup_area" type="text" name="builtup_area"
                                                           placeholder="{{ trans('messages.label_builtup_area', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->builtup_area }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">
                                                    {{ trans('messages.label_builtup_area', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input type="text" name="construction_area" class="form-control decimal-mask"
                                                           placeholder="{{ trans('messages.label_builtup_area', [], null, $adminLang) }}"
                                                           value="{{ $building->construction_area }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.ceilling_height" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_ceilling_height', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.ceilling_height" type="text" name="celling_height"
                                                           placeholder="{{ trans('messages.label_ceiling_height', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->celling_height }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.length" class="col-lg-4 control-label">
                                                    {{ trans('messages.length', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.length" type="text" name="length"
                                                           placeholder="{{ trans('messages.length', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->length }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.width" class="col-lg-4 control-label">
                                                    {{ trans('messages.width', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.width" type="text" name="width"
                                                           placeholder="{{ trans('messages.width', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->width }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label data-i18n="admin.floors" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_floors', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <select name="floors_number" id="floors_number" class="form-control">
                                                        @for($i=1; $i <= 4; $i++)
                                                            <option value="{{ $i }}" {{($building->floors_number == $i)?"selected":''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.room_number" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_room_number', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.room_number" type="text" name="rooms_number"
                                                           placeholder="{{ trans('messages.label_room_number', [], null, $adminLang) }}"
                                                           class="form-control" value="{{ $building->rooms_number }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.bathrooms_number" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_bathrooms_number', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.bathrooms_number" type="text" name="bathrooms_number"
                                                           placeholder="{{ trans('messages.label_bathrooms_number', [], null, $adminLang) }}"
                                                           class="form-control" value="{{ $building->bathrooms_number or 0 }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.garages_number" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_garages_number', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.garages_number" type="text" name="garages_number"
                                                           placeholder="{{ trans('messages.label_garages_number', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->garages_number or 0 }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.building_price" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_building_price', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.price" type="text" name="building_price"
                                                           placeholder="{{ trans('messages.label_price', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->building_price }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.first_payment" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_first_payment', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.price" type="text" name="first_payment"
                                                           placeholder="{{ trans('messages.label_price', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->first_payment }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.ready_percentage" class="col-lg-4 control-label">
                                                    {{ trans('messages.ready_percentage', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.ready_percentage" type="text" name="ready_percentage"
                                                           placeholder="{{ trans('messages.ready_percentage', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $building->ready_percentage }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label data-i18n="admin.release_date" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_release_date', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.release_date" type="text" name="release_date"
                                                           placeholder="{{ trans('messages.label_release_date', [], null, $adminLang) }}"
                                                           class="form-control date-mask valid date" value="{{ $building->release_date }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h1 data-i18n="admin.technology_and_design">{{ trans('messages.label_technology_and_design', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <h2 data-i18n="admin.technology_and_design">
                                                    {{ trans('messages.label_technology_and_design', [], null, $adminLang) }}
                                                    <span><img src="/img/common/lang/{{ $dataLang }}.png" alt=""></span>
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="col-lg-6"></div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label data-i18n="admin.exterior_walls" class="col-lg-1 control-label">
                                                {{ trans('messages.label_exterior_walls', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-11">
                                                <textarea data-i18n="[placeholder]admin.exterior_walls" class="form-control"
                                                          rows="3" name="exterior_walls" style="resize: vertical;"
                                                          placeholder="{{ trans('messages.label_exterior_walls', [], null, $adminLang) }}">
                                                    {{ isset($building->translation[$dataLang]->exterior_walls)?$building->translation[$dataLang]->exterior_walls:'' }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label data-i18n="admin.overlappings" class="col-lg-1 control-label">
                                                {{ trans('messages.label_overlappings', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-11">
                                                <textarea data-i18n="[placeholder]admin.overlappings" class="form-control"
                                                          rows="3" name="overlappings" style="resize: vertical;"
                                                          placeholder="{{ trans('messages.label_overlappings', [], null, $adminLang) }}">
                                                    {{ isset($building->translation[$dataLang]->overlappings)?$building->translation[$dataLang]->overlappings:'' }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label data-i18n="admin.housetop" class="col-lg-1 control-label">
                                                {{ trans('messages.label_housetop', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-11">
                                                <textarea data-i18n="[placeholder]admin.housetop" class="form-control"
                                                          rows="3" name="roof" style="resize: vertical;"
                                                          placeholder="{{ trans('messages.label_housetop', [], null, $adminLang) }}">
                                                    {{ isset($building->translation[$dataLang]->roof)?$building->translation[$dataLang]->roof:'' }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="col-lg-1 control-label">
                                                {{ trans('messages.label_boiler', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-11">
                                                <textarea class="form-control"
                                                          rows="3" name="boiler" style="resize: vertical;"
                                                          placeholder="{{ trans('messages.label_boiler', [], null, $adminLang) }}">
                                                    {{ isset($building->translation[$dataLang]->boiler)?$building->translation[$dataLang]->boiler:'' }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="col-lg-1 control-label">
                                                {{ trans('messages.label_facade', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-11">
                                                <textarea class="form-control"
                                                          rows="3" name="facade" style="resize: vertical;"
                                                          placeholder="{{ trans('messages.label_facade', [], null, $adminLang) }}">
                                                    {{ isset($building->translation[$dataLang]->facade)?$building->translation[$dataLang]->facade:'' }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="col-lg-1 control-label">
                                                {{ trans('messages.label_windows', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-11">
                                                <textarea class="form-control"
                                                          rows="3" name="windows" style="resize: vertical;"
                                                          placeholder="{{ trans('messages.label_windows', [], null, $adminLang) }}">
                                                    {{ isset($building->translation[$dataLang]->windows)?$building->translation[$dataLang]->windows:'' }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="col-lg-1 control-label">
                                                {{ trans('messages.label_reparation', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-11">
                                                <textarea class="form-control"
                                                          rows="3" name="reparation" style="resize: vertical;"
                                                          placeholder="{{ trans('messages.label_reparation', [], null, $adminLang) }}">
                                                    {{ isset($building->translation[$dataLang]->reparation)?$building->translation[$dataLang]->reparation:'' }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label data-i18n="admin.description" class="col-lg-1 control-label">
                                                {{ trans('messages.label_description', [], null, $adminLang) }}:
                                            </label>
                                            <div class="col-lg-11">
                                                <textarea data-i18n="[placeholder]admin.description" class="form-control"
                                                          rows="7" name="description" style="resize: vertical;"
                                                          placeholder="{{ trans('messages.label_description', [], null, $adminLang) }}">
                                                    {{ isset($building->translation[$dataLang]->description)?$building->translation[$dataLang]->description:'' }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h1 data-i18n="admin.gallery">{{ trans('messages.label_gallery', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input type="file" name="images[]">
                                        <input id="building_images_here" type="hidden" value="{{ $building->images }}">
                                    </div>
                                </div>
                            </fieldset>
                            <h1 data-i18n="admin.general_plan_and_section">{{ trans('messages.label_general_plan_and_section', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <h2 data-i18n="admin.general_plan">
                                                    {{ trans('messages.label_general_plan', [], null, $adminLang) }}
                                                </h2>
                                                <input type="file" class="simple_image" name="general_plan"
                                                       @if(!empty($building->general_plan))
                                                           data-content="{{'/'.config('custom.BUILDING_IMAGES_PATH').'original/'.$building->general_plan}}"
                                                       @endif>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <h2 data-i18n="admin.house_section">
                                                    {{ trans('messages.label_house_section', [], null, $adminLang) }}
                                                </h2>
                                                <input type="file" class="simple_image" name="house_section"
                                                       @if(!empty($building->house_section))
                                                            data-content="{{'/'.config('custom.BUILDING_IMAGES_PATH').'original/'.$building->house_section}}"
                                                       @endif>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <input type="hidden" name="data_lang" value="{{$dataLang}}">
                            {{ method_field('PUT') }}
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                </div>
            </div>
            <div id="floors_container" class="col-lg-4">
                <floors
                        buttoncreate="{{ trans('messages.label_add_floor', [], null, $adminLang) }}"
                        namespace="{{ $namespace }}"
                        architecture_id="{{ $building->id }}"
                        house_zone_placeholder="{{ trans('messages.label_add_house_zone', [], null, $adminLang) }}"
                        f_key="building_id"
                        locale="{{ $adminLang }}"
                ></floors>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInDown">
                            <div class="ibox m-b-n">
                                <div class="ibox-title">
                                    <h5 data-i18n="[html]admin.facades_images">
                                        {!! trans('messages.facades_images', [], null, $adminLang) !!}
                                    </h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>
                                <div class="ibox-content" style="display:none">
                                    <form action="/{{ $namespace }}/building/{{ $building->id }}/save_galleries"
                                          class="form-horizontal architecture-gallery" enctype="multipart/form-data" method="POST">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <h2 data-i18n="admin.facade">
                                                        {{ trans('messages.label_facade', [], null, $adminLang) }}
                                                    </h2>
                                                    <input type="file" name="facade_gallery[]">
                                                    <input type="hidden" id="facade_images_here"
                                                           value="{{ $building->facadeBuildingGalleryImages()->get() }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-lg-offset-5">
                                                <button class="btn btn-w-m btn-primary" type="submit">
                                                    {{ trans('messages.button_send', [], null, $adminLang) }}
                                                </button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInDown">
                            <div class="ibox m-b-n">
                                <div class="ibox-title">
                                    <h5 data-i18n="[html]admin.design_images">
                                        {!! trans('messages.design_images', [], null, $adminLang) !!}
                                    </h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>
                                <div class="ibox-content" style="display:none">
                                    <form action="/{{ $namespace }}/building/{{ $building->id }}/save_galleries"
                                          class="form-horizontal architecture-gallery" enctype="multipart/form-data" method="POST">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <h2 data-i18n="admin.catalog_interior_design">
                                                        {{ trans('messages.catalog_interior_design', [], null, $adminLang) }}
                                                    </h2>
                                                    <input type="file" name="interior_gallery[]">
                                                    <input type="hidden" id="interior_images_here" value="{{ $building->interiorBuildingGalleryImages()->get() }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-lg-offset-5">
                                                <button class="btn btn-w-m btn-primary" type="submit">
                                                    {{ trans('messages.button_send', [], null, $adminLang) }}
                                                </button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInDown">
                            <div class="ibox m-b-n">
                                <div class="ibox-title">
                                    <h5 data-i18n="[html]admin.construction_images">
                                        {!! trans('messages.construction_images', [], null, $adminLang) !!}
                                    </h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>
                                <div class="ibox-content" style="display:none">
                                    <form action="/{{ $namespace }}/building/{{ $building->id }}/save_galleries"
                                          class="form-horizontal architecture-gallery" enctype="multipart/form-data" method="POST">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <h2 data-i18n="admin.construction_photo">
                                                        {{ trans('messages.construction_photo', [], null, $adminLang) }}
                                                    </h2>
                                                    <input type="file" name="underconstruct_gallery[]">
                                                    <input type="hidden" id="underconstruct_images_here"
                                                           value="{{ $building->underconstructBuildingGalleryImages()->get() }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="hr-line-dashed"></div>
                                            <div class="col-lg-offset-5">
                                                <button class="btn btn-w-m btn-primary" type="submit">
                                                    {{ trans('messages.button_send', [], null, $adminLang) }}
                                                </button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <attached-lands
                                :lands="{{($building->lands()->get())}}"
                                namespace="{{ $namespace }}"
                                model="building"
                                :model_id="{{$building->id}}"
                                locale="{{ $adminLang }}"
                        ></attached-lands>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
