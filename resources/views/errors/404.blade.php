<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MyVillage - 404 </title>
    <link rel="icon" type="image/jpg" href="/img/favicon.jpg">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{!! asset('css/plugins/chosen/bootstrap-chosen.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/frontend/awesome_bootstrap_checkbox.css') !!}" />


    @section('styles')

    @show

    <link rel="stylesheet" href="{{ url('css/frontend/main.css') }}">

</head>
<body>
    <div id="wrapper">

        @section('content')
            <div class="green-line"></div>
            <div class="container container-404">
                <div class="page-404 col-md-12">
                    <div class="row">
                        <div class="error-404-code">
                            404
                        </div>
                    </div>
                    <div class="row">
                        <div data-i18n="admin.error" class="error-404-name">
                            Ошибка
                        </div>
                    </div>
                    <div class="row">
                        <div data-i18n="admin.page_not_found" class="error-404-description">
                            Извините, эта страница не найдена
                        </div>
                    </div>
                    <div class="row">
                        <div class="error-404-btn-return">
                            <a data-i18n="admin.page_not_found" href="/" class="btn b-btn--color-green-light">
                                Вернуться на главную
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="error-404-image">
                <img src="/img/common/city.png" alt="image">
            </div>
            <div class="green-line"></div>
            <div class="footer-line"></div>
        @show

    </div>
</body>
</html>
