<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>
<?php $dataLang = (request()->lang)?:config('app.fallback_locale')?>

@extends($extend_path)

@section('styles')
    @parent
@stop

@section('title', trans('messages.label_land_list', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10 col-md-10">
            <h2 data-i18n="admin.land_list">{{ trans('messages.label_land_list', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('lands_backend', $namespace, $adminLang) !!}
        </div>
        <div class="col-lg-2 col-md-2">
            <br>
            <ul class="nav navbar-nav b-navbar-nav">
                <li class="dropdown dropdown-language">
                    <a href="#" class="dropdown-toggle dropdown-language--link" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        <img src="{{ url('img/common/lang/'.$dataLang.'.png') }}" class="lang-icon"> {{ strtoupper($dataLang) }}
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu b-dropdown-menu">
                        <?php $url = 1?>
                        @foreach(config('locales') as $locale => $enabled)
                            @if($enabled)
                                <li>
                                    <a href="{{ url(Request::getPathInfo().'?lang='.$locale) }}">
                                        <img src="{{ url('img/common/lang/'.$locale.'.png') }}" class="lang-icon">
                                        {{ strtoupper($locale) }}
                                    </a>
                                </li>
                                <li role="separator" class="divider"></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        <table id="landList" class="table table-foo" data-sorting="true" data-paging="true" data-filtering="true">
                            <thead>
                            <tr>
                                <th data-i18n="admin.article">
                                    {{ trans('messages.label_article', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.images">
                                    {{ trans('messages.label_images', [], null, $adminLang) }}
                                </th>
                                @if( $namespace == "admin")
                                <th data-i18n="admin.user" data-breakpoints="all">
                                    {{ trans('messages.label_user', [], null, $adminLang) }}
                                </th>
                                @endif
                                <th data-i18n="admin.cadastral_number" data-breakpoints="xs sm">
                                    {{ trans('messages.label_cadastral_number', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.address" data-breakpoints="">
                                    {{ trans('messages.label_address', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.area" data-breakpoints="all">
                                    {{ trans('messages.label_area', [], null, $adminLang) }}/{{ trans('messages.label_a', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.created" data-breakpoints="all" data-type="date">
                                    {{ trans('messages.label_created', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.purpose_type" data-breakpoints="xs sm">
                                    {{ trans("messages.label_purpose_type", [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.status" data-breakpoints="xs sm md">
                                    {{ trans("messages.label_status", [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.status_active" data-breakpoints="xs">
                                    {{ trans("messages.label_status_active", [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.price" data-breakpoints="xs">
                                    {{ trans('messages.label_price', [], null, $adminLang) }}/euro
                                </th>
                                <th data-i18n="admin.description" data-breakpoints="all">
                                    {{ trans("messages.label_description", [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.translations" data-breakpoints="xs sm md">
                                    {{ trans('messages.translations', [], null, $adminLang) }}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lands as $land)
                                <tr data-content="{{  $land->id }}" data-lang="{{ $dataLang }}">
                                    <td>
                                        {{ str_replace('-', ' ', $land->article) }}
                                    </td>
                                    <td>
                                        <div class="img-land">
                                            @if(isset($land->images[0]))
                                                <img class="img-thumbnail" alt="image"
                                                     src="{{asset(config('custom.LAND_IMAGES_PATH')."/thumbnails/".$land->images[0]->name)}}">
                                            @else
                                                <img class="img-thumbnail" src="{{asset("/img/common/placeholder.jpg")}}" alt="image">
                                            @endif
                                        </div>
                                    </td>
                                    @if( $namespace == "admin" )
                                        <?php $author = $land->user['attributes']?>
                                        <td>
                                            <div class="dropdown profile-element">
                                                <a href="#">
                                                    <span class="clear" title="{{$author['first_name']}} {{$author['last_name']}}">
                                                        <span>
                                                            @if($avatar = $author['avatar'])
                                                                <img class="img-thumbnail img-md" alt="image"
                                                                     src="{{asset(config('custom.PROFILE_AVATAR_PATH').$avatar)}}">
                                                            @else
                                                                <img class="img-thumbnail img-md" alt="image"
                                                                     src="{{asset(config('custom.PROFILE_AVATAR_PATH').'anonym.jpg')}}">
                                                            @endif
                                                        </span>
                                                        <span class="hidden">{{$author['first_name']}}&nbsp;{{$author['last_name']}}</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </td>
                                    @endif
                                    <td>
                                        {{$land->cadastral_number}}
                                    </td>
                                    <td>
                                        {{ ($land->translation[$dataLang]->country)?$land->translation[$dataLang]->country:'' }}
                                        {{ ($land->translation[$dataLang]->locality)?trans('messages.label_city_short', [], null, $dataLang):'' }}
                                        {{ ($land->translation[$dataLang]->locality)?$land->translation[$dataLang]->locality:'' }}
                                    </td>
                                    <td>
                                        {{$land->area}}
                                    </td>
                                    <td>
                                        {{$land->created_at}}
                                    </td>
                                    <td>
                                        {{ trans('messages.'.$land->purpose->slug, [], null, $dataLang) }}
                                    </td>
                                    <td>
                                        <select name="status_id" class="chosen-no-search form-control" data-land="{{ $land->id }}">
                                            @foreach($statuses as $status)
                                                <option {{ ($status->id == $land->sale_status_id)?'selected':'' }}
                                                        value="{{ $status->id }}">{{ trans('messages.'.$status->slug, [], null, $dataLang) }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        @if($user->inRole('admin') or $user->inRole('moderator'))
                                            <label class="switch">
                                                <input type="checkbox" {{($land->is_active)?"checked":""}}>
                                                <div class="slider round"></div>
                                            </label>
                                        @else
                                            <span class="label {{($land->is_active)?"label-primary":"label-danger"}}land-status">
                                                {{ trans('messages.label_status_'.(($land->is_active)?'active':'not_active'), [], null, $dataLang) }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <span class="btn btn-outline btn-primary">
                                            {{$land->price}}
                                        </span>
                                    </td>
                                    <td>
                                        {{ isset($land->translation[$dataLang])?$land->translation[$dataLang]->description:'' }}
                                    </td>
                                    <td>
                                        <a data-i18n="admin.add_translation" href="#" data-model="land" id="{{ $land->id }}"
                                           data-toggle="modal" data-target="#modalChoseLang">
                                            {{ trans('messages.add_translation', [], null, $adminLang) }}
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        if(!!window.performance && window.performance.navigation.type === 2)
        {
            window.location.reload();
        }
    </script>
@endsection