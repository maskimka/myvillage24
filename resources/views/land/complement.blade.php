<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('title', trans('messages.label_additional_information'))

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/cropper/cropper.min.css') !!}" />
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{trans('messages.menu_land')}} / {{trans('messages.label_additional_information')}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">{{trans('messages.menu_dashboard')}}</a>
                </li>
                <li>
                    <a href="/{{$namespace}}/land">{{trans('messages.label_land_list')}}</a>
                </li>
                <li class="active">
                    <strong>{{trans('messages.menu_land')}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            {{--{{dd($land->land_size_map)}}--}}
            @include('errors.error_list')
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            {{trans('messages.menu_land')}}
                            <small>{{trans('messages.label_additional_information')}}</small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form id="complementland_form" action="/{{$namespace}}/land/{{$land->id}}/complement_save" class="" enctype="multipart/form-data" method="POST">
                            <label for="">Размер</label> <small>Длина и ширина дома помещающегося на данной земле</small>
                            <div class="row">
                                <div class="col-sm-4 b-r">
                                    <div class="form-group row">
                                        <div class="col-sm-6" style="display: table">
                                            <input type="text" class="form-control" placeholder="Длина" name="rectangle_length" value="{{ $land->rectangle_length }}">
                                            <span class="input-group-addon">m</span>
                                        </div>
                                        <div class="col-sm-6" style="display: table">
                                            <input type="text" class="form-control" placeholder="Ширина" name="rectangle_width" value="{{ $land->rectangle_width }}">
                                            <span class="input-group-addon">m</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            {{--{{dd($land->land_size_map[1]->value)}}--}}
                                            <div class="col-sm-6">
                                                <select id="landParties" class="chosen-select">
                                                    <option value="" {{count($land->land_size_map)?"":"selected"}}>{{trans('messages.label_parties')}}</option>
                                                    @for($i=1; $i <= 25; $i++)
                                                        @if(count($land->land_size_map) == $i)
                                                            <option value="{{$i}}" selected>{{$i}}</option>
                                                        @else
                                                            <option value="{{$i}}">{{$i}}</option>
                                                        @endif;
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <input name="priority" class="form-control" type="text" placeholder="Приоритет" value="{{ $land->priority }}">
                                            </div>
                                        </div>
                                        <p>&nbsp;</p>
                                        <div class="row parts-container">
                                            @foreach($land->land_size_map as $item)
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input class="form-control" type="text" name="parties[{{$item->name}}]" value="{{$item->value}}">
                                                            <span class="input-group-addon">
                                                                m<sup><small>2</small></sup>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 b-r">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h3>Карта размеры</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="image-upload">
                                                <input id="imageSizeMap" type="file" class="simple_image_complement" name="imageSizeMap" @if($land->size_map) data-content="{{ '/'.config('custom.LAND_IMAGES_PATH')."original/".$land->size_map }}" @endif/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 b-r">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h3>Изображение участка</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="image-upload">
                                                <input type="file" class="simple_image_complement" name="imageCadastralMap" @if($land->cadastral_map) data-content="{{'/'.config('custom.LAND_IMAGES_PATH')."original/".$land->cadastral_map}}" @endif/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="col-lg-offset-5">
                                    <button class="btn btn-w-m btn-danger" type="submit">{{trans('messages.button_back')}}</button>
                                    <button class="btn btn-w-m btn-primary" type="submit">{{trans('messages.button_send')}}</button>
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('panel.modal.imagecrop')
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/cropper/cropper.min.js') !!}" type="text/javascript"></script>
@stop