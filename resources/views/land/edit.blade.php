<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>
<?php $dataLang = (request()->lang)?:config('app.fallback_locale')?>

@extends($extend_path)

@section('title', trans('messages.menu_dashboard', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.add_land">{{ trans('messages.label_add_land', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('lands_edit_backend', $namespace, $land->id, $adminLang) !!}
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <form id="wizard_land_form" action="/{{$namespace}}/land/{{$land->id}}" class="wizard-big edit_profile" enctype="multipart/form-data" method="POST" style="overflow: visible">
                            <h1 data-i18n="admin.land_information">{{ trans('messages.label_land_information', [], null, $adminLang) }}</h1>
                            <fieldset class="grey-fieldset">
                                <h2 data-i18n="admin.land_information">{{ trans('messages.label_land_information', [], null, $adminLang) }}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label data-i18n="admin.cadastral_number">{{ trans('messages.label_cadastral_number', [], null, $adminLang) }}</label>
                                            <input id="landCadastralNumber" name="cadastral_number" type="text" class="form-control"
                                                   placeholder="{{ trans('messages.label_cadastral_number', [], null, $adminLang) }}" value="{{$land->cadastral_number}}">
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.land_area">{{ trans('messages.label_land_area', [], null, $adminLang) }}</label>
                                            @if ($errors->has('area'))
                                                <label class="error">{{ $errors->first('area') }}</label>
                                            @endif
                                            <div class="input-group">
                                                <input data-i18n="[placeholder]admin.land_area" id="landArea" name="area"
                                                       type="text" class="form-control decimal-mask{{ ($errors->has('purpose_id'))? ' error':'' }}"
                                                       placeholder="{{ trans('messages.label_land_area', [], null, $adminLang) }}" value="{{$land->area}}">
                                                <span data-i18n="admin.label_a" class="input-group-addon">
                                                   &nbsp;{{ trans('messages.label_a', [], null, $adminLang) }}<sup>2</sup>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.from_city">{{ trans('messages.label_from_city', [], null, $adminLang) }}</label>
                                            @if ($errors->has('from_city'))
                                                <label class="error">{{ $errors->first('from_city') }}</label>
                                            @endif
                                            <div class="input-group">
                                                <input data-i18n="[placeholder]admin.from_city" id="landFromCity" name="from_city"
                                                       type="text" class="form-control decimal-mask{{ ($errors->has('purpose_id'))? ' error':'' }}"
                                                       placeholder="{{ trans('messages.label_from_city', [], null, $adminLang) }}" value="{{$land->from_city}}">
                                                <span data-i18n="admin.km" class="input-group-addon">
                                                    {{ trans('messages.label_km', [], null, $adminLang) }}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.price">{{ trans('messages.label_price', [], null, $adminLang) }}</label>
                                            <div class="input-group">
                                                <input data-i18n="[placeholder]admin.price" id="landPrice" name="price" type="text"
                                                       class="form-control decimal-mask" placeholder="{{ trans('messages.label_price', [], null, $adminLang) }}"
                                                       value="{{$land->price}}">
                                                <span class="input-group-addon">
                                                    &nbsp;<i class="fa fa-euro"></i>&nbsp;
                                                 </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.first_payment">{{ trans('messages.label_first_payment', [], null, $adminLang) }}</label>
                                            <div class="input-group">
                                                <input data-i18n="[placeholder]admin.first_payment" id="landPrePrice" name="first_payment"
                                                       type="text" class="form-control decimal-mask" placeholder="{{ trans('messages.label_first_payment', [], null, $adminLang) }}"
                                                       value="{{$land->first_payment}}">
                                                <span class="input-group-addon">
                                                    &nbsp;<i class="fa fa-euro"></i>&nbsp;
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label data-i18n="admin.purpose_type">{{ trans('messages.label_purpose_type', [], null, $adminLang) }}</label>
                                            @if ($errors->has('purpose_id'))
                                                <label class="error">{{ $errors->first('purpose_id') }}</label>
                                            @endif
                                            <select id="landPurpose" class="chosen-select{{ ($errors->has('purpose_id'))? ' error':'' }}" name="purpose_id">
                                                <option data-i18n="admin.purpose_type" value="" disabled>{{ trans('messages.label_purpose_type', [], null, $adminLang) }}</option>
                                                @foreach ($purposes as $purpose)
                                                    @if ($land->purpose_id == $purpose->id)
                                                        <option data-i18n="admin.{{ $purpose->slug }}" selected value="{{ $purpose->id }}"
                                                                data-content="{{$purpose->phone_mask}}">{{ trans('messages.'.$purpose->slug, [], null, $adminLang) }}</option>
                                                    @else
                                                        <option data-i18n="admin.{{ $purpose->slug }}" value="{{ $purpose->id }}"
                                                                data-content="{{$purpose->phone_mask}}">{{ trans('messages.'.$purpose->slug, [], null, $adminLang) }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label data-i18n="admin.description">{{ trans('messages.label_description', [], null, $adminLang) }}</label>
                                            @if ($errors->has('description'))
                                                <label class="error">{{ $errors->first('description') }}</label>
                                            @endif
                                            <span><img src="/img/common/lang/{{ $dataLang }}.png" alt=""></span>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <textarea data-i18n="[placeholder]admin.description" name="description"
                                                          class="form-control{{ ($errors->has('description'))? ' error':'' }}"
                                                          id="landDescription" style="resize: none; width: 100%; min-height: 253px" placeholder="{{ trans('messages.label_description', [], null, $adminLang) }}">
                                                    {{ isset($land->translation[$dataLang]->description)?$land->translation[$dataLang]->description:'' }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <h1 data-i18n="admin.address">{{ trans('messages.label_address', [], null, $adminLang) }}</h1>
                            <fieldset class="grey-fieldset">
                                <h2 data-i18n="admin.address">{{ trans('messages.label_address', [], null, $adminLang) }}</h2>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label data-i18n="admin.country">{{ trans('messages.label_country', [], null, $adminLang) }}</label>
                                            <input type="text" name="country" id="country" class="form-control" value="{{ $land->translation[$dataLang]->country }}">
                                        </div>
                                        <input id="userCountryIso" type="hidden" value="{{ $user->country ? $user->country->iso2code : config('custom.default_country_iso') }}">
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label data-i18n="admin.region">{{ trans('messages.label_region', [], null, $adminLang) }}</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker"></i>
                                                </span>
                                                <input type="text" name="administrative" id="administrative"
                                                       class="form-control" value="{{ $land->translation[$dataLang]->administrative }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label data-i18n="admin.locality">{{ trans('messages.label_locality', [], null, $adminLang) }}</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker"></i>
                                                </span>
                                                <input type="text" name="locality" id="locality" class="form-control" value="{{ $land->translation[$dataLang]->locality }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label data-i18n="admin.district">{{ trans('messages.label_district', [], null, $adminLang) }}</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker"></i>
                                                </span>
                                                <input type="text" name="sublocality" id="sublocality" class="form-control" value="{{ $land->translation[$dataLang]->sublocality }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label data-i18n="admin.address">{{ trans('messages.label_address', [], null, $adminLang) }}</label>
                                            <span><img src="/img/common/lang/{{ $dataLang }}.png" alt=""></span>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker"></i>
                                                </span>
                                                <input data-i18n="[placeholder]admin.address" name="street" id="street" type="text"
                                                       value="{{ $land->translation[$dataLang]->street }}" class="form-control"
                                                       placeholder="{{ trans('messages.label_address', [], null, $adminLang) }}" onFocus="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label data-i18n="admin.number">{{ trans('messages.number', [], null, $adminLang) }}</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker"></i>
                                                </span>
                                                <input data-i18n="[placeholder]admin.number" name="street_number" id="street_number"
                                                       type="text" value="{{ $land->translation[$dataLang]->street_number }}"
                                                       class="form-control" placeholder="{{ trans('messages.number', [], null, $adminLang) }}" onFocus="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label data-i18n="admin.land_lat">{{ trans('messages.label_land_lat', [], null, $adminLang) }}</label>
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-map-marker"></i>
                                            </span>
                                                <input data-i18n="[placeholder]admin.land_lat" id="mapLat" name="lat" type="text"
                                                       placeholder="{{ trans('messages.label_land_lat', [], null, $adminLang) }}"
                                                       class="form-control" value="{{ $land->lat }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label data-i18n="admin.land_lng">{{ trans('messages.label_land_lng', [], null, $adminLang) }}</label>
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-map-marker"></i>
                                            </span>
                                                <input data-i18n="[placeholder]admin.land_lng" id="mapLng" name="lng"
                                                       placeholder="{{ trans('messages.label_land_lng', [], null, $adminLang) }}"
                                                       class="form-control" type="text" value="{{ $land->lng }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input data-i18n="admin.enter_location" id="searchInput" class="input-controls"
                                                   type="text" placeholder="{{ trans('messages.enter_location', [], null, $adminLang) }}">
                                            <div id="map-canvas"></div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="country_code" name="country_code" value="{{ $land->country_code }}">
                            </fieldset>

                            <h1 data-i18n="admin.infrastructure_communications">{{ trans('messages.infrastructure_communications', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <h2 data-i18n="admin.infrastructure">{{ trans('messages.label_infrastructure', [], null, $adminLang) }}</h2>
                                        <p>&nbsp;</p>
                                        @foreach($infrastructure as $item)
                                            <?php $checked = "" ?>
                                            @foreach($land->infrastructure as $land_infrastructure)
                                                @if ($land_infrastructure->id == $item->id)
                                                    <?php $checked = "checked" ?>
                                                @endif
                                            @endforeach
                                            <div class="checkbox i-checks col-lg-6">
                                                <label>
                                                    <input type="checkbox" name="infrastructure[]" value="{{$item->id}}" {{$checked}}>&nbsp;<i></i>
                                                    <span data-i18n="admin.{{ $item->slug }}">{{ trans('messages.'.$item->slug, [], null, $adminLang) }}</span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-lg-4">
                                        <h2 data-i18n="admin.transport">{{ trans('messages.label_transport', [], null, $adminLang) }}</h2>
                                        <p>&nbsp;</p>
                                        @foreach($transport as $item)
                                            <?php $checked = "" ?>
                                            @foreach($land->transport as $land_transport)
                                                @if ($land_transport->id == $item->id)
                                                    <?php $checked = "checked" ?>
                                                @endif
                                            @endforeach
                                            <div class="checkbox i-checks col-lg-6">
                                                <label>
                                                    <input type="checkbox" name="transport[]" value="{{$item->id}}" {{$checked}}>&nbsp;<i></i>
                                                    <span data-i18n="admin.{{ $item->slug }}">{{ trans('messages.'.$item->slug, [], null, $adminLang) }}</span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-lg-4">
                                        <h2 data-i18n="admin.communications">{{ trans('messages.label_communications', [], null, $adminLang) }}</h2>
                                        <p>&nbsp;</p>
                                        @foreach($communications as $communication)
                                            <?php $checked = "" ?>
                                            @foreach($land->communications as $land_communication)
                                                @if ($land_communication->id == $communication->id)
                                                    <?php $checked = "checked"?>
                                                @endif
                                            @endforeach
                                            <div class="checkbox i-checks col-lg-6">
                                                <label>
                                                    <input type="checkbox" name="communications[]" value="{{$communication->id}}" {{$checked}}>&nbsp;<i></i>
                                                    <span data-i18n="admin.{{ $communication->slug }}">{{ trans('messages.'.$communication->slug, [], null, $adminLang) }}</span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </fieldset>
                            <h1 data-i18n="admin.images">{{ trans('messages.label_images', [], null, $adminLang) }}</h1>
                            <fieldset class="grey-fieldset">
                                <h2 data-i18n="admin.images">{{ trans('messages.label_images', [], null, $adminLang) }}</h2>
                                <div class="row">
                                    <input type="file" class="hidden" name="images"/>
                                </div>
                                <h2 data-i18n="admin.video" class="table-cell">{{ trans('messages.label_video', [], null, $adminLang) }}</h2>
                                <span data-i18n="admin.video_example" class="table-cell">&nbsp;{{ trans('messages.video_example', [], null, $adminLang) }}</span>
                                @if ($errors->has('land_video'))
                                    <label class="error">{{ $errors->first('land_video') }}</label>
                                @endif
                                <div class="row padding-v-15">
                                    <input data-i18n="[placeholder]admin.video_link" type="text" class="form-control{{ ($errors->has('land_video'))? ' error':'' }}"
                                           placeholder="{{ trans('messages.video_link', [], null, $adminLang) }}" name="land_video" value="{{ $land->land_video }}">
                                </div>
                            </fieldset>

                            <h1 data-i18n="admin.finish">{{ trans('messages.label_finish', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <h2 data-i18n="admin.finish">{{ trans('messages.label_finish', [], null, $adminLang) }}</h2>
                                <div class="checkbox i-checks">
                                    <label>
                                        <input type="checkbox" name="terms" checked>&nbsp;<i></i>
                                        <a data-i18n="admin.terms" href="/terms">{{ trans('messages.label_terms', [], null, $adminLang) }}</a>
                                    </label>
                                </div>
                            </fieldset>
                            <input type="hidden" name="data_lang" value="{{$dataLang}}">
                            {{ method_field('PUT') }}
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                    <div class="ibox-footer">
                        <i class="fa fa-warning text-warning"></i><span data-i18n="admin.lang_explanation"> {{ trans('messages.lang_explanation', [], null, $adminLang) }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($user->inRole('seomanager'))
        <div class="wrapper wrapper-content animated fadeInRight m-t-n">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5 data-i18n="[html]admin.facebook_meta_tags">
                                {{ trans('messages.facebook_meta_tags', [], null, $adminLang) }}
                            </h5>
                        </div>
                        <div class="ibox-content">
                            <form id="opGraphForm" role="form" action="/admin/land/{{$land->id}}/opengraph" class="" method="POST">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label data-i18n="admin.fb_header">{{ trans('messages.fb_header', [], null, $adminLang) }}</label>
                                        <input data-i18n="[placeholder]admin.header" type="text" name="name" class="form-control"
                                               placeholder="{{ trans('messages.header', [], null, $adminLang) }}" value="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label data-i18n="admin.fb_description">{{ trans('messages.fb_description', [], null, $adminLang) }}</label>
                                        <textarea class="form-control" name="meta_description" rows="10" style="resize: vertical"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label data-i18n="admin.fb_image">{{ trans('messages.fb_image', [], null, $adminLang) }}</label>
                                        <input type="file" name="">
                                    </div>
                                </div>
                                <div class="col-lg-12" style="text-align: right;">
                                    <input data-i18n="admin.save" id="seoTemplateFormSubmit" type="button"
                                           class="btn btn-primary" value="{{ trans('messages.label_save', [], null, $adminLang) }}">
                                </div>
                                {{ method_field('PUT') }}
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($user->inRole('moderator') || $user->inRole('admin'))
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                {{--{{dd($land->land_size_map)}}--}}
                @include('errors.error_list')
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 data-i18n="[html]admin.land_add_info">
                                {!! trans('messages.land_add_info', [], null, $adminLang) !!}
                            </h5>
                        </div>
                        <div class="ibox-content">
                            <form id="complementland_form" action="/{{$namespace}}/land/{{$land->id}}/complement_save" enctype="multipart/form-data" method="POST">
                                <label data-i18n="admin.measure" for="">{{ trans('messages.label_measure', [], null, $adminLang) }}</label>
                                <small data-i18n="admin.measure_explanation">{{ trans('messages.measure_explanation', [], null, $adminLang) }}</small>
                                <div class="row">
                                    <div class="col-sm-4 b-r">
                                        <div class="form-group row">
                                            <div class="col-sm-6" style="display: table">
                                                <input data-i18n="[placeholder]admin.length" type="text" class="form-control"
                                                       placeholder="{{ trans('messages.length', [], null, $adminLang) }}"
                                                       name="rectangle_length" value="{{ $land->rectangle_length }}">
                                                <span class="input-group-addon">m</span>
                                            </div>
                                            <div class="col-sm-6" style="display: table">
                                                <input data-i18n="admin.width" type="text" class="form-control"
                                                       placeholder="{{ trans('messages.width', [], null, $adminLang) }}"
                                                       name="rectangle_width" value="{{ $land->rectangle_width }}">
                                                <span class="input-group-addon">m</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <select id="landParties" class="chosen-select">
                                                        <option data-i18n="admin.parties" value="" {{count($land->land_size_map)?"":"selected"}}>
                                                            {{ trans('messages.label_parties', [], null, $adminLang) }}</option>
                                                        @for($i=1; $i <= 25; $i++)
                                                            @if(count($land->land_size_map) == $i)
                                                                <option value="{{$i}}" selected>{{$i}}</option>
                                                            @else
                                                                <option value="{{$i}}">{{$i}}</option>
                                                            @endif
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input data-i18n="[placeholder]admin.priority" name="priority"
                                                           placeholder="{{ trans('messages.priority', [], null, $adminLang) }}"
                                                           class="form-control" type="text" value="{{ $land->priority }}">
                                                </div>
                                            </div>
                                            <p>&nbsp;</p>
                                            <div class="row parts-container">
                                                @foreach($land->land_size_map as $item)
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input class="form-control" type="text" name="parties[{{$item->name}}]" value="{{$item->value}}">
                                                            <span class="input-group-addon">
                                                                m<sup><small>2</small></sup>
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 b-r">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3 data-i18n="admin.map_measure">{{ trans('messages.map_measure', [], null, $adminLang) }}</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="image-upload">
                                                    <input id="imageSizeMap" type="file" class="simple_image_complement"
                                                           name="imageSizeMap" @if($land->size_map) data-content="{{ '/'.config('custom.LAND_IMAGES_PATH')."original/".$land->size_map }}" @endif/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 b-r">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3 data-i18n="admin.land_image">{{ trans('messages.land_image', [], null, $adminLang) }}</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="image-upload">
                                                    <input type="file" class="simple_image_complement" name="imageCadastralMap"
                                                           @if($land->cadastral_map) data-content="{{'/'.config('custom.LAND_IMAGES_PATH')."original/".$land->cadastral_map}}" @endif/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="row">
                                    <div class="col-lg-offset-5">
                                        <button data-i18n="admin.button_back" class="btn btn-w-m btn-danger" type="submit">
                                            {{ trans('messages.button_back', [], null, $adminLang) }}
                                        </button>
                                        <button data-i18n="admin.button_send" class="btn btn-w-m btn-primary" type="submit">
                                            {{ trans('messages.button_send', [], null, $adminLang) }}
                                        </button>
                                    </div>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="data_lang" value="{{ $dataLang }}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('panel.modal.imagecrop')
    @endif

    <input type="hidden" id="land_images_here" value="{{$land_images}}">
@endsection

<script>
    window['countries'] = {!! $countries !!}
</script>