<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>
<?php $dataLang = (request()->lang)?:config('app.fallback_locale')?>

@extends($extend_path)

@section('title', trans('messages.menu_villages', [], null, $adminLang))

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/uploader/jquery.fileuploader.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/plugins/uploader/jquery.fileuploader-thumb.css') !!}" />
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('messages.add_village', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('villages_edit_backend', $namespace, $village->id, $adminLang) !!}
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInDown">
        <div class="row">
            <div class="wrapper wrapper-content animated fadeInDown col-lg-8">
                <div class="ibox m-b-n">
                    <div class="ibox-title">
                        <h5 data-i18n="[html]admin.info_project">
                            {!! trans('messages.info_project', [], null, $adminLang) !!}
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <form id="wizard_village_form"  action="/{{ $namespace }}/village/{{ $village->id }}"
                              class="form-horizontal" enctype="multipart/form-data" method="POST">
                            @include('errors.error_list')
                            <h1 data-i18n="admin.info">{{ trans('messages.info', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <h2>
                                                <span></span>
                                                {{ trans('messages.label_project_title', [], null, $adminLang) }}:
                                                <span><img src="/img/common/lang/{{ $dataLang }}.png" alt=""></span>
                                            </h2>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group ">
                                                <input data-i18n="[placeholder]admin.project_title" type="text"
                                                       name="title" class="form-control text-capitalize"
                                                       placeholder="{{ trans('messages.label_project_title', [], null, $adminLang) }}"
                                                       value="{{ isset($village->translation[$dataLang]->title)?$village->translation[$dataLang]->title:'' }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <h2 id="top-general_area">{{ $village->general_area or 0 }}  m<sup><small>2</small></sup></h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label data-i18n="admin.article" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_article', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <h2>{{ $village->article }}</h2>
                                                    <input data-i18n="[placeholder]admin.article" type="hidden" disabled
                                                           placeholder="{{ trans('messages.label_article', [], null, $adminLang) }}"
                                                           class="form-control" value="{{ $village->article }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label data-i18n="admin.general_area" class="col-lg-4 control-label">
                                                    {{ trans('messages.label_general_area', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input data-i18n="[placeholder]admin.general_area" type="text" name="general_area"
                                                           placeholder="{{ trans('messages.label_general_area', [], null, $adminLang) }}"
                                                           class="form-control decimal-mask" value="{{ $village->general_area }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">
                                                    {{ trans('messages.label_from_city', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-8">
                                                    <input type="text" name="from_city"
                                                           placeholder="{{ trans('messages.label_from_city', [], null, $adminLang) }}"
                                                           class="form-control" value="{{ $village->from_city }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label data-i18n="admin.description" class="col-lg-1 control-label">
                                                    {{ trans('messages.label_description', [], null, $adminLang) }}:
                                                </label>
                                                <div class="col-lg-11">
                                                    <textarea data-i18n="[placeholder]admin.description" class="form-control"
                                                              rows="7" name="description" style="resize: vertical;"
                                                              placeholder="{{ trans('messages.label_description', [], null, $adminLang) }}">
                                                        {{ isset($architecture->translation[$dataLang]->description)?$architecture->translation[$dataLang]->description:'' }}
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h1 data-i18n="admin.general_plan_and_section">{{ trans('messages.label_general_plan', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <h2 data-i18n="admin.general_plan">
                                                    {{ trans('messages.label_general_plan', [], null, $adminLang) }}
                                                </h2>
                                                <input type="file" class="simple_image" name="general_plan"
                                                       @if(!empty($village->general_plan))
                                                       data-content="{{'/'.config('custom.VILLAGE_IMAGES_PATH').'original/'.$village->general_plan}}"
                                                        @endif>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h1 data-i18n="admin.infrastructure_communications">{{ trans('messages.infrastructure_communications', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 data-i18n="admin.infrastructure">{{ trans('messages.label_infrastructure', [], null, $adminLang) }}</h2>
                                        <p>&nbsp;</p>
                                        @foreach($infrastructure as $item)
                                            <?php $checked = "" ?>
                                            @foreach($village->infrastructure as $village_infrastructure)
                                                @if ($village_infrastructure->id == $item->id)
                                                    <?php $checked = "checked" ?>
                                                @endif
                                            @endforeach
                                            <div class="checkbox i-checks col-lg-6">
                                                <label>
                                                    <input type="checkbox" name="infrastructure[]" value="{{$item->id}}" {{$checked}}>&nbsp;<i></i>
                                                    <span data-i18n="admin.{{ $item->slug }}">{{ trans('messages.'.$item->slug, [], null, $adminLang) }}</span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-lg-6">
                                        <h2 data-i18n="admin.communications">{{ trans('messages.label_communications', [], null, $adminLang) }}</h2>
                                        <p>&nbsp;</p>
                                        @foreach($communications as $communication)
                                            <?php $checked = "" ?>
                                            @foreach($village->communications as $village_communication)
                                                @if ($village_communication->id == $communication->id)
                                                    <?php $checked = "checked"?>
                                                @endif
                                            @endforeach
                                            <div class="checkbox i-checks col-lg-6">
                                                <label>
                                                    <input type="checkbox" name="communications[]" value="{{$communication->id}}" {{$checked}}>&nbsp;<i></i>
                                                    <span data-i18n="admin.{{ $communication->slug }}">{{ trans('messages.'.$communication->slug, [], null, $adminLang) }}</span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </fieldset>
                            <h1 data-i18n="admin.label_transport">{{ trans('messages.label_transport', [], null, $adminLang) }}</h1>
                            <fieldset>
                                <div class="col-lg-6">
                                    <h2 data-i18n="admin.transport">{{ trans('messages.label_transport', [], null, $adminLang) }}</h2>
                                    <p>&nbsp;</p>
                                    @foreach($transport as $item)
                                        <?php $checked = "" ?>
                                        @foreach($village->transport as $village_transport)
                                            @if ($village_transport->id == $item->id)
                                                <?php $checked = "checked" ?>
                                            @endif
                                        @endforeach
                                        <div class="checkbox i-checks col-lg-6">
                                            <label>
                                                <input type="checkbox" name="transport[]" value="{{$item->id}}" {{$checked}}>&nbsp;<i></i>
                                                <span data-i18n="admin.{{ $item->slug }}">{{ trans('messages.'.$item->slug, [], null, $adminLang) }}</span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </fieldset>
                            <input type="hidden" name="data_lang" value="{{$dataLang}}">
                            {{ method_field('PUT') }}
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                </div>
            </div>
            <div id="floors_container" class="col-lg-4">
                <div class="row">
                    <div class="col-lg-12">
                        <attached-lands
                                :lands="{{($village->lands()->get())}}"
                                namespace="{{ $namespace }}"
                                model="village"
                                :model_id="{{$village->id}}"
                                locale="{{ $adminLang }}"
                        ></attached-lands>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <attached-buildings
                            :buildings="{{($village->buildings()->get())}}"
                            namespace="{{ $namespace }}"
                            model="village"
                            :model_id="{{$village->id}}"
                            locale="{{ $adminLang }}"
                        ></attached-buildings>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
