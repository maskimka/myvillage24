<?php $extend_path = 'layouts.'.$namespace.'.app' ?>
<?php $dataLang = (request()->lang)?:config('app.fallback_locale')?>
@extends($extend_path)

@section('styles')
    @parent
@stop

@section('title', trans('messages.village_list', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.village_list">{{ trans('messages.village_list', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('villages_backend', $namespace, $adminLang) !!}
        </div>
        <div class="col-lg-2 col-md-2">
            <br>
            <ul class="nav navbar-nav b-navbar-nav">
                <li class="dropdown dropdown-language">
                    <a href="#" class="dropdown-toggle dropdown-language--link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ url('img/common/lang/'.$dataLang.'.png') }}" class="lang-icon"> {{ strtoupper($dataLang) }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu b-dropdown-menu">
                        <?php $url = 1?>
                        @foreach(config('locales') as $locale => $enabled)
                            @if($enabled)
                                <li>
                                    <a href="{{ url(Request::getPathInfo().'?lang='.$locale) }}">
                                        <img src="{{ url('img/common/lang/'.$locale.'.png') }}" class="lang-icon"> {{ strtoupper($locale) }}
                                    </a>
                                </li>
                                <li role="separator" class="divider"></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        <table class="table table-foo" data-sorting="true" data-filtering="true" data-paging="true">
                            <thead>
                            <tr>
                                <th data-breakpoints="xs">
                                    {{ trans('messages.label_article', [], null, $adminLang) }}
                                </th>
                                <th>
                                    {{ trans('messages.label_images', [], null, $adminLang) }}
                                </th>
                                <th>
                                    {{ trans('messages.label_title', [], null, $adminLang) }}
                                </th>
                                <th data-breakpoints="xs">
                                    {{ trans('messages.label_general_area', [], null, $adminLang) }}/m<sup>2</sup>
                                </th>
                                <th data-breakpoints="xs sm">
                                    {{ trans('messages.label_status_active', [], null, $adminLang) }}
                                </th>
                                <th data-breakpoints="xs sm md">
                                    {{ trans('messages.translations', [], null, $adminLang) }}
                                </th>
                                @if($namespace == "admin")
                                    <th data-i18n="admin.user" data-breakpoints="all">
                                        {{ trans('messages.label_user', [], null, $adminLang) }}
                                    </th>
                                @endif
                                <th>
                                    {{ trans('messages.label_created', [], null, $adminLang) }}
                                </th>
                                <th>
                                    {{ trans('messages.label_updated', [], null, $adminLang) }}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($village as $item)
                                <tr data-content="{{$item->id}}" data-lang="{{ $dataLang }}">
                                    <td>
                                        {{ str_replace('-', ' ', $item->article) }}
                                    </td>
                                    <td>
                                        <div class="img-land">
                                            @if(isset($item->images[0]))

                                                <img class="img-thumbnail" src="{{asset(config('custom.BUILDING_IMAGES_PATH')."/thumbnails/".$item->images->get(0)->name)}}" alt="image">
                                            @else
                                                <img class="img-thumbnail" src="{{asset("/img/common/placeholder.jpg")}}" alt="image">
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        {{$item->title or '--'}}
                                    </td>
                                    <td>
                                        <span class="accent-label-green">
                                            {{$item->general_area}}
                                        </span>
                                    </td>
                                    <td>
                                        @if($namespace == "admin")
                                            <label class="switch">
                                                <input type="checkbox" {{($item->is_active)?"checked":""}}>
                                                <div class="slider round"></div>
                                            </label>
                                        @else
                                            <span data-i18n="admin.{{ ($item->is_active)?'status_active':'status_not_active' }}"
                                                  class="label {{($item->is_active)?"label-primary":"label-danger"}} land-status">
                                               {{ trans('messages.label_status_'.(($item->is_active)?'active':'not_active'), [], null, $adminLang) }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a data-i18n="admin.add_translation" href="#" data-model="village"
                                           id="{{ $item->id }}" data-toggle="modal" data-target="#modalChoseLang">
                                            {{ trans('messages.add_translation', [], null, $adminLang) }}
                                        </a>
                                    </td>
                                    @if( $namespace == "admin" )
                                        <?php $author = $item->user['attributes']?>
                                        <td>
                                            <div class="dropdown profile-element">
                                                <a href="#">
                                                    <span class="clear" title="{{$author['first_name']}} {{$author['last_name']}}">
                                                        <span>
                                                            @if($avatar = $author['avatar'])
                                                                <img class="img-thumbnail img-md" src="{{asset(config('custom.PROFILE_AVATAR_PATH').$avatar)}}" alt="image">
                                                            @else
                                                                <img class="img-thumbnail img-md" src="{{asset(config('custom.PROFILE_AVATAR_PATH').'anonym.jpg')}}" alt="image">
                                                            @endif
                                                        </span>
                                                        <span class="hidden">{{$author['first_name']}}&nbsp;{{$author['last_name']}}</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </td>
                                    @endif
                                    <td>
                                        {{$item->created_at}}
                                    </td>
                                    <td>
                                        {{$item->updated_at}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        if (!!window.performance && window.performance.navigation.type === 2) {
            window.location.reload();
        }
    </script>
@endsection