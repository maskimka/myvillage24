@extends('layouts/admin.app')

@section('title', trans('messages.menu_dashboard', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.menu_dashboard">{{ trans('messages.menu_dashboard', [], null, $adminLang) }}</h2>
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="/">Главная</a>--}}
                {{--</li>--}}
                {{--<li class="active">--}}
                    {{--<strong>Главная</strong>--}}
                {{--</li>--}}
            {{--</ol>--}}
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @if($user->inRole('moderator'))
                <div class="col-lg-12">
                    <bar-chart
                        lands-daily-count       = "{{ $landsDailyCount }}"
                        lands-monthly-count     = "{{ $landsMonthlyCount }}"
                        lands-annual-count      = "{{ $landsAnnualCount }}"
                        projects-daily-count    = "{{ $projectsDailyCount }}"
                        projects-monthly-count  = "{{ $projectsMonthlyCount }}"
                        projects-annual-count   = "{{ $projectsAnnualCount }}"
                        buildings-daily-count   = "{{ $buildingsDailyCount }}"
                        buildings-monthly-count = "{{ $buildingsMonthlyCount }}"
                        buildings-annual-count  = "{{ $buildingsAnnualCount }}"
                    ></bar-chart>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')

    @parent
    <script src="{!! asset('js/plugins/chartJs/Chart.min.js') !!}" type="text/javascript"></script>

@endsection

<script type="text/javascript">
    if (window.location.hash && window.location.hash == '#_=_') {
        window.location.href = window.location.origin + window.location.pathname;
    }
</script>