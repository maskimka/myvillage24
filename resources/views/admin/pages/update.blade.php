@extends('layouts.admin.app')

@section('title', trans('messages.edit_pages', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.seo_pages">{{ trans('messages.seo_pages', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('seo_page_edit_backend', $namespace, $page->id, $adminLang) !!}
        </div>
    </div>
    {{--Content--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="col-lg-offset-2 col-lg-8 col-md-12">
                            <form id="editPageForm" role="form" action="/admin/pages/{{ $page->id }}" class="" method="POST">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label data-i18n="admin.page_name">
                                                {{ trans('messages.page_name', [], null, $adminLang) }}
                                            </label>
                                            <input data-i18n="[placeholder]admin.header" name="title" class="form-control"
                                                   type="text" value="{{ $page->title }}" @if(!$user->inRole('admin')) readonly @endif
                                                   placeholder="{{ trans('messages.header', [], null, $adminLang) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label data-i18n="admin.controller">
                                                {{ trans('messages.controller', [], null, $adminLang) }}
                                            </label>
                                            <input data-i18n="[placeholder]admin.controller" name="controller" class="form-control"
                                                   type="text" value="{{ $page->controller }}" @if(!$user->inRole('admin')) readonly @endif
                                                   placeholder="{{ trans('messages.controller', [], null, $adminLang) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label data-i18n="admin.method">
                                                {{ trans('messages.method', [], null, $adminLang) }}
                                            </label>
                                            <input data-i18n="[placeholder]admin.method" type="text" name="action" class="form-control"
                                                   placeholder="{{ trans('messages.method', [], null, $adminLang) }}"
                                                   value="{{ $page->action }}" @if(!$user->inRole('admin')) readonly @endif>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="text-align: right;">
                                    <input data-i18n="[value]admin.save" id="pageFormSubmit" class="btn btn-primary"
                                           type="button" value="{{ trans('messages.label_save', [], null, $adminLang) }}">
                                </div>
                                {{ method_field('PUT') }}
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection