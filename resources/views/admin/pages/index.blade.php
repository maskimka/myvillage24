@extends('layouts.admin.app')

@section('title', trans('messages.pages', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.seo_pages">{{ trans('messages.seo_pages', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('seo_pages_backend', $namespace, $adminLang) !!}
        </div>
    </div>
    {{--Content--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        <table class="table-foo" data-sorting="true" data-filtering="true" data-paging="true">
                            <thead>
                                <tr>
                                    <th data-i18n="admin.no">
                                        {{ trans('messages.no', [], null, $adminLang) }}
                                    </th>
                                    <th data-i18n="admin.page">
                                        {{ trans('messages.page', [], null, $adminLang) }}
                                    </th>
                                    <th data-i18n="admin.controller" data-breakpoints="xs">
                                        {{ trans('messages.controller', [], null, $adminLang) }}
                                    </th>
                                    <th data-i18n="admin.method" data-breakpoints="xs sm">
                                        {{ trans('messages.method', [], null, $adminLang) }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($pages as $key => $page)
                                <tr data-content="{{$page->id}}">
                                    <td>{{($key + 1)}}</td>
                                    <td>{{$page->title}}</td>
                                    <td>{{$page->controller or '--'}}</td>
                                    <td>{{$page->action or '--'}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        if(!!window.performance && window.performance.navigation.type === 2) {

            window.location.reload();
        }
    </script>
@endsection