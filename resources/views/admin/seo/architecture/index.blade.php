<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('title', trans('messages.label_architecture_list', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.architecture_list">{{ trans('messages.label_architecture_list', [], null, $adminLang) }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a data-i18n="admin.menu_dashboard" href="/{{$namespace}}">{{ trans('messages.menu_dashboard', [], null, $adminLang) }}</a>
                </li>
                <li class="active">
                    <strong data-i18n="admin.menu_seoland">{{ trans('messages.menu_seoarchitecture', [], null, $adminLang) }}</strong>
                </li>
            </ol>
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        <table class="table table-foo" data-sorting="true" data-paging="true" data-filtering="true">
                            <thead>
                            <tr>
                                <th data-i18n="admin.article">
                                    {{ trans('messages.label_article', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.images">
                                    {{ trans('messages.label_images', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.title">
                                    {{ trans('messages.label_title', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.floors">
                                    {{ trans('messages.label_floors', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.general_area">
                                    {{ trans('messages.label_general_area', [], null, $adminLang) }}/m<sup>2</sup>
                                </th>
                                <th data-i18n="admin.status_active">
                                    {{ trans('messages.label_status_active', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.price">
                                    {{ trans('messages.label_price', [], null, $adminLang) }}/euro
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($architecture as $item)
                                <tr data-content="{{$item->id}}">
                                    <td>
                                        {{$item->article}}
                                    </td>
                                    <td>
                                        <div class="img-land">
                                            @if(isset($item->images[0]))

                                                <img class="img-thumbnail" alt="image"
                                                     src="{{asset(config('custom.ARCHITECTURE_IMAGES_PATH')."/thumbnails/".$item->images->get(0)->name)}}">
                                            @else
                                                <img class="img-thumbnail" src="{{asset("/img/common/placeholder.jpg")}}" alt="image">
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        {{$item->title or '--'}}
                                    </td>
                                    <td>
                                        <span class="accent-label-grey">
                                            ({{$item->floors_number}})
                                        </span>
                                    </td>
                                    <td>
                                        <span class="accent-label-green">
                                            {{$item->general_area}}
                                        </span>
                                    </td>
                                    <td>
                                        <span data-i18n="admin.{{ ($item->is_active)?'status_active':'status_not_active' }}"
                                              class="label {{ 'label-'.(($item->is_active) ? 'primary' : 'danger') }} land-status">
                                            {{ trans('messages.label_status_'.(($item->is_active)?'active':'not_active'), [], null, $adminLang) }}</span>
                                    </td>
                                    <td>
                                        <span class="btn btn-outline btn-primary">
                                            {{ number_format($item->building_price * $item->general_area, 0, '.', ' ') }}
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        if(!!window.performance && window.performance.navigation.type === 2)
        {
            window.location.reload();
        }
    </script>
@endsection