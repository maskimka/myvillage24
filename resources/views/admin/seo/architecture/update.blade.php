<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('title', trans('messages.label_architecture_list', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.architecture_list">{{ trans('messages.label_architecture_list', [], null, $adminLang) }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a data-i18n="admin.menu_dashboard" href="/{{$namespace}}">{{ trans('messages.menu_dashboard', [], null, $adminLang) }}</a>
                </li>
                <li>
                    <a data-i18n="admin.menu_architecture" href="/{{$namespace}}/seo/architecture">{{ trans('messages.menu_architecture', [], null, $adminLang) }}</a>
                </li>
                <li class="active">
                    <strong data-i18n="admin.menu_seoarchitecture">{{ trans('messages.menu_seoarchitecture', [], null, $adminLang) }}</strong>
                </li>
            </ol>
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-heading">
                        <div class="ibox-title">
                            <h5>{{ trans('messages.architecture_no', ['no' => $architecture->article], null, $adminLang) }}</h5>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="seoArchitectureForm" action="/admin/seo/architecture/{{ $architecture->id }}" class="form-horizontal" method="POST">
                            @foreach($architecture_images->chunk(3) as $chunk)
                                <br/>
                                <div class="row">
                                    <div class="col-lg-6 col-lg-offset-3">
                                        @foreach($chunk as $key => $image)
                                            <div class="col-lg-4">
                                                <input data-i18n="[placeholder]admin.title" class="form-control" type="text"
                                                       name="images[{{ $key }}][title]"  value="{{ $image->alt }}" style="margin-bottom: 5px"
                                                       placeholder="{{ trans('messages.label_title', [], null, $adminLang) }}">
                                                <input type="hidden" name="images[{{$key}}][id]" value="{{$image->id}}">
                                                <img class="img-responsive" src="{{asset($image->path.'thumbnails/'.$image->name)}}" alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="col-lg-6 col-lg-offset-3">
                                    <label data-i18n="admin.description" for="">{{ trans('messages.label_description', [], null, $adminLang) }}:</label>
                                    <textarea class="form-control" name="description" id="" cols="30" rows="10">{{ $architecture->description }}</textarea>
                                </div>
                            </div>
                            <div class="row text-center">
                                <br>
                                <input data-i18n="[value]admin.save" id="seoArchitectureFormSubmit" class="btn btn-primary"
                                       type="button" value="{{ trans('messages.label_save', [], null, $adminLang) }}">
                            </div>
                            {{ method_field('PUT') }}
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection