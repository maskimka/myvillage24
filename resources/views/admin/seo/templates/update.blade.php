@extends('layouts.admin.app')

@section('title', trans('messages.seo_config', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.seo_config">{{ trans('messages.seo_config', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('seo_template_edit_backend', $namespace, $template->id, $adminLang) !!}
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox-content">
                    <div class="col-lg-offset-2 col-lg-8">
                        <form id="seoTemplateForm" action="/admin/seo/{{$template->id}}" class="form-horizontal" method="POST">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label data-i18n="admin.title">{{ trans('messages.label_title', [], null, $adminLang) }}</label>
                                        <input data-i18n="[placeholder]admin.template_name" type="text" name="name" class="form-control"
                                               placeholder="{{ trans('messages.template_name', [], null, $adminLang) }}" value="{{$template->name}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <label data-i18n="admin.meta_header">{{ trans('messages.meta_header', [], null, $adminLang) }}</label>
                                        <input data-i18n="[placeholder]admin.meta_header" type="text" name="meta_title"
                                               class="form-control" value="{{$template->meta_title}}"
                                               placeholder="{{ trans('messages.meta_header', [], null, $adminLang) }}">
                                    </div>
                                    <div class="col-lg-6">
                                        <label data-i18n="admin.meta_key">{{ trans('messages.meta_key', [], null, $adminLang) }}</label>
                                        <input data-i18n="[placeholder]admin.meta_key" type="text" name="meta_key"
                                               class="form-control tagsinput" placeholder="{{ trans('messages.meta_key', [], null, $adminLang) }}"
                                               value="{{$template->meta_key}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label data-i18n="admin.meta_description">{{ trans('messages.meta_description', [], null, $adminLang) }}</label>
                                        <textarea class="form-control" name="meta_description" rows="10" style="resize: vertical">
                                            {{$template->meta_description}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <label data-i18n="admin.template_page">
                                            {{ trans('messages.template_page', [], null, $adminLang) }}
                                        </label>
                                        <select name="page_id" id="" class="chosen-select">
                                            <option data-i18n="admin.not_chosen" value="0">
                                                {{ trans('messages.not_chosen', [], null, $adminLang) }}
                                            </option>
                                            @foreach($pages as $item)
                                                <option value="{{$item->id}}"
                                                        @if(isset($template->page->id) && $item->id == $template->page->id) selected @endif>
                                                    {{$item->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-6" style="text-align: right;">
                                        <input data-i18n="[value]admin.save" id="seoTemplateFormSubmit" type="button"
                                               class="btn btn-primary" value="{{ trans('messages.label_save', [], null, $adminLang) }}">
                                    </div>
                                </div>
                            </div>
                            {{ method_field('PUT') }}
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                    <div class="col-lg-offset-2 col-lg-8">
                        <span>
                            <strong><span class="text-danger">*</span> %%Name%% </strong>- Наименование продукта
                        </span>
                    </div>
                    <div class="col-lg-offset-2 col-lg-8">
                        <span>
                            <strong><span class="text-danger">*</span> %%Price%% </strong>- Цена продукта
                        </span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
