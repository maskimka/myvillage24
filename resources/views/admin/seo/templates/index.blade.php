@extends('layouts.admin.app')

@section('title', trans('messages.seo_templates', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.seo_config">{{ trans('messages.seo_config', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('seo_templates_backend', $namespace, $adminLang) !!}
        </div>
    </div>
    {{--Content--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        <table class="table table-foo" data-sorting="true" data-filtering="true" data-paging="true">
                            <thead>
                                <tr>
                                    <th data-i18n="admin.no" width="2%">
                                        {{ trans('messages.no', [], null, $adminLang) }}
                                    </th>
                                    <th data-i18n="admin.template_name" width="10%">
                                        {{ trans('messages.template_name', [], null, $adminLang) }}
                                    </th>
                                    <th data-i18n="admin.meta_header" data-breakpoints="xs" width="15%">
                                        {{ trans('messages.meta_header', [], null, $adminLang) }}
                                    </th>
                                    <th data-i18n="admin.meta_key" data-breakpoints="xs sm" width="15%">
                                        {{ trans('messages.meta_key', [], null, $adminLang) }}
                                    </th>
                                    <th data-i18n="admin.meta_description" data-breakpoints="xs sm md" width="25%">
                                        {{ trans('messages.meta_description', [], null, $adminLang) }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($templates as $key => $template)
                                <tr data-content="{{$template->id}}">
                                    <td>{{($key + 1)}}</td>
                                    <td>{{$template->name}}</td>
                                    <td>{{$template->meta_title or '--'}}</td>
                                    <td>{{$template->meta_key or '--'}}</td>
                                    <td>{{$template->meta_description or '--'}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        if(!!window.performance && window.performance.navigation.type === 2) {

            window.location.reload();
        }
    </script>
@endsection