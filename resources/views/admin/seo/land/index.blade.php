<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/footable/footable.core.css') !!}" />
@stop

@section('title', trans('messages.menu_seoland', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.land_list">{{ trans('messages.menu_seoland', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('lands_seo_backend', $namespace, $adminLang) !!}
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        <table id="landList" class="table table-foo" data-sorting="true" data-paging="true" data-filtering="true">
                            <thead>
                            <tr data-i18n="admin.article">
                                <th data-breakpoints="xs">
                                    {{ trans('messages.label_article', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.images">
                                    {{ trans('messages.label_images', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.address">
                                    {{ trans('messages.label_address', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.price" data-breakpoints="xs">
                                    {{ trans('messages.label_price', [], null, $adminLang) }}/euro
                                </th>
                                <th data-i18n="admin.description" data-breakpoints="xs sm md" width="25%">
                                    {{ trans('messages.label_description', [], null, $adminLang) }}
                                </th>
                                <th data-i18n="admin.created" data-breakpoints="xs sm md">
                                    {{ trans('messages.label_created', [], null, $adminLang) }}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lands as $land)
                                <tr data-content="{{$land->id}}">
                                    <td>
                                        {{$land->article}}
                                    </td>
                                    <td>
                                        <div class="img-land">
                                            @if(isset($land->images[0]))
                                                <img class="img-thumbnail" src="{{asset(config('custom.LAND_IMAGES_PATH')."/thumbnails/".$land->images[0]->name)}}" alt="image">
                                            @else
                                                <img class="img-thumbnail" src="{{asset("/img/common/placeholder.jpg")}}" alt="image">
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        {{ ($land->country)?$land->country:'' }}
                                        {{ ($land->locality)?trans('messages.label_city_short', [], null, $adminLang):'' }}
                                        {{ ($land->locality)?$land->locality:'' }}
                                    </td>
                                    <td>
                                        <span class="btn btn-outline btn-primary">
                                            {{$land->price}}
                                        </span>
                                    </td>
                                    <td>
                                        {{$land->description}}
                                    </td>
                                    <td>
                                        {{$land->created_at}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        if(!!window.performance && window.performance.navigation.type === 2)
        {
            window.location.reload();
        }
    </script>
@endsection