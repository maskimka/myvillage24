<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('title', trans('messages.menu_seobuilding', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.building_list">{{ trans('messages.menu_seobuilding', [], null, $adminLang) }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a data-i18n="admin.menu_dashboard" href="/{{$namespace}}">{{ trans('messages.menu_dashboard', [], null, $adminLang) }}</a>
                </li>
                <li>
                    <a data-i18n="admin.menu_buildings" href="/{{$namespace}}/seo/building">{{ trans('messages.menu_buildings', [], null, $adminLang) }}</a>
                </li>
                <li class="active">
                    <strong data-i18n="admin.menu_seobuilding">{{ trans('messages.menu_seobuilding', [], null, $adminLang) }}</strong>
                </li>
            </ol>
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-heading">
                        <div class="ibox-title">
                            <h5 data-i18n="admin.building_no" data-i18n-options={"no":"{{$building->article}}"}>
                                {{ trans('messages.building_no', ['no' => $building->article], null, $adminLang) }}
                            </h5>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="seoBuildingForm" action="/admin/seo/building/{{$building->id}}" class="form-horizontal" method="POST">
                            @foreach($building_images->chunk(3) as $chunk)
                                <br/>
                                <div class="row">
                                    <div class="col-lg-6 col-lg-offset-3">
                                        @foreach($chunk as $key => $image)
                                            <div class="col-lg-4">
                                                <input data-i18n="[placeholder]admin.title" class="form-control" type="text"
                                                       name="images[{{$key}}][title]" value="{{$image->alt}}" style="margin-bottom: 5px"
                                                       placeholder="{{ trans('messages.label_title', [], null, $adminLang) }}">
                                                <input type="hidden" name="images[{{$key}}][id]" value="{{$image->id}}">
                                                <img class="img-responsive" src="{{asset($image->path.'thumbnails/'.$image->name)}}" alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="col-lg-6 col-lg-offset-3">
                                    <label data-i18n="admin.description" for="">{{ trans('messages.label_description', [], null, $adminLang) }}:</label>
                                    <textarea class="form-control" name="description" id="" cols="30" rows="10">{{$building->description}}</textarea>
                                </div>
                            </div>
                            <div class="row text-center">
                                <br>
                                <input data-i18n="[value]admin.save" id="seoBuildingFormSubmit" type="button"
                                       class="btn btn-primary" value="{{ trans('messages.label_save', [], null, $adminLang) }}">
                            </div>
                            {{ method_field('PUT') }}
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection