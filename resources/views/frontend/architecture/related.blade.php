@extends('layouts.frontend.app')

@section('title', 'Проекты')

<meta property="og:title"              content="Строительство домов и коттеджей" />
<meta property="og:description"        content='Выберите участок и архитектурный проект из каталога My Village и уже через 12 месяцев вы заселитесь в готовый дом!' />
<meta property="og:image"              content="{{ url('/img/tags/meta11.jpg') }}" />

@section('styles')

    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/swiper/swiper.min.css') !!}" />

    {{-- max styles --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

@endsection

@section('content')

    <div class="container">
        @include('layouts.frontend.breadcrumb')
    </div>

    <div class="b-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="sidebar">
                        <architecture-filter
                                ref="architecturefilter"
                                price-from="{{ round($architectureInitParams['priceRange']['min']) }}"
                                price-to="{{ round($architectureInitParams['priceRange']['max']) }}"
                                area-from="{{ round($architectureInitParams['areaRange']['min']) }}"
                                area-to="{{ round($architectureInitParams['areaRange']['max']) }}">
                        </architecture-filter>
                    </div>
                </div>
                <div class="col-sm-9">
                    <architecture-list
                            ref="architecturelist"
                            related_to="{{ $related_to }}">
                    </architecture-list>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    {{-- max --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
@endsection
