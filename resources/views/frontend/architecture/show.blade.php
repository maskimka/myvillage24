@extends('layouts.frontend.app')

@section('title', 'Архитектурный проект')

@section('meta')
    @parent
    @if ($item->id == 27)
        <meta property="og:title"              content="Архитектурный проект AP-27" />
        <meta property="og:description"        content="Проект 2-этажного коттеджа в современном стиле площадью 211.40 м² с 4 жилыми комнатами и гаражом." />
        <meta property="og:image"              content="{{ url('/img/tags/ap-27.jpg') }}" />
    @endif
@endsection

@section('styles')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">

@endsection

@section('content')

    <div class="container">
        {!! Breadcrumbs::render('project', $item) !!}
    </div>

    <div class="b-object">
        <div class="container">
            {{-- <p class="b-object-title"><span class="text-uppercase">{{ $item->title }}</span> <span class="u-text-color--gray-light">{{ $item->general_area }} m<sup>2</sup></span></p> --}}
            <div class="row">
                <div class="col-sm-7">
                    <div class="b-object-gallery">
                        <div class="b-object-gallery__main b-object-carousel">

                            <a href="{{ url($item->images[0]->path.'/original/'.$item->images[0]->name) }}" class="b-object-carousel__slide-img">
                                <img src="{{ url($item->images[0]->path.'/original/'.$item->images[0]->name) }}" alt="" class="img-responsive b-object-gallery__main-img">
                            </a>
                        </div>
                        <div class="js-product-carousel b-object-carousel">
                            @foreach($item->images as $photo)
                                <div class="b-object-carousel__slide">
                                    @include('frontend.partials._image-card', [
                                    'original' => $photo->path.'/original/'.$photo->name,
                                    'thumb' => $photo->path.'/thumbnails/'.$photo->name,
                                    'alt' => $photo->alt
                                    ])
                                </div>
                            @endforeach
                        </div>
                    </div>

                    @if($item->description)
                        <p>{{ $item->description }}</p>
                        <br>
                        {{-- <div class="row">
                            <div class="col-sm-9 col-md-7">
                                <div class="b-section-heading">
                                    <p class="b-section-heading__title u-text-color--green-darken">
                                        @lang('messages.label_description')
                                    </p>
                                </div>
                            </div>
                        </div> --}}
                    @endif

                    @foreach($item->floors as $key => $floor)
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="b-benefits">
                                    <p class="b-benefits__title">{{ trans('messages.label_levels_plan', ['number' => ($key + 1) ]) }}</p>
                                </div>

                                <ul class="b-card-desc__list">
                                    <li class="b-grid b-grid-space-between">
                                        <span class="b-card-desc__list-key">@lang('messages.label_general_area')</span>
                                        <span>{{ $floor->area }} m<sup>2</sup></span>
                                    </li>
                                    @foreach($floor->house_zone as $i => $zone)
                                        <li class="b-grid b-grid-space-between">
                                            <span class="b-card-desc__list-key">@lang('messages.'.$zone->slug)</span>
                                            <span>{{ $zone['original']['pivot_house_zone_value'] }} m<sup>2</sup></span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <div class="js-image-cards-gallery-group">
                                    @include('frontend.partials._image-card', [
                                    'original' => $floor['image_original'],
                                    'thumb' => $floor['image_thumbnail'],
                                    ])
                                </div>
                            </div>
                        </div>
                        <br>
                    @endforeach

                </div>
                <div class="col-sm-5">

                    {{-- <div class="b-section-heading">
                        <p class="b-section-heading__title">
                            @lang('messages.label_project_complectation'):
                        </p>
                    </div>

                    <ul class="b-benefits-list b-grid b-grid-2 b-grid-wrap u-text-color--gray">
                        @foreach($item->aac_kit as $aac_kit)
                            <li class="b-benefits-list-item b-grid-item"><i class="fa fa-check b-benefits-list-item__icon"></i>@lang('messages.'.$aac_kit->slug)</li>
                        @endforeach
                    </ul>
                    <br> --}}
                    <p class="b-benefits__title">{{ $item->title }}</p>
                     <div class="b-object-price__wrapper line">
                        <div class="b-object-price__item">
                            <p class="b-object-title">{{ $item->general_area }} m<sup>2</sup></p>
                            @if($item->building_price > 0)
                                <div>
                                    <span class="b-object-price__value">
                                        {{ number_format($prices['arch_gray_price']->value * $item->general_area, 0, '', ' ') }} €
                                    </span>
                                    -
                                    <span>
                                        @lang('messages.label_grey_version'):
                                        <span id="priceBuildingPopover" data-toggle="popover" data-html="true" >
                                            <img src="/img/common/icons/info.png" alt="info">
                                        </span>
                                    </span>
                                </div>
                            @endif
                            @if($item->key_price > 0)
                                <div>
                                    <span class="b-object-price__value">
                                        {{ number_format($prices['arch_gray_facade_price']->value * $item->general_area, 0, '', ' ') }} €
                                    </span>
                                    -
                                    <span>
                                        @lang('messages.label_grey_facade_version'):
                                        <span id="priceBuildingFacadePopover" data-toggle="popover" data-html="true">
                                            <img src="/img/common/icons/info.png" alt="info">
                                        </span>
                                    </span>
                                </div>
                            @endif
                        </div>
                        <div class="b-object-price__item">
                            <a class="btn-favorite {{ ($item->is_favorite)?'active':'' }}" id="favorite" data-id="{{ $item->id }}" data-type="Architecture" title="@lang('messages.label_favorite')">
                                {{-- @lang('messages.label_favorite') --}}
                                <i class="fa fa-bookmark icon icon-on" aria-hidden="true"></i>
                                <i class="fa fa-bookmark-o icon icon-off" aria-hidden="true"></i>
                            </a>
                            <a type="button" class="btn-share" data-container="body" data-toggle="popover" data-placement="top" data-trigger="click" title="@lang('messages.label_share')">
                                <i class="fa fa-share-alt icon" aria-hidden="true"></i>
                                {{-- @lang('messages.label_share') --}}
                            </a>
                        </div>
                    </div>
                    {{-- <p class="b-object-title">{{ $item->general_area }} m<sup>2</sup></p> --}}
                    <div class="b-object-contacts">
                        <div class="b-object-contacts__item phone"><i class="icon fa fa-mobile" aria-hidden="true"></i><span>+373</span> 79 00 1000</div>
                        <div class="b-object-contacts__item">
                            <button role="button" class="btn b-btn b-btn--color-green b-btn--size-lg b-btn--wide b-btn--rounded-light" data-toggle="modal" data-target="#ordersForm" id="{{ $item->id }}" value="{{ '/'.config('app.locale').'/architecture/order'}}">
                                @lang('messages.label_send_request')
                            </button>
                        </div>
                    </div>
                    <p class="b-card-desc__title">
                        @lang('pages.architecture_info'):
                    </p>
                    <ul class="b-card-desc__list">
                        {{-- @if($item->building_price > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">
                                    <b>@lang('messages.label_grey_version'):</b>
                                    <b id="priceBuildingPopover" data-toggle="popover" data-html="true" >
                                        <img src="/img/common/icons/info.png" alt="info">
                                    </b>
                                </span>
                                <span class="u-text-color--green">
                                    <strong>{{ number_format($prices['arch_gray_price']->value * $item->general_area, 0, '', ' ') }} €</strong>
                                </span>
                            </li>
                        @endif
                        @if($item->key_price > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">
                                    <b>@lang('messages.label_grey_facade_version'):</b>
                                    <b id="priceBuildingFacadePopover" data-toggle="popover" data-html="true">
                                        <img src="/img/common/icons/info.png" alt="info">
                                    </b>
                                </span>
                                <span class="u-text-color--green-darken">
                                    <strong>{{ number_format($prices['arch_gray_facade_price']->value * $item->general_area, 0, '', ' ') }} €</strong>
                                </span>
                            </li>
                        @endif --}}
                        @if($item->first_payment > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">
                                    <b>@lang('messages.label_grey_facade_version'):</b>
                                </span>
                                <span class="u-text-color--green-darken">
                                    <strong>{{ $item->first_payment}} €</strong>
                                </span>
                            </li>
                        @endif
                        @if($item->article)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">ID</span>
                                <span>{{ $item->article }}</span>
                            </li>
                        @endif
                        @if($item->general_area)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_general_area')</span>
                                <span>{{ $item->general_area }} m <sup>2</sup></span>
                            </li>
                        @endif
                        @if($item->living_area)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_living_area')</span>
                                <span>{{ $item->living_area }} m <sup>2</sup></span>
                            </li>
                        @endif
                        @if($item->floors_number)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_floors_number')</span>
                                <span>{{ $item->floors_number }}</span>
                            </li>
                        @endif
                        @if($item->bathrooms_number)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_restrooms_number')</span>
                                <span>{{ $item->bathrooms_number }}</span>
                            </li>
                        @endif
                        @if($item->rooms_number)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_rooms_number')</span>
                                <span>{{ $item->rooms_number }}</span>
                            </li>
                        @endif
                        @if($item->garages_number)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_garages_number')</span>
                                <span>{{ $item->garages_number }}</span>
                            </li>
                        @endif
                        @if($item->celling_height)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_ceilling_height')</span>
                                <span>{{ $item->celling_height }} m</span>
                            </li>
                        @endif
                        @if($item->width > 0 && $item->length > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_building_size')</span>
                                <span>{{ $item->width }} x {{ $item->length }} m</span>
                            </li>
                        @endif
                        <li class="b-grid b-grid-space-between">
                            <span class="b-card-desc__list-key">@lang('pages.construction_period')</span>
                            <span>{{ number_format($item->general_area, 0, '', ' ') }} @lang('pages.days')</span>
                        </li>
                    </ul>
                    <br>
                    {{-- <div class="b-object-order">
                        <a type="button" class="btn-favorite" data-container="body" data-toggle="popover" data-placement="top" data-trigger="click">
                            <i class="fa fa-share-square-o icon icon-on" aria-hidden="true"></i>
                            <i class="fa fa-share-square-o icon icon-off" aria-hidden="true"></i>
                            @lang('messages.label_share')
                        </a>
                        <a class="btn-favorite {{ ($item->is_favorite)?'active':'' }}" id="favorite" data-id="{{ $item->id }}" data-type="Architecture">
                            @lang('messages.label_favorite')
                            <i class="fa fa-heart icon icon-on" aria-hidden="true"></i>
                            <i class="fa fa-heart-o icon icon-off" aria-hidden="true"></i>
                        </a>
                        <button role="button" class="btn b-btn b-btn--color-green b-btn--size-lg b-btn--wide b-btn--rounded-light" data-toggle="modal" data-target="#ordersForm" id="{{ $item->id }}" value="{{ '/'.config('app.locale').'/architecture/order'}}">
                            @lang('messages.label_send_request')
                        </button>
                    </div> --}}
                    @if ($item->facadeGalleryImages->count())

                        @include('frontend.partials._image-cards-gallery', [
                            'title' => trans('messages.label_facade'),
                            'images' => $item->facadeGalleryImages
                            ])

                    @endif
                    <br>

                    @if ( !empty($item->general_plan) || !empty($item->house_section))
                        <div class="b-benefits">
                            <p class="b-benefits__title">@lang('messages.label_general_plan_and_section')</p>
                        </div>
                        <div class="b-object-thumbs__list js-image-cards-gallery">
                            <div class="row">
                                <div class="col-sm-6">
                                    @include('frontend.partials._image-card', [
                                        'original' => config('custom.ARCHITECTURE_IMAGES_PATH').'original/'.$item->general_plan,
                                        'thumb' => config('custom.ARCHITECTURE_IMAGES_PATH').'original/'.$item->general_plan
                                        ])
                                </div>
                                <div class="col-sm-6">
                                    @include('frontend.partials._image-card', [
                                        'original' => config('custom.ARCHITECTURE_IMAGES_PATH').'original/'.$item->house_section,
                                        'thumb' => config('custom.ARCHITECTURE_IMAGES_PATH').'original/'.$item->house_section
                                        ])
                                </div>
                            </div>
                        </div>
                    @endif
                    <br>

                </div>
            </div>

            @if ($item->interiorGalleryImages->count())

                @include('frontend.partials._image-cards-design', [
                    'title' => trans('messages.catalog_interior_design'),
                    'images' => $item->interiorGalleryImages
                    ])
            @endif
            <p class="b-object__source">
                @lang('pages.object_source')
            </p>

            {{-- <div>
                <a class="b-btn btn-favorite b-btn--color-green b-btn--size-lg b-btn--wide btn-favorite-green {{ ($item->is_favorite)?'active':'' }}" data-id="{{ $item->id }}" data-type="Architecture">
                    <i class="fa fa-heart icon icon-on" aria-hidden="true"></i>
                    <i class="fa fa-heart-o icon icon-off" aria-hidden="true"></i>
                    @lang('messages.label_add_favorite')
                </a>
                <button role="button" class="btn b-bt
                n b-btn--color-green b-btn--size-lg b-btn--wide b-btn--rounded-light" data-toggle="modal" data-target="#ordersForm" id="{{ $item->id }}" value="{{ '/'.config('app.locale').'/architecture/order'}}">
                    @lang('messages.label_send_request')
                </button>
            </div>
            <br> --}}

            {{-- @if ($item->facadeGalleryImages->count())

                @include('frontend.partials._image-cards-gallery', [
                    'title' => trans('messages.label_facade'),
                    'images' => $item->facadeGalleryImages
                    ])

            @endif
            <br>
            @if ($item->interiorGalleryImages->count())

                @include('frontend.partials._image-cards-gallery', [
                    'title' => trans('messages.catalog_interior_design'),
                    'images' => $item->interiorGalleryImages
                    ])
            @endif
            <p class="b-object__source">
                @lang('pages.object_source')
            </p> --}}
        </div>
        <br>
        <br>
        <div class="l-inner-section u-bg-color--gray-lighter">
            <div class="container">
                <land-list
                        ref="relatedLands"
                        :limit="4"
                        title="@lang('messages.label_related_lands')"
                        :related_to={{ $item->id }}
                        class_objects="related_objects"
                ></land-list>
            </div>
        </div>
    </div>

    @include('layouts.frontend.modal.order_modal')
    @include('layouts.frontend.modal.modal_land_map')
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="{!! asset('js/plugins/validate/jquery.validate.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/validate/additional-methods.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/clipboard/clipboard.min.js') !!}" type="text/javascript"></script>

@endsection
