@extends('layouts.frontend.app')

@section('title', 'Проекты')
@section('meta')
    <meta property="og:title"              content="Строительство домов и коттеджей">
    <meta property="og:description"        content="Выберите участок и архитектурный проект из каталога My Village и уже через 12 месяцев вы заселитесь в готовый дом!">
    <meta property="og:image"              content="{{ url('/img/tags/meta11.jpg') }}">
@endsection

@section('styles')

    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/swiper/swiper.min.css') !!}" />

    {{-- max styles --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

@endsection

@section('content')

    {{-- <div class="b-nav-pills__objects">
        <div class="container">
            <ul class="nav b-grid b-grid-3 b-nav-pills">
                <li class="b-grid-item {{ Route::is('lands') ? 'active' : '' }}">
                    <a href="{{ url('lands') }}">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/street-map.png') }}" alt=""></i>
                        @lang('messages.menu_lands_f') | {{ $landCount }}
                    </a>
                </li>
                <li class="b-grid-item {{ Route::is('projects') ? 'active' : '' }}">
                    <a href="{{ url('architecture') }}">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/blueprint.png') }}" alt=""></i>
                        @lang('messages.menu_projects_f') | {{ $architectureCount }}
                    </a>
                </li>
                <li class="b-grid-item {{ Route::is('buildings') ? 'active' : '' }}">
                    <a href="{{ url('buildings') }}">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/construction.png') }}" alt=""></i>
                        @lang('messages.menu_buildings_f') | {{ $buildingCount }}
                    </a>
                </li>
            </ul>
        </div>
    </div> --}}

    {{-- <div class="container">
        {!! Breadcrumbs::render('projects') !!}
    </div> --}}

    {{--<div class="container">
        <div class="embed-responsive embed-responsive-21by9">
            <div class="embed-responsive-item embed-bg-img" style="background-image: url({{ url('/img/objects/main-projects.png') }});"></div>
        </div>
    </div>--}}

    <div class="b-view-header">
        <div class="b-view-header__filter">
            <a class="btn btn-primary b-filter-collapse__btn" role="button" data-toggle="collapse" href="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <i class="fa fa-filter b-filter-collapse__icon" aria-hidden="true"></i>Фильтры
                <span class="caret"></span>
            </a>
            <div class="collapse b-filter-collapse__content" id="collapseFilter">
                <div class="b-filter-collapse__inner">
                    <architecture-filter
                        ref="architecturefilter"
                        building-type="{{ ceil($architectureInitParams['buildingType']) }}"
                        floors-from="{{ ceil($architectureInitParams['floorsRange']['min']) }}"
                        floors-to="{{ ceil($architectureInitParams['floorsRange']['max']) }}"
                        rooms-from="{{ ceil($architectureInitParams['roomsRange']['min']) }}"
                        rooms-to="{{ ceil($architectureInitParams['roomsRange']['max']) }}"
                        price-from="{{ ceil($architectureInitParams['priceRange']['min']) }}"
                        price-to="{{ ceil($architectureInitParams['priceRange']['max']) }}"
                        price-max="{{ ceil($architectureInitParams['priceRange']['maximalPrice']) }}"
                        price-min="{{ ceil($architectureInitParams['priceRange']['minimalPrice']) }}"
                        area-from="{{ ceil($architectureInitParams['areaRange']['min']) }}"
                        area-to="{{ ceil($architectureInitParams['areaRange']['max']) }}"
                        area-max="{{ ceil($architectureInitParams['areaRange']['maximalArea']) }}"
                        area-min="{{ ceil($architectureInitParams['areaRange']['minimalArea']) }}">
                     </architecture-filter>
                </div>
            </div>
        </div>
        {{-- <div class="b-view-header__map">
            <div class="b-map-checkbox">
                <input type="checkbox" name="checkbox-cats[]" id="view-change" value="1" checked>
                <label for="view-change">Карта</label>
            </div>
        </div> --}}
    </div>

    <div class="b-view-inner">
        <div class="container">
            <architecture-list
                ref="architecturelist"
                sort-prop="{{ $architectureInitParams['sortBy'] or '' }}"
                :limit-prop={{ $architectureInitParams['limit'] }}
                :offset={{ ceil($architectureInitParams['page']) }}
                :page={{ ceil($architectureInitParams['page']) }}
            >
            </architecture-list>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
@endsection
