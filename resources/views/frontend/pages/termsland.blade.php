@extends('layouts.frontend.app')

@section('title', 'Регламент добавления земельного участка')

@section('content')
    <div class="b-terms-main">
        <div class="b-terms-content">
            <div class="container">
                <h2 class="b-terms-title">@lang('pages.termsland_title')</h2>
                <ul class="b-terms-list">
                    <li class="text">@lang('pages.termsland_list_c1')</li>
                    <li><img src="{{ url('img/pages/terms-arrow.png') }}" alt="" class="b-terms-list__arrow"></li>
                    <li class="text">@lang('pages.termsland_list_c2')</li>
                    <li><img src="{{ url('img/pages/terms-arrow.png') }}" alt="" class="b-terms-list__arrow"></li>
                    <li class="text">@lang('pages.termsland_list_c3')</li>
                </ul>
                <a href="/auth/registration" class="b-terms-btn">@lang('pages.terms_start')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="b-terms-section">
        <div class="container">
            <h3 class="b-terms-section__title">@lang('pages.termsland_section_title')</h3>
            <div class="b-media-wrapper">
                <div class="row">
                    <div class="col-md-6 col-md-push-5">
                        <img src="{{ url('img/pages/terms-1.jpg') }}" alt="" class="b-media-img">
                    </div>
                    <div class="col-md-5 col-md-pull-6">
                        <div class="media b-media">
                            <div class="media-left b-media-count">
                                1
                            </div>
                            <div class="media-body b-media-body">
                                <h4 class="media-heading b-media-title">@lang('pages.termsland_section_1_title')</h4>
                                <p class="b-media-text">@lang('pages.termsland_section_1_content')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="b-media-wrapper">
                <div class="row">
                    <div class="col-md-5">
                        <img src="{{ url('img/pages/terms-2.jpg') }}" alt="" class="b-media-img">
                    </div>
                    <div class="col-md-5">
                        <div class="media b-media">
                            <div class="media-left b-media-count">
                                2
                            </div>
                            <div class="media-body b-media-body">
                                <h4 class="media-heading b-media-title">@lang('pages.termsland_section_2_title')</h4>
                                <p class="b-media-text">@lang('pages.termsland_section_2_content')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="b-media-wrapper">
                <div class="row">
                    <div class="col-md-6 col-md-push-5">
                        <img src="{{ url('img/pages/terms-3.jpg') }}" alt="" class="b-media-img">
                    </div>
                    <div class="col-md-5 col-md-pull-6">
                        <div class="media b-media">
                            <div class="media-left b-media-count">
                                3
                            </div>
                            <div class="media-body b-media-body">
                                <h4 class="media-heading b-media-title">@lang('pages.termsland_section_3_title')</h4>
                                <p class="b-media-text">@lang('pages.termsland_section_3_content')</p>
                                <br>
                                <br>
                                <p><strong>@lang('pages.termsland_section_read') <a href="{{ url('uploads/presentation/Rezerv_'. Lang::getLocale() .'.pdf') }}">@lang('messages.button_download')</a></strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="b-terms-reg">
        <div class="container">
            <p class="b-terms-reg__title">@lang('pages.add_land')</p>
            <a href="/auth/registration" class="b-terms-btn">@lang('pages.terms_start')</a>
        </div>
    </div>

@endsection
