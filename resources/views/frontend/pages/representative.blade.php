@extends('layouts.frontend.app')

@section('title', 'Строительная компания')

@section('meta')
    @parent
    <meta property="og:title"              content="" />
    <meta property="og:description"        content="" />
    <meta property="og:image"              content="{{ url('/img/tags/partners1.jpg') }}" />
@endsection

@section('content')
@php
    $locale = App::getLocale();
@endphp
    <div class="l-inner-page">
        <div class="container">
            <div class="text-center">
                <div class="b-page-header">
                    <h1 class="b-page-header__title">
                        @lang('pages.construct_title')
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <img src="{{ url('img/partners/i4.jpg') }}" class="img-responsive">
                </div>
                <div class="col-sm-6">
                    <div class="b-partner-info">
                        <div class="b-partner-info__title">
                            @lang('pages.representative_title')
                        </div>
                        <div class="b-partner-info__content">
                            <p>
                                @lang('pages.representative_subtitle')
                            </p>
                            <ul class="u-check-list">
                                <li>
                                    @lang('pages.representative_text1')
                                </li>
                                <li>
                                    @lang('pages.representative_text2')
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="b-partner-info">
                @if($locale == 'ru')
                    <p class="b-partner-info__title">
                        Начните с нами работать уже сейчас
                    </p>
                    <ul class="u-check-list">
                        <li>
                            <strong> Регистрация</strong>
                            <br>
                            Заполните ваши данные на портале MyVillage24.
                        </li>
                        <li>
                            <strong> Подтверждение</strong>
                            <br>
                            С вами свяжется представитель  MyVillage24.
                        </li>
                        <li>
                            <strong> Договор</strong>
                            <br>
                            После заключения договора к вам поступают заявки от клиентов в вашем регионе. 
                        </li>
                    </ul>
                @endif
                @if($locale == 'en')
                    <p class="b-partner-info__title">
                        Start working with us now
                    </p>
                    <ul class="u-check-list">
                        <li>
                            <strong> Sign up</strong>
                            <br>
                            Fill out your details on the portal MyVillage24.
                        </li>
                        <li>
                            <strong> Verify</strong>
                            <br>
                            A representative of MyVillage24 will contact you.
                        </li>
                        <li>
                            <strong> Contract</strong>
                            <br>
                            After the conclusion of the contract, you receive applications from customers in your area.
                        </li>
                    </ul>
                @endif
                @if($locale == 'ro')
                    <p class="b-partner-info__title">
                        Începeți să lucrați cu noi acum
                    </p>
                    <ul class="u-check-list">
                        <li>
                            <strong> Înscrieți-vă</strong>
                            <br>
                            Completați detaliile dvs. pe portalul MyVillage24.
                        </li>
                        <li>
                            <strong> Verificați</strong>
                            <br>
                            Un reprezentant al MyVillage24 vă va contacta.
                        </li>
                        <li>
                            <strong> Contract</strong>
                            <br>
                            După încheierea contractului, primiți cereri de la clienți din zona dvs.
                        </li>
                    </ul>
                @endif
                @if($locale == 'es')
                    <p class="b-partner-info__title">
                        Comience a trabajar con nosotros ahora
                    </p>
                    <ul class="u-check-list">
                        <li>
                            <strong> Registrarse</strong>
                            <br>
                            Complete sus datos en el portal MyVillage24.
                        </li>
                        <li>
                            <strong> Verificar</strong>
                            <br>
                            Un representante de MyVillage24 se pondrá en contacto con usted.
                        </li>
                        <li>
                            <strong> Contrato</strong>
                            <br>
                            Después de la conclusión del contrato, recibe solicitudes de clientes en su área.
                        </li>
                    </ul>
                @endif
                @if($locale == 'de')
                    <p class="b-partner-info__title">
                        Fangen Sie jetzt an, mit uns zu arbeiten
                    </p>
                    <ul class="u-check-list">
                        <li>
                            <strong> Registrieren</strong>
                            <br>
                            Tragen Sie Ihre Angaben auf dem Portal MyVillage24 ein.
                        </li>
                        <li>
                            <strong> Überprüfen</strong>
                            <br>
                            Ein Vertreter von MyVillage24 wird sich mit Ihnen in Verbindung setzen.
                        </li>
                        <li>
                            <strong> Vertrag</strong>
                            <br>
                            Nach Vertragsabschluss erhalten Sie Bewerbungen von Kunden in Ihrer Nähe.
                        </li>
                    </ul>
                @endif
            </div>
            <div class="text-center">
                <a href="/auth/registration" class="btn b-btn b-btn--size-wide b-btn--color-green">@lang('messages.menu_registration')</a>
            </div>
        </div>
    </div>

@endsection