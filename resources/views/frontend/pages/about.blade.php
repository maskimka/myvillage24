@extends('layouts.frontend.app')

@section('title', 'О нас')

@section('content')

    <div class="b-about">
        <div class="b-about-header">
            <img src="{{ url('img/about/about-bg.png') }}" alt="" class="b-about-header-img">
            <div class="b-about-header-title__wrapper">
                <p class="b-about-header-title">@lang('pages.about_title') <strong>MyVillage<sup>24</sup></strong></p>
            </div>
        </div>

        <div class="b-section b-about-section">
            <div class="container">

                <p class="b-about-section-text">@lang('pages.about_text1')</p>
                <p class="b-about-section-text">@lang('pages.about_text2')</p>
                <p class="b-about-section-text">@lang('pages.about_text3')</p>
                <ul class="b-about-list">
                    <li>
                        <img src="{{ url('img/about/about-map.png') }}" class="b-about-list-img">
                        <p class="b-about-list-text">
                            @lang('pages.about_lands')
                            <span>500+</span>
                        </p>
                    </li>
                    <li>
                        <img src="{{ url('img/about/about-print.png') }}" class="b-about-list-img">
                        <p class="b-about-list-text">
                            @lang('pages.about_projects')
                            <span>1000+</span>
                        </p>
                    </li>
                    <li>
                        <img src="{{ url('img/about/about-world.png') }}" class="b-about-list-img">
                        <p class="b-about-list-text">
                            @lang('pages.about_countries')
                            <span>8+</span>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="b-section-bg b-about-partner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="b-about-partner-header">
                            <h1 class="b-about-partner-header__title">
                                @lang('pages.partners_title')
                            </h1>
                            <div class="b-about-partner-header__subtitle">
                                @lang('pages.partners_text1')
                            </div>
                        </div>
                        <div class="embed-responsive embed-responsive-16by9 b-about-partner-video">
                            <iframe src="https://www.youtube.com/embed/wxWKJaB-POk" class="embed-responsive-item"></iframe>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <p class="">
                    @lang('pages.partners_text_pdf')
                </p>
                <br>
                <a href="{{ asset('uploads/presentation/MyVillage24_'.\App::getLocale().'.pdf') }}" class="btn b-btn b-btn--size-xlg b-btn--color-green" download>@lang('messages.button_download') pdf</a>
                <br>
                <br>
            </div>
        </div>

        <div class="b-about-partner">
            <div class="container">
                <div class="b-about-partner-header">
                    <h1 class="b-about-partner-header__title text-center">
                        @lang('pages.about_cooperation_title')
                    </h1>
                    <div class="b-about-partner-header__subtitle">
                        @lang('pages.partners_text1')
                    </div>
                </div>
                <h1 class="b-page-header__title">
                    @lang('pages.partners_text_select')
                </h1>
                <br>
                <br>
                <div class="b-partners">
                    <a href="/realtor" class="b-partners__link">
                        <div class="b-partners__img">
                            <img src="{{ url('img/about/realtor-icon.png') }}" alt="">
                        </div>
                        <p>@lang('pages.partners_realtor')</p>
                    </a>
                    <a href="/architect" class="b-partners__link">
                        <div class="b-partners__img">
                            <img src="{{ url('img/about/architecture-icon.png') }}" alt="">
                        </div>
                        <p>@lang('pages.partners_architect')</p>
                    </a>
                    <a href="/representative" class="b-partners__link">
                        <div class="b-partners__img">
                            <img src="{{ url('img/about/representative-icon.png') }}" alt="">
                        </div>
                        <p>@lang('pages.partners_representative')</p>
                    </a>
                    <a href="/construction" class="b-partners__link">
                        <div class="b-partners__img">
                            <img src="{{ url('img/about/construction-icon.png') }}" alt="">
                        </div>
                        <p>@lang('pages.partners_construction_company')</p>
                    </a>
                    <a href="/finance" class="b-partners__link">
                        <div class="b-partners__img">
                            <img src="{{ url('img/about/finance-icon.png') }}" alt="">
                        </div>
                        <p>@lang('pages.partners_finance_company')</p>
                    </a>
                    <a href="/modularstructures" class="b-partners__link">
                        <div class="b-partners__img">
                            <img src="{{ url('img/about/modularstructures-icon.png') }}" alt="">
                        </div>
                        <p>@lang('pages.partners_modular_homes')</p>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
