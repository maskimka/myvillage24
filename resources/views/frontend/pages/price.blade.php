@extends('layouts.frontend.app')

@section('title', 'Регламент добавления земельного участка')

@section('content')

    <div class="container">
        @include('layouts.frontend.breadcrumb')
    </div>

    <div class="b-prices">
        <div class="container">
            <div class="text-center">
                <div class="b-page-header">
                    <h1 class="b-page-header__title">Выберите для себя подходящий вариант дома</h1>
                    <div class="b-page-header__subtitle">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">Любое домостроительство завершается стадией накрывания крыши и отделки фасада, предлогаем Вам подобрать идеальную комбинацию этих двух основных элементов, чтобы ваш дом выглядел эстетично и привлекательно.</div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs b-prices-nav" role="tablist">
                <li role="presentation" class="active"><a href="#prices-1" aria-controls="prices-1" role="tab" data-toggle="tab">В стоимость серого варианта входит</a></li>
                <li role="presentation"><a href="#prices-2" aria-controls="prices-2" role="tab" data-toggle="tab">В стоимость серого варианта с фасадом входит</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content b-prices-tab-content">
                <div role="tabpanel" class="tab-pane active" id="prices-1">
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="b-prices__title">@lang('pages.prices_subtitle1')</p>
                            <ul class="u-check-list">
                                <li>@lang('pages.prices_text1')</li>
                                <li>@lang('pages.prices_text2')</li>
                                <li>@lang('pages.prices_text3')</li>
                            </ul>
                            <p class="b-prices__title">@lang('pages.prices_subtitle2')</p>
                            <ul class="u-check-list">
                                <li>@lang('pages.prices_text4')</li>
                                <li>@lang('pages.prices_text5')</li>
                                <li>@lang('pages.prices_text6')</li>
                                <li>@lang('pages.prices_text7')</li>
                                <li>@lang('pages.prices_text8')</li>
                                <li>@lang('pages.prices_text9')</li>
                                <li>@lang('pages.prices_text10')</li>
                            </ul>
                            <p class="b-prices__title">@lang('pages.prices_subtitle3')</p>
                            <ul class="u-check-list">
                                <li>@lang('pages.prices_text11')</li>
                                <li>@lang('pages.prices_text12')</li>
                            </ul>
                            <p class="b-prices__title">@lang('pages.prices_subtitle4')</p>
                            <ul class="u-check-list">
                                <li>@lang('pages.prices_text13')</li>
                                <li>@lang('pages.prices_text14')</li>
                                <li>@lang('pages.prices_text15')</li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <img src="{{ url('img/pages/prices-img.jpg') }}" alt="" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="prices-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="b-prices__title">@lang('pages.prices_subtitle1')</p>
                            <ul class="u-check-list">
                                <li>@lang('pages.prices_text1')</li>
                                <li>@lang('pages.prices_text2')</li>
                                <li>@lang('pages.prices_text3')</li>
                            </ul>
                            <p class="b-prices__title">@lang('pages.prices_subtitle2')</p>
                            <ul class="u-check-list">
                                <li>@lang('pages.prices_text4')</li>
                                <li>@lang('pages.prices_text5')</li>
                                <li>@lang('pages.prices_text6')</li>
                                <li>@lang('pages.prices_text7')</li>
                                <li>@lang('pages.prices_text8')</li>
                                <li>@lang('pages.prices_text9')</li>
                                <li>@lang('pages.prices_text10')</li>
                            </ul>
                            <p class="b-prices__title">@lang('pages.prices_subtitle3')</p>
                            <ul class="u-check-list">
                                <li>@lang('pages.prices_text11')</li>
                                <li>@lang('pages.prices_text12')</li>
                            </ul>
                            <p class="b-prices__title">@lang('pages.prices_subtitle4')</p>
                            <ul class="u-check-list">
                                <li>@lang('pages.prices_text13')</li>
                                <li>@lang('pages.prices_text14')</li>
                                <li>@lang('pages.prices_text15')</li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <img src="{{ url('img/pages/prices-img-2.jpg') }}" alt="" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection