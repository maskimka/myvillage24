@extends('layouts.frontend.app')

@section('title', 'Ипотека')

@section('meta')
    @parent

    <meta property="og:title"              content="С My Village начни строить уже сейчас" />
    <meta property="og:description"        content="Компания My Village предоставляет выгодные условия ипотечного кредитования и удобный график выплат." />
    <meta property="og:image"              content="{{ url('/img/tags/meta999.jpg') }}" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:locale:alternate" content="ro_RO" />

@endsection

@section('content')

    <div class="l-inner-page">

        <div class="container">
            <div class="page-hypothec">
                <div class="page-hypothec-header">
                    <div class="row text-center">
                        <div class="col-lg-12">
                            <h2>@lang('pages.hypothec_title')</h2>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-lg-8 col-lg-offset-2">
                            <p>
                                @lang('pages.hypothec_subtitle')
                            </p>
                        </div>
                    </div>
                </div>
                <br>
                <div class="page-hypothec-content">
                    <div class="row">
                        <div class="col-lg-5 ">
                            <img class="img-responsive" src="/img/pages/hypothec1.jpg" alt="image">
                        </div>
                        <br>
                        <div class="col-lg-7">
                            <h3>
                                @lang('pages.hypothec_subtitle1')
                            </h3>
                            <p>
                                @lang('pages.hypothec_text1')
                            </p>
                        </div>
                    </div>

                    <br>
                    <br>

                    {{--<div class="row">--}}
                        {{--<div class="col-lg-5 ">--}}
                            {{--<img class="img-responsive" src="/img/pages/hypothec2.jpg" alt="image">--}}
                        {{--</div>--}}
                        {{--<br>--}}
                        {{--<div class="col-lg-7">--}}
                            {{--<h3>--}}
                                {{--@lang('pages.hypothec_subtitle2')--}}
                            {{--</h3>--}}
                            {{--<p>--}}
                                {{--@lang('pages.hypothec_text2')--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<br>--}}
                    {{--<br>--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-lg-5 ">--}}
                            {{--<img class="img-responsive" src="/img/pages/hypothec3.jpg" alt="image">--}}
                        {{--</div>--}}
                        {{--<br>--}}
                        {{--<div class="col-lg-7">--}}
                            {{--<h3>--}}
                                {{--@lang('pages.hypothec_subtitle3')--}}
                            {{--</h3>--}}
                            {{--<p>--}}
                                {{--@lang('pages.hypothec_text3')--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>

    </div>

@endsection
