@extends('layouts.frontend.app')

@section('title', 'Контакты')

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/sweetalert/sweetalert.css') !!}" />
@endsection

@section('content')

    <div class="l-inner-page">

        <div class="container">

            <google-map
                    id="js-objects-map"
                    class="embed-responsive embed-responsive-4by1"
                    :center-coords="[47.020751, 28.856090]"
                    show-type="offices"
                    :enable-geolocation=false>
            </google-map>

            <br>
            <br>

            <div class="row">
                <div class="col-sm-6">

                    <div class="b-contact-block">
                        <div class="b-contact-block__label">
                            @lang('pages.address'):
                        </div>
                        <div class="b-contact-block__value">
                            @lang('messages.label_city_short') @lang('pages.office_address_city'), @lang('pages.office_address_street')
                        </div>
                    </div>

                    <div class="b-contact-block">
                        <div class="b-contact-block__label">
                            @lang('messages.label_phone'):
                        </div>
                        <div class="b-contact-block__value">
                            @lang('pages.sales_department') +373 79 005 888
                        </div>
                        <div class="b-contact-block__value">
                            @lang('pages.architecture_department') +373 79 050 888
                        </div>
                    </div>

                    <div class="b-contact-block">
                        <div class="b-contact-block__label">
                            E-mail:
                        </div>
                        <div class="b-contact-block__value">
                            info@myvillage24.com
                        </div>
                    </div>

                </div>
                <div class="col-sm-6">
                    <form id="contactForm" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group b-form-group">
                                    <input type="text" name="name" class="form-control b-form-control" placeholder="@lang('messages.label_your_name')">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group b-form-group">
                                    <input type="text" name="email" class="form-control b-form-control" placeholder="E-mail">
                                </div>
                            </div>
                        </div>
                        <div class="form-group b-form-group">
                            <textarea name="message" class="form-control b-form-control" rows="4"></textarea>
                        </div>
                        <button class="btn b-btn b-btn--color-green b-btn--wide">
                            @lang('messages.button_send')
                        </button>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/sweetalert/sweetalert.min.js') !!}" type="text/javascript"></script>
@endsection