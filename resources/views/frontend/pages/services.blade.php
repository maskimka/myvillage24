@extends('layouts.frontend.app')

@section('title', 'Услуги')

@section('meta')
    @parent
    <meta property="og:title"              content="Проектное бюро My Village" />
    <meta property="og:description"        content="Специалисты нашей компании предоставляют услуги по проектированию домов." />
    <meta property="og:image"              content="{{ url('/img/tags/meta333.jpg') }}" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:locale:alternate" content="ro_RO" />
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/sweetalert/sweetalert.css') !!}" />
@endsection

@section('content')

    <div class="l-inner-page">

        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="b-page-header">
                        <h1 class="b-page-header__title">
                            @lang('pages.services')
                        </h1>
                    </div>

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel b-panel">
                            <a class="b-panel__heading" role="button" data-toggle="collapse" data-parent="#accordion" href="#architecture" aria-expanded="true" aria-controls="architecture">
                            <span class="b-panel__heading-icon-wrapper">
                                <span class="b-panel__heading-icon b-panel__heading-icon--plus">
                                    +
                                </span>
                                <span class="b-panel__heading-icon b-panel__heading-icon--minus">
                                    -
                                </span>
                            </span>
                                <span class="b-panel__heading-text">
                                @lang('pages.architecture_services_title')
                            </span>

                            </a>
                            <div id="architecture" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <p>
                                        @lang('pages.architecture_services_text1'):
                                    </p>
                                    <ul>
                                        <li>
                                            @lang('pages.architecture_services_text2')
                                        </li>
                                        <li>
                                            @lang('pages.architecture_services_text3')
                                        </li>
                                        <li>
                                            @lang('pages.architecture_services_text4')
                                        </li>
                                        <li>
                                            @lang('pages.architecture_services_text5')
                                        </li>
                                        <li>
                                            @lang('pages.architecture_services_text6')
                                        </li>
                                        <li>
                                            @lang('pages.architecture_services_text7')
                                        </li>
                                        <li>
                                            @lang('pages.architecture_services_text8')
                                        </li>
                                    </ul>
                                    <p>
                                        @lang('pages.architecture_services_text9')
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel b-panel">
                            <a class="b-panel__heading collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#constructiv" aria-expanded="false" aria-controls="collapseTwo">
                            <span class="b-panel__heading-icon-wrapper">
                                <span class="b-panel__heading-icon b-panel__heading-icon--plus">
                                    +
                                </span>
                                <span class="b-panel__heading-icon b-panel__heading-icon--minus">
                                    -
                                </span>
                            </span>
                                <span class="b-panel__heading-text">
                                @lang('pages.construction_services_title')
                            </span>
                            </a>
                            <div id="constructiv" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p>
                                        @lang('pages.construction_services_text1'):
                                    </p>
                                    <ul>
                                        <li>
                                            @lang('pages.construction_services_text2')
                                        </li>
                                        <li>
                                            @lang('pages.construction_services_text3')
                                        </li>
                                        <li>
                                            @lang('pages.construction_services_text4')
                                        </li>
                                        <li>
                                            @lang('pages.construction_services_text5')
                                        </li>
                                        <li>
                                            @lang('pages.construction_services_text6')
                                        </li>
                                        <li>
                                            @lang('pages.construction_services_text7')
                                        </li>
                                        <li>
                                            @lang('pages.construction_services_text8')
                                        </li>
                                    </ul>
                                    <p>
                                        @lang('pages.construction_services_text9')
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel b-panel">
                            <a class="b-panel__heading collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#interiorDesign" aria-expanded="false" aria-controls="collapseThree">
                            <span class="b-panel__heading-icon-wrapper">
                                <span class="b-panel__heading-icon b-panel__heading-icon--plus">
                                    +
                                </span>
                                <span class="b-panel__heading-icon b-panel__heading-icon--minus">
                                    -
                                </span>
                            </span>
                                <span class="b-panel__heading-text">
                                @lang('pages.interior_design_title')
                            </span>
                            </a>
                            <div id="interiorDesign" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        @lang('pages.interior_design_text1'):
                                    </p>
                                    <ul>
                                        <li>
                                            @lang('pages.interior_design_text2')
                                        </li>
                                        <li>
                                            @lang('pages.interior_design_text3')
                                        </li>
                                        <li>
                                            @lang('pages.interior_design_text4')
                                        </li>
                                        <li>
                                            @lang('pages.interior_design_text5')
                                        </li>
                                        <li>
                                            @lang('pages.interior_design_text6')
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel b-panel">
                            <a class="b-panel__heading collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#landscapeDesign" aria-expanded="false" aria-controls="collapseThree">
                            <span class="b-panel__heading-icon-wrapper">
                                <span class="b-panel__heading-icon b-panel__heading-icon--plus">
                                    +
                                </span>
                                <span class="b-panel__heading-icon b-panel__heading-icon--minus">
                                    -
                                </span>
                            </span>
                                <span class="b-panel__heading-text">
                                @lang('pages.landscape_design_title')
                            </span>
                            </a>
                            <div id="landscapeDesign" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        @lang('pages.landscape_design_text1')
                                    </p>
                                    <p>
                                        @lang('pages.landscape_design_text2'):
                                    </p>
                                    <ul>
                                        <li>
                                            @lang('pages.landscape_design_text3')
                                        </li>
                                        <li>
                                            @lang('pages.landscape_design_text4')
                                        </li>
                                        <li>
                                            @lang('pages.landscape_design_text5')
                                        </li>
                                        <li>
                                            @lang('pages.landscape_design_text6')
                                        </li>
                                        <li>
                                            @lang('pages.landscape_design_text7')
                                        </li>
                                        <li>
                                            @lang('pages.landscape_design_text8')
                                        </li>
                                        <li>
                                            @lang('pages.landscape_design_text9')
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel b-panel">
                            <a class="b-panel__heading collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#juridicalServices" aria-expanded="false" aria-controls="collapseThree">
                            <span class="b-panel__heading-icon-wrapper">
                                <span class="b-panel__heading-icon b-panel__heading-icon--plus">
                                    +
                                </span>
                                <span class="b-panel__heading-icon b-panel__heading-icon--minus">
                                    -
                                </span>
                            </span>
                                <span class="b-panel__heading-text">
                                @lang('pages.juridical_services_title')
                            </span>
                            </a>
                            <div id="juridicalServices" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        @lang('pages.juridical_services_text1'):
                                    </p>
                                    <ul>
                                        <li>
                                            @lang('pages.juridical_services_text2')
                                        </li>
                                        <li>
                                            @lang('pages.juridical_services_text3')
                                        </li>
                                        <li>
                                            @lang('pages.juridical_services_text4')
                                        </li>
                                        <li>
                                            @lang('pages.juridical_services_text5')
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel b-panel">
                            <a class="b-panel__heading collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#constructionServices" aria-expanded="false" aria-controls="collapseThree">
                            <span class="b-panel__heading-icon-wrapper">
                                <span class="b-panel__heading-icon b-panel__heading-icon--plus">
                                    +
                                </span>
                                <span class="b-panel__heading-icon b-panel__heading-icon--minus">
                                    -
                                </span>
                            </span>
                                <span class="b-panel__heading-text">
                                @lang('pages.building_services_title')
                            </span>
                            </a>
                            <div id="constructionServices" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        @lang('pages.building_services_text1'):
                                    </p>
                                    <ul>
                                        <li>
                                            @lang('pages.building_services_text2')
                                        </li>
                                        <li>
                                            @lang('pages.building_services_text3')
                                        </li>
                                        <li>
                                            @lang('pages.building_services_text4')
                                        </li>
                                        <li>
                                            @lang('pages.building_services_text5')
                                        </li>
                                        <li>
                                            @lang('pages.building_services_text6')
                                        </li>
                                        <li>
                                            @lang('pages.building_services_text7')
                                        </li>
                                        <li>
                                            @lang('pages.building_services_text8')
                                        </li>
                                        <li>
                                            @lang('pages.building_services_text9')
                                        </li>
                                        <li>
                                            @lang('pages.building_services_text10')
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel b-panel">
                            <a class="b-panel__heading collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#repairServices" aria-expanded="false" aria-controls="collapseThree">
                            <span class="b-panel__heading-icon-wrapper">
                                <span class="b-panel__heading-icon b-panel__heading-icon--plus">
                                    +
                                </span>
                                <span class="b-panel__heading-icon b-panel__heading-icon--minus">
                                    -
                                </span>
                            </span>
                                <span class="b-panel__heading-text">
                                @lang('pages.repair_services_title')
                            </span>
                            </a>
                            <div id="repairServices" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        @lang('pages.repair_services_text1'):
                                    </p>
                                    <ul>
                                        <li>
                                            @lang('pages.repair_services_text2')
                                        </li>
                                        <li>
                                            @lang('pages.repair_services_text3')
                                        </li>
                                        <li>
                                            @lang('pages.repair_services_text4')
                                        </li>
                                        <li>
                                            @lang('pages.repair_services_text5')
                                        </li>
                                        <li>
                                            @lang('pages.repair_services_text6')
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel b-panel">
                            <a class="b-panel__heading collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#preparationLand" aria-expanded="false" aria-controls="collapseThree">
                            <span class="b-panel__heading-icon-wrapper">
                                <span class="b-panel__heading-icon b-panel__heading-icon--plus">
                                    +
                                </span>
                                <span class="b-panel__heading-icon b-panel__heading-icon--minus">
                                    -
                                </span>
                            </span>
                                <span class="b-panel__heading-text">
                                @lang('pages.preparation_land_title')
                            </span>
                            </a>
                            <div id="preparationLand" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        @lang('pages.preparation_land_text1'):
                                    </p>
                                    <ul>
                                        <li>
                                            @lang('pages.preparation_land_text2')
                                        </li>
                                        <li>
                                            @lang('pages.preparation_land_text3')
                                        </li>
                                        <li>
                                            @lang('pages.preparation_land_text4')
                                        </li>
                                        <li>
                                            @lang('pages.preparation_land_text5')
                                        </li>
                                        <li>
                                            @lang('pages.preparation_land_text6')
                                        </li>
                                        <li>
                                            @lang('pages.preparation_land_text7')
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-md-3">
                    <div class="b-page-header">
                        <h1 class="b-page-header__title">
                            @lang('messages.label_send_question')
                        </h1>
                    </div>
                    <form id="contactForm" method="post">
                        {{ csrf_field() }}
                            <div class="form-group b-form-group">
                                <input type="text" name="name" class="form-control b-form-control" placeholder="@lang('messages.label_your_name')">
                            </div>
                            <div class="form-group b-form-group">
                                <input type="text" name="email" class="form-control b-form-control" placeholder="E-mail">
                            </div>
                        <div class="form-group b-form-group">
                            <textarea name="message" class="form-control b-form-control" rows="4"></textarea>
                        </div>
                        <button id="sendMail" class="btn b-btn b-btn--color-green b-btn--wide">
                            @lang('messages.button_send')
                        </button>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/sweetalert/sweetalert.min.js') !!}" type="text/javascript"></script>
@endsection
