@extends('layouts.frontend.app_new')

@section('title', 'Главная')

@section('meta')
    @parent

        <meta property="og:title"       content="В нашем каталоге более 800 проектов" />
        <meta property="og:description" content="При заказе строительства дома в My Village - мы подарим вам архитектурный проект!" />
        <meta property="og:image"       content="{{ url('/img/tags/meta888.jpg') }}" />
        <meta property="og:locale" content="ru_RU" />
        <meta property="og:locale:alternate" content="ro_RO" />

@endsection

@section('styles')

    @parent
    {{-- <link rel="stylesheet" href="{!! asset('css/plugins/swiper/swiper.min.css') !!}" /> --}}
    {{-- max styles --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

@endsection

@section('content')

    <div id="modalMainVideo" tabindex="-1" role="dialog" class="modal fade b-modal b-modal--video" style="display: none;">
        <div role="document" class="modal-dialog modal-lg b-modal__dialog">
            <div class="modal-content b-modal__content">
                <div class="modal-header b-modal__header">
                    <div class="b-modal__close">
                        <button type="button" data-dismiss="modal" class="btn b-btn b-btn--rect b-btn--size-lg">
                            <i aria-hidden="true" class="fa fa-times"></i>
                        </button>
                    </div>
                    <p id="myModalLabel" class="b-modal__title">
                        MyVillage
                    </p>
                </div>
                <div class="modal-body b-modal__body">
                    <div class="embed-responsive embed-responsive-16by9">
                        @if(Lang::getLocale() == 'de')
                            <iframe src="https://www.youtube.com/embed/m5nywQAv8cE" class="embed-responsive-item"></iframe>
                        @elseif(Lang::getLocale() == 'en')
                            <iframe src="https://www.youtube.com/embed/_oC8KimE8QI" class="embed-responsive-item"></iframe>
                        @elseif(Lang::getLocale() == 'es')
                            <iframe src="https://www.youtube.com/embed/MBVw1LCvWzA" class="embed-responsive-item"></iframe>
                        @else
                            <iframe src="https://www.youtube.com/embed/DWfDUMDfYPc" class="embed-responsive-item"></iframe>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="b-main-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <div class="world-map__wrapper">
                        @include('layouts.frontend.world')
                        <div class="world-info">
                            <div class="world-info__inner">
                                <i class="world-info__icon fa fa-map-marker"></i>
                                <span class="world-info__country">Молдова</span>
                                {{-- <span class="world-info__count">(12.503)</span> --}}
                            </div>
                        </div>
                    </div>
                    <div class="b-country-dropdown__wrapper">
                        <div class="dropdown b-country-dropdown">
                            <a href="#" class="dropdown-toggle b-country-dropdown__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="b-country-icon fa fa-map-marker"></i>
                                @if(isset($countries[request()->clientCountry]))
                                    {{ mb_strtoupper(trans('countries.'.$countries[request()->clientCountry]->name)) }} <i class="triangle fa fa-angle-down" aria-hidden="true"></i>
                                    <input type="hidden" id="userCountry" value="{{ strtoupper($countries[request()->clientCountry]->iso2code) }}">
                                @else
                                    {{ mb_strtoupper(trans('countries.'.$countries['MD']->name)) }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    <input type="hidden" id="userCountry" value="{{ strtoupper($countries['MD']->iso2code) }}">
                                @endif
                            </a>
                            <ul class="dropdown-menu b-dropdown-menu">
                                @foreach($countries as $iso2code => $country)
                                    <li>
                                        <a href="{{ Request::getPathInfo().'?country='.$country->iso2code }}">
                                            {{ mb_strtoupper(trans('countries.'.$country->name)) }}
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="b-main-content__text-wrapper">
                        <strong>My Village<sup>24</sup></strong>
                        <h2 class="b-main-content__text">
                            @lang('pages.main_title')
                        </h2>
                    </div>
                    <a href="#" class="b-main-content__btn" data-toggle="modal" data-target="#modalMainVideo"><i aria-hidden="true" class="fa fa-play icon-play"></i></a>
                    <a href="{{Lang::getLocale()}}/about" class="b-main-content__btn" type="button">
                        @lang('pages.btn_about_project')
                    </a>
                </div>
            </div>
        </div>
        <div class="b-main-social">
            <ul class="b-main-social__list">
                <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="b-main-arrow">
        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
    </div>
    {{-- <div class="b-filter">
        <div class="container">
            <ul class="nav b-grid b-grid-3 b-nav-pills">
                <li class="active b-grid-item">
                    <a data-toggle="tab" href="#tab1" data-type="land">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/street-map.png') }}" alt=""></i>
                        @lang('messages.menu_lands_f') | 150
                    </a>
                </li>
                <li class="b-grid-item">
                    <a data-toggle="tab" href="#tab2" data-type="architecture">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/blueprint.png') }}" alt=""></i>
                        @lang('messages.menu_projects_f') | 204
                    </a>
                </li>
                <li class="b-grid-item">
                    <a data-toggle="tab" href="#tab3" data-type="building">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/construction.png') }}" alt=""></i>
                        @lang('messages.menu_buildings_f') | 204
                    </a>
                </li>
            </ul>

            <div class="tab-content b-tab-content">
                <div id="tab1" class="tab-pane fade in active">
                    <home-land-filter
                            ref="homelandfilter"
                            price-from="{{ ceil($landInitParams['priceRange']['min']) }}"
                            price-to="{{ ceil($landInitParams['priceRange']['max']) }}"
                            area-from="{{ ceil($landInitParams['areaRange']['min']) }}"
                            area-to="{{ ceil($landInitParams['areaRange']['max']) }}"
                    ></home-land-filter>
                </div>
                <div id="tab2" class="tab-pane fade">
                    <home-architecture-filter
                            ref="homearchitecturefilter"
                            price-from="{{ ceil($architectureInitParams['priceRange']['min']) }}"
                            price-to="{{ ceil($architectureInitParams['priceRange']['max']) }}"
                            area-from="{{ ceil($architectureInitParams['areaRange']['min']) }}"
                            area-to="{{ ceil($architectureInitParams['areaRange']['max']) }}">
                    </home-architecture-filter>
                </div>
                <div id="tab3" class="tab-pane fade">
                    <home-building-filter
                            ref="homebuildingfilter"
                            floors-count="{{ ceil($buildingInitParams['floors']) }}"
                            price-from="{{ ceil($buildingInitParams['priceRange']['min']) }}"
                            price-to="{{ ceil($buildingInitParams['priceRange']['max']) }}"
                            area-from="{{ ceil($buildingInitParams['areaRange']['min']) }}"
                            area-to="{{ ceil($buildingInitParams['areaRange']['max']) }}">
                    </home-building-filter>
                </div>
            </div>
        </div>
    </div> --}}
    {{-- <div class="b-filter">
        <div class="container">
            <ul class="nav b-grid b-grid-3 b-nav-pills">
                <li class="b-grid-item">
                    <a href="/lands">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/street-map.png') }}" alt=""></i>
                        @lang('messages.menu_lands_f') <span class="b-nav-pills__count">{{$landCount}}</span>
                    </a>
                </li>
                <li class="b-grid-item">
                    <a href="/architecture">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/blueprint.png') }}" alt=""></i>
                        @lang('messages.menu_projects_f') <span class="b-nav-pills__count">{{$architectureCount}}</span>
                    </a>
                </li>
                <li class="b-grid-item">
                    <a href="/buildings">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/construction.png') }}" alt=""></i>
                        @lang('messages.menu_buildings_f') <span class="b-nav-pills__count">{{$buildingCount}}</span>
                    </a>
                </li>
            </ul>
        </div>
    </div> --}}


@endsection

@section('content_main')

    <div class="b-section-steps">
        <div class="container">
            <h3 class="b-section-steps__title">@lang('pages.home_steps_title')</h3>
            <ul class="b-section-steps__list">
                <li>
                    <div class="b-section-steps__header">
                        <span class="count">1</span>
                        <img src="{{ url('img/pages/home/step-1.png') }}" alt="step-1">
                    </div>
                    <p class="b-section-steps__content">@lang('pages.home_steps_1c')</p>
                </li>
                <li class="line">
                    <img src="{{ url('img/pages/home/step-line.png') }}" alt="line" class="line-img">
                </li>
                <li>
                    <div class="b-section-steps__header">
                        <img src="{{ url('img/pages/home/step-2.png') }}" alt="step-1">
                        <span class="count">2</span>
                    </div>
                    <p class="b-section-steps__content">@lang('pages.home_steps_2c')</p>
                </li>
            </ul>
        </div>
    </div>

    {{-- <home-page ref="homepagecontentcomponent"></home-page> --}}

    <div class="b-section-home">
        <div class="container">
            <div class="b-section-heading b-section-heading__home">
                <a href="/lands" class="b-section-heading__title b-section-home__title">
                    <i class="b-section-heading__home-icon"><img src="{{ url('img/filters/street-map.png') }}" alt=""></i>
                    <span class="b-section-heading__title-self">@lang('messages.menu_lands_f')</span> <span class="b-text-label">{{$landCount}}</span>
                </a>
                <div class="b-section-home__nav">
                    <div class="b-list-lands-prev b-swiper-btn"><i class="icon fa fa-angle-left" aria-hidden="true"></i></div>
                    <div class="b-list-lands-next b-swiper-btn"><i class="icon fa fa-angle-right" aria-hidden="true"></i></div>
                </div>
            </div>
            <!-- Slider main container -->
            <div class="b-list-objects">
                <div class="swiper-container b-list-lands__container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach($lands as $key => $land)
                            <div class="swiper-slide">

                                <a href="{{ 'land/'.$land->id }}" class="b-list-objects__link">
                                    <div class="b-list-objects__img">
                                        <img src="{{ url(isset($land->images[0])?$land->images[0]->path.'/thumbnails/'.$land->images[0]->name:'#') }}" alt="image" class="img-responsive">
                                    </div>
                                    <div class="b-list-objects__content">
                                        <p class="b-list-objects__title">{{ isset($land->locality)?$land->locality:'' }} {{ ($land->street)?', '.$land->street:'' }}</p>
                                        <p class="b-list-objects__country">{{ isset($land->country)?$land->country:'' }}</p>
                                        <p class="b-list-objects__area"><span>{{ number_format($land->area, 0, '.', ' ') }} @lang('pages.ares')</span> <span>{{$land['per_are']}} €/@lang('pages.ares')</span></p>
                                        <ul class="b-list-objects__prices">
                                            <li class="">
                                                <span class="b-list-objects__prices-value">{{ number_format($land->first_payment, 0, '.', ' ') }} €</span>
                                                <span class="b-list-objects__prices-title">@lang('messages.label_first_payment')</span>
                                            </li>
                                            <li class="b-list-objects__prices-all">
                                                {{ number_format($land->price, 0, '.', ' ') }} €
                                            </li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            {{-- <div class="b-home-objects">
                @foreach($lands as $key => $land)
                    <div class="b-home-objects__card">
                        <a href="{{ 'land/'.$land->id }}" class="b-home-objects__link">
                            <div class="b-home-objects__img">
                                <img src="{{ url(isset($land->images[0])?$land->images[0]->path.'/thumbnails/'.$land->images[0]->name:'#') }}" alt="image" class="img-responsive">
                            </div>
                            <div class="b-home-objects__content">
                                <p class="b-home-objects__title">{{ isset($land->country)?$land->country:'' }} {{ isset($land->locality)?$land->locality:'' }} {{ ($land->street)?', '.$land->street:'' }}</p>
                                <p class="b-home-objects__area">{{ number_format($land->area, 0, '.', ' ') }} @lang('pages.ares')&nbsp;/&nbsp;{{$land['per_are']}} € @lang('pages.home_object_price_for_area')<sup>2</sup></p>
                                <ul class="b-card-desc__list">
                                    <li class="b-grid b-grid-space-between">
                                        <span class="b-card-desc__list-key">@lang('messages.label_whole_price')</span>
                                        <span class="u-text-color--green-darken text-medium">{{ number_format($land->price, 0, '.', ' ') }} €</span>
                                    </li>
                                    <li class="b-grid b-grid-space-between">
                                        <span class="b-card-desc__list-key">@lang('messages.label_first_payment')</span>
                                        <span class="u-text-color--green">{{ number_format($land->first_payment, 0, '.', ' ') }} €</span>
                                    </li>
                                </ul>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div> --}}
        </div>
    </div>

    <div class="b-section-home b-section-bg">
        <div class="container">
            <div class="b-section-heading b-section-heading__home">
                <a href="/architecture" class="b-section-heading__title b-section-home__title">
                    <i class="b-section-heading__home-icon"><img src="{{ url('img/filters/blueprint.png') }}" alt=""></i>
                    <span class="b-section-heading__title-self">@lang('messages.menu_projects_f')</span>
                    <span class="b-text-label">{{$architectureCount}}</span>
                </a>
                <div class="b-section-home__nav">
                    <div class="b-list-architecture-prev b-swiper-btn"><i class="icon fa fa-angle-left" aria-hidden="true"></i></div>
                    <div class="b-list-architecture-next b-swiper-btn"><i class="icon fa fa-angle-right" aria-hidden="true"></i></div>
                </div>
            </div>
            <!-- Slider main container -->
            <div class="b-list-objects b-list-objects__architecture">
                <div class="swiper-container b-list-architecture__container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach($architectures as $key => $architecture)
                            <div class="swiper-slide">

                                <a href="{{ 'architecture/'.$architecture->id }}" class="b-list-objects__link">
                                    <div class="b-list-objects__img">
                                        <img src="{{ url(isset($architecture->images[0])?$architecture->images[0]->path.'/thumbnails/'.$architecture->images[0]->name:'#') }}" alt="image" class="img-responsive">
                                    </div>
                                    <div class="b-list-objects__content">
                                        {{-- <p class="b-list-objects__title">{{str_limit($architecture->description, $limit = 40, $end = '...')}}</p> --}}
                                        <p class="b-list-objects__country">{{ $architecture->title }}</p>
                                        <ul class="b-list-objects__params">
                                            <li class="item">
                                                {{ number_format($architecture->general_area, 0, '.', ' ') }} m<sup>2</sup>
                                            </li>
                                            <li class="item">
                                                <i class="fa fa-bed item-icon" aria-hidden="true"></i>{{ $architecture->rooms_number }}
                                            </li>
                                            <li class="item">
                                                <i class="fa fa-bath item-icon" aria-hidden="true"></i>{{ $architecture->bathrooms_number }}
                                            </li>
                                        </ul>
                                        <p class="b-list-objects__time">@lang('pages.construction_period') <span>{{ number_format($architecture->general_area, 0, '', ' ') }} @lang('pages.days')</span></p>
                                        <ul class="b-list-objects__prices">
                                            <li class="">
                                                <span class="b-list-objects__prices-value">{{ number_format($architecture->building_price*$architecture->general_area/100*30, 0, '.', ' ') }} €</span>
                                                <span class="b-list-objects__prices-title">@lang('messages.label_first_payment')</span>
                                            </li>
                                            <li class="line"></li>
                                            <li class="b-list-objects__prices-all">
                                                <small>@lang('messages.label_from_lower')</small> {{ number_format($architecture->building_price*$architecture->general_area, 0, '.', ' ') }} €
                                            </li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            {{-- <div class="b-home-objects">
                @foreach($architectures as $key => $architecture)
                    <div class="b-home-objects__card">
                        <a href="{{ 'architecture/'.$architecture->id }}" class="b-home-objects__link">
                            <div class="b-home-objects__img">
                                <img src="{{ url(isset($architecture->images[0])?$architecture->images[0]->path.'/thumbnails/'.$architecture->images[0]->name:'#') }}" alt="image" class="img-responsive">
                            </div>
                            <div class="b-home-objects__content">
                                <p class="b-home-objects__title">{{ $architecture->title }} {{ number_format($architecture->general_area, 0, '.', ' ') }} m<sup>2</sup></p>
                                <ul class="b-home-objects__params">
                                    <li class="item">
                                        <i class="fa fa-bed item-icon" aria-hidden="true"></i>{{ $architecture->rooms_number }}
                                    </li>
                                    <li class="item">
                                        <i class="fa fa-bath item-icon" aria-hidden="true"></i>{{ $architecture->bathrooms_number }}
                                    </li>
                                </ul>
                                <ul class="b-card-desc__list">
                                    <li class="b-grid b-grid-space-between">
                                        <span class="b-card-desc__list-key">@lang('pages.cost_building_from')</span>
                                        <span class="u-text-color--green-darken text-medium">{{ number_format($architecture->building_price*$architecture->general_area, 0, '.', ' ') }} €</span>
                                    </li>
                                    <li class="b-grid b-grid-space-between">
                                        <span class="b-card-desc__list-key">@lang('pages.construction_period')</span>
                                        <span class="u-text-color--green">{{ number_format($architecture->general_area, 0, '', ' ') }} @lang('pages.days')</span>
                                    </li>
                                </ul>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div> --}}
        </div>
    </div>

    {{-- <div class="container">
        <img src="{{ url('img/banner.jpg') }}" alt="" class="banner img-responsive">
    </div> --}}

    <div class="b-section-home">
        <div class="container">
            <div class="b-section-heading b-section-heading__home">
                <a href="/buildings" class="b-section-heading__title b-section-home__title">
                    <i class="b-section-heading__home-icon"><img src="{{ url('img/filters/construction.png') }}" alt=""></i>
                    <span class="b-section-heading__title-self">@lang('messages.menu_buildings_f')</span>
                    <span class="b-text-label">{{$buildingCount}}</span>
                </a>
                <div class="b-section-home__nav">
                    <div class="b-list-buildings-prev b-swiper-btn"><i class="icon fa fa-angle-left" aria-hidden="true"></i></div>
                    <div class="b-list-buildings-next b-swiper-btn"><i class="icon fa fa-angle-right" aria-hidden="true"></i></div>
                </div>
            </div>
            <!-- Slider main container -->
            <div class="b-list-objects b-list-objects__buildings">
                <div class="swiper-container b-list-buildings__container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach($buildings as $key => $building)
                            <div class="swiper-slide">

                                <a href="{{ 'building/'.$building->id }}" class="b-list-objects__link">
                                    <div class="b-list-objects__img">
                                        <img src="{{ url(isset($building->images[0])?$building->images[0]->path.'/original/'.$building->images[0]->name:'#') }}" alt="image" class="img-responsive">
                                    </div>
                                    <div class="b-list-objects__content">
                                        {{-- <p class="b-list-objects__title">{{str_limit($architecture->description, $limit = 40, $end = '...')}}</p> --}}
                                        <p class="b-list-objects__title">{{ $building->title }}</p>
                                        <p class="b-list-objects__country">
                                            @if (isset($building->lands[0]))
                                                {{ isset($building->lands[0]->country)?$building->lands[0]->country:'' }} {{ isset($building->lands[0]->locality)?$building->lands[0]->locality:'' }} {{ isset($building->lands[0]->street)?', '.$building->lands[0]->street:'' }}
                                            @endif
                                        </p>
                                        <ul class="b-list-objects__params">
                                            <li class="item">
                                                {{ number_format($building->general_area, 0, '.', ' ') }} m<sup>2</sup>
                                            </li>
                                            <li class="item">
                                                <i class="fa fa-bed item-icon" aria-hidden="true"></i>{{ $building->rooms_number }}
                                            </li>
                                            <li class="item">
                                                <i class="fa fa-bath item-icon" aria-hidden="true"></i>{{ $building->bathrooms_number }}
                                            </li>
                                        </ul>
                                        <div class="b-list-objects__time"></div>
                                        <ul class="b-list-objects__estimate">
                                            <li>
                                                <span class="icon icon-ready warning"><i class="icon-item fa fa-battery-quarter" aria-hidden="true"></i></span>
                                                <span class="title">@lang('pages.readiness')</span>
                                                <span class="value warning">{{ $building->ready_percentage }}%</span>
                                            </li>
                                            <li>
                                                <span class="icon"><i class="icon-item fa fa-calendar" aria-hidden="true"></i></span>
                                                <span class="title">@lang('messages.label_release_date')</span>
                                                <span class="value">{{ $building->release_date }}</span>
                                            </li>
                                            <li>
                                                <span class="icon"><i class="icon-item fa fa-map-marker" aria-hidden="true"></i></span>
                                                <span class="title">@lang('pages.address')</span>
                                                <span class="value">{{ isset($building->lands[0]->street)?' '.$building->lands[0]->street:'' }}</span>
                                            </li>
                                        </ul>
                                        <ul class="b-list-objects__prices">
                                            <li class="">
                                                @lang('messages.label_first_payment') - <span>{{ number_format($building->building_price/100*30, 0, '.', ' ') }} €</span>
                                            </li>
                                            <li class="b-list-objects__prices-all">
                                                {{ number_format($building->building_price, 0, '.', ' ') }} €
                                            </li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            {{-- <div class="b-home-objects">
                @foreach($buildings as $key => $building)
                    <div class="b-home-objects__card">
                        <a href="{{ 'building/'.$building->id }}" class="b-home-objects__link">
                            <div class="b-home-objects__img">
                                <img src="{{ url(isset($building->images[0])?$building->images[0]->path.'/thumbnails/'.$building->images[0]->name:'#') }}" alt="image" class="img-responsive">
                            </div>
                            <div class="b-home-objects__content">
                                <p class="b-home-objects__title">{{ $building->title }} {{ number_format($building->general_area, 0, '.', ' ') }} m<sup>2</sup></p>
                                <ul class="b-home-objects__params">
                                    <li class="item">
                                        <i class="fa fa-bed item-icon" aria-hidden="true"></i>{{ $building->rooms_number }}
                                    </li>
                                    <li class="item">
                                        <i class="fa fa-bath item-icon" aria-hidden="true"></i>{{ $building->bathrooms_number }}
                                    </li>
                                </ul>
                                <div class="b-home-objects__desc-list">
                                    <i class="icon fa fa-wrench u-text-color--green-darken"></i>
                                    @lang('pages.readiness') &nbsp;<strong>{{ $building->ready_percentage }}%</strong>
                                </div>
                                <div class="b-home-objects__desc-list">
                                    <i class="icon fa fa-calendar-check-o u-text-color--green-darken"></i>
                                    @lang('messages.label_release_date') &nbsp;<strong>{{ $building->release_date }}</strong>
                                </div>
                                <div class="b-home-objects__desc-list">
                                    <i class="icon fa fa-map-marker u-text-color--green-darken"></i>
                                     @if (isset($building->lands[0]))
                                        <strong>{{ isset($building->lands[0]->country)?$building->lands[0]->country:'' }} {{ isset($building->lands[0]->locality)?$building->lands[0]->locality:'' }} {{ isset($building->lands[0]->street)?', '.$building->lands[0]->street:'' }}</strong>
                                     @endif
                                </div>
                                <ul class="b-card-desc__list">
                                    <li class="b-grid b-grid-space-between">
                                        <span class="b-card-desc__list-key">@lang('messages.label_full_price')</span>
                                        <span class="u-text-color--green-darken text-medium">{{ number_format($building->building_price, 0, '.', ' ') }} €</span>
                                    </li>
                                </ul>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div> --}}
        </div>
    </div>

    {{-- <div class="b-section-home">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="b-article">
                        <div class="b-article-item">
                            <div class="b-article-item__img">
                                <div class="img_object" style="background-image: url(/img/steps/step-1.png)"></div>
                                <div class="counter">1</div>
                            </div>
                            <div class="caption">
                                <p class="caption_title">@lang('pages.home_article_1')</p>
                            </div>
                        </div>
                        <div class="b-article-item">
                            <div class="b-article-item__img">
                                <div class="img_object" style="background-image: url(/img/steps/step-2.png)"></div>
                                <div class="counter">2</div>
                            </div>
                            <div class="caption">
                                <p class="caption_title">@lang('pages.home_article_2')</p>
                            </div>
                        </div>
                        <div class="b-article-item">
                            <div class="b-article-item__img">
                                <div class="img_object" style="background-image: url(/img/steps/step-3.png)"></div>
                                <div class="counter">3</div>
                            </div>
                            <div class="caption">
                                <p class="caption_title">@lang('pages.home_article_3')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    @include('layouts.frontend.modal.modal_video')
    @include('layouts.frontend.modal.modal_map')
    @include('layouts.frontend.modal.modal_land_map')

@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(document).ready(function(){
            $('a[data-toggle="tab"]').on('click', function(){

                let type = $(this).data('type');

                app.$refs.homepagecontentcomponent.toggleContent(type);
            });
        })
    </script>
@endsection