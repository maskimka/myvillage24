@extends('layouts.frontend.app')

@section('title', 'Партнеры')

@section('meta')
    @parent
    <meta property="og:title"              content="Внимание! Разыскиваем партнеров!" />
    <meta property="og:description"        content="My Village предлагает партнерство риелторам и владельцам земельных участков, эффективное рекламное продвижение, юридическую поддержку и гарантирует высокую прибыль." />
    <meta property="og:image"              content="{{ url('/img/tags/partners1.jpg') }}" />
@endsection

@section('content')

    <div class="l-inner-page">
        <div class="container">
            <div class="text-center">
                <div class="b-page-header">
                    <h1 class="b-page-header__title">
                        @lang('pages.partners_title')
                    </h1>
                    <div class="b-page-header__subtitle">
                        @lang('pages.partners_text1')
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/wxWKJaB-POk" class="embed-responsive-item"></iframe>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <p class="b-partner-info__content"><strong>@lang('pages.partners_text_pdf') </strong>&nbsp;<a href="{{ asset('uploads/presentation/MyVillage24_'.\App::getLocale().'.pdf') }}" class="btn b-btn b-btn--size-sm b-btn--color-green" download>@lang('messages.button_download') pdf</a></p>
            <br>
            <br>

            <div class="b-page-header text-center">
                <h1 class="b-page-header__title">
                   @lang('pages.partners_text_select')
                </h1>
            </div>
            <div class="b-partners">
                <a href="/realtor" class="b-partners__link">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item embed-bg-img" style="background-image: url({{ url('img/pages/partners/2.jpg') }});"></div>
                    </div>
                    <p>@lang('pages.partners_realtor')</p>
                </a>
                <a href="/architect" class="b-partners__link">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item embed-bg-img" style="background-image: url({{ url('img/pages/partners/1.jpg') }});"></div>
                    </div>
                    <p>@lang('pages.partners_architect')</p>
                </a>
                <a href="/representative" class="b-partners__link">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item embed-bg-img" style="background-image: url({{ url('img/pages/partners/5.jpg') }});"></div>
                    </div>
                    <p>@lang('pages.partners_representative')</p>
                </a>
                <a href="/construction" class="b-partners__link">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item embed-bg-img" style="background-image: url({{ url('img/pages/partners/3.jpg') }});"></div>
                    </div>
                    <p>@lang('pages.partners_construction_company')</p>
                </a>
                <a href="/finance" class="b-partners__link">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item embed-bg-img" style="background-image: url({{ url('img/pages/partners/6.jpg') }});"></div>
                    </div>
                    <p>@lang('pages.partners_finance_company')</p>
                </a>
                <a href="/modularstructures" class="b-partners__link">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item embed-bg-img" style="background-image: url({{ url('img/pages/partners/4.jpg') }});"></div>
                    </div>
                    <p>@lang('pages.partners_modular_homes')</p>
                </a>
            </div>
            <br>
            <br>
            <br>
            
            <div class="row">
                <div class="col-sm-6">
                    <p class="b-partner-info__content">
                        <strong>@lang('pages.partners_text13')</strong>
                    </p>
                    <br>
                    <div class="b-contact-block">
                        <div class="b-contact-block__label">
                            @lang('messages.label_phone'):
                        </div>
                        <div class="b-contact-block__value">
                            @lang('pages.sales_department') +373 79 005 888
                        </div>
                        <div class="b-contact-block__value">
                            @lang('pages.architecture_department') +373 79 050 888
                        </div>
                    </div>

                    <div class="b-contact-block">
                        <div class="b-contact-block__label">
                            E-mail:
                        </div>
                        <div class="b-contact-block__value">
                            info@myvillage24.com
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <form id="contactForm" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group b-form-group">
                                    <input type="text" name="name" class="form-control b-form-control" placeholder="@lang('messages.label_your_name')">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group b-form-group">
                                    <input type="text" name="email" class="form-control b-form-control" placeholder="E-mail">
                                </div>
                            </div>
                        </div>
                        <div class="form-group b-form-group">
                            <textarea name="message" class="form-control b-form-control" rows="4"></textarea>
                        </div>
                        <button class="btn b-btn b-btn--color-green b-btn--wide">
                            @lang('messages.button_send')
                        </button>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
