@extends('layouts.frontend.app')

@section('title', 'Регламент добавления земельного участка')

@section('content')

    <div class="container">
        {!! Breadcrumbs::render('prices') !!}
    </div>

    <div class="b-prices">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h5>@lang('pages.prices_title1'):</h5>
                    <p class="b-prices__title">@lang('pages.prices_subtitle1')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text1')</li>
                        <li>@lang('pages.prices_text2')</li>
                        <li>@lang('pages.prices_text3')</li>
                    </ul>
                    <p class="b-prices__title">@lang('pages.prices_subtitle2')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text4')</li>
                        <li>@lang('pages.prices_text5')</li>
                        <li>@lang('pages.prices_text6')</li>
                        <li>@lang('pages.prices_text7')</li>
                        <li>@lang('pages.prices_text8')</li>
                        <li>@lang('pages.prices_text9')</li>
                        <li>@lang('pages.prices_text10')</li>
                    </ul>
                    <p class="b-prices__title">@lang('pages.prices_subtitle3')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text11')</li>
                        <li>@lang('pages.prices_text12')</li>
                    </ul>
                    <p class="b-prices__title">@lang('pages.prices_subtitle4')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text13')</li>
                        <li>@lang('pages.prices_text14')</li>
                        <li>@lang('pages.prices_text15')</li>
                    </ul>
                    <p>@lang('pages.prices_text23')</p>
                    <br>
                    <p>@lang('pages.prices_text24')</p>
                    <br>
                    <p>@lang('pages.prices_text25')</p>
                    <br>
                </div>
                <div class="col-sm-6">
                    <h5>@lang('pages.prices_title2'):</h5>
                    <p class="b-prices__title">@lang('pages.prices_subtitle1')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text1')</li>
                        <li>@lang('pages.prices_text2')</li>
                        <li>@lang('pages.prices_text3')</li>
                    </ul>
                    <p class="b-prices__title">@lang('pages.prices_subtitle2')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text4')</li>
                        <li>@lang('pages.prices_text5')</li>
                        <li>@lang('pages.prices_text6')</li>
                        <li>@lang('pages.prices_text7')</li>
                        <li>@lang('pages.prices_text8')</li>
                        <li>@lang('pages.prices_text9')</li>
                        <li>@lang('pages.prices_text10')</li>
                    </ul>
                    <p class="b-prices__title">@lang('pages.prices_subtitle3')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text11')</li>
                        <li>@lang('pages.prices_text12')</li>
                    </ul>
                    <p class="b-prices__title">@lang('pages.prices_subtitle4')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text13')</li>
                        <li>@lang('pages.prices_text14')</li>
                        <li>@lang('pages.prices_text15')</li>
                    </ul>
                    <p class="b-prices__title">@lang('pages.prices_subtitle5')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text16')</li>
                        <li>@lang('pages.prices_text17')</li>
                    </ul>
                    <p class="b-prices__title">@lang('pages.prices_subtitle6')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text18')</li>
                        <li>@lang('pages.prices_text19')</li>
                    </ul>
                    <p class="b-prices__title">@lang('pages.prices_subtitle7')</p>
                    <ul class="u-check-list">
                        <li>@lang('pages.prices_text20')</li>
                        <li>@lang('pages.prices_text21')</li>
                        <li>@lang('pages.prices_text22')</li>
                    </ul>
                </div>
            </div>
        </div>        
    </div>

@endsection