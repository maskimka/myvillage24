@extends('layouts.frontend.app')

@section('title', 'GL')

@section('meta')
    @parent



@endsection

@section('styles')

    @parent

@endsection

@section('content')

    <br><br><br><br><br><br><br>

    <div class="container">

        <h1>h1. Heading 1</h1>
        <h2>h2. Heading 2</h2>
        <h3>h3. Heading 3</h3>
        <h4>h4. Heading 4</h4>
        <h5>h5. Heading 5</h5>
        <h6>h6. Heading 6</h6>

        <hr>

        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </p>


        <p class="b-text--lg">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </p>

        <p class="b-text--xlg">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </p>

        <hr>

        <div class="row">
            <div class="col-md-4">

                <div class="u-bg-color--red">
                    <br>
                    <br>
                    <br>
                </div>
                <div class="u-bg-color--green">
                    <br>
                    <br>
                    <br>
                </div>
                <div class="u-bg-color--blue">
                    <br>
                    <br>
                    <br>
                </div>

                <br>
                <br>
                <br>

            </div>

            <div class="col-md-4">

                <div class="u-bg-color--black">
                    <br>
                    <br>
                    <br>
                </div>
                <div class="u-bg-color--gray-darken">
                    <br>
                    <br>
                    <br>
                </div>
                <div class="u-bg-color--gray-dark">
                    <br>
                    <br>
                    <br>
                </div>
                <div class="u-bg-color--gray">
                    <br>
                    <br>
                    <br>
                </div>
                <div class="u-bg-color--gray-light">
                    <br>
                    <br>
                    <br>
                </div>
                <div class="u-bg-color--gray-lighter">
                    <br>
                    <br>
                    <br>
                </div>

            </div>

            <div class="col-md-4">

            </div>
        </div>

        <hr>

        <h1>h1. Heading 1 <span class="b-text-label b-text-label--gray-light">label</span> </h1>
        <h2>h2. Heading 2 <span class="b-text-label b-text-label--gray-light">label</span> </h2>
        <h3>h3. Heading 3 <span class="b-text-label b-text-label--gray-light">label</span> </h3>
        <h4>h4. Heading 4 <span class="b-text-label b-text-label--gray-light">label</span> </h4>
        <h5>h5. Heading 5 <span class="b-text-label b-text-label--gray-light">label</span> </h5>
        <h6>h6. Heading 6 <span class="b-text-label b-text-label--gray-light">label</span> </h6>

        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <span class="b-text-label b-text-label--gray-light">label</span>
        </p>


        <p class="b-text--lg">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <span class="b-text-label b-text-label--gray-light">label</span>
        </p>

        <p class="b-text--xlg">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <span class="b-text-label b-text-label--gray-light">label</span>
        </p>

        <hr>

        <button type="button" class="btn b-btn">
            Button
        </button>

        <hr>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-xsm b-btn--rect">
            <i class="fa fa-angle-right"></i>
        </button>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-sm b-btn--rect">
            <i class="fa fa-angle-right"></i>
        </button>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--rect">
            <i class="fa fa-angle-right"></i>
        </button>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-lg b-btn--rect">
            <i class="fa fa-angle-right"></i>
        </button>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-xlg b-btn--rect">
            <i class="fa fa-angle-right"></i>
        </button>

        <hr>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-xsm b-btn--rect b-btn--rounded">
            <i class="fa fa-angle-right"></i>
        </button>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-sm b-btn--rect b-btn--rounded">
            <i class="fa fa-angle-right"></i>
        </button>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--rect b-btn--rounded">
            <i class="fa fa-angle-right"></i>
        </button>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-lg b-btn--rect b-btn--rounded">
            <i class="fa fa-angle-right"></i>
        </button>

        <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-xlg b-btn--rect b-btn--rounded">
            <i class="fa fa-angle-right"></i>
        </button>

        <hr>

        <div class="row">
            <div class="col-md-3 text-right">
                <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-xsm">
                    Button
                </button>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" class="form-control b-form-control b-form-control--size-xsm">
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn b-btn b-btn--color-blue b-btn--size-xsm b-btn--rounded">
                    Rounded button
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 text-right">
                <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-sm">
                    Button
                </button>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" class="form-control b-form-control b-form-control--size-sm">
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn b-btn b-btn--color-blue b-btn--size-sm b-btn--rounded">
                    Rounded button
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 text-right">
                <button type="button" class="btn b-btn b-btn--color-gray">
                    Button
                </button>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" class="form-control b-form-control">
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn b-btn b-btn--color-blue b-btn--rounded">
                    Rounded button
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 text-right">
                <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-lg">
                    Button
                </button>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" class="form-control b-form-control b-form-control--size-lg">
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn b-btn b-btn--color-blue b-btn--size-lg b-btn--rounded">
                    Rounded button
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 text-right">
                <button type="button" class="btn b-btn b-btn--color-gray b-btn--size-xlg">
                    Button
                </button>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" class="form-control b-form-control b-form-control--size-xlg">
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn b-btn b-btn--color-blue b-btn--size-xlg b-btn--rounded">
                    Rounded button
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">

                <button type="button" class="btn b-btn b-btn--color-gray b-btn--wide">
                    Button
                </button>

            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-6">
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-gray">
                    Normal
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-gray hover">
                    Hover
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-gray focus">
                    Pressed
                </button>

                <br>
                <br>

                <button type="button" class="btn b-btn b-btn--wide b-btn--color-blue">
                    Normal
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-blue hover">
                    Hover
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-blue focus">
                    Pressed
                </button>

                <br>
                <br>

                <button type="button" class="btn b-btn b-btn--wide b-btn--color-red">
                    Normal
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-red hover">
                    Hover
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-red focus">
                    Pressed
                </button>

                <br>
                <br>

                <button type="button" class="btn b-btn b-btn--wide b-btn--color-green">
                    Normal
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-green hover">
                    Hover
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-green focus">
                    Pressed
                </button>
            </div>
            <div class="col-md-6">
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-gray b-btn--outline">
                    Normal
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-gray b-btn--outline hover">
                    Hover
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-gray b-btn--outline focus">
                    Pressed
                </button>

                <br>
                <br>

                <button type="button" class="btn b-btn b-btn--wide b-btn--color-blue b-btn--outline">
                    Normal
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-blue b-btn--outline hover">
                    Hover
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-blue b-btn--outline focus">
                    Pressed
                </button>

                <br>
                <br>

                <button type="button" class="btn b-btn b-btn--wide b-btn--color-red b-btn--outline">
                    Normal
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-red b-btn--outline hover">
                    Hover
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-red b-btn--outline focus">
                    Pressed
                </button>

                <br>
                <br>

                <button type="button" class="btn b-btn b-btn--wide b-btn--color-green b-btn--outline">
                    Normal
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-green b-btn--outline hover">
                    Hover
                </button>
                <button type="button" class="btn b-btn b-btn--wide b-btn--color-green b-btn--outline focus">
                    Pressed
                </button>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group b-form-group">
                    <label for="example-input-1" class="b-form-group__label">
                        Label
                    </label>
                    <input type="text" id="example-input-1" class="form-control b-form-control">
                </div>

                <div class="input-group b-input-group">
                    <span class="input-group-addon b-input-group__addon">@</span>
                    <input type="text" class="form-control b-form-control b-form-control--size-xsm" placeholder="Username">
                </div>

                <div class="input-group b-input-group">
                    <span class="input-group-addon b-input-group__addon">@</span>
                    <input type="text" class="form-control b-form-control b-form-control--size-sm" placeholder="Username">
                </div>

                <div class="input-group b-input-group">
                    <span class="input-group-addon b-input-group__addon">@</span>
                    <input type="text" class="form-control b-form-control" placeholder="Username">
                </div>

                <div class="input-group b-input-group">
                    <span class="input-group-addon b-input-group__addon">@</span>
                    <input type="text" class="form-control b-form-control b-form-control--size-lg" placeholder="Username">
                </div>

                <div class="input-group b-input-group">
                    <span class="input-group-addon b-input-group__addon">@</span>
                    <input type="text" class="form-control b-form-control b-form-control--size-xlg" placeholder="Username">
                </div>

                <br>

                <div class="dropdown">
                    <button class="btn b-btn b-btn--color-blue dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Dropdown
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu b-dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>

                <br>

                <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn b-btn b-btn--color-blue">Left</button>
                    <button type="button" class="btn b-btn b-btn--color-blue">Middle</button>
                    <button type="button" class="btn b-btn b-btn--color-blue">Right</button>
                </div>

                <br>
                <br>

                <button type="button" class="btn b-btn b-btn--color-blue" data-toggle="modal" data-target="#myModal">
                    Launch demo modal
                </button>

            </div>
            <div class="col-md-4">

                <div class="form-group b-form-group">
                    <label for="example-input-1" class="b-form-group__label">
                        Label
                    </label>
                    <input type="text" id="example-input-1" class="form-control b-form-control">
                </div>

                <div class="input-group b-input-group">
                    <input type="text" class="form-control b-form-control b-form-control--size-xsm" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn b-btn b-btn--color-blue b-btn--size-xsm" type="button">Go!</button>
                    </span>
                </div>

                <div class="input-group b-input-group">
                    <input type="text" class="form-control b-form-control b-form-control--size-sm" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn b-btn b-btn--color-blue b-btn--size-sm" type="button">Go!</button>
                    </span>
                </div>

                <div class="input-group b-input-group">
                    <input type="text" class="form-control b-form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn b-btn b-btn--color-blue" type="button">Go!</button>
                    </span>
                </div>

                <div class="input-group b-input-group">
                    <input type="text" class="form-control b-form-control b-form-control--size-lg" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn b-btn b-btn--color-blue b-btn--size-lg" type="button">Go!</button>
                    </span>
                </div>

                <div class="input-group b-input-group">
                    <input type="text" class="form-control b-form-control b-form-control--size-xlg" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn b-btn b-btn--color-blue b-btn--size-xlg" type="button">Go!</button>
                    </span>
                </div>

            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-4">

                <a href="#" class="b-logo">
                    <img src="{{ url('img/logo.png') }}" alt="Kirsan International" class="b-logo__img">
                </a>

            </div>
        </div>

        <hr>

        <p class="h3">
            Подходящие проекты
        </p>
        <div class="row">
            <div class="col-md-3">

                <div class="b-project-card">
                    <div class="embed-responsive embed-responsive-4by3 b-project-card__img" style="background-image: url(http://myvillage24.com/img/uploads/architecture//thumbnails/30fa46b384d8efbc2f7eeb7455ad1437.jpg);"></div>
                    <div class="b-project-card__content">
                        <div class="b-project-card__title">
                            AP-237 183.65 m<sup>2</sup>
                        </div>
                        <div class="b-project-card__stats">
                            <ul class="l-card-stats-grid">
                                <li class="l-card-stats-grid__item">
                                    <div class="b-card-stat-block">
                                        <div class="b-card-stat-block__icon">
                                            <i class="fa fa-car"></i>
                                        </div>
                                        <div class="b-card-stat-block__label">
                                            3
                                        </div>
                                    </div>
                                </li>
                                <li class="l-card-stats-grid__item">
                                    <div class="b-card-stat-block">
                                        <div class="b-card-stat-block__icon">
                                            <i class="fa fa-car"></i>
                                        </div>
                                        <div class="b-card-stat-block__label">
                                            3
                                        </div>
                                    </div>
                                </li>
                                <li class="l-card-stats-grid__item">
                                    <div class="b-card-stat-block">
                                        <div class="b-card-stat-block__icon">
                                            <i class="fa fa-car"></i>
                                        </div>
                                        <div class="b-card-stat-block__label">
                                            3
                                        </div>
                                    </div>
                                </li>
                                <li class="l-card-stats-grid__item">
                                    <div class="b-card-stat-block">
                                        <div class="b-card-stat-block__icon">
                                            <i class="fa fa-car"></i>
                                        </div>
                                        <div class="b-card-stat-block__label">
                                            3
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <br>
    <br>
    <br>





    <br><br><br><br><br><br><br>

    <div class="modal fade b-modal" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog b-modal__dialog" role="document">
            <div class="modal-content b-modal__content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <p class="h4 b-modal__title" id="myModalLabel">
                        Modal title
                    </p>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn b-btn b-btn--wide b-btn--color-gray b-btn--outline" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn b-btn b-btn--wide b-btn--color-blue">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')


@endsection
