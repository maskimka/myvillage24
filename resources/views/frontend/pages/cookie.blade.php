@extends('layouts.frontend.app')

@section('title', 'О нас')

@section('content')

    <div class="l-inner-page">
        <div class="container">

            <h2 class="text-center">@lang('pages.cookie_title')</h2>
            <p><strong>@lang('pages.cookie_subtitle_1')</strong></p>
            <p>@lang('pages.cookie_text_1')</p>
            <p><strong>@lang('pages.cookie_subtitle_2')</strong></p>
            <p>@lang('pages.cookie_text_2')</p>
            <ul>
                <li>@lang('pages.cookie_list_1_i1')</li>
                <li>@lang('pages.cookie_list_1_i2')</li>
                <li>@lang('pages.cookie_list_1_i3')</li>
                <li>@lang('pages.cookie_list_1_i4')</li>
                <li>@lang('pages.cookie_list_1_i5')</li>
                <li>@lang('pages.cookie_list_1_i6')</li>
            </ul>
            <p><strong>@lang('pages.cookie_subtitle_3')</strong></p>
            <p>@lang('pages.cookie_text_3')</p>
            <ul>
                <li>@lang('pages.cookie_list_2_i1')</li>
                <li>@lang('pages.cookie_list_2_i2')</li>
            </ul>
            <p>@lang('pages.cookie_text_4')</p>

        </div>
    </div>

@endsection
