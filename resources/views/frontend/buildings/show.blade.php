@extends('layouts.frontend.app')

@section('title', 'Архитектурный проект')

@section('meta')
    @parent
    @if ($item->id == 3)
        <meta property="og:title"              content="Современный двухуровневый дуплекс  на Телецентре" />
        <meta property="og:description"        content="Продается секция дуплекса в благоустроенном районе Телецентра. Стильный интерьер, гостиная, 4 жилые комнаты, много свободного пространства." />
        <meta property="og:image"              content="{{ url('/img/tags/building2NEW.jpg') }}" />
    @endif

@endsection

@section('styles')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">
    <link rel="stylesheet" href="{!! asset('css/plugins/swiper/swiper.min.css') !!}" />

@endsection

@section('content')

    <div class="container">
        {!! Breadcrumbs::render('building', $item) !!}
    </div>
    
    <div class="b-object">
        <div class="container">
            <p class="b-object-title">{{ $item->title }} {{ isset($item->lands[0]->locality)?$item->lands[0]->locality:'' }} {{ isset($item->lands[0]->street)?', '.$item->lands[0]->street:'' }}</p>
            @if (isset($item->lands[0]))
                <p class="b-object-country">{{ isset($item->lands[0]->country)?$item->lands[0]->country:'' }}</p>
            @endif
            <div class="row">
                <div class="col-sm-7">
                    <div class="b-object-gallery">
                        <div class="b-object-gallery__main b-object-carousel">
                            <a href="{{ url($item->images[0]->path.'/original/'.$item->images[0]->name) }}" class="b-object-carousel__slide-img">
                                <img src="{{ url($item->images[0]->path.'/original/'.$item->images[0]->name) }}" alt="" class="img-responsive b-object-gallery__main-img">
                            </a>
                        </div>
                        <div class="js-product-carousel b-object-carousel">
                            @foreach($item->images as $photo)
                                <div class="b-object-carousel__slide">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <a href="{{ url($photo->path.'/original/'.$photo->name) }}" title="{{$photo->alt}}" class="embed-responsive-item embed-bg-img b-object-carousel__slide-img" style="background-image: url({{ url($photo->path.'/thumbnails/'.$photo->name) }})"></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    @if($item->description)
                        <p>{{ $item->description }}</p>
                    @endif

                    @foreach($item['floors'] as $key => $floor)
                        <p class="b-card-desc__title h5 u-text-color--green collapse collapsed" type="button" data-toggle="collapse" data-target="#collapseFloors-{{$key}}" aria-expanded="false" aria-controls="collapseExample">
                            @if($item->id == 3)
                                @if($key == 0)
                                    @lang('messages.label_parterre_plan')
                                @else
                                    {{ trans('messages.label_levels_plan', ['number' => $key ]) }}
                                @endif
                            @else
                                {{ trans('messages.label_levels_plan', ['number' => ($key + 1) ]) }}
                            @endif
                            <i aria-hidden="true" class="icon fa fa-angle-down"></i>
                        </p>
                        <div class="collapse" id="collapseFloors-{{$key}}">
                            <div class="row">
                                <div class="col-sm-7">
                                        <ul class="b-card-desc__list">
                                            <li class="b-grid b-grid-space-between">
                                                <span class="b-card-desc__list-key">@lang('messages.label_general_area')</span>
                                                <span>{{ $floor->area }} m<sup>2</sup></span>
                                            </li>
                                            @foreach($floor->house_zone as $i => $zone)
                                                <li class="b-grid b-grid-space-between">
                                                    <span class="b-card-desc__list-key">@lang('messages.'.$zone->slug)</span>
                                                    <span>{{ $zone['original']['pivot_house_zone_value'] }} m<sup>2</sup></span>
                                                </li>
                                            @endforeach
                                        </ul>
                                </div>
                                <div class="col-sm-5">
                                    <div class="b-object-thumb__list-group">
                                        <a href="{{ url($floor['image_original']) }}" class="embed-responsive embed-responsive-4by3">
                                            <div class="embed-responsive-item embed-bg-img b-object-thumbs__item-img" style="background-image: url({{ url($floor['image_thumbnail']) }}); border: {{ $floor['is_placeholder']?'1px dashed #ddd':0 }}"></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endforeach

                    @if (count($item->lands) > 0)
                        {{-- <a href="{{ url('land/'.$item->lands[0]->id) }}" class="b-object-building-link">
                            <i class="fa fa-map-marker building-address-pin pin-icon"></i>
                            {{ $item->lands[0]->address }}
                        </a> --}}
                        <p>{{ $item->lands[0]->address }}</p>
                        
                        <div class="swiper-container b-swiper-lands">
                            <div class="swiper-wrapper">
                                @foreach($item->lands[0]->images as $photo)
                                    <div class="swiper-slide embed-responsive embed-responsive-16by9">
                                        <div class="embed-responsive-item embed-bg-img" style="background-image: url({{ url($photo->path.'/original/'.$photo->name) }})"></div>
                                    </div>
                                @endforeach
                            </div>
                            <!-- If we need pagination -->
                            <div class="b-swiper-lands-pagination"></div>

                            <!-- If we need navigation buttons -->
                            <div class="b-swiper-lands-btn b-swiper-lands-btn-prev">
                                 <i aria-hidden="true" class="btn-icon fa fa-angle-left"></i>
                            </div>
                            <div class="b-swiper-lands-btn b-swiper-lands-btn-next">
                                <i aria-hidden="true" class="btn-icon fa fa-angle-right"></i>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="b-card-desc__list hidden-xs hidden-sm">  
                                    @if(!empty($item->lands[0]->area))
                                        <li>
                                            <span>@lang('messages.label_area')</span>
                                            <span>{{ number_format($item->lands[0]->area, 0, '.', ' ') }} {{ trans_choice('messages.label_ares', $item->lands[0]->area) }}<sup>2</sup></span>
                                        </li>
                                    @endif
                                    <li>
                                        <span class="b-card-desc__list-key">ID</span>
                                        <span>{{ $item->lands[0]->article }}</span>
                                    </li>
                                    @if(!empty($item->lands[0]->country))
                                        <li>
                                            <span>@lang('messages.label_country')</span>
                                            <span>{{ $item->lands[0]->country }}</span>
                                        </li>
                                    @endif
                                    @if(!empty($item->lands[0]->locality))
                                        <li>
                                            <span>@lang('messages.label_city')</span>
                                            <span>{{ $item->lands[0]->locality }}</span>
                                        </li>
                                    @endif
                                    @if(!empty($item->lands[0]->sublocality))
                                        <li>
                                            <span>@lang('messages.label_district')</span>
                                            <span>{{ $item->lands[0]->sublocality }}</span>
                                        </li>
                                    @endif
                                    @if($item->lands[0]->from_city > 0)
                                        <li>
                                            <span>@lang('messages.label_from_city')</span>
                                            <span>{{ number_format($item->lands[0]->from_city, 0, '.', ' ') }} @lang('messages.label_km')</span>
                                        </li>
                                    @endif
                                    @if(!empty($item->lands[0]->purpose))
                                        <li>
                                            <span>@lang('messages.label_land_purpose')</span>
                                            <span>@lang('messages.'.$item->lands[0]->purpose->slug)</span>
                                        </li>
                                    @endif
                                    <li>
                                        <span>@lang('messages.label_cadastral_number')</span>
                                        <span>{{ $item->lands[0]->cadastral_number }}</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <google-map
                                    id="js-objects-map"
                                    class="b-object-map"
                                    :center-coords="[{{ $item->lands[0]->lat }}, {{  $item->lands[0]->lng }}]"
                                    land-id="{{ $item->lands[0]->id }}"
                                    show-type="points"
                                    :enable-geolocation=false>
                                </google-map>
                            </div>
                        </div>

                        <div class="b-benefits">
                            @if($item->lands[0]->communications->count() > 0)
                                <p class="b-benefits__title">
                                    @lang('messages.label_communications')
                                </p>
                                <ul class="b-benefits-list">
                                    @foreach($item->lands[0]->communications as $communication)
                                        {{-- <li class="b-benefits-list-item"><i class="fa fa-check b-benefits-list-item__icon"></i>@lang('messages.'.$communication->slug)</li> --}}
                                        <li class="b-benefits-list-item">@lang('messages.'.$communication->slug)</li>
                                    @endforeach
                                </ul>
                            @endif
                            @if($item->lands[0]->infrastructure->count() > 0)
                                <p class="b-benefits__title">
                                    @lang('messages.label_infrastructure')
                                </p>
                                <ul class="b-benefits-list">
                                    @foreach($item->lands[0]->infrastructure as $infrastructure_item)
                                        {{-- <li class="b-benefits-list-item"><i class="fa fa-check b-benefits-list-item__icon"></i>@lang('messages.'.$infrastructure_item->slug)</li> --}}
                                        <li class="b-benefits-list-item">@lang('messages.'.$infrastructure_item->slug)</li>
                                    @endforeach
                                </ul>
                            @endif
                            @if($item->lands[0]->transport->count() > 0)
                                <p class="b-benefits__title">
                                    @lang('messages.label_transport')
                                </p>
                                <ul class="b-benefits-list">
                                    @foreach($item->lands[0]->transport as $transport_item)
                                        {{-- <li class="b-benefits-list-item"><i class="fa fa-check b-benefits-list-item__icon"></i>@lang('messages.'.$transport_item->slug)</li> --}}
                                        <li class="b-benefits-list-item">@lang('messages.'.$transport_item->slug)</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                    @endif

                </div>
                <div class="col-sm-5">
                    <div class="b-object-price__wrapper">
                        <div class="b-object-price__item">
                            <div><span class="b-object-price__all">{{ number_format($item->building_price, 0, '.', ' ') }} €</span> <span class="b-object-price__area">/ {{ $item->general_area }} {{ trans_choice('messages.label_ares', $item->general_area) }}<sup>2</sup></span></div>
                            <div><span class="b-object-price__firstpayment">{{ $item->first_payment }} € - @lang('pages.first_payment')</span></div>
                        </div>
                        <div class="b-object-price__item">
                            <a class="btn-favorite {{ ($item->is_favorite)?'active':'' }}" id="favorite" data-id="{{ $item->id }}" data-type="Building" title="@lang('messages.label_favorite')">
                                <i class="fa fa-bookmark icon icon-on" aria-hidden="true"></i>
                                <i class="fa fa-bookmark-o icon icon-off" aria-hidden="true"></i>
                            </a>
                            <a type="button" class="btn-share" data-container="body" data-toggle="popover" data-placement="top" data-trigger="click" title="@lang('messages.label_share')">
                                <i class="fa fa-share-alt icon" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <br>
                    <ul class="b-list-objects__estimate">
                        @if($item->ready_percentage > 0)
                            <li>
                                <span class="icon icon-ready warning"><i aria-hidden="true" class="icon-item fa fa-battery-quarter"></i></span>
                                <span class="title">@lang('messages.label_readiness')</span>
                                <span class="value warning">{{ $item->ready_percentage }} %</span>
                            </li>
                        @endif
                        @if(!empty($item->release_date))
                            <li>
                                <span class="icon"><i aria-hidden="true" class="icon-item fa fa-calendar"></i></span>
                                <span class="title">@lang('messages.label_release_date')</span>
                                <span class="value">{{ $item->release_date }}</span>
                            </li>
                        @endif
                    </ul>
                    <br>
                    <div class="b-object-contacts">
                        <div class="b-object-contacts__item phone"><i class="icon fa fa-mobile" aria-hidden="true"></i><span>+373</span> 79 00 1000</div>
                        <div class="b-object-contacts__item">
                            <button role="button" class="btn b-btn b-btn--color-green b-btn--size-lg b-btn--wide b-btn--rounded-light" data-toggle="modal" data-target="#ordersForm" id="{{ $item->id }}" value="{{ '/'.config('app.locale').'/buildings/order'}}">
                                @lang('messages.label_order_view_home')
                            </button>
                        </div>
                    </div>
                    <p class="b-card-desc__title">
                        @lang('pages.building_info'):
                    </p>
                    <ul class="b-card-desc__list">
                        <li class="b-grid b-grid-space-between">
                            <span class="b-card-desc__list-key">ID</span>
                            <span>{{ $item->article }}</span>
                        </li>
                        @if($item->general_area > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_general_area')</span>
                                <span>{{ $item->general_area }} m <sup>2</sup></span>
                            </li>
                        @endif
                        @if($item->living_area > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_living_area')</span>
                                <span>{{ $item->living_area }} m <sup>2</sup></span>
                            </li>
                        @endif
                        @if($item->floors_area > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_floor_area')</span>
                                <span>{{ $item->floors_area }} m <sup>2</sup></span>
                            </li>
                        @endif
                        @if($item->roof_area > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_roof_area')</span>
                                <span>{{ $item->roof_area }} m <sup>2</sup></span>
                            </li>
                        @endif
                        @if($item->construction_area > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_builtup_area')</span>
                                <span>{{ $item->construction_area }} m <sup>2</sup></span>
                            </li>
                        @endif
                        @if($item->celling_height > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_ceilling_height')</span>
                                <span>{{ $item->celling_height }}</span>
                            </li>
                        @endif
                        @if($item->floors_number > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_floors_number')</span>
                                <span>{{ $item->floors_number }}</span>
                            </li>
                        @endif
                        @if($item->bathrooms_number > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_restrooms_number')</span>
                                <span>{{ $item->bathrooms_number }}</span>
                            </li>
                        @endif
                        @if($item->rooms_number > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_rooms_number')</span>
                                <span>{{ $item->rooms_number }}</span>
                            </li>
                        @endif
                        @if($item->garages_number > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_garages_number')</span>
                                <span>{{ $item->garages_number }}</span>
                            </li>
                        @endif
                        <li class="b-grid b-grid-space-between">
                            <span class="b-card-desc__list-key">@lang('messages.label_building_size')</span>
                            <span>{{ $item->width.' x '.$item->length }} m</span>
                        </li>
                    </ul>
                    @if ($item->exterior_walls != "" || $item->overlappings != "" || $item->roof != "" || $item->boiler != "")
                        <p class="b-card-desc__title">
                            @lang('messages.label_technology_and_design')
                        </p>
                        <ul class="b-card-desc__list">
                            @if($item->exterior_walls)
                                <li class="b-grid b-grid-space-between">
                                    <span class="b-card-desc__list-key">@lang('messages.label_exterior_walls')</span>
                                    <span>{{ $item->exterior_walls }}</span>
                                </li>
                            @endif
                            @if($item->exterior_walls)
                                <li class="b-grid b-grid-space-between">
                                    <span class="b-card-desc__list-key">@lang('messages.label_overlappings')</span>
                                    <span>{{ $item->overlappings }}</span>
                                </li>
                            @endif
                            @if($item->exterior_walls)
                                <li class="b-grid b-grid-space-between">
                                    <span class="b-card-desc__list-key">@lang('messages.label_housetop')</span>
                                    <span>{{ $item->roof }}</span>
                                </li>
                            @endif
                            @if($item->exterior_walls)
                                <li class="b-grid b-grid-space-between">
                                    <span class="b-card-desc__list-key">@lang('messages.label_boiler')</span>
                                    <span>{{ $item->boiler }}</span>
                                </li>
                            @endif
                        </ul>
                    @endif

                    @if ($item->facadeBuildingGalleryImages->count())

                        @include('frontend.partials._image-cards-gallery', [
                            'title' => trans('messages.label_facade'),
                            'images' => $item->facadeBuildingGalleryImages
                            ])
                        <br>
                    @endif
                    @if ($item->interiorBuildingGalleryImages->count())

                        @include('frontend.partials._image-cards-gallery', [
                            'title' => trans('messages.catalog_interior_design'),
                            'images' => $item->interiorBuildingGalleryImages
                        ])
                        <br>
                    @endif
                    @if($item->general_plan || $item->house_section)
                        <div class="b-card-desc__title h4">
                            @lang('messages.label_general_plan_and_section')
                        </div>
                        <div class="row">
                            @if($item->general_plan)
                            <div class="col-sm-6">
                                <div class="b-object-thumb__list">
                                    <a href="{{ url(config('custom.BUILDING_IMAGES_PATH').'original/'.$item->general_plan) }}" class="embed-responsive embed-responsive-4by3 b-object-thumbs__list-item">
                                        <div class="embed-responsive-item embed-bg-img b-object-thumbs__item-img" style="background-image: url({{ url(config('custom.BUILDING_IMAGES_PATH').'thumbnails/'.$item->general_plan) }})"></div>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @if($item->house_section)
                            <div class="col-sm-6">
                                <div class="b-object-thumb__list">
                                    <a href="{{ url(config('custom.BUILDING_IMAGES_PATH').'original/'.$item->house_section) }}" class="embed-responsive embed-responsive-4by3 b-object-thumbs__list-item">
                                        <div class="embed-responsive-item embed-bg-img b-object-thumbs__item-img" style="background-image: url({{ url(config('custom.BUILDING_IMAGES_PATH').'thumbnails/'.$item->house_section) }})"></div>
                                    </a>
                                </div>
                            </div>
                            @endif
                        </div>
                    @endif
                    
                </div>
            </div>

            <div class="b-section-heading__home">
                <div class="b-card-desc__title h4">
                    Отчет со строй площадки - Февраль 2018
                    <a href="#" class="b-object-report__link">Все месяцы</a>
                </div>
                <div class="b-section-home__nav">
                    <div class="b-list-lands-prev b-swiper-btn"><i class="icon fa fa-angle-left" aria-hidden="true"></i></div>
                    <div class="b-list-lands-next b-swiper-btn"><i class="icon fa fa-angle-right" aria-hidden="true"></i></div>
                </div>
            </div>
            @if ($item->underconstructBuildingGalleryImages->count())

                <div class="b-list-objects">
                    <div class="swiper-container b-list-lands__container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            @foreach($item->underconstructBuildingGalleryImages as $photo)
                                <div class="swiper-slide">

                                    <a href="{{ url($photo->path.'/original/'.$photo->name) }}" class="embed-responsive embed-responsive-5by3">
                                        <span class="embed-responsive-item embed-bg-img" style="background-image: url({{ url($photo->path.'/thumbnails/'.$photo->name) }})"></span>
                                        {{-- <div class="b-list-objects__img">
                                            <img src="{{ url(isset($land->images[0])?$land->images[0]->path.'/thumbnails/'.$land->images[0]->name:'#') }}" alt="image" class="img-responsive">
                                        </div> --}}
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            @endif

            <br>

        </div>
    </div>

    @include('layouts.frontend.modal.order_modal')
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="{!! asset('js/plugins/validate/jquery.validate.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/validate/additional-methods.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/clipboard/clipboard.min.js') !!}" type="text/javascript"></script>

@endsection
