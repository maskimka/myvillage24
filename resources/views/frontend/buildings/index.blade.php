@extends('layouts.frontend.app')

@section('title', 'Дома')

@section('styles')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">

    {{-- max styles --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

@endsection

@section('content')

    {{-- <div class="b-nav-pills__objects">
        <div class="container">
            <ul class="nav b-grid b-grid-3 b-nav-pills">
                <li class="b-grid-item {{ Route::is('lands') ? 'active' : '' }}">
                    <a href="{{ url('lands') }}">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/street-map.png') }}" alt=""></i>
                        @lang('messages.menu_lands_f') | {{ $landCount }}
                    </a>
                </li>
                <li class="b-grid-item {{ Route::is('projects') ? 'active' : '' }}">
                    <a href="{{ url('architecture') }}">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/blueprint.png') }}" alt=""></i>
                        @lang('messages.menu_projects_f') | {{ $architectureCount }}
                    </a>
                </li>
                <li class="b-grid-item {{ Route::is('buildings') ? 'active' : '' }}">
                    <a href="{{ url('buildings') }}">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/construction.png') }}" alt=""></i>
                        @lang('messages.menu_buildings_f') | {{ $buildingCount }}
                    </a>
                </li>
            </ul>
        </div>
    </div> --}}

    {{-- <div class="container">
        {!! Breadcrumbs::render('buildings') !!}
    </div> --}}

    <div class="b-view-header">
        <div class="b-view-header__filter">
            <a class="btn btn-primary b-filter-collapse__btn" role="button" data-toggle="collapse" href="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <i class="fa fa-filter b-filter-collapse__icon" aria-hidden="true"></i>Фильтры
                <span class="caret"></span>
            </a>
            <div class="collapse b-filter-collapse__content" id="collapseFilter">
                <div class="b-filter-collapse__inner">
                     <building-filter
                        ref="buildingfilter"
                        building-type="{{ ceil($initParams['buildingType']) }}"
                        floors-from="{{ ceil($initParams['floorsRange']['min']) }}"
                        floors-to="{{ ceil($initParams['floorsRange']['max']) }}"
                        price-from="{{ ceil($initParams['priceRange']['min']) }}"
                        price-to="{{ ceil($initParams['priceRange']['max']) }}"
                        price-max="{{ ceil($initParams['priceRange']['maximalPrice']) }}"
                        price-min="{{ ceil($initParams['priceRange']['minimalPrice']) }}"
                        area-from="{{ ceil($initParams['areaRange']['min']) }}"
                        area-to="{{ ceil($initParams['areaRange']['max']) }}"
                        area-max="{{ ceil($initParams['areaRange']['maximalArea']) }}"
                        area-min="{{ ceil($initParams['areaRange']['minimalArea']) }}">
                    </building-filter>
                </div>
            </div>
        </div>
        <div class="b-view-header__map">
            <div class="b-map-checkbox">
                <input type="checkbox" name="checkbox-cats[]" id="view-change" value="1" checked>
                <label for="view-change">Карта</label>
            </div>
        </div>
    </div>

    <div class="b-view-inner">
        <div class="b-view-lands">
            <building-list
                ref="buildinglist"
                sort-prop="{{ $initParams['sortBy'] or '' }}"
                :limit-prop={{ $initParams['limit'] }}
                :offset={{ ceil($initParams['page']) }}
                :page={{ ceil($initParams['page']) }}>
            </building-list>
        </div>
        <div class="b-view-map">
            <google-map
                    id="js-objects-map"
                    class="b-objects-map__gm b-view-map__render"
                    :center-coords="[47.020751, 28.856090]"
                    show-type="buildings"
                    :enable-geolocation=false>
            </google-map>
            <google-place-popup ref="googlemarkerplacepopup"></google-place-popup>
        </div>
    </div>

    {{-- <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <building-filter
                            ref="buildingfilter"
                            building-type="{{ ceil($initParams['buildingType']) }}"
                            floors-from="{{ ceil($initParams['floorsRange']['min']) }}"
                            floors-to="{{ ceil($initParams['floorsRange']['max']) }}"
                            price-from="{{ ceil($initParams['priceRange']['min']) }}"
                            price-to="{{ ceil($initParams['priceRange']['max']) }}"
                            price-max="{{ ceil($initParams['priceRange']['maximalPrice']) }}"
                            price-min="{{ ceil($initParams['priceRange']['minimalPrice']) }}"
                            area-from="{{ ceil($initParams['areaRange']['min']) }}"
                            area-to="{{ ceil($initParams['areaRange']['max']) }}"
                            area-max="{{ ceil($initParams['areaRange']['maximalArea']) }}"
                            area-min="{{ ceil($initParams['areaRange']['minimalArea']) }}">
                    </building-filter>
                </div>
            </div>
            <div class="col-md-9">
                <div class="b-lands-header">
                    <div class="b-lands-header__map">
                        <div class="embed-responsive embed-responsive-21by9">
                            <google-map
                                    id="js-objects-map"
                                    class="b-objects-map__gm embed-responsive-item embed-bg-img"
                                    :center-coords="[47.020751, 28.856090]"
                                    show-type="buildings"
                                    :enable-geolocation=false>
                            </google-map>
                            <google-place-popup ref="googlemarkerplacepopup"></google-place-popup>
                        </div>
                    </div>
                </div>
                <building-list
                    ref="buildinglist"
                    sort-prop="{{ $initParams['sortBy'] or '' }}"
                    :limit-prop={{ $initParams['limit'] }}
                    :offset={{ ceil($initParams['page']) }}
                    :page={{ ceil($initParams['page']) }}>
                </building-list>
            </div>
        </div>
    </div> --}}

    @include('layouts.frontend.modal.modal_video')
    @include('layouts.frontend.modal.modal_land_map')
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>

    {{-- max --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $( "#price-range" ).slider({
            range: true,
            min: 0,
            max: 60000,
            values: [ 8000, 50000 ],
            slide: function( event, ui ) {
                $( "#price-amount-from" ).html($( "#price-range" ).slider( "values", 0 ));
                $( "#price-amount-to" ).html($( "#price-range" ).slider( "values", 1 ));
            }
        });
        $( "#price-amount-from" ).html($( "#price-range" ).slider( "values", 0 ));
        $( "#price-amount-to" ).html($( "#price-range" ).slider( "values", 1 ));

        $( "#area-range" ).slider({
            range: true,
            min: 0,
            max: 20000,
            values: [ 4000, 20000 ],
            slide: function( event, ui ) {
                $( "#area-amount-from" ).html($( "#area-range" ).slider( "values", 0 ));
                $( "#area-amount-to" ).html($( "#area-range" ).slider( "values", 1 ));
            }
        });
        $( "#area-amount-from" ).html($( "#area-range" ).slider( "values", 0 ));
        $( "#area-amount-to" ).html($( "#area-range" ).slider( "values", 1 ));
    </script>
@endsection
