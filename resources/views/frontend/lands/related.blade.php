@extends('layouts.frontend.app')

@section('title', 'Участки')

@section('styles')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    {{-- max styles --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

@endsection

@section('content')

    <div class="container">
        @include('layouts.frontend.breadcrumb')
    </div>

    <div class="container">
        <div class="embed-responsive embed-responsive-4by1">
            <google-map
                    id="js-objects-map"
                    class="b-objects-map__gm embed-responsive-item embed-bg-img"
                    :center-coords="[47.020751, 28.856090]"
                    show-type="points"
                    :enable-geolocation=false>
            </google-map>
        </div>
    </div>
    <div class="b-section">
        <div class="container">
            <div class="row">
                {{--<div class="col-sm-3">--}}
                    {{--<div class="sidebar">--}}
                        {{--<land-filter--}}
                                {{--price-from="{{ round($initParams['priceRange']['min']) }}"--}}
                                {{--price-to="{{ round($initParams['priceRange']['max']) }}"--}}
                                {{--area-from="{{ round($initParams['areaRange']['min']) }}"--}}
                                {{--area-to="{{ round($initParams['areaRange']['max']) }}">--}}
                        {{--</land-filter>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-sm-12">
                    <land-list
                            ref="landlist"
                            for_sale="@lang('messages.status_for_sale')"
                            reserved="@lang('messages.status_sold')"
                            sold="@lang('messages.status_reserved')"
                            related_to="{{ $related_to }}"
                    ></land-list>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.frontend.modal.modal_land_map')
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    {{-- max --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
@endsection
