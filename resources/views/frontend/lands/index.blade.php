@extends('layouts.frontend.app')

@section('title', 'Участки')

@section('styles')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    {{-- max styles --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

@endsection

@section('content')

    {{-- <div class="b-nav-pills__objects">
        <div class="container">
            <ul class="nav b-grid b-grid-3 b-nav-pills">
                <li class="b-grid-item {{ Route::is('lands') ? 'active' : '' }}">
                    <a href="{{ url('lands') }}">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/street-map.png') }}" alt=""></i>
                        @lang('messages.menu_lands_f') | {{ $landCount }}
                    </a>
                </li>
                <li class="b-grid-item {{ Route::is('projects') ? 'active' : '' }}">
                    <a href="{{ url('architecture') }}">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/blueprint.png') }}" alt=""></i>
                        @lang('messages.menu_projects_f') | {{ $architectureCount }}
                    </a>
                </li>
                <li class="b-grid-item {{ Route::is('buildings') ? 'active' : '' }}">
                    <a href="{{ url('buildings') }}">
                        <i class="b-nav-pills__icon"><img src="{{ url('img/filters/construction.png') }}" alt=""></i>
                        @lang('messages.menu_buildings_f') | {{ $buildingCount }}
                    </a>
                </li>
            </ul>
        </div>
    </div> --}}

    {{-- <div class="container">
        {!! Breadcrumbs::render('lands') !!}
    </div> --}}
    <div class="b-view-header">
        <div class="b-view-header__filter">
            <a class="btn btn-primary b-filter-collapse__btn" role="button" data-toggle="collapse" href="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <i class="fa fa-filter b-filter-collapse__icon" aria-hidden="true"></i>Фильтры
                <span class="caret"></span>
            </a>
            <div class="collapse b-filter-collapse__content" id="collapseFilter">
                <div class="b-filter-collapse__inner">
                    <land-filter
                        ref="landfilter"
                        incoming-string="{{ $initParams['requestedCountry'] }}"
                        {{--country-code="{{ $initParams['requestedCountryIsoCode'] }}"--}}
                        price-from="{{ ceil($initParams['priceRange']['min']) }}"
                        price-to="{{ ceil($initParams['priceRange']['max']) }}"
                        price-max="{{ ceil($initParams['priceRange']['maximalPrice']) }}"
                        price-min="{{ ceil($initParams['priceRange']['minimalPrice']) }}"
                        area-from="{{ ceil($initParams['areaRange']['min']) }}"
                        area-to="{{ ceil($initParams['areaRange']['max']) }}"
                        area-max="{{ ceil($initParams['areaRange']['maximalArea']) }}"
                        area-min="{{ ceil($initParams['areaRange']['minimalArea']) }}">
                    </land-filter>
                </div>
            </div>
        </div>
        <div class="b-view-header__map">
            <div class="b-map-checkbox">
                <input type="checkbox" name="checkbox-cats[]" id="view-change" value="1" checked>
                <label for="view-change">Карта</label>
            </div>
        </div>
    </div>
    {{-- <a href="land/2" class="b-list-objects__link b-list-objects__link-slider">
        <div class="swiper-container b-list-objects__slider">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="http://myvillage24.loc/img/uploads/lands//thumbnails/af071df9a812c4ff8a16ec1ea02f48ca.JPG" alt="image" class="b-list-objects-img">
                </div>
                <div class="swiper-slide">
                    <img src="http://myvillage24.loc/img/uploads/lands//thumbnails/af071df9a812c4ff8a16ec1ea02f48ca.JPG" alt="image" class="b-list-objects-img">
                </div>
                <div class="swiper-slide">
                    <img src="http://myvillage24.loc/img/uploads/lands//thumbnails/af071df9a812c4ff8a16ec1ea02f48ca.JPG" alt="image" class="b-list-objects-img">
                </div>
            </div>
            <div class="swiper-pagination"></div>

            <div class="b-list-objects-btn b-list-objects-btn-prev">
                <i aria-hidden="true" class="btn-icon fa fa-angle-left"></i>
            </div>
            <div class="b-list-objects-btn b-list-objects-btn-next">
                <i aria-hidden="true" class="btn-icon fa fa-angle-right"></i>
            </div>
            <a class="btn-favorite">
                <i class="fa fa-bookmark icon icon-on" aria-hidden="true"></i>
                <i class="fa fa-bookmark-o icon icon-off" aria-hidden="true"></i>
            </a>
        </div>
        <div class="b-list-objects__slider">
            <img src="http://myvillage24.loc/img/uploads/lands//thumbnails/af071df9a812c4ff8a16ec1ea02f48ca.JPG" alt="image" class="img-responsive">
        </div>
        <div class="b-list-objects__content">
            <p class="b-list-objects__title">Хынчешты</p>
            <p class="b-list-objects__country">Молдова</p>
            <p class="b-list-objects__area"><span>600 м<sup>2</sup></span> <span>13.33 €/м<sup>2</sup></span></p>
            <ul class="b-list-objects__prices">
                <li><span class="b-list-objects__prices-value">2 400 €</span> <span class="b-list-objects__prices-title">Первый взнос</span></li> <li class="b-list-objects__prices-all">
                    8 000 €
                </li>
            </ul>
        </div>
    </a> --}}
    <div class="b-view-inner">
        <div class="b-view-lands">
            <land-list
                ref="landlist"
                sort-prop="{{ $initParams['sortBy'] or '' }}"
                :limit-prop={{ $initParams['limit'] }}
                :offset={{ ceil($initParams['page']) }}
                :page={{ ceil($initParams['page']) }}
            ></land-list>
        </div>
        <div class="b-view-map">
            <google-map
                ref="landgooglemap"
                id="js-objects-map"
                class="b-objects-map__gm b-view-map__render"
                :center-coords="[{{$countries[$currentCountry]['center']['lat']}}, {{$countries[$currentCountry]['center']['lng']}}]"
                :map-zoom="{{$countries[$currentCountry]['map_zoom']}}"
                show-type="points"
                :countries="{{ $countries }}"
                :enable-geolocation=false>
            </google-map>
            <google-place-popup ref="googlemarkerplacepopup"></google-place-popup>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                {{-- <div class="b-lands-header">
                    <div class="b-lands-header__map">
                        <div class="embed-responsive embed-responsive-21by9">
                            <google-map
                                ref="landgooglemap"
                                id="js-objects-map"
                                class="b-objects-map__gm embed-responsive-item embed-bg-img"
                                :center-coords="[{{$countries[$currentCountry]['center']['lat']}}, {{$countries[$currentCountry]['center']['lng']}}]"
                                :map-zoom="{{$countries[$currentCountry]['map_zoom']}}"
                                show-type="points"
                                :countries="{{ $countries }}"
                                :enable-geolocation=false>
                            </google-map>
                            <google-place-popup ref="googlemarkerplacepopup"></google-place-popup>
                        </div>
                    </div>
                </div> --}}
                {{-- <land-list
                    ref="landlist"
                    sort-prop="{{ $initParams['sortBy'] or '' }}"
                    :limit-prop={{ $initParams['limit'] }}
                    :offset={{ ceil($initParams['page']) }}
                    :page={{ ceil($initParams['page']) }}
                ></land-list> --}}
            </div>
        </div>
    </div>
    @include('layouts.frontend.modal.modal_land_map')
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    {{-- max --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
@endsection
