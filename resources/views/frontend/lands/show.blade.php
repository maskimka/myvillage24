@extends('layouts.frontend.app')

@section('title', 'Партнеры')

@section('styles')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">

@endsection

@section('content')

    <div class="container">
        {!! Breadcrumbs::render('land', $item) !!}
    </div>

    <div class="container">
        <div class="b-object">
            <p class="b-object-title">
                {{ $item->address }}
            </p>
            <p class="b-object-country">{{ $item->country }}</p>
            <div class="row">
                <div class="col-md-7">
                    <div class="b-object-gallery__main b-object-carousel">

                        <a href="{{ url($item->images[0]->path.'/original/'.$item->images[0]->name) }}" class="b-object-carousel__slide-img">
                            <img src="{{ url($item->images[0]->path.'/original/'.$item->images[0]->name) }}" alt="" class="img-responsive b-object-gallery__main-img">
                        </a>
                    </div>
                    <div class="js-product-carousel b-object-carousel">
                        @foreach($item->images as $photo)
                            <div class="b-object-carousel__slide">
                                @include('frontend.partials._image-card', [
                                'original' => $photo->path.'/original/'.$photo->name,
                                'thumb' => $photo->path.'/thumbnails/'.$photo->name,
                                'alt' => $photo->alt
                                ])
                            </div>
                        @endforeach
                        <div class="b-object-carousel__slide">
                            <div class="embed-responsive embed-responsive-16by9 b-object-thumbs__item-video" data-toggle="modal" data-target="#modalLandVideo">
                                <i class="fa fa-youtube-play icon-play" aria-hidden="true"></i>
                                <div class="embed-responsive-item embed-bg-img" style="background-image: url({{ ($item->land_video)?'/img/video/video-placeholder.jpg':'/img/common/no-video-b.png' }});"></div>
                            </div>
                        </div>
                    </div>
                    <p class="b-object-desc">{{ $item->description }}</p>
                    <p class="b-card-desc__title h5 hidden-md hidden-lg">
                        @lang('pages.land_info'):
                    </p>
                    <ul class="b-card-desc__list hidden-md hidden-lg"> 
                        <li class="b-grid b-grid-space-between">
                            <span class="b-card-desc__list-key">@lang('messages.label_land_price')</span>
                            <span class="u-text-color--green-darken"><strong>{{ number_format($item->price, 0, '.', ' ') }} €</strong></span>
                        </li>
                        <li class="b-grid b-grid-space-between">
                            <span class="b-card-desc__list-key">@lang('messages.label_are_price')<sup>2</sup></span>
                            <span class="u-text-color--green text-medium">{{ $item->per_are }} €</span>
                        </li> 
                        <li class="b-grid b-grid-space-between">
                            <span class="b-card-desc__list-key">@lang('messages.label_article')</span>
                            <span>{{ $item->article }}</span>
                        </li>
                        @if(!empty($item->country))
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_country')</span>
                                <span>{{ $item->country }}</span>
                            </li>
                        @endif
                        @if(!empty($item->locality))
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_city')</span>
                                <span>{{ $item->locality }}</span>
                            </li>
                        @endif
                        @if(!empty($item->sublocality))
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_district')</span>
                                <span>{{ $item->sublocality }}</span>
                            </li>
                        @endif
                        @if(!empty($item->area))
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_area')</span>
                                <span>{{ number_format($item->area, 0, '.', ' ') }} {{ trans_choice('messages.label_ares', $item->area) }}<sup>2</sup></span>
                            </li>
                        @endif
                        @if($item->from_city > 0)
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_from_city')</span>
                                <span>{{ number_format($item->from_city, 0, '.', ' ') }} @lang('messages.label_km')</span>
                            </li>
                        @endif
                        @if(!empty($item->purpose))
                            <li class="b-grid b-grid-space-between">
                                <span class="b-card-desc__list-key">@lang('messages.label_land_purpose')</span>
                                <span>@lang('messages.'.$item->purpose->slug)</span>
                            </li>
                        @endif
                        <li class="b-grid b-grid-space-between">
                            <span class="b-card-desc__list-key">@lang('messages.label_cadastral_number')</span>
                            <span>{{ $item->cadastral_number }}</span>
                        </li>
                    </ul>
                    <div class="b-benefits">
                        @if($item->communications->count() > 0)
                            <p class="b-benefits__title">
                                @lang('messages.label_communications')
                            </p>
                            <ul class="b-benefits-list">
                                @foreach($item->communications as $communication)
                                    {{-- <li class="b-benefits-list-item"><i class="fa fa-check b-benefits-list-item__icon"></i>@lang('messages.'.$communication->slug)</li> --}}
                                    <li class="b-benefits-list-item">@lang('messages.'.$communication->slug)</li>
                                @endforeach
                            </ul>
                        @endif
                        @if($item->infrastructure->count() > 0)
                            <p class="b-benefits__title">
                                @lang('messages.label_infrastructure')
                            </p>
                            <ul class="b-benefits-list">
                                @foreach($item->infrastructure as $infrastructure_item)
                                    {{-- <li class="b-benefits-list-item"><i class="fa fa-check b-benefits-list-item__icon"></i>@lang('messages.'.$infrastructure_item->slug)</li> --}}
                                    <li class="b-benefits-list-item">@lang('messages.'.$infrastructure_item->slug)</li>
                                @endforeach
                            </ul>
                        @endif
                        @if($item->transport->count() > 0)
                            <p class="b-benefits__title">
                                @lang('messages.label_transport')
                            </p>
                            <ul class="b-benefits-list">
                                @foreach($item->transport as $transport_item)
                                    {{-- <li class="b-benefits-list-item"><i class="fa fa-check b-benefits-list-item__icon"></i>@lang('messages.'.$transport_item->slug)</li> --}}
                                    <li class="b-benefits-list-item">@lang('messages.'.$transport_item->slug)</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="b-object-price__wrapper">
                        <div class="b-object-price__item">
                            <div><span class="b-object-price__all">{{ number_format($item->price, 0, '.', ' ') }} €</span> <span class="b-object-price__area">/ {{ number_format($item->area, 0, '.', ' ') }} {{ trans_choice('messages.label_ares', $item->area) }}<sup>2</sup></span></div>
                            <div><span class="b-object-price__perarea">{{ $item->per_are }} € / m<sup>2</sup></span> <span class="b-object-price__firstpayment">{{ $item->first_payment }} € - @lang('pages.first_payment')</span></div>
                        </div>
                        <div class="b-object-price__item">
                            <a class="btn-favorite {{ ($item->is_favorite)?'active':'' }}" id="favorite" data-id="{{ $item->id }}" data-type="Land" title="@lang('messages.label_favorite')">
                                {{-- @lang('messages.label_favorite') --}}
                                <i class="fa fa-bookmark icon icon-on" aria-hidden="true"></i>
                                <i class="fa fa-bookmark-o icon icon-off" aria-hidden="true"></i>
                            </a>
                            <a type="button" class="btn-share" data-container="body" data-toggle="popover" data-placement="top" data-trigger="click" title="@lang('messages.label_share')">
                                <i class="fa fa-share-alt icon" aria-hidden="true"></i>
                                {{-- @lang('messages.label_share') --}}
                            </a>
                        </div>
                    </div>
                    <google-map
                            id="js-objects-map"
                            class="b-object-map"
                            :center-coords="[{{ $item->lat }}, {{  $item->lng }}]"
                            land-id="{{ $item->id }}"
                            show-type="points"
                            :enable-geolocation=false>
                    </google-map>
                    <div class="b-object-contacts">
                        <div class="b-object-contacts__item phone"><i class="icon fa fa-mobile" aria-hidden="true"></i><span>+373</span> 79 00 1000</div>
                        <div class="b-object-contacts__item">
                            <button role="button" class="btn b-btn b-btn--color-green b-btn--size-lg b-btn--wide b-btn--rounded-light" data-toggle="modal" data-target="#ordersForm" id="{{ $item->id }}" value="{{ '/'.config('app.locale').'/architecture/order'}}">
                                @lang('messages.label_send_request')
                            </button>
                        </div>
                    </div>
                    <p class="b-card-desc__title hidden-xs hidden-sm">
                        @lang('pages.land_info'):
                    </p>
                    <ul class="b-card-desc__list hidden-xs hidden-sm">  
                        @if(!empty($item->area))
                            <li>
                                <span>@lang('messages.label_area')</span>
                                <span>{{ number_format($item->area, 0, '.', ' ') }} {{ trans_choice('messages.label_ares', $item->area) }}<sup>2</sup></span>
                            </li>
                        @endif
                        <li>
                            <span class="b-card-desc__list-key">ID</span>
                            <span>{{ $item->article }}</span>
                        </li>
                        @if(!empty($item->country))
                            <li>
                                <span>@lang('messages.label_country')</span>
                                <span>{{ $item->country }}</span>
                            </li>
                        @endif
                        @if(!empty($item->locality))
                            <li>
                                <span>@lang('messages.label_city')</span>
                                <span>{{ $item->locality }}</span>
                            </li>
                        @endif
                        @if(!empty($item->sublocality))
                            <li>
                                <span>@lang('messages.label_district')</span>
                                <span>{{ $item->sublocality }}</span>
                            </li>
                        @endif
                        @if($item->from_city > 0)
                            <li>
                                <span>@lang('messages.label_from_city')</span>
                                <span>{{ number_format($item->from_city, 0, '.', ' ') }} @lang('messages.label_km')</span>
                            </li>
                        @endif
                        @if(!empty($item->purpose))
                            <li>
                                <span>@lang('messages.label_land_purpose')</span>
                                <span>@lang('messages.'.$item->purpose->slug)</span>
                            </li>
                        @endif
                        <li>
                            <span>@lang('messages.label_cadastral_number')</span>
                            <span>{{ $item->cadastral_number }}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="l-inner-section">--}}
        {{--<div class="container">--}}
            {{--<div class="l-inner-section__header">--}}
                {{--<div class="b-section-heading">--}}
                    {{--<a href="{{ url('/related_architecture/'.$item->id) }}" class="b-section-heading__title">--}}
                        {{--@lang('messages.label_related_projects')--}}
                        {{--<span class="b-text-label b-text-label--green">--}}
                        {{--{{ $item['relatedArchitecture']->count() }}--}}
                        {{--</span>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="b-object-appropriate">--}}
                {{--<div class="row">--}}
                    {{--@foreach($item['relatedArchitecture'] as $key => $project)--}}
                        {{--@if ($key < 4)--}}
                            {{--<div class="col-xs-6 col-sm-4 col-md-3">--}}
                                {{--<a class="b-object-appropriate__item" href="{{ '/architecture/'.$project->id }}">--}}
                                    {{--<div class="b-card">--}}
                                        {{--<div class="b-card-img">--}}
                                            {{--<div class="embed-responsive embed-responsive-6by5">--}}
                                                {{--<div class="embed-responsive-item embed-bg-img" style="background-image: url({{ url($project->images[0]->path.'/thumbnails/'.$project->images[0]->name) }});"></div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="b-card-desc">--}}
                                            {{--<p class="b-card-desc__title">{{ $project->title }} {{ number_format($project->general_area, 2, '.', ' ') }} m<sup>2</sup></p>--}}
                                            {{--<div class="b-grid b-grid-space-between b-grid-4">--}}
                                                {{--<div class="b-card-desc__item b-grid-item">--}}
                                                    {{--<div class="b-card-desc__item-img" style="background-image: url('/img/common/stairs.png')"></div><span>{{ $project->floors_number }}</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="b-card-desc__item b-grid-item">--}}
                                                    {{--<i class="fa fa-bed b-card-desc__item-icon" aria-hidden="true"></i>{{ $project->rooms_number }}--}}
                                                {{--</div>--}}
                                                {{--<div class="b-card-desc__item b-grid-item">--}}
                                                    {{--<i class="fa fa-bath b-card-desc__item-icon" aria-hidden="true"></i>{{ $project->bathrooms_number }}--}}
                                                {{--</div>--}}
                                                {{--<div class="b-card-desc__item b-grid-item">--}}
                                                    {{--<i class="fa fa-car b-card-desc__item-icon" aria-hidden="true"></i>{{ $project->garages_number }}--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="l-inner-section u-bg-color--gray-lighter">
        <div class="container">
            <div class="architecture-list-wrapper">
                <architecture-list
                        ref="relatedArchitecture"
                        :limit="4"
                        title="@lang('messages.label_related_projects')"
                        related_to="{{ $item->id }}"
                ></architecture-list>
            </div>
        </div>
    </div>
    @include('layouts.frontend.modal.modal_land_video')
    @include('layouts.frontend.modal.order_modal')
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="{!! asset('js/plugins/validate/jquery.validate.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/clipboard/clipboard.min.js') !!}" type="text/javascript"></script>
@endsection
