<div class="b-image-card">
    <a href="{{ url($original) }}" class="embed-responsive embed-responsive-16by9 b-image-card__cover" title="{{ (isset($alt) ? $alt : null) }}" style="background-image: url({{ url($thumb) }})">

    </a>
</div>