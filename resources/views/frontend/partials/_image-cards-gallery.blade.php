{{-- <div class="b-section-heading ">
    <p class="b-section-heading__title u-text-color--green-darken">
        {{ $title }}
    </p>
</div> --}}
<div class="b-benefits">
    <p class="b-benefits__title">{{ $title }}</p>
</div>
<div class="b-image-cards-grid js-image-cards-gallery">
    <div class="b-image-cards-grid__inner">
        @foreach($images as $i => $photo)
            @if($loop->index < 4)

                <div class="b-image-cards-grid__item">

                    @include('frontend.partials._image-card', [
                    'original' => $photo->path.'/original/'.$photo->name,
                    'thumb' => $photo->path.'/thumbnails/'.$photo->name,
                    ])

                </div>

            @endif

        @endforeach

    </div>
</div>
