<div id="wl-success-alert" class="event_result alert-success" role="alert">
    @lang('messages.done')
</div>
<div id="wl-danger-alert" class="event_result alert-danger" role="alert">
    @lang('messages.add_favorites_error')
</div>