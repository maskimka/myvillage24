{{-- <div class="b-section-heading ">
    <p class="b-section-heading__title u-text-color--green-darken">
        {{ $title }}
    </p>
</div> --}}
<div class="b-section-heading__home">
    <div class="b-card-desc__title h4">
        {{ $title }}
    </div>
    <div class="b-section-home__nav">
        <div class="b-list-lands-prev b-swiper-btn"><i class="icon fa fa-angle-left" aria-hidden="true"></i></div>
        <div class="b-list-lands-next b-swiper-btn"><i class="icon fa fa-angle-right" aria-hidden="true"></i></div>
    </div>
</div>
<div class="b-list-objects">
    <div class="swiper-container b-list-lands__container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            @foreach($images as $i => $photo)
                <div class="swiper-slide">

                    <a href="{{ url($photo->path.'/original/'.$photo->name) }}" class="embed-responsive embed-responsive-5by3">
                        <span class="embed-responsive-item embed-bg-img" style="background-image: url({{ url($photo->path.'/thumbnails/'.$photo->name) }})"></span>
                        {{-- <div class="b-list-objects__img">
                            <img src="{{ url(isset($land->images[0])?$land->images[0]->path.'/thumbnails/'.$land->images[0]->name:'#') }}" alt="image" class="img-responsive">
                        </div> --}}
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
{{-- <div class="b-image-cards-grid b-image-cards-design js-image-cards-gallery">
    <div class="b-image-cards-grid__inner">
        @foreach($images as $i => $photo)
            @if($loop->index < 4)

                <div class="b-image-cards-grid__item">

                    @include('frontend.partials._image-card', [
                    'original' => $photo->path.'/original/'.$photo->name,
                    'thumb' => $photo->path.'/thumbnails/'.$photo->name,
                    ])

                </div>

            @endif

        @endforeach

    </div>
</div> --}}
