@extends('layouts.frontend.app')

@section('title', 'Главная')

@section('meta')
    @parent

    <meta property="og:title"              content="В нашем каталоге более 800 проектов" />
    <meta property="og:description"        content="При заказе строительства дома в My Village - мы подарим вам архитектурный проект!" />
    <meta property="og:image"              content="{{ url('/img/tags/meta888.jpg') }}" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:locale:alternate" content="ro_RO" />

@endsection

@section('styles')

    @parent
    {{-- max styles --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

@endsection

@section('content')
    <div class="container">
        {!! Breadcrumbs::render('wishlist') !!}
    </div>
    <div class="container">
        <wishlist></wishlist>
    </div>
    <google-place-popup
            ref="googlemarkerplacepopup">
    </google-place-popup>
    @include('modals.wishlist_order')
    @include('layouts.frontend.modal.modal_video')
    @include('layouts.frontend.modal.modal_map')
    @include('layouts.frontend.modal.modal_land_map')

@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
@endsection
