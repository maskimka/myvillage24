<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MyVillage - @yield('title') </title>
    <link rel="icon" type="image/jpg" href="/img/favicon.jpg">
    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/plugins/iCheck/custom.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/plugins/chosen/bootstrap-chosen.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/myvillage.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/frontend/bootstrap-social.css') !!}" />
    <script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
</head>
<body class="i18_links white-bg">
    <div class="middle-box loginscreen  animated fadeInDown">
        <div>
            <div class="text-center">
                <a href="/" class="b-logo">
                    <img src="{{ url('img/admin-logo.png') }}" class="img-responsive">
                </a>
                <h3>@yield('auth_h3_title')</h3>
                <p>@yield('auth_p_title')</p>
            </div>
            <!-- Content  -->
            @yield('content')
        </div>
    </div>
    <!-- Footer -->
    @include('layouts/authentication.footer')

<script src="{!! asset('js/plugins/iCheck/icheck.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/plugins/chosen/chosen.jquery.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/auth/registration.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/plugins/validate/jquery.validate.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/plugins/validate/additional-methods.js') !!}" type="text/javascript"></script>

@section('scripts')
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.chosen-select').chosen();

            $('button[type="submit"]').on('click', function(){
                $(this).find('i.fa-spinner').removeClass('hidden');
            });
            /* languages */
            // $('#language_links li a').click(function(){
            //     var locale = $(this).attr('href');
            //     var _token = $("input[name='_token']").val();
            //
            //     $.ajax({
            //         url: "/language",
            //         type: "POST",
            //         data: {locale:locale, _token:_token},
            //         dataType:'json',
            //         success: function(response){
            //
            //         },
            //         error: function(response){
            //
            //         },
            //         complete: function(){
            //            window.location.reload(true);
            //         }
            //     });
            //
            //     return false;
            // })

        });
    </script>
@show

</body>
</html>
