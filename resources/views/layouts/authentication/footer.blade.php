<div class="footer auth">
    <div class="">
        <ul id="language_links">
            @foreach(config('locales') as $locale => $enabled)
                @if($enabled)
                    <li>
                        <?php $segments = request()->segments()?>
                        <?php $len = array_key_exists($segments[1], config('locales')) ? 1 : 0 ?>
                        <?php array_splice($segments, 1, $len, $locale)?>
                        @if(session('admin_locale') !== $locale)
                            <a href="{{ '/'.implode('/', $segments) }}">{{ trans('messages.label_'.$locale, [], null, session('admin_locale')) }}</a>
                        @else
                            <span>{{ trans('messages.label_'.$locale, [], null, session('admin_locale')) }}</span>
                        @endif
                    </li>
                @endif
            @endforeach
        </ul>
        {{csrf_field()}}
    </div>
</div>
