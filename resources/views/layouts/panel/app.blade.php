<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>MyVillage - @yield('title') </title>

    <link rel="icon" type="image/jpg" href="/img/favicon.jpg">
    @section('styles')
        <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/plugins/steps/jquery.steps.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/plugins/iCheck/custom.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/plugins/datapicker/datepicker3.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/plugins/chosen/bootstrap-chosen.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/plugins/toastr/toastr.min.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/plugins/footable/footable.bootstrap.min.css') !!}" />
        <link rel="stylesheet" href="{!! asset('css/plugins/sweetalert/sweetalert.css') !!}" />
        @if( in_array($controller, array('LandController', 'ArchitectureController')))
            <link rel="stylesheet" href="{!! asset('css/plugins/uploader/jquery.fileuploader.css') !!}" />
            <link rel="stylesheet" href="{!! asset('css/plugins/uploader/jquery.fileuploader-thumb.css') !!}" />
        @endif
    @show

    <link rel="stylesheet" href="{!! asset('css/myvillage.css') !!}" />
    <script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>

</head>
<body class="i18_links {{($action == 'edit') ? "mini-navbar":""}}">

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        @include('layouts/panel.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            @include('layouts/panel.topnavbar')

            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('layouts/panel.footer')

        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

@section('scripts')

    <script src="{!! asset('/js/messages.js') !!}"></script>
    <script src="{!! asset('js/plugins/session/jquery.session.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/validate/jquery.validate.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/validate/additional-methods.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/steps/jquery.steps.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/iCheck/icheck.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/datapicker/bootstrap-datepicker.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/chosen/chosen.jquery.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/toastr/toastr.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/inputmask/jquery.inputmask.bundle.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/footable/footable.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/sweetalert/sweetalert.min.js') !!}" type="text/javascript"></script>

    @if ($controller === 'ProfileController')
        <script src="{!! asset('js/profile/profile.js') !!}" type="text/javascript"></script>
    @endif
    @if ($controller === 'LandController' && $action == "edit")
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrjopciGqx6HdT_73a9_zEJMN_bVEHCYQ&libraries=places&language={{strtolower($user->country->iso2code)."-".$user->country->iso2code}}"></script>
        {{--<script src="{!! asset('js/my.google.map.js') !!}" type="text/javascript"></script>--}}
        <script src="{!! asset('js/land/google_map.js') !!}" type="text/javascript"></script>
    @endif
@show
  <script type="text/javascript">
      window['masks'] = {!! $masks !!};
      window['user_country'] = {!! $user->country_id !!};
      window['namespace_path'] = <?php echo json_encode($namespace); ?>;
  </script>
  @if ($controller === 'ArchitectureController')
      <script src="{!! asset('js/plugins/uploader/jquery.fileuploader.js') !!}" type="text/javascript"></script>
      <script src="{!! asset('js/architecture/architecture.js') !!}" type="text/javascript"></script>
      <script src="{!! asset('js/architecture/architecture_uploader.js') !!}" type="text/javascript"></script>
  @endif
  @if ($controller === 'LandController')
      <script src="{!! asset('js/land/land.js') !!}" type="text/javascript"></script>
      <script src="{!! asset('js/plugins/uploader/jquery.fileuploader.js') !!}" type="text/javascript"></script>
      <script src="{!! asset('js/land/land_uploader.js') !!}" type="text/javascript"></script>
  @endif
  <script src="{!! asset('js/masks.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('js/panel.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('js/bundle.js') !!}"></script>
</body>
</html>
