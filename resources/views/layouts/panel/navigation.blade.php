<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span>
                                @if($avatar = $user->avatar)
                                    <img class="img-circle img-md"
                                         src="{{asset(config('custom.PROFILE_AVATAR_PATH').$avatar)}}" alt="image">
                                @else
                                    <img class="img-circle img-md"
                                         src="{{asset(config('custom.PROFILE_AVATAR_PATH').'anonym.jpg')}}" alt="image">
                                @endif
                            </span>
                            <span class="block m-t-xs">
                                <strong class="font-bold">
                                    {{$user->first_name}}&nbsp;
                                    {{$user->last_name}}
                                </strong>
                            </span> <span data-i18n="admin.{{ $user->roles()->first()->slug }}" class="text-muted text-xs block">
                                {{trans('messages.'.$user->roles()->first()->slug, [], null, $adminLang)}}
                                <b class="caret"></b>
                            </span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li>
                            <a data-i18n="admin.profile" href="/panel/profile">
                                {{trans('messages.label_profile', [], null, $adminLang)}}
                            </a>
                        </li>
                        <li>
                            <a data-i18n="admin.logout" href="#" class="logout" onclick="document.getElementById('logout_form').submit();">
                                {{trans('messages.label_logout', [], null, $adminLang)}}
                            </a>
                        </li>
                    </ul>
                    <form id="logout_form" action="/auth/logout" method="post">
                        {{csrf_field()}}
                    </form>
                </div>
                <div class="logo-element">
                    MV+
                </div>
            </li>
            <li class="{{ isActiveRoute('panel') }}">
                <a href="{{ url('/panel/dashboard') }}"><i class="fa fa-th-large"></i>
                    <span data-i18n="admin.menu_dashboard" class="nav-label">
                        {{trans('messages.menu_dashboard', [], null, $adminLang)}}
                    </span>
                </a>
            </li>
            @if ($user->inRole('realtor') || $user->inRole('admin'))
            <li class="{{ isActiveRoute('/panel/land/create') }}{{ isActiveRoute('/panel/land') }}">
                <a href="#">
                    <i class="fa fa-globe"></i>
                    <span data-i18n="admin.menu_land" class="nav-label">{{trans('messages.menu_land', [], null, $adminLang)}}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" style="">
                    <li class="{{ isActiveRoute('/panel/land/create') }}">
                        <a data-i18n="admin.menu_create" href="#" data-model="land" id="" data-toggle="modal" data-target="#modalChoseLang">
                            <i class="fa fa-plus"></i>
                            {{trans('messages.menu_create', [], null, $adminLang)}}
                        </a>
                    </li>
                    <li class="{{ isActiveRoute('/panel/land') }}">
                        <a data-i18n="admin.menu_create" href="/panel/land">
                            <i class="fa fa-list-alt"></i>
                            {{trans('messages.menu_show', [], null, $adminLang)}}
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            @if ($user->inRole('architect') || $user->inRole('admin'))
            <li class="{{ isActiveRoute('/panel/architecture') }}">
                <a href="{{ url('/panel/posts') }}">
                    <i class="fa fa-th-large"></i>
                    <span data-i18n="admin.menu_architecture" class="nav-label">{{trans('messages.menu_architecture', [], null, $adminLang)}}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" style="">
                    <li class="{{ isActiveRoute('/panel/architecture/create') }}">
                        <a data-i18n="admin.menu_create" href="#" data-model="architecture" id="" data-toggle="modal" data-target="#modalChoseLang">
                            <i class="fa fa-plus"></i>
                            {{trans('messages.menu_create', [], null, $adminLang)}}
                        </a>
                    </li>
                    <li class="{{ isActiveRoute('/panel/architecture') }}">
                        <a data-i18n="admin.menu_show" href="/panel/architecture">
                            <i class="fa fa-list-alt"></i>
                            {{trans('messages.menu_show', [], null, $adminLang)}}
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            <li class="{{ isActiveRoute('/panel/wishlist') }}">
                <a data-i18n="admin.menu_favorite" href="/panel/wishlist">
                    <i class="fa fa-heart"></i>
                    <span class="nav-label">{{ trans('messages.menu_favorite', [], null, $adminLang) }}</span>
                </a>
            </li>
            <li class="landing_link">
                <a data-i18n="admin.start_work" href="/panel/profile/roles">
                    <i class="fa fa-hand-o-up"></i>
                    <span class="nav-label">{{ trans('messages.start_work', [], null, $adminLang) }}</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
@include('modals.modal_chose_lang')
