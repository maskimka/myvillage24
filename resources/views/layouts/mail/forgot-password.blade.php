<html>
<head>
    <title>Восстонавление пароля</title>
</head>
<body>
<h1>Восстонавление пароля</h1>
<h3>Здравствуйте, {{$notifiable->first_name}}&nbsp;{{$notifiable->last_name}}</h3>
<p>
   Пожалуйста перейдите по ссылке, чтобы восстановить пароль,
    <a href="{{env('APP_URL')}}/auth/reset/{{$notifiable->id}}/{{$code}}">восстановить пароль</a>
</p>
</body>
</html>