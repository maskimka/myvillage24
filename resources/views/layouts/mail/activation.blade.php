<html>
<head>
    <title>Activation email</title>
</head>
<body>
    <h1>Activation an account</h1>
    <h3>Hello {{$notifiable->first_name}}&nbsp;{{$notifiable->last_name}}</h3>
    <p>
        Please click the following link to activate your account,
        <a href="{{env('APP_URL')}}/auth/activation/{{$notifiable->id}}/{{$code}}">activate account</a>
    </p>
</body>
</html>