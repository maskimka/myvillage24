<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MyVillage - @yield('title') </title>

    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/myvillage.css') !!}" />
</head>
<body class="gray-bg">
    <div class="middle-box text-center animated fadeInDown">
        <div>
            <div class="text-center">
                <h1 class="logo-name">MV+</h1>
                <h3>@yield('auth_h3_title')</h3>
                <p>@yield('auth_p_title')</p>
            </div>
            <!-- Content  -->
            @yield('content')
        </div>
    </div>

<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/plugins/iCheck/icheck.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/plugins/chosen/chosen.jquery.js') !!}" type="text/javascript"></script>

</body>
</html>
