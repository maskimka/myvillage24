<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span>
                                @if($avatar = $user->avatar)
                                    <img class="img-circle img-md" src="{{asset(config('custom.PROFILE_AVATAR_PATH').$avatar)}}" alt="image">
                                @else
                                    <img class="img-circle img-md" src="{{asset(config('custom.PROFILE_AVATAR_PATH').'anonym.jpg')}}" alt="image">
                                @endif
                            </span>
                            <span class="block m-t-xs">
                                <strong class="font-bold">
                                    {{ $user->first_name }}&nbsp;
                                    {{ $user->last_name }}
                                </strong>
                            </span> <span data-i18n="admin.{{ $user->roles()->first()->slug }}" class="text-muted text-xs block">
                                {{ trans('messages.'.$user->roles()->first()->slug, [], null, $adminLang) }}
                                <b class="caret"></b>
                            </span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li>
                            <a data-i18n="admin.profile" href="/admin/profile">
                                {{ trans('messages.label_profile', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li>
                            <a data-i18n="admin.logout" href="#" class="logout" onclick="document.getElementById('logout_form').submit();">
                                {{ trans('messages.label_logout', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                    <form id="logout_form" action="/auth/logout" method="post">
                        {{csrf_field()}}
                    </form>
                </div>
                <div class="logo-element">
                    MV+
                </div>
            </li>
            <li class="{{ isActiveRoute('admin') }}">
                <a href="{{ url('/admin/dashboard') }}"><i class="fa fa-th-large"></i>
                    <span data-i18n="admin.menu_dashboard" class="nav-label">
                        {{ trans('messages.menu_dashboard', [], null, $adminLang) }}
                    </span>
                </a>
            </li>
            @if (
                $user->inRole('admin') ||
                $user->inRole('manager') ||
                ($user->inRole('realtor') && $user->inRole('moderator'))
            )
                <li class="{{ isActiveRoute('/admin/land/create') }}{{ isActiveRoute('/admin/land') }}">
                    <a href="#">
                        <i class="fa fa-globe"></i>
                        <span data-i18n="admin.menu_land" class="nav-label">{{ trans('messages.menu_land', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/land/create') }}">
                            <a data-i18n="admin.menu_create" class="addRegistration" href="#" data-model="land" id="" data-toggle="modal" data-target="#modalChoseLang">
                                <i class="fa fa-plus"></i>
                                {{ trans('messages.menu_create', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/land') }}">
                            <a data-i18n="admin.menu_show" href="/admin/land">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                        @if ($user->inRole('admin'))
                            <li class="{{ isActiveRoute('/admin/seo/land') }}">
                                <a data-i18n="admin.show_seo" href="/admin/seo/land">
                                    <i class="fa fa-list-alt"></i>
                                    {{ trans('messages.show_seo', [], null, $adminLang) }}
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            @if($user->inRole('seo_manager'))
                <li class="{{ isActiveRoute('/admin/land') }}">
                    <a href="#">
                        <i class="fa fa-globe"></i>
                        <span data-i18n="admin.menu_land" class="nav-label">{{ trans('messages.menu_land', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/land') }}">
                            <a data-i18n="admin.menu_show" href="/admin/seo/land">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="{{ isActiveRoute('/admin/land/architecture') }}">
                    <a href="#">
                        <i class="fa fa-home"></i>
                        <span data-i18n="admin.menu_architecture" class="nav-label">{{ trans('messages.menu_architecture', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/architecture') }}">
                            <a data-i18n="admin.menu_show" href="/admin/seo/architecture">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="{{ isActiveRoute('/admin/land/building') }}">
                    <a href="#">
                        <i class="fa fa-building-o"></i>
                        <span data-i18n="admin.menu_buildings" class="nav-label">{{ trans('messages.menu_buildings', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/building') }}">
                            <a data-i18n="admin.menu_show" href="/admin/seo/building">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if (
                $user->inRole('admin') ||
                $user->inRole('manager') ||
                ($user->inRole('architect') && $user->inRole('moderator'))
            )
                <li class="{{ isActiveRoute('/admin/architecture') }}">
                    <a href="{{ url('/admin/architecture') }}">
                        <i class="fa fa-home"></i>
                        <span data-i18n="admin.menu_architecture" class="nav-label">{{ trans('messages.menu_architecture', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/architecture/create') }}">
                            <a data-i18n="admin.menu_create" href="#" data-model="architecture" id="" data-toggle="modal" data-target="#modalChoseLang">
                                <i class="fa fa-plus"></i>
                                {{ trans('messages.menu_create', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/architecture') }}">
                            <a data-i18n="admin.menu_show" href="/admin/architecture">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if (
                $user->inRole('admin') ||
                $user->inRole('manager') ||
                ($user->inRole('architect') && $user->inRole('moderator'))
            )
                <li class="{{ isActiveRoute('/admin/building') }}">
                    <a href="{{ url('/admin/building') }}">
                        <i class="fa fa-building-o"></i>
                        <span data-i18n="admin.menu_buildings" class="nav-label">{{ trans('messages.menu_buildings', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/building/create') }}">
                            <a data-i18n="admin.menu_create" href="#" data-model="building" id="" data-toggle="modal" data-target="#modalChoseLang">
                                <i class="fa fa-plus"></i>
                                {{ trans('messages.menu_create', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/building') }}">
                            <a data-i18n="admin.menu_show" href="/admin/building">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if (
                $user->inRole('admin') ||
                $user->inRole('manager') ||
                $user->inRole('moderator')
            )
                <li class="{{ isActiveRoute('/admin/village') }}">
                    <a href="{{ url('/admin/village') }}">
                        <i class="fa fa-building-o"></i>
                        <span data-i18n="admin.menu_villages" class="nav-label">{{ trans('messages.menu_villages', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/village/create') }}">
                            <a href="#" data-model="village" id="" data-toggle="modal" data-target="#modalChoseLang">
                                <i class="fa fa-plus"></i>
                                {{ trans('messages.menu_create', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/village') }}">
                            <a data-i18n="admin.menu_show" href="/admin/village">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            <li class="{{ isActiveRoute('/admin/wishlist') }}">
                <a href="/admin/wishlist">
                    <i class="fa fa-heart"></i>
                    <span data-i18n="admin.menu_favorite" class="nav-label">{{ trans('messages.menu_favorite', [], null, $adminLang) }}</span>
                </a>
            </li>
            @if ($user->inRole('manager') || $user->inRole('admin'))
                <li class="{{ isActiveRoute('/admin/order') }}">
                    <a href="/admin/order">
                        <i class="fa fa-envelope-open-o"></i>
                        <span data-i18n="admin.orders" class="nav-label">{{ trans('messages.menu_orders', [], null, $adminLang) }}</span>
                    </a>
                </li>
            @endif
            @if ($user->inRole('manager') || $user->inRole('admin'))
                <li class="{{ isActiveRoute('/admin/pto_request') }}">
                    <a href="/admin/pto_request">
                        <i class="fa fa-calculator"></i>
                        <span data-i18n="admin.pto_requests" class="nav-label">{{ trans('messages.pto_requests', [], null, $adminLang) }}</span>
                    </a>
                </li>
            @endif
            @if ($user->inRole('admin'))
                <li class="{{ isActiveRoute('/admin/users') }}">
                    <a href="{{ url('/admin/users') }}">
                        <i class="fa fa-user"></i>
                        <span data-i18n="admin.users" class="nav-label">{{ trans('messages.menu_users', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/users/create') }}">
                            <a data-i18n="admin.menu_create" href="/admin/users/create">
                                <i class="fa fa-plus"></i>
                                {{ trans('messages.menu_create', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/users') }}">
                            <a data-i18n="admin.menu_show" href="/admin/users">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if ($user->inRole('admin'))
                <li class="{{ isActiveRoute('/admin/roles') }}">
                    <a href="{{ url('/admin/roles') }}">
                        <i class="fa fa-users"></i>
                        <span data-i18n="admin.roles" class="nav-label">{{ trans('messages.menu_roles', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/roles/create') }}">
                            <a data-i18n="admin.menu_create" href="/admin/roles/create">
                                <i class="fa fa-plus"></i>
                                {{ trans('messages.menu_create', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/roles') }}">
                            <a data-i18n="admin.menu_show" href="/admin/roles">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if ($user->inRole('admin') || $user->inRole('seo_manager'))
                <li class="{{ isActiveRoute('/admin/seo') }}">
                    <a href="{{ url('/admin/seo') }}">
                        <i class="fa fa-paste"></i>
                        <span data-i18n="admin.seo_templates" class="nav-label">{{ trans('messages.seo_templates', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/seo/create') }}">
                            <a data-i18n="admin.menu_create" href="/admin/seo/create">
                                <i class="fa fa-plus"></i>
                                {{ trans('messages.menu_create', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/seo') }}">
                            <a data-i18n="admin.menu_show" href="/admin/seo">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if ($user->inRole('admin'))
                <li class="{{ isActiveRoute('/admin/pages') }}">
                    <a href="{{ url('/admin/pages') }}">
                        <i class="fa fa-file-o"></i>
                        <span data-i18n="admin.pages" class="nav-label">{{ trans('messages.pages', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/pages/create') }}">
                            <a data-i18n="admin.menu_create" href="/admin/pages/create">
                                <i class="fa fa-plus"></i>
                                {{ trans('messages.menu_create', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/pages') }}">
                            <a data-i18n="admin.menu_show" href="/admin/pages">
                                <i class="fa fa-list-alt"></i>
                                {{ trans('messages.menu_show', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if ($user->inRole('admin') || $user->inRole('moderator'))
                <li class="{{ isActiveRoute('/admin/config_general') }}">
                    <a href="/admin/config_general">
                        <i class="fa fa-cog"></i>
                        <span data-i18n="admin.settings" class="nav-label">{{ trans('messages.settings', [], null, $adminLang) }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ isActiveRoute('/admin/config_general') }}">
                            <a data-i18n="admin.common" href="/admin/config_general">
                                <i class="fa fa-wrench"></i>
                                {{ trans('messages.common', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/config/land') }}">
                            <a data-i18n="admin.menu_land" href="/admin/config/land">
                                <i class="fa fa-globe"></i>
                                {{ trans('messages.menu_land', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/config/architecture') }}">
                            <a data-i18n="admin.menu_architecture" href="/admin/config/architecture">
                                <i class="fa fa-home"></i>
                                {{ trans('messages.menu_architecture', [], null, $adminLang) }}
                            </a>
                        </li>
                        <li class="{{ isActiveRoute('/admin/config/building') }}">
                            <a data-i18n="admin.menu_buildings" href="/admin/config/building">
                                <i class="fa fa-building-o"></i>
                                {{ trans('messages.menu_buildings', [], null, $adminLang) }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            <li class="landing_link">
                <a href="/admin/profile/roles">
                    <i class="fa fa-hand-o-up"></i>
                    <span data-i18n="admin.start_work" class="nav-label">{{ trans('messages.start_work', [], null, $adminLang) }}</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
@include('modals.modal_chose_lang')
