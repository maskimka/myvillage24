<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" method="post" action="/">
                <div class="form-group">
                    <input data-i18n="[placeholder]admin.search_smth" type="text" name="top-search" id="top-search"
                           placeholder="{{ trans('messages.search_smth', [], null, $adminLang) }}" class="form-control"/>
                </div>
            </form>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            {{--<li><a class="btn btn-white set_lang" data-lang="en">EN</a></li>--}}
            {{--<li><a class="btn btn-white set_lang" data-lang="ru">RU</a></li>--}}
            <li><a data-i18n="admin.on_site" href="/" target="_blank">{{ trans('messages.on_site', [], null, $adminLang) }}</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i>  <span class="label label-warning">0</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    {{--<li>--}}
                    {{--<div class="dropdown-messages-box">--}}
                    {{--<a href="profile.html" class="pull-left">--}}
                    {{--<img alt="image" class="img-circle" src="img/a7.jpg">--}}
                    {{--</a>--}}
                    {{--<div class="media-body">--}}
                    {{--<small class="pull-right">46h ago</small>--}}
                    {{--<strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>--}}
                    {{--<small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="divider"></li>--}}
                    <li>
                        <div class="text-center link-block">
                            <a href="#">
                                <strong data-i18n="admin.show_all">{{ trans('messages.show_all', [], null, $adminLang) }}</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i>  <span class="label label-primary">0</span>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    {{--<li>--}}
                    {{--<a href="mailbox.html">--}}
                    {{--<div>--}}
                    {{--<i class="fa fa-envelope fa-fw"></i> You have 16 messages--}}
                    {{--<span class="pull-right text-muted small">4 minutes ago</span>--}}
                    {{--</div>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="divider"></li>--}}
                    <li>
                        <div class="text-center link-block">
                            <a href="notifications.html">
                                <strong data-i18n="admin.show_all">{{ trans('messages.show_all', [], null, $adminLang) }}</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <a data-i18n="admin.logout" href="#" class="logout" onclick="document.getElementById('logout_form').submit()">
                    <i class="fa fa-sign-out"></i> {{ trans('messages.label_logout', [], null, $adminLang) }}
                </a>
                <form id="logout_form" action="/auth/logout" method="post">
                {{csrf_field()}}
                </form>
            </li>
        </ul>
    </nav>
</div>
