<div class="modal fade" id="modalMainVideo" tabindex="-1">
    <div class="modal-dialog modal-dialog-container">
        <div class="modal-content">
            <div class="modal-header">
                My<span class="u-color--green">Villa</span>ge
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/OEoo6zBVW2k"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>