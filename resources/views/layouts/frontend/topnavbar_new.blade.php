<div class="b-topnav">
    <ul class="b-topnav-list">
        <li>
            <a href="#"><i class="b-topnav-list__icon fa fa-map-marker"></i>@lang('pages.map_representative')</a>
        </li>
        <li>
            <a href="/about">@lang('pages.btn_about_project')</a>
        </li>
        <li>
            <a href="{{ url('services') }}">@lang('pages.services')</a>
        </li>
    </ul>
    <ul class="b-topnav-list">
        <li>
            <a href="/realtor">@lang('pages.partners_realtor')</a>
        </li>
        <li>
            <a href="/architect">@lang('pages.partners_architect')</a>
        </li>
        <li>
            <a href="/construction">@lang('pages.partners_construction')</a>
        </li>
    </ul>
</div>
<nav class="b-navbar">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed b-navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand b-navbar-logo" href="{{ url('/') }}">
            <img src="{{ url('img/logo.svg') }}" alt="Logo">
        </a>
    </div>
    <div class="collapse navbar-collapse b-navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul id="googleTranslateBar" class="nav navbar-nav b-navbar-nav">
        <li><a href="/lands">@lang('messages.menu_lands_f')</a></li>
        <li><a href="/architecture">@lang('messages.menu_projects_f')</a></li>
        <li><a href="/architecture">@lang('messages.menu_modal_f')</a></li>
        <li><a href="/buildings">@lang('messages.menu_buildings_f')</a></li>
      </ul>
      <ul class="nav navbar-nav b-navbar-nav navbar-right">
        <li>
            <a href="{{ url('terms/land') }}" class="b-navbar-link__add">
                <i class="b-navbar-icon-add"><img src="{{ url('img/icons/add-icon.png') }}" alt=""></i>
                @lang('messages.label_add_land')
            </a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="{{ url('img/common/lang/'.App::getLocale().'.png') }}" class="lang-icon">
          </a>
          <ul class="dropdown-menu b-dropdown-menu">
            <?php $segments = Request::segments(); unset($segments[0])?>
            @foreach(config('locales') as $locale => $enabled)
                @if($enabled)
                    <li>
                        <a class="lang-link" href="#" data-locale="{{ $locale }}">
                            <img src="{{ url('img/common/lang/'.$locale.'.png') }}" class="lang-icon"> {{ strtoupper($locale) }}
                        </a>
                    </li>
                    <li role="separator" class="divider"></li>
                @endif
            @endforeach
          </ul>
        </li>
        <li class="dropdown">
            {{-- <a href="#"><img src="{{ url('img/icons/user-icon.png') }}" alt=""></a> --}}
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="{{ url('img/icons/user-icon.png') }}" alt="" class="user-icon">
            </a>
            <ul class="dropdown-menu b-dropdown-menu">
                <li>
                    <a href="/auth/login">@lang('messages.menu_login')</a>
                </li>
                <li role="separator" class="divider"></li>
                <li>
                    <a href="/auth/registration">@lang('messages.menu_registration')</a>
                </li>
                <li role="separator" class="divider"></li>
                <li>
                    <a href="/{{ App::getLocale() }}/wishlist">
                        @lang('messages.menu_favorite')
                        <i class="text-divider"></i>
                        <span id="wishlistCountItems" class="u-text-color--green">
                            {{ request()->wishlistCountItems }}
                        </span>
                    </a>
                </li>
            </ul>
        </li>
      </ul>
    </div>
    {{-- <button type="button" class="navbar-toggle collapsed b-navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button> --}}
</nav>