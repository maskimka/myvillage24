<ul id="language_links">
    <li>
        @if(Lang::getLocale() !== "en")
            <a href="en">English</a>
        @else
            <span>English</span>
        @endif
    </li>
    <li>
        @if(Lang::getLocale() !== "ru")
            <a href="ru">Русский</a>
        @else
            <span>Русский</span>
        @endif
    </li>
    <li>
        @if(Lang::getLocale() !== "ro")
            <a href="ro">Româna</a>
        @else
            <span>Româna</span>
        @endif
    </li>
    <li>
        @if(Lang::getLocale() !== "es")
            <a href="es">Español</a>
        @else
            <span>Español</span>
        @endif
    </li>
</ul>
{{csrf_field()}}