<div class="b-sidebar">
    <div class="b-sidebar__header">
        <div class="b-sidebar__close">
            <button type="button" class="btn b-btn b-btn--color-green-light b-btn--transparent b-btn--rect js-close-sidebar">
                <i class="fa fa-times" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <ul class="b-sidebar-menu">
        <li class="b-sidebar-menu__item">
            <a href="/auth/login" class="b-sidebar-menu__link">
                <i class="b-sidebar__icon">
                    <img src="{{ url('img/sidebar/user.png') }}" alt="login">
                </i>
                @lang('messages.menu_login')
            </a>
        </li>
        {{-- <li class="b-sidebar-menu__item">
            <a href="/auth/registration" class="b-sidebar-menu__link">
                <i class="b-sidebar__icon">
                    <img src="{{ url('img/filters/street-map.png') }}" alt="lands icon">
                </i>
                @lang('messages.menu_registration')
            </a>
        </li> --}}
        <li class="b-sidebar-menu__item">
            <a href="/{{ App::getLocale() }}/wishlist" class="b-sidebar-menu__link">
                <i class="b-sidebar__icon">
                    <img src="{{ url('img/sidebar/heart.png') }}" alt="lands icon">
                </i>
                @lang('messages.menu_favorite')
                <i class="text-divider"></i>
                <span id="wishlistCountItems" class="u-text-color--green">
                    {{ request()->wishlistCountItems }}
                </span>
            </a>
        </li>
        <hr>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('lands') }}" class="b-sidebar-menu__link">
                <i class="b-sidebar__icon">
                    <img src="{{ url('img/filters/street-map.png') }}" alt="lands icon">
                </i>
                @lang('messages.menu_lands_f')
            </a>
        </li>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('architecture') }}" class="b-sidebar-menu__link">
                <i class="b-sidebar__icon">
                    <img src="{{ url('img/filters/blueprint.png') }}" alt="lands icon">
                </i>
                @lang('messages.menu_projects_f')
            </a>
        </li>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('buildings') }}" class="b-sidebar-menu__link">
                <i class="b-sidebar__icon">
                    <img src="{{ url('img/filters/construction.png') }}" alt="lands icon">
                </i>
                @lang('messages.menu_buildings_f')
            </a>
        </li>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('terms/land') }}" class="b-sidebar-menu__link">
                <i class="b-sidebar__icon">
                    <img src="{{ url('img/sidebar/01.svg') }}" alt="lands icon">
                </i>
                @lang('pages.add_land')
            </a>
        </li>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('prices') }}" class="b-sidebar-menu__link">
                 <i class="b-sidebar__icon">
                    <img src="{{ url('img/sidebar/02.svg') }}" alt="lands icon">
                </i>
                @lang('messages.menu_prices')
            </a>
        </li>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('services') }}" class="b-sidebar-menu__link">
                 <i class="b-sidebar__icon">
                    <img src="{{ url('img/sidebar/03.svg') }}" alt="lands icon">
                </i>
                @lang('pages.services')
            </a>
        </li>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('about') }}" class="b-sidebar-menu__link">
                 <i class="b-sidebar__icon">
                    <img src="{{ url('img/sidebar/04.svg') }}" alt="lands icon">
                </i>
                @lang('pages.about')
            </a>
        </li>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('hypothec') }}" class="b-sidebar-menu__link">
                 <i class="b-sidebar__icon">
                    <img src="{{ url('img/sidebar/05.svg') }}" alt="lands icon">
                </i>
                @lang('pages.hypothec')
            </a>
        </li>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('partners') }}" class="b-sidebar-menu__link">
                 <i class="b-sidebar__icon">
                    <img src="{{ url('img/sidebar/06.svg') }}" alt="lands icon">
                </i>
                @lang('pages.partners')
            </a>
        </li>
        <li class="b-sidebar-menu__item">
            <a href="{{ url('contacts') }}" class="b-sidebar-menu__link">
                 <i class="b-sidebar__icon">
                    <img src="{{ url('img/sidebar/07.svg') }}" alt="lands icon">
                </i>
                @lang('pages.contacts')
            </a>
        </li>
    </ul>
</div>
<div class="b-sidebar-overlay"></div>