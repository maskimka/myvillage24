<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    @section('meta')
        @if(isset($Meta) && $Meta::$has_template)
            <meta name="title" content="{{$Meta->getTitle()}}">
            <meta name="description" content="{{$Meta->getDescription()}}">
            {{--<meta name="keywords" content="{{$Meta->getKeyWords()}}">--}}
        @endif
    @show
    @if(isset($Meta) && $Meta::$has_template)
        <title class="no-print">{{$Meta->getTitle()}}</title>
    @else
        <title class="no-print">MyVillage - @yield('title') </title>
    @endif

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('/css/frontend/jquery-eu-cookie-law-popup.css') !!}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css">

    @section('styles')
    @show

    <link rel="stylesheet" href="{{ url('css/frontend/new/styles.css') }}">

</head>
<body class="@if($action === 'home') home @endif">
  <!-- Wrapper-->
    <div id="wrapper">
        @include('layouts.frontend.topnavbar_new')
        <!-- Page wrapper -->
        {{-- @include('layouts.frontend.main_topnavbar') --}}

        <!-- Main view  -->
        @yield('content')

        <!-- Footer -->
        @include('layouts.frontend.footer')

    </div>
    {{-- @include('layouts.frontend.sidebar') --}}

  <div id="fb-root"></div>
  @include('frontend.partials.alert')
    <!-- End wrapper-->

    {{-- cookie --}}
    <div class="eupopup eupopup-bottom"></div>
    {{-- End cookie --}}
@section('scripts')
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js" type="text/javascript"></script>
      <script src="{!! asset('js/plugins/validate/jquery.validate.min.js') !!}" type="text/javascript"></script>
      {{-- <script src="{!! asset('js/plugins/swiper/swiper.min.js') !!}" type="text/javascript"></script> --}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>
      <script src="{!! asset('/js/messages.js') !!}"></script>
{{--      <script src="{!! asset('/js/frontend/frontend.js') !!}"></script>--}}
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8G4whljl8a5JsEbcaOCYSHNwt8JhWWN8&libraries=geometry,places"></script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
      <script type="text/javascript">
          function googleTranslateElementInit() {
              new google.translate.TranslateElement({pageLanguage: 'ru'}, 'googleTranslateBar');
          }
      </script>
@show

  <script src="{!! asset('/js/frontend/front-components.js') !!}"></script>
  <script src="{!! asset('/js/frontend/common.js') !!}"></script>
  <script src="{!! asset('/js/frontend/jquery-eu-cookie-law-popup.js') !!}"></script>

@if(config('app.env') != 'local')

    <script>

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '{Your-mail-App_id}',
                xfbml      : true,
                version    : 'v2.0'
            });
            FB.Canvas.setSize({ width: 770, height: 735 }); // set your own figure
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/" + Lang.get('pages.fb_lang') + "/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    </script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter45629937 = new Ya.Metrika({
                        id:45629937,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        trackHash:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/45629937" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-101523310-2', 'auto');
        ga('send', 'pageview');

    </script>
    {{-- <script type="text/javascript" src="//api.venyoo.ru/wnew.js?wc=venyoo/default/science&widget_id=6606461960192000"></script> --}}

@endif

</body>
</html>
