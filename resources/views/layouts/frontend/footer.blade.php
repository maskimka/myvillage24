<div class="b-footer no-print">
    <div class="b-advantages-wrapper">
        <div class="container">
            <ul class="b-advantages-grid">
                <li class="b-advantages-grid__item">
        
                    <div class="b-advantages-card">
                        <div class="b-advantages-card__img-wrapper">
                            <img src="/img/common/icons/wallet.png" class="b-advantages-card__img">
                        </div>
                        <div class="b-advantages-card__title">
                            <span>@lang('pages.advantage1row1')</span>
                            <span>@lang('pages.advantage1row2')</span>
                        </div>
                    </div>
        
                </li>
                <li class="b-advantages-grid__item">
        
                    <div class="b-advantages-card">
                        <div class="b-advantages-card__img-wrapper">
                            <img src="/img/common/icons/quality.png" class="b-advantages-card__img">
                        </div>
                        <div class="b-advantages-card__title">
                            <span>@lang('pages.advantage2row1')</span>
                            <span>@lang('pages.advantage2row2')</span>
                        </div>
                    </div>
        
                </li>
                <li class="b-advantages-grid__item">
        
                    <div class="b-advantages-card">
                        <div class="b-advantages-card__img-wrapper">
                            <img src="/img/common/icons/guarantee.png" class="b-advantages-card__img">
                        </div>
                        <div class="b-advantages-card__title">
                            <span>100%</span>
                            <span>@lang('pages.advantage3row1')</span>
                            <span>@lang('pages.advantage3row2')</span>
                        </div>
                    </div>
        
                </li>
                <li class="b-advantages-grid__item">
        
                    <div class="b-advantages-card">
                        <div class="b-advantages-card__img-wrapper">
                            <img src="/img/common/icons/mortgage.png" class="b-advantages-card__img">
                        </div>
                        <div class="b-advantages-card__title">
                            <span>@lang('pages.advantage4row1')</span>
                            <span>@lang('pages.advantage4row2')</span>
                        </div>
                    </div>
        
                </li>
                <li class="b-advantages-grid__item">
        
                    <div class="b-advantages-card">
                        <div class="b-advantages-card__img-wrapper">
                            <img src="/img/common/icons/law.png" class="b-advantages-card__img">
                        </div>
                        <div class="b-advantages-card__title">
                            <span>@lang('pages.advantage5row1')</span>
                            <span>@lang('pages.advantage5row2')</span>
                        </div>
                    </div>
        
                </li>
            </ul>
        </div>    
    </div>
    <div class="b-footer__nav-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <p class="b-footer__nav-title">
                        @lang('messages.options')
                    </p>
                    <ul class="b-footer__nav">
                        <li>
                            <a href="{{ url('terms/land')}}" class="b-footer__nav-link">
                                @lang('pages.add_land') <span>+</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('lands')}}" class="b-footer__nav-link">
                                @lang('messages.menu_lands') <span><sup>235</sup></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('buildings')}}" class="b-footer__nav-link">
                                @lang('messages.menu_projects') <span><sup>1038</sup></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('architecture')}}" class="b-footer__nav-link">
                                @lang('messages.menu_buildings') <span><sup>8</sup></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <p class="b-footer__nav-title">
                        @lang('pages.about_title')
                    </p>
                    <ul class="b-footer__nav">
                        <li>
                            <a href="{{ url('about') }}" class="b-footer__nav-link">
                                @lang('pages.about')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('services') }}" class="b-footer__nav-link">
                                @lang('pages.services')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('partners') }}" class="b-footer__nav-link">
                                @lang('pages.partners')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('contacts') }}" class="b-footer__nav-link">
                                @lang('pages.contacts')
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <p class="b-footer__nav-title">
                        @lang('pages.services')
                    </p>
                    <ul class="b-footer__nav">
                        <li>
                            <a href="{{ url('prices') }}" class="b-footer__nav-link">
                                @lang('messages.menu_prices')
                            </a>
                        </li>
                        {{-- <li>
                            <a href="{{ url('hypothec') }}" class="b-footer__nav-link">
                                @lang('pages.hypothec')
                            </a>
                        </li> --}}
                    </ul>
                </div>
                <div class="col-md-2">
                    <p href="#" class="b-footer__nav-title">
                        @lang('messages.footer_users')
                    </p>
                    <ul class="b-footer__nav">
                        <li>
                            <a href="/auth/login" class="b-footer__nav-link">
                                 @lang('messages.personal_panel')
                            </a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/wishlist" class="b-footer__nav-link">
                                @lang('messages.menu_favorite')
                            </a>
                        </li>
                        <li>
                            <a href="/auth/registration" class="b-footer__nav-link">
                                @lang('messages.menu_registration')
                            </a>
                        </li>
                        <li>
                            <a href="/cookie-policy" class="b-footer__nav-link">
                                @lang('messages.menu_registration')
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <p class="b-footer__nav-title">
                        @lang('pages.contacts')
                    </p>
                    <ul class="b-footer__nav">
                        <li>
                            @lang('pages.sales_department'): +373 79 005 888
                        </li>
                        <li>
                            @lang('pages.architecture_department'): +373 79 050 888
                        </li>
                        <li>
                            @lang('messages.label_city_short') @lang('pages.office_address_city'), @lang('pages.office_address_street')
                        </li>
                        <li>
                            info@myvillage24.com
                        </li>
                    </ul>
                </div>
            </div>
            <div class="b-copyright">
                <div>

                    © 2018 MyVillage24.com
                </div>
                {{-- <div class="col-sm-5">
                </div> --}}
                <ul class="b-footer-social">
                    <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li><div class="fb-like" data-href="https://www.facebook.com/myvillagemd" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div></li>
                </ul>
                {{-- <div class="row">
                    <div class="col-sm-7">
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>