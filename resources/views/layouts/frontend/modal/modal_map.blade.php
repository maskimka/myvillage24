<div class="modal fade b-modal__map" id="modalMap" tabindex="-1">
    <div class="modal-dialog modal-dialog-container">
        <div class="modal-content">
            <button type="button" class="btn b-btn b-btn--color-green close-btn b-modal__close" data-dismiss="modal" aria-label="Close">
                <span>@lang('messages.label_close')</span>
                <i class="fa fa-close close-x"></i>
            </button>
            <google-map
                    id="js-objects-map"
                    class="b-objects-map__gm"
                    :center-coords="[47.020751, 28.856090]"
                    :init-wait-time=0
                    modal-id='modalMap'
                    show-type="all"
                    :enable-geolocation=false>
            </google-map>
            <google-place-popup ref="googlemarkerplacepopup"></google-place-popup>
        </div>
    </div>
</div>