<div class="modal fade" id="modalMainVideo-old" tabindex="-1">
    <div class="modal-dialog modal-dialog-container">
        <div class="modal-content">
            <div class="modal-header">
                @lang('pages.presentation')
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/OEoo6zBVW2k"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade b-modal b-modal--video" id="modalMainVideo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg b-modal__dialog" role="document">
        <div class="modal-content b-modal__content">
            <div class="modal-header b-modal__header">
                <div class="b-modal__close">
                    <button type="button" class="btn b-btn b-btn--rect b-btn--size-lg" data-dismiss="modal">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                </div>
                <p class="b-modal__title" id="myModalLabel">
                    @lang('pages.presentation')
                </p>
            </div>
            <div class="modal-body b-modal__body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/OEoo6zBVW2k"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>