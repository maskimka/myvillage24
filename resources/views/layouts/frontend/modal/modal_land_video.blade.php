<div class="modal fade b-modal b-modal--video" id="modalLandVideo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg b-modal__dialog" role="document">
        <div class="modal-content b-modal__content">
            <div class="modal-header b-modal__header">
                <div class="b-modal__close">
                    <button type="button" class="btn b-btn b-btn--rect b-btn--size-lg" data-dismiss="modal">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                </div>
                <p class="b-modal__title" id="myModalLabel">
                    MyVillage
                </p>
            </div>
            <div class="modal-body b-modal__body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="{{ str_replace('watch?v=', 'embed/', $item->land_video) }}"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>