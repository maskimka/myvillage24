<div class="modal fade b-modal__map" id="modalLandMap" tabindex="-1">
    <div class="modal-dialog modal-dialog-container">
        <div class="modal-content">
            <button type="button" class="btn b-btn b-btn--color-green close-btn b-modal__close" data-dismiss="modal" aria-label="Close">
                <span>@lang('messages.label_close')</span>
                <i class="fa fa-close close-x"></i>
            </button>
            <google-map
                    ref="modalMap"
                    id="js-objects-land-map"
                    ref="modalMap"
                    class="b-objects-map__gm"
                    :center-coords="[47.088176843700765000, 28.769065206302685000]"
                    :init-wait-time=300
                    modal-id='modalLandMap'
                    show-type="points"
                    land-id="16"
                    :enable-geolocation=false>
            </google-map>
        </div>
    </div>
</div>
