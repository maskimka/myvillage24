<div id="ordersForm" class="modal inmodal" aria-hidden="true" role="dialog" tabindex="-1" style="display: none;">

    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">

            <div class="modal-header">

                <button class="close" data-dismiss="modal" type="button">

                    <span aria-hidden="true">×</span>

                    <span class="sr-only">Close</span>

                </button>
                <div class="modal-header-grid">
                    <div class="modal-header-grid__item modal-header__icon">
                        <img src="{{ url('img/logo-icon.png') }}" alt="Logo" class="img-responsive">
                    </div>
                    <div class="modal-header-grid__item">
                        <h4 class="modal-title">@lang('messages.label_order_form')</h4>
        
                        <small>@lang('messages.label_modal_order_text')</small>
                    </div>
                </div>

            </div>

            <form id="OrderForm" action="" class="" method="POST">

                <div class="modal-body">


                    <div class="form-group b-form-group">

                        <input class="form-control b-form-control" type="text" name="name" placeholder="{{ trans('messages.label_full_name') }}" value="">

                    </div>

                    <div class="form-group b-form-group">

                        <input class="form-control b-form-control" type="text" name="phone" placeholder="{{ trans('messages.label_phone') }}" value="">

                    </div>

                    <div class="form-group b-form-group">

                        <textarea class="form-control b-form-control" name="comments" rows="4" placeholder="{{ trans('messages.label_comments') }}"></textarea>

                    </div>

                    {{csrf_field()}}

                    <input type="hidden" name="id" value="">

                </div>

                <div class="modal-footer">

                    <button class="btn btn-default" data-dismiss="modal" type="submit">@lang('messages.button_cancel')</button>

                    <button class="btn b-btn b-btn--color-green b-btn--rounded-light" type="submit">@lang('messages.button_send')</button>

                </div>

            </form>

        </div>

    </div>

</div>

<div id="orderSuccessModal" class="modal inmodal" aria-hidden="true" role="dialog" tabindex="-1" style="display: none;">

    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">

            <div class="modal-header">

                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>

                <h4 class="modal-title b-object-page__list_title">@lang('messages.text_thanks')</h4>

            </div>

            <div class="modal-body">

                <div class="col-lg-10 col-lg-offset-1">

                    <p>
                        @lang('messages.label_order_sent_message')
                    </p>

                </div>

                <div class="clearfix"></div>
            </div>

            <div class="modal-footer">

                <button class="btn b-btn--color-green b-btn--rounded-light center-block" data-dismiss="modal" type="button">@lang('messages.label_close')</button>

            </div>

        </div>

    </div>

</div>
