<nav class="navbar navbar-default b-header__wrapper">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-wrapper" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="nav-wrapper">
            <ul class="nav navbar-nav navbar-right">
                <li class="top-nav-item">
                    <span>Отдел продаж: </span>
                    <span class="bigger">+373 79 005 888</span>
                </li>
                <li class="top-nav-item">
                    <span>Отдел архитектуры:</span>
                    <span class="bigger">+373 79 050 888</span>
                </li>
                <li class="top-nav-item">
                    <span>e-mail:</span>
                    <span class="bigger">info@myvillage.md</span>
                </li>
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                        {{--Молдова <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--Украина--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--Испания--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--Россия--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="dropdown text-uppercase">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usd <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--Euro--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--Mdl--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                        {{--<img src="{{ url('img/common/lang/ru.png') }}" alt=""> <span class="caret"></span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<img src="{{ url('img/common/lang/en.png') }}" alt="">--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<img src="{{ url('img/common/lang/es.png') }}" alt="">--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li><a href="#">Поддержка</a></li>--}}
            </ul>
        </div>

    </div>
</nav>

<div class="container">
    <nav class="navbar navbar-default b-header">
        <div class="col-lg-5">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="row">
                    <div class="col-md-7">
                        <a class="navbar-brand b-logo b-header__logo" href="{{ url('/') }}">
                            <img src="{{ url('img/logo.jpg') }}" alt="My village" class="b-logo__img">
                        </a>
                    </div>
                    <div class="col-md-1">
                        <a class="navbar-brand b-logo b-header__logo b-header__logo-kirsan" href="http://kirsan.md" target="_blank">
                            <img src="{{ url('img/common/label_kirsan.jpg') }}" alt="Kirsan" class="b-logo__img">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-7">

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div class="contacts-header">
                            <a href="{{ url('terms/land')}}" class="btn b-btn b-btn--color-green-light b-btn--outline btn-cabinet" rel="nofollow">+ Добавить участок</a>
                        </div>
                    </li>
                    <li class="hidden">
                        <a href="{{ url('/')}}"><i class="fa fa-map-marker fa-menu-item">&nbsp;</i>&nbsp;Земельные участки</a>
                    </li>
                    <li class="hidden">
                        <a href="{{ url('architecture')}}"><i class="fa fa-edit fa-menu-item">&nbsp;</i>&nbsp;Проекты домов</a>
                    </li>
                    <li class="hidden">
                        <a href="{{ url('buildings')}}"><i class="fa fa-home fa-menu-item">&nbsp;</i>&nbsp;Дома в строительстве</a>
                    </li>
                    <li><a href="{{ url('services') }}">Услуги</a></li>
                    <li><a href="{{ url('about') }}">О нас</a></li>
                    <li><a href="{{ url('hypothec') }}">Ипотека</a></li>
                    <li><a href="{{ url('partners') }}">Партнерам</a></li>
                    <li><a href="{{ url('contacts') }}">Контакты</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
