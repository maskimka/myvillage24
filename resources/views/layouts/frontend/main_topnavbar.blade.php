<nav class="navbar navbar-default b-header @if($action === 'home') b-header-home @endif">
    <div class="container">
        <div class="navbar-header b-navbar-header">
            <button type="button" class="navbar-toggle b-navbar-toggle js-open-sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand b-logo" href="{{ url('/') }}">
                <img src="{{ url('img/logo.svg') }}" alt="My village" class="b-logo__img b-logo__img-def">
                <img src="{{ url('img/logo-reverse.png') }}" alt="My village" class="b-logo__img b-logo__img-reverse">
            </a>
            <ul class="nav navbar-nav b-navbar-nav pull-right">
                <li class="dropdown dropdown-account">
                    <button class="btn b-btn b-btn--wide b-btn--color-green dropdown-toggle dropdown-toggle-desktop" type="button" data-toggle="dropdown">
                        <i class="dropdown-account-icon fa fa-user" aria-hidden="true"></i> @lang('messages.menu_my_account')
                    </button>
                    <a href="#" type="button" data-toggle="dropdown" class="btn-account">
                        <i class="fa fa-user icon" aria-hidden="true"></i>
                    </a>
                    <button type="button" class="btn b-btn b-btn--color-green-light b-btn--transparent b-btn--rect close-dropdown" data-toggle="dropdown">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                    <ul class="dropdown-menu b-dropdown-menu">
                        <li>
                            <a href="/auth/login">@lang('messages.menu_login')</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="/auth/registration">@lang('messages.menu_registration')</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="/{{ App::getLocale() }}/wishlist">
                                @lang('messages.menu_favorite')
                                <i class="text-divider"></i>
                                <span id="wishlistCountItems" class="u-text-color--green">
                                    {{ request()->wishlistCountItems }}
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle dropdown-language--link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        @if(isset($countries[request()->clientCountry]))
                            {{ mb_strtoupper(trans('countries.'.$countries[request()->clientCountry]->name)) }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                            <input type="hidden" id="userCountry" value="{{ strtoupper($countries[request()->clientCountry]->iso2code) }}">
                        @else
                            {{ mb_strtoupper(trans('countries.'.$countries['MD']->name)) }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                            <input type="hidden" id="userCountry" value="{{ strtoupper($countries['MD']->iso2code) }}">
                        @endif
                    </a>
                    <ul class="dropdown-menu b-dropdown-menu">
                        @foreach($countries as $iso2code => $country)
                            <li>
                                <a href="{{ Request::getPathInfo().'?country='.$country->iso2code }}">
                                    {{ mb_strtoupper(trans('countries.'.$country->name)) }}
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                        @endforeach
                    </ul>
                </li>
                {{--<li class="dropdown dropdown-language">--}}
                    {{--<a href="#" class="dropdown-toggle dropdown-language--link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                        {{--USD <i class="fa fa-angle-down" aria-hidden="true"></i>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu b-dropdown-menu">--}}
                        {{--@foreach($countries as $iso2code => $country)--}}
                            {{--<li>--}}
                                {{--<a href="{{ Request::getPathInfo().'?country='.$country->iso2code }}">--}}
                                    {{--{{ mb_strtoupper(trans('countries.'.$country->name)) }}--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li role="separator" class="divider"></li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li class="dropdown dropdown-language">
                    <a href="#" class="dropdown-toggle dropdown-language--link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ url('img/common/lang/'.App::getLocale().'.png') }}" class="lang-icon"> {{ strtoupper(App::getLocale()) }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu b-dropdown-menu">
                        <?php $segments = Request::segments(); unset($segments[0])?>
                        @foreach(config('locales') as $locale => $enabled)
                            @if($enabled)
                                <li>
                                    <a href="{{ '/'.$locale.'/'.implode('/', $segments) }}">
                                        <img src="{{ url('img/common/lang/'.$locale.'.png') }}" class="lang-icon"> {{ strtoupper($locale) }}
                                    </a>
                                </li>
                                <li role="separator" class="divider"></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div>
        <div class="collapse navbar-collapse b-navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav b-nav-pages navbar-right">
                <li>
                    <div class="contacts-header add-land-btn">
                        <a href="{{ url('terms/land')}}" class="btn b-btn b-btn--color-green b-btn--outline btn-cabinet btn-add-land">@lang('pages.add_land')</a>
                    </div>
                </li>
                <li class="hidden">
                    <a href="{{ url('lands')}}"><i class="fa fa-map-marker fa-menu-item">&nbsp;</i>&nbsp;@lang('pages.menu_lands_f')</a>
                </li>
                <li class="hidden">
                    <a href="{{ url('architecture')}}"><i class="fa fa-edit fa-menu-item">&nbsp;</i>&nbsp;@lang('pages.menu_projects_f')</a>
                </li>
                <li class="hidden">
                    <a href="{{ url('buildings')}}"><i class="fa fa-home fa-menu-item">&nbsp;</i>&nbsp;@lang('pages.menu_buildings_f')</a>
                </li>
                <li><a href="{{ url('services') }}">@lang('pages.services')</a></li>
                <li><a href="{{ url('about') }}">@lang('pages.about')</a></li>
                <li><a href="{{ url('hypothec') }}">@lang('pages.hypothec')</a></li>
                <li><a href="{{ url('partners') }}">@lang('pages.partners')</a></li>
                <li><a href="{{ url('contacts') }}">@lang('pages.contacts')</a></li>
            </ul>
        </div>
    </div>
</nav>