<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    @section('meta')

    @show

    <title>MyVillage - @yield('title') </title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic" rel="stylesheet"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
    {{--<link rel="stylesheet" href="{!! asset('css/plugins/chosen/bootstrap-chosen.css') !!}" />--}}
    {{--<link rel="stylesheet" href="{!! asset('css/frontend/awesome_bootstrap_checkbox.css') !!}" />--}}


    @section('styles')

    @show

    <link rel="stylesheet" href="{{ url('css/frontend/main.css') }}">

</head>
<body class="">

<!-- Wrapper-->
<div id="wrapper">

    <!-- Page wrapper -->
    @include('layouts.frontend.main_topnavbar')

            <!-- Main view  -->
    @yield('content')

            <!-- Footer -->
    @include('layouts.frontend.footer')

</div>
<div id="fb-root"></div>
{{--<script>(function(d, s, id) {--}}
        {{--var js, fjs = d.getElementsByTagName(s)[0];--}}
        {{--if (d.getElementById(id)) return;--}}
        {{--js = d.createElement(s); js.id = id;--}}
        {{--js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.9";--}}
        {{--fjs.parentNode.insertBefore(js, fjs);--}}
    {{--}(document, 'script', 'facebook-jssdk'));</script>--}}
<!-- End wrapper-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>

{{--<script src="{!! asset('js/plugins/chosen/chosen.jquery.js') !!}" type="text/javascript"></script>--}}
{{----}}
<script src="{!! asset('js/frontend/front-components.js') !!}"></script>
{{--  <script src="{!! asset('js/frontend/filters.js') !!}"></script>--}}

@section('scripts')
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function(){--}}
            {{--$(".logout").on('click', function(e){--}}
                {{--e.preventDefault();--}}
                {{--$(this).closest('form').submit();--}}
            {{--})--}}
        {{--})--}}
    {{--</script>--}}
@show
<script src="{!! asset('js/messages.js') !!}"></script>
<script src="{!! asset('js/frontend/frontend.js') !!}"></script>
{{--<script type="text/javascript">--}}
    {{--$(document).ready(function(){--}}

        {{--$('[data-toggle="popover"]').popover();--}}
    {{--})--}}
{{--</script>--}}

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45080496 = new Ya.Metrika({
                    id:45080496,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45080496" style="position:absolute; left:-9999px;" alt="image" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-101523310-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>
