@extends('layouts/panel.app')

@section('title', 'Dashboard')

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/cropper/cropper.min.css') !!}" />
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{trans('messages.label_edit_land')}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/panel">{{trans('messages.menu_dashboard')}}</a>
                </li>
                <li>
                    <a href="/panel/land">{{trans('messages.label_land_list')}}</a>
                </li>
                <li class="active">
                    <strong>{{trans('messages.label_edit_land')}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @include('errors.error_list')
            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-content">
                        <form id="wizard_land_form" action="/panel/land/{{$land->id}}" class="wizard-big edit_profile" enctype="multipart/form-data" method="POST">
                            <h1>{{trans('messages.label_cadastral_number')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_cadastral_number')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="landCadastralNumber" name="cadastral_number" type="text" class="form-control" placeholder="{{trans('messages.label_cadastral_number')}}" value="{{$land->cadastral_number or '--'}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">

                                    </div>
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_land_information')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_land_information')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <select id="landPurpose" class="chosen-select" name="purpose_id">
                                                    <option value="" disabled>{{trans('messages.label_purpose_type')}}</option>
                                                    @foreach ($purposes as $purpose)
                                                        @if (Request::old('purpose_id') == $purpose->id || $purpose->id == $land->purpose_id)
                                                            <option selected value="{{ $purpose->id }}" data-content="{{$purpose->phone_mask}}">{{ $purpose->name }}</option>
                                                        @else
                                                            <option value="{{ $purpose->id }}" data-content="{{$purpose->phone_mask}}">{{ $purpose->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="landArea" name="area" type="text" class="form-control" placeholder="{{trans('messages.label_land_area')}}" value="{{$land->area}}">
                                                <span class="input-group-addon">
                                                   &nbsp;{{trans('messages.label_a')}}<sup>2</sup>&nbsp;&nbsp;
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="landFromCity" name="from_city" type="text" class="form-control" placeholder="{{trans('messages.label_from_city')}}" value="{{$land->from_city}}">
                                                <span class="input-group-addon">
                                                    {{trans('messages.label_km')}}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="landPrice" name="price" type="text" class="form-control" placeholder="{{trans('messages.label_price')}}" value="{{$land->price}}">
                                                <span class="input-group-addon">
                                                    {{trans('messages.label_euro')}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <textarea class="form-control" id="landDescription" name="description" style="resize: none; width: 100%; min-height: 185px" placeholder="{{trans('messages.label_description')}}">{{$land->description}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_address')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_address')}}</h2>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <div id="map-canvas"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <select id="landCountry" class="chosen-select" name="country_id">
                                                    <option value="" disabled>{{trans('messages.label_country')}}</option>
                                                    @foreach ($countries as $country)
                                                        @if (Request::old('country_id') == $country->id || $country->id == $land->country_id)
                                                            <option selected value="{{ $country->id }}" data-content="{{$country->iso2code}}">{{ $country->name }}</option>
                                                        @else
                                                            <option value="{{ $country->id }}" data-content="{{$country->iso2code}}">{{ $country->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <input id="userCountryIso" type="hidden" value="{{$user->country->iso2code}}">
                                        </div>
                                        <div class="col-lg-12">
                                            <p>
                                                Choose the country before.
                                                Than tray to find you place inside the country by typing in the form below
                                            </p>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-search"></i>
                                                </span>
                                                    <input id="searchBox" type="text" class="form-control" placeholder="{{trans('messages.label_address')}}" onFocus="geolocate()">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 nopadding-right">
                                            <div class="form-group">
                                                <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                    <input id="mapLat" name="lat" type="text" class="form-control" placeholder="{{trans('messages.label_land_lat')}}" value="{{$land->lat}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 nopadding-left">
                                            <div class="form-group">
                                                <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                    <input id="mapLng" name="lng" type="text" class="form-control" placeholder="{{trans('messages.label_land_lng')}}" value="{{$land->lng}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-address-book-o"></i>
                                                    </span>
                                                    <input id="locality" name="city" type="text" class="form-control" placeholder="{{trans('messages.label_city')}}" value="{{$land->city}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 nopadding-right">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-address-book-o"></i>
                                                    </span>
                                                    <input id="administrative_area_level_1" name="state" type="text" class="form-control" placeholder="{{trans('messages.label_state')}}" value="{{$land->state}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 nopadding-left">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-address-book-o"></i>
                                                    </span>
                                                    <input id="landRegion" name="region" type="text" class="form-control" placeholder="{{trans('messages.label_region')}}" value="{{$land->region}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_communications')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_communications')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    @foreach($communications as $communication)
                                        <?php $checked = "" ?>
                                        @foreach($land->communications()->get() as $land_communication)
                                            @if ($land_communication->id == $communication->id)
                                                <?php $checked = "checked"?>
                                            @endif
                                        @endforeach
                                        <div class="checkbox i-checks col-lg-3">
                                            <label>
                                                <input type="checkbox" name="communications[]" value="{{$communication->id}}" {{$checked}}>&nbsp;<i></i>
                                                {{trans('messages.'.$communication->slug)}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_infrastructure')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_infrastructure')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    @foreach($infrastructure as $item)
                                        <?php $checked = "" ?>
                                        @foreach($land->infrastructure()->get() as $land_infrastructure)
                                            @if ($land_infrastructure->id == $item->id)
                                                 <?php $checked = "checked" ?>
                                            @endif
                                        @endforeach
                                        <div class="checkbox i-checks col-lg-2">
                                            <label>
                                                <input type="checkbox" name="infrastructure[]" value="{{$item->id}}" {{$checked}}>&nbsp;<i></i>
                                                {{trans('messages.'.$item->slug)}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_transport')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_transport')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    @foreach($transport as $item)
                                        <?php $checked = "" ?>
                                        @foreach($land->transport()->get() as $land_transport)
                                            @if ($land_transport->id == $item->id)
                                                <?php $checked = "checked" ?>
                                            @endif
                                        @endforeach
                                        <div class="checkbox i-checks col-lg-3">
                                            <label>
                                                <input type="checkbox" name="transport[]" value="{{$item->id}}" {{$checked}}>&nbsp;<i></i>
                                                {{trans('messages.'.$item->slug)}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_images')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_images')}}</h2>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="image-upload">
                                            <label for="land_img_1">
                                                @if(isset($land_images[0]))
                                                    <div class="image_preview filled" style="background-image: url('{{asset($land_images[0]->path."/original/".$land_images[0]->name)}}');"></div>
                                                @else
                                                    <div class="image_preview"></div>
                                                @endif
                                            </label>
                                            <div class="file_upload_buttons">
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                            <input id="land_img_1" type="file" class="hidden" name="images[]"/>
                                            <input type="hidden" name="imageCroppedCoords[]">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="image-upload">
                                            <label for="land_img_2">
                                                @if(isset($land_images[1]))
                                                    <div class="image_preview filled" style="background-image: url('{{asset($land_images[1]->path."/original/".$land_images[1]->name)}}');"></div>
                                                @else
                                                    <div class="image_preview"></div>
                                                @endif
                                            </label>
                                            <div class="file_upload_buttons">
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                            <input id="land_img_2" type="file" class="hidden" name="images[]"/>
                                            <input type="hidden" name="imageCroppedCoords[]">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="image-upload">
                                            <label for="land_img_3">
                                                @if(isset($land_images[2]))
                                                    <div class="image_preview filled" style="background-image: url('{{asset($land_images[2]->path."/original/".$land_images[2]->name)}}');"></div>
                                                @else
                                                    <div class="image_preview"></div>
                                                @endif
                                            </label>
                                            <div class="file_upload_buttons">
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                            <input id="land_img_3" type="file" class="hidden" name="images[]"/>
                                            <input type="hidden" name="imageCroppedCoords[]">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="image-upload">
                                            <label for="land_img_4">
                                                @if(isset($land_images[3]))
                                                    <div class="image_preview filled" style="background-image: url('{{asset($land_images[3]->path."/original/".$land_images[3]->name)}}');"></div>
                                                @else
                                                    <div class="image_preview"></div>
                                                @endif
                                            </label>
                                            <div class="file_upload_buttons close">
                                                <i class="fa fa-trash-o"></i>
                                            </div>
                                            <input id="land_img_4" type="file" class="hidden" name="images[]"/>
                                            <input type="hidden" name="imageCroppedCoords[]">
                                        </div>
                                    </div>
                                    </div>
                            </fieldset>

                            <h1>{{trans('messages.label_finish')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_finish')}}</h2>
                                <div class="checkbox i-checks">
                                    <label>
                                        <input type="checkbox" name="terms" checked>&nbsp;<i></i>  <a href="/terms">{{trans('messages.label_terms')}}</a>
                                    </label>
                                </div>
                            </fieldset>
                            {{ method_field('PUT') }}
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('panel.modal.imagecrop')
@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function(){
            $('#mapLat').trigger("change");
        })
    </script>
    <script src="{!! asset('js/plugins/cropper/cropper.min.js') !!}" type="text/javascript"></script>
@stop