@extends('layouts.panel.app')

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/footable/footable.core.css') !!}" />
@stop

@section('title', trans('messages.label_land_list'))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{trans('messages.label_land_list')}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/panel">{{trans('messages.menu_dashboard')}}</a>
                </li>
                <li class="active">
                    <strong>{{trans('messages.menu_land')}}</strong>
                </li>
            </ol>
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <table id="landList" class="footable table table-stripped toggle-arrow-tiny" data-page-size="10" data-filter="#top-search">
                            <thead>
                            <tr>
                                <th width="7%" data-toggle="true">{{trans('messages.label_article')}}</th>
                                <th width="10%">{{trans('messages.label_cadastral_number')}}</th>
                                <th width="13%">{{trans('messages.label_address')}}</th>
                                <th width="7%" data-sort-ignore="true">{{trans('messages.label_images')}}</th>
                                <th width="10%">{{trans('messages.label_area')}}/{{trans('messages.label_a')}}</th>
                                <th width="10%">{{trans('messages.label_price')}}/euro</th>
                                <th width="10%">{{trans('messages.label_created')}}</th>
                                <th width="10%">{{trans("messages.label_purpose_type")}}</th>
                                <th width="10%">{{trans("messages.label_status")}}</th>
                                <th data-hide="all">{{trans("messages.label_description")}}</th>
                                <th width="10%" data-sort-ignore="true">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lands as $land)
                                <tr data-content="{{$land->id}}">
                                    <td>
                                        {{$land->article}}
                                    </td>
                                    <td>
                                        {{$land->cadastral_number}}
                                    </td>
                                    <td>
                                        {{trans('messages.label_state_short')}}
                                        {{$land->state." "}}
                                        {{trans('messages.label_city_short')}}
                                        {{$land->city}}
                                    </td>
                                    <td>
                                        <div class="img-land">
                                            <img class="img-thumbnail" src="{{asset($land->images[0]->path."/thumbnails/".$land->images[0]->name)}}" alt="image">
                                        </div>
                                    </td>
                                    <td>
                                        {{$land->area}}
                                    </td>
                                    <td>
                                        <span class="btn btn-outline btn-primary" style="font-size: 1.2em">
                                            {{$land->price}}
                                        </span>
                                    </td>
                                    <td>
                                        {{$land->created_at}}
                                    </td>
                                    <td>
                                        {{trans('messages.'.$land->purpose->slug)}}
                                    </td>
                                    <td>
                                        @if($land->is_active)
                                            <span class="label label-primary">{{trans('messages.label_status_active')}}</span>
                                        @else
                                            <span class="label label-danger">{{trans('messages.label_status_not_active')}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{$land->description}}
                                    </td>
                                    <td>
                                        <button id="edit-button" class="btn btn-info dim">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button id="delete-button" class="btn btn-danger dim">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="10">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/footable/footable.all.min.js') !!}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            $('.footable').footable();

            $("#delete-button").on("click", function(){
                var currentRow = $(this).closest("tr");
                var land_id = $(currentRow).attr("data-content");
                var _token = $("input[name='_token']").val();
                swal({
                    title: "{{trans('messages.text_confirm_delete')}}",
                    text: null,
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('messages.button_delete')}}",
                    cancelButtonText: "{{trans('messages.button_cancel')}}",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true,
                },
                function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            url: "/panel/land/"+land_id,
                            type: "POST",
                            data: {_method:"DELETE", _token:_token},
                            dataType:'json',
                            success: function(){
                                swal({
                                    title: "{{trans('messages.text_delete_success')}}",
                                    text: null,
                                    type: "success",
                                    confirmButtonColor: "#1AB394",
                                },function(){
//                                    $(currentRow).remove();
                                    location.reload();
                                });
                            },
                            error: function(){
                                swal({
                                    title: "{{trans('messages.text_error_occurred')}}",
                                    text: null,
                                    type: "error",
                                    //confirmButtonColor: "#1AB394",
                                });
                            }
                        })
                    } else {
                        swal({
                            title: "{{trans('messages.text_cancel_success')}}",
                            text: null,
                            type: "error",
                            //confirmButtonColor: "#1AB394",
                        });
                    }
                });
            });

            $("#edit-button").on("click", function(){
                window.location.href = "/panel/land/"+$(this).closest("tr").attr("data-content")+"/edit";
            })
        })
    </script>
@endsection