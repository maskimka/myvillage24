@extends('layouts/panel.app')

@section('title', 'Dashboard')

@section('styles')
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/cropper/cropper.min.css') !!}" />
@stop

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{trans('messages.label_add_land')}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/panel">{{trans('messages.menu_dashboard')}}</a>
                </li>
                <li class="active">
                    <strong>{{trans('messages.menu_land')}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @include('errors.error_list')
            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-content">
                        <form id="wizard_land_form" action="/panel/land" class="wizard-big edit_profile" enctype="multipart/form-data" method="POST">
                            <h1>{{trans('messages.label_cadastral_number')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_cadastral_number')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="landCadastralNumber" name="cadastral_number" type="text" class="form-control" placeholder="{{trans('messages.label_cadastral_number')}}" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">

                                    </div>
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_land_information')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_land_information')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <select id="landPurpose" class="chosen-select" name="purpose_id">
                                                    <option value="" disabled>{{trans('messages.label_purpose_type')}}</option>
                                                    @foreach ($purposes as $purpose)
                                                        @if (Request::old('purpose_id') == $purpose->id)
                                                            <option selected value="{{ $purpose->id }}" data-content="{{$purpose->phone_mask}}">{{ $purpose->name }}</option>
                                                        @else
                                                            <option value="{{ $purpose->id }}" data-content="{{$purpose->phone_mask}}">{{ $purpose->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="landArea" name="area" type="text" class="form-control" placeholder="{{trans('messages.label_land_area')}}" value="">
                                                <span class="input-group-addon">
                                                   &nbsp;{{trans('messages.label_a')}}<sup>2</sup>&nbsp;&nbsp;
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="landFromCity" name="from_city" type="text" class="form-control" placeholder="{{trans('messages.label_from_city')}}" value="">
                                                <span class="input-group-addon">
                                                    {{trans('messages.label_km')}}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="landPrice" name="price" type="text" class="form-control" placeholder="{{trans('messages.label_price')}}" value="">
                                                <span class="input-group-addon">
                                                    {{trans('messages.label_euro')}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <textarea class="form-control" id="landDescription" name="description" style="resize: none; width: 100%; min-height: 185px" placeholder="{{trans('messages.label_description')}}"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_address')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_address')}}</h2>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <div id="map-canvas"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <select id="landCountry" class="chosen-select" name="country_id">
                                                    <option value="" disabled>{{trans('messages.label_country')}}</option>
                                                    @foreach ($countries as $country)
                                                        @if (Request::old('country_id') == $country->id || $country->id == $user->country->id)
                                                            <option selected value="{{ $country->id }}" data-content="{{$country->iso2code}}">{{ $country->name }}</option>
                                                        @else
                                                            <option value="{{ $country->id }}" data-content="{{$country->iso2code}}">{{ $country->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <input id="userCountryIso" type="hidden" value="{{$user->country->iso2code}}">
                                        </div>
                                        <div class="col-lg-12">
                                            <p>
                                                Choose the country before.
                                                Than tray to find you place inside the country by typing in the form below
                                            </p>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-search"></i>
                                                </span>
                                                    <input id="searchBox" type="text" class="form-control" placeholder="{{trans('messages.label_address')}}" onFocus="geolocate()">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 nopadding-right">
                                            <div class="form-group">
                                                <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                    <input id="mapLat" name="lat" type="text" class="form-control" placeholder="{{trans('messages.label_land_lat')}}" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 nopadding-left">
                                            <div class="form-group">
                                                <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                    <input id="mapLng" name="lng" type="text" class="form-control" placeholder="{{trans('messages.label_land_lng')}}" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-address-book-o"></i>
                                                    </span>
                                                    <input id="locality" name="city" type="text" class="form-control" placeholder="{{trans('messages.label_city')}}" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 nopadding-right">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-address-book-o"></i>
                                                    </span>
                                                    <input id="administrative_area_level_1" name="state" type="text" class="form-control" placeholder="{{trans('messages.label_state')}}" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 nopadding-left">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-address-book-o"></i>
                                                    </span>
                                                    <input id="landRegion" name="region" type="text" class="form-control" placeholder="{{trans('messages.label_region')}}" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_communications')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_communications')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    @foreach($communications as $communication)
                                        <div class="checkbox i-checks col-lg-3">
                                            <label>
                                                <input type="checkbox" name="communications[]" value="{{$communication->id}}">&nbsp;<i></i>
                                                {{trans('messages.'.$communication->slug)}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_infrastructure')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_infrastructure')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    @foreach($infrastructure as $item)
                                        <div class="checkbox i-checks col-lg-2">
                                            <label>
                                                <input type="checkbox" name="infrastructure[]" value="{{$item->id}}">&nbsp;<i></i>
                                                {{trans('messages.'.$item->slug)}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_transport')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_transport')}}</h2>
                                <p>&nbsp;</p>
                                <div class="row">
                                    @foreach($transport as $item)
                                        <div class="checkbox i-checks col-lg-3">
                                            <label>
                                                <input type="checkbox" name="infrastructure[]" value="{{$item->id}}">&nbsp;<i></i>
                                                {{trans('messages.'.$item->slug)}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </fieldset>

                            <h1>{{trans('messages.label_images')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_images')}}</h2>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="image-upload">
                                            <label for="land_img_1">
                                                <div class="image_preview"></div>
                                            </label>
                                            <input id="land_img_1" type="file" class="hidden" name="images[]"/>
                                            <input type="hidden" name="imageCroppedCoords[]">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="image-upload">
                                            <label for="land_img_2">
                                                <div class="image_preview"></div>
                                            </label>
                                            <input id="land_img_2" type="file" class="hidden" name="images[]"/>
                                            <input type="hidden" name="imageCroppedCoords[]">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="image-upload">
                                            <label for="land_img_3">
                                                <div class="image_preview"></div>
                                            </label>
                                            <input id="land_img_3" type="file" class="hidden" name="images[]"/>
                                            <input type="hidden" name="imageCroppedCoords[]">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="image-upload">
                                            <label for="land_img_4">
                                                <div class="image_preview"></div>
                                            </label>
                                            <input id="land_img_4" type="file" class="hidden" name="images[]"/>
                                            <input type="hidden" name="imageCroppedCoords[]">
                                        </div>
                                    </div>
                                    </div>
                            </fieldset>

                            <h1>{{trans('messages.label_finish')}}</h1>
                            <fieldset>
                                <h2>{{trans('messages.label_finish')}}</h2>
                                <div class="checkbox i-checks">
                                    <label>
                                        <input type="checkbox" name="terms" checked>&nbsp;<i></i>  <a href="/terms">{{trans('messages.label_terms')}}</a>
                                    </label>
                                </div>
                            </fieldset>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('panel.modal.imagecrop')
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/cropper/cropper.min.js') !!}" type="text/javascript"></script>
@stop