<div class="modal inmodal" id="imagecrop" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-camera-retro modal-icon"></i>
                <h4 class="modal-title">Modal title</h4>
                <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="img-preview-container img-preview">
                            <img src="" alt="">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="image-crop">
                            <img src="" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="imagecrop-close-btn btn btn-white" data-dismiss="modal">{{ trans('messages.label_close', [], null, $adminLang) }}</button>
                <button type="button" class="imagecrop-btn btn btn-primary">{{ trans('messages.label_save_changes', [], null, $adminLang) }}</button>
            </div>
        </div>
    </div>
</div>