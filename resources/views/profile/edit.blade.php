<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('title', trans('messages.label_profile_edit', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.profile_edit">{{ trans('messages.label_profile_edit', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('profile_edit_backend', $namespace, $user->id, $adminLang) !!}
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <form id="wizard-form" action="/{{$namespace}}/profile/update" class="wizard-big edit_profile" method="POST">
                        <h1 data-i18n="admin.personal_info">{{trans('messages.label_personal_info', [], null, $adminLang)}}</h1>
                        <fieldset>
                            <h2 data-i18n="admin.personal_info">{{trans('messages.label_personal_info', [], null, $adminLang)}}</h2>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        @if ($errors->has('first_name'))
                                            <label class="error">{{ $errors->first('first_name') }}</label>
                                        @endif
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.first_name" id="userName"
                                                   value="{{$user->first_name}}" name="first_name" type="text"
                                                   class="form-control required{{ ($errors->has('first_name'))? ' error':'' }}"
                                                   placeholder="{{trans('messages.label_first_name', [], null, $adminLang)}} *">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        @if ($errors->has('last_name'))
                                            <label class="error">{{ $errors->first('last_name') }}</label>
                                        @endif
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                            <input data-i18n="[placeholder]admin.last_name" id="" value="{{$user->last_name}}"
                                                   name="last_name" type="text" class="form-control required{{ ($errors->has('last_name'))? ' error':'' }}"
                                                   placeholder="{{trans('messages.label_last_name', [], null, $adminLang)}} *">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        @if ($errors->has('birthday'))
                                            <label class="error">{{ $errors->first('birthday') }}</label>
                                        @endif
                                        <div class="input-group birthday date">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.birthday" id="userBirthday"
                                                   value="{{$user->birthday}}" name="birthday" type="text"
                                                   class="form-control date-mask{{ ($errors->has('last_name'))? ' error':'' }}"
                                                   placeholder="{{trans('messages.label_birthday', [], null, $adminLang)}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        @if ($errors->has('gender'))
                                            <label class="error">{{ $errors->first('gender') }}</label>
                                        @endif
                                        <select name="gender" class="form-control">
                                            <option data-i18n="admin.not_chosen" value="N" {{ ($user->gender === 'N')?'selected':'' }}>
                                                {{trans('messages.not_chosen', [], null, $adminLang)}}
                                            </option>
                                            <option data-i18n="admin.man" value="M" {{ ($user->gender === 'M')?'selected':'' }}>
                                                {{trans('messages.man', [], null, $adminLang)}}
                                            </option>
                                            <option data-i18n="admin.woman" value="W" {{ ($user->gender === 'W')?'selected':'' }}>
                                                {{trans('messages.woman', [], null, $adminLang)}}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="text-center">
                                        <div style="margin-top: 20px">
                                            <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1 data-i18n="admin.password">{{trans('messages.label_password', [], null, $adminLang)}}</h1>
                        <fieldset>
                            <h2 data-i18n="admin.password">{{trans('messages.label_password', [], null, $adminLang)}}</h2>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        @if ($errors->has('password'))
                                            <label class="error">{{ $errors->first('password') }}</label>
                                        @endif
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-lock"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.password" id="password" name="password"
                                                   type="password" class="form-control{{ ($errors->has('password'))? ' error':'' }}"
                                                   placeholder="{{trans('messages.label_password', [], null, $adminLang)}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                             <span class="input-group-addon">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                            <input data-i18n="[placeholder]admin.confirm_password" id="confirm"
                                                   name="password_confirmation" type="password" class="form-control"
                                                   placeholder="{{trans('messages.label_confirm_password', [], null, $adminLang)}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="text-center">
                                        <div style="margin-top: 20px">
                                            <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1 data-i18n="admin.address">{{trans('messages.label_address', [], null, $adminLang)}}</h1>
                        <fieldset>
                            <h2 data-i18n="admin.address">{{trans('messages.label_address', [], null, $adminLang)}}</h2>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        @if ($errors->has('country_id'))
                                            <label class="error">{{ $errors->first('country_id') }}</label>
                                        @endif
                                        <select id="userCountry" name="country_id"
                                                class="chosen-select{{ ($errors->has('country_id'))? ' error':'' }}">
                                            <option data-i18n="admin.country" value="" disabled>
                                                {{trans('messages.label_country', [], null, $adminLang)}}
                                            </option>
                                            @foreach ($countries as $country)
                                                @if ($user->country_id == $country->id)
                                                    <option data-i18n="admin.{{ $country->name }}" selected value="{{ $country->id }}">
                                                        {{trans('countries.'.$country->name, [], null, $adminLang)}}
                                                    </option>
                                                @else
                                                    <option data-i18n="admin.{{ $country->name }}" value="{{ $country->id }}">
                                                        {{trans('countries.'.$country->name, [], null, $adminLang)}}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.zip" id="userZip" name="zip" type="text"
                                                   class="form-control" placeholder="{{trans('messages.label_zip', [], null, $adminLang)}}" value="{{$user->zip}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.state" id="userState" name="state" type="text"
                                                   class="form-control" placeholder="{{trans('messages.label_state', [], null, $adminLang)}}" value="{{$user->state}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input id="userRegion" name="region" type="text" class="form-control"
                                                   placeholder="{{trans('messages.label_region', [], null, $adminLang)}}" value="{{$user->region}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input id="userCity" name="city" type="text" class="form-control"
                                                   placeholder="{{trans('messages.label_city', [], null, $adminLang)}}" value="{{$user->city}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input id="userStreet" name="street" type="text" class="form-control"
                                                   placeholder="{{trans('messages.label_street', [], null, $adminLang)}}" value="{{$user->street}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 nopadding">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="userBlock" name="block" type="text" class="form-control"
                                                       placeholder="{{trans('messages.label_block_long', [], null, $adminLang)}}" value="{{$user->block}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 nopadding">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input id="userApartment" name="apartment" type="text" class="form-control"
                                                       placeholder="{{trans('messages.label_ap_long', [], null, $adminLang)}}" value="{{$user->apartment}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="text-center">
                                        <div style="margin-top: 20px">
                                            <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1>{{trans('messages.label_contacts', [], null, $adminLang)}}</h1>
                        <fieldset>
                            <h2>{{trans('messages.label_contacts', [], null, $adminLang)}}</h2>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        @if ($errors->has('email'))
                                            <label class="error">{{ $errors->first('email') }}</label>
                                        @endif
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                            <input id="userEmail" name="email" type="text" value="{{$user->email}}"
                                                   class="form-control email-mask {{ ($errors->has('email'))? ' error':'' }}"
                                                   placeholder="{{trans('messages.label_email', [], null, $adminLang)}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-mobile"></i>
                                            </span>
                                            <input id="userPersonalPhone" name="personal_phone" type="text" class="form-control phone mobile-mask"
                                                   placeholder="{{trans('messages.label_mobile_phone', [], null, $adminLang)}}"
                                                   value="{{$user->personal_phone}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </span>
                                            <input id="userMobilePhone" name="mobile_phone" type="text"
                                                   class="form-control phone phone-mask" value="{{$user->mobile_phone}}"
                                                   placeholder="{{trans('messages.label_personal_phone', [], null, $adminLang)}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="text-center">
                                        <div style="margin-top: 20px">
                                            <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1>{{trans('messages.label_finish', [], null, $adminLang)}}</h1>
                        <fieldset>
                            <h2>{{trans('messages.label_terms', [], null, $adminLang)}}</h2>
                            @if ($errors->has('terms'))
                                <label class="error">{{ $errors->first('terms') }}</label>
                            @endif
                            <div class="checkbox i-checks">
                                <label>
                                    <input type="checkbox" name="terms" checked>&nbsp;<i></i>
                                    <a href="/terms">{{trans('messages.label_terms', [], null, $adminLang)}}</a>
                                </label>
                            </div>
                        </fieldset>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/inputmask/jquery.inputmask.js') !!}" type="text/javascript"></script>
@stop