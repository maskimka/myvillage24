<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('styles')
    <link rel="stylesheet" href="{!! asset('css/plugins/ladda/ladda.min.css') !!}" />
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/jasny/jasny-bootstrap.min.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/plugins/cropper/cropper.min.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/plugins/ladda/ladda-themeless.min.css') !!}" />
@stop

@section('title', trans('messages.label_profile', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('messages.label_start_your_work', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('profile_roles_backend', $namespace, $adminLang) !!}
        </div>
    </div>
     {{--CONTENT--}}
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="ibox">
                    <div class="row">
                        <div class="col-lg-9 col-md-9">
                            <div class="tabs-container">
                                <div class="tabs-left">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a data-toggle="tab" href="#tab-1">
                                                {{trans('messages.realtor', [], null, $adminLang)}}
                                            </a>
                                        </li>
                                        <li class="">
                                            <a data-toggle="tab" href="#tab-2">
                                                {{trans('messages.architect', [], null, $adminLang)}}
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content ">
                                        <div id="tab-1" class="tab-pane active">
                                            <div class="panel-body">
                                                <p>
                                                    <strong>{{trans('messages.realtor', [], null, $adminLang)}}</strong>
                                                    - {{trans('messages.text_realtor_role', [], null, $adminLang)}}
                                                </p>
                                                <br>
                                                <form id="roleRealtor" class="role-add-container" method="POST">
                                                    <input type="hidden" name="role" value="3">
                                                    <button id="getRoleRealtorBtn" type="button" class="ladda-button btn btn-primary"
                                                            data-style="slide-down" {{ $user->hasRole('realtor')?'disabled':'' }}>
                                                        @if ($user->hasRole('realtor'))
                                                            {{trans('messages.realtor', [], null, $adminLang)}}
                                                        @else
                                                            {{trans('messages.become_realtor', [], null, $adminLang)}}
                                                        @endif
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div id="tab-2" class="tab-pane">
                                            <div class="panel-body">
                                                <p>
                                                    <strong>{{trans('messages.architect', [], null, $adminLang)}}</strong>
                                                     - {{trans('messages.text_architect_role', [], null, $adminLang)}}
                                                </p>
                                                <p>
                                                    <strong>{{trans('messages.architect_license', [], null, $adminLang)}}</strong>
                                                     - {{trans('messages.text_architect_license', [], null, $adminLang)}}
                                                </p>
                                                <br>
                                                <form id="roleArchitect" class="role-add-container" enctype="multipart/form-data" method="POST">
                                                    <div class="form-group">
                                                        <label id="docsLabel">{{trans('messages.docs', [], null, $adminLang)}}</label>&nbsp;
                                                        <small>{{trans('messages.text_docs', [], null, $adminLang)}}</small>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <span class="btn btn-default btn-file">
                                                                <span class="fileinput-new">
                                                                    {{trans('messages.upload_archive', [], null, $adminLang)}}
                                                                </span>
                                                                <span class="fileinput-exists">
                                                                    {{trans('messages.modify', [], null, $adminLang)}}
                                                                </span>
                                                                <input id="userDocs" type="file" name="docs" accept=".zip,.rar"/>
                                                            </span>
                                                            <span class="fileinput-filename"></span>
                                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="role" value="4">
                                                    <button id="getRoleArchitectBtn" class="ladda-button btn btn-primary" data-style="slide-up" {{ $user->hasRole('architect')?'disabled':'' }}>
                                                        @if ($user->hasRole('architect'))
                                                            {{trans('messages.architect', [], null, $adminLang)}}
                                                        @else
                                                            {{trans('messages.become_architect', [], null, $adminLang)}}
                                                        @endif
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="com-lg-3 col-md-3">
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>{{trans('messages.current_roles', [], null, $adminLang)}}: </label>
                                    <ol>
                                        @foreach($user->roles as $role)
                                            <li>{{trans('messages.'.$role->slug, [], null, $adminLang)}}</li>
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/jasny/jasny-bootstrap.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/cropper/cropper.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/ladda/spin.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/ladda/ladda.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/ladda/ladda.jquery.min.js') !!}" type="text/javascript"></script>
@stop