<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('styles')
    <link rel="stylesheet" href="{!! asset('css/plugins/ladda/ladda.min.css') !!}" />
    @parent
    <link rel="stylesheet" href="{!! asset('css/plugins/jasny/jasny-bootstrap.min.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/plugins/cropper/cropper.min.css') !!}" />
{{--    <link rel="stylesheet" href="{!! asset('css/plugins/ladda/ladda-themeless.min.css') !!}" />--}}
@stop

@section('title', trans('messages.label_profile', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('messages.menu_profile', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('profile_backend', $namespace, $adminLang) !!}
        </div>
    </div>
    {{-- CONTENT --}}
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            @if(session('error'))
                                <div class="alert alert-danger animated fadeInDown alert-dismissable">
                                    <button class="close" type="button" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{session('error')}}
                                </div>
                            @endif
                            @if(session('success'))
                                <div class="alert alert-success animated fadeInDown alert-dismissable">
                                    <button class="close" type="button" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{session('success')}}
                                </div>
                            @endif
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="profile-image">
                                        @if($avatar = $user->avatar)
                                            <img class="img-thumbnail m-b-md" alt="profile"
                                                 src="{{asset(config('custom.PROFILE_AVATAR_PATH').$avatar)}}">
                                        @else
                                            <img class="img-thumbnail m-b-md" alt="profile"
                                                 src="{{asset(config('custom.PROFILE_AVATAR_PATH').'anonym.jpg')}}">
                                        @endif
                                    </div>
                                    <div class="profile-info">
                                        <div class="">
                                            <div>
                                                <h2 class="no-margins">
                                                    {{$user->first_name}}&nbsp;
                                                    {{$user->last_name}}
                                                </h2>
                                                <h4>
                                                    {{trans('messages.'.$user->roles()->first()->slug, [], null, $adminLang)}}
                                                </h4>
                                                <form id="avatarForm" action="/{{$namespace}}/profile/change_avatar"
                                                      enctype="multipart/form-data" method="POST">
                                                    {{csrf_field()}}
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <span class="btn btn-primary btn-outline btn-file">
                                                            <i class="fa fa-upload"></i>
                                                            <span class="fileinput-new">
                                                                {{trans('messages.label_change_avatar', [], null, $adminLang)}}
                                                            </span>
                                                            <span class="fileinput-exists">
                                                                {{trans('messages.label_change', [], null, $adminLang)}}
                                                            </span>
                                                            <input type="file" name="avatar" id="file-input"/>
                                                            <input type="hidden" name="cropperParams" value="">
                                                        </span>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-b-md">
                                    <a href="/{{$namespace}}/profile/{{$user->id}}/edit" class="btn btn-white btn-xs pull-right">
                                        {{trans('messages.label_edit_user', [], null, $adminLang)}}
                                    </a>
                                    <button id="destroyUser" class="btn btn-danger btn-xs pull-right" data-action="/{{$namespace}}/profile/{{$user->id}}/delete" data-toggle="modal" data-target="#delete">
                                        {{trans('messages.label_delete', [], null, $adminLang)}}
                                    </button>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <dl class="dl-horizontal">
                                    <dt>{{trans('messages.label_status', [], null, $adminLang)}}:</dt>
                                    <dd><span class="label label-primary">Active</span></dd>
                                </dl>
                            </div>
                            <div class="col-lg-6">
                                <dl class="dl-horizontal" >
                                    <dt>{{trans('messages.label_email', [], null, $adminLang)}}:</dt>
                                    <dd><a class="text-navy" href="#">{{$user->email}}</a></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <dl class="dl-horizontal">
                                    <dt>{{trans('messages.label_country', [], null, $adminLang)}}:</dt>
                                    <dd>{{($user->country_id)?$user->country->name:'--'}}</dd>
                                    <dt>{{trans('messages.label_zip', [], null, $adminLang)}}:</dt>
                                    <dd>{{$user->zip ?: '--'}}</dd>
                                    <dt>{{trans('messages.label_state', [], null, $adminLang)}}:</dt>
                                    <dd>{{$user->state ?: '--'}}</dd>
                                    <dt>{{trans('messages.label_region', [], null, $adminLang)}}:</dt>
                                    <dd>{{$user->region ?: '--'}}</dd>
                                    <dt>{{trans('messages.label_city', [], null, $adminLang)}}:</dt>
                                    <dd>{{$user->city ?: '--'}}</dd>
                                    <dt>{{trans('messages.label_address', [], null, $adminLang)}}:</dt>
                                    <dd>
                                        @if (!empty($street))
                                            {{$user->street}}&nbsp;
                                            {{trans('messages.label_block', [], null, $adminLang)}}&nbsp;{{$user->block}}&nbsp;
                                            {{trans('messages.label_ap', [], null, $adminLang)}}&nbsp;{{$user->apartment}}
                                        @endif
                                    </dd>
                                </dl>
                            </div>
                            <div class="col-lg-7" id="cluster_info">
                                <dl class="dl-horizontal" >
                                    <dt>{{trans('messages.label_gender', [], null, $adminLang)}}:</dt>
                                    <dd>{{trans('messages.label_'.$user->gender, [], null, $adminLang) ?: '--'}}</dd>
                                    <dt>{{trans('messages.label_age', [], null, $adminLang)}}:</dt>
                                    <dd>{{ $age ? : '--'}}</dd>
                                    <dt>{{trans('messages.label_mobile_phone', [], null, $adminLang)}}:</dt>
                                    <dd>{{$user->mobile_phone ?: '--'}}</dd>
                                    <dt>{{trans('messages.label_personal_phone', [], null, $adminLang)}}:</dt>
                                    <dd>{{$user->personal_phone ?: '--'}}</dd>
                                    <dt>{{trans('messages.label_last_updated', [], null, $adminLang)}}:</dt>
                                    <dd>{{$user->updated_at}}</dd>
                                    <dt>{{trans('messages.label_created', [], null, $adminLang)}}:</dt>
                                    <dd>{{$user->created_at}} </dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <dl class="dl-horizontal">
                                    <dt>{{trans('messages.label_completed', [], null, $adminLang)}}:</dt>
                                    <dd>
                                        <div class="progress progress-striped active m-b-sm">
                                            @if ($user->row_completed < 50)
                                                <?php $progress_status="danger"?>
                                            @elseif($user->row_completed >= 50 && $user->row_completed < 80)
                                                <?php $progress_status="warning"?>
                                            @else
                                                <?php $progress_status=""?>
                                            @endif
                                            <div style="width: {{$user->row_completed}}%;" class="progress-bar {{$progress_status}}"></div>
                                        </div>
                                        <small>{!! trans('messages.text_profile_completed',['percent'=>$user->row_completed], null, $adminLang) !!}</small>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('panel.modal.imagecrop')
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/jasny/jasny-bootstrap.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/cropper/cropper.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/ladda/spin.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/ladda/ladda.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/plugins/ladda/ladda.jquery.min.js') !!}" type="text/javascript"></script>
@stop