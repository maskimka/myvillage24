@extends('layouts/authentication.app')

@section('title', trans('messages.label_restore_password', [], null, $adminLang))

@section('auth_h3_title', trans('messages.label_restore_password', [], null, $adminLang)." Myvillage")

@section('auth_p_title', trans('messages.label_restore_password_type_new_password', [], null, $adminLang))

@section('content')
    <form class="m-t" role="form" action="/auth/reset" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            @if (count($errors) > 0)
                <label class="error">{{ $errors->first() }}</label>
            @endif
            <input data-i18n="[placeholder]admin.password" type="password" name="password" class="form-control"
                   placeholder="{{trans('messages.label_password', [], null, $adminLang)}}">
        </div>
        <div class="form-group">
            <input data-i18n="[placeholder]admin.confirm_password" type="password" name="password_confirmation" class="form-control"
                   placeholder="{{trans('messages.label_confirm_password', [], null, $adminLang)}}">
        </div>
        <input type="hidden" name="user_id" value="{{$user_id}}">
        <input type="hidden" name="code" value="{{$code}}">
        <button data-i18n="admin.send" type="submit" class="btn btn-primary block full-width m-b">
            {{trans('messages.label_send', [], null, $adminLang)}}
        </button>
    </form>
@endsection