@extends('layouts/authentication.app')

@section('title', trans('messages.auth_h3_title', [], null, $adminLang))

@if(!$isCompleted)
    @section('auth_h3_title', trans('messages.auth_h3_title', [], null, $adminLang))
    @section('auth_p_title', trans('messages.registration_p_title', [], null, $adminLang))
    @section('content')
        <form id="registrationUserForm" class="m-t" role="form" action="/auth/registration" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            @if ($errors->has('first_name'))
                <label class="error">{{ $errors->first('first_name') }}</label>
            @endif
            <input data-i18n="[placeholder]admin.first_name" type="text" name="first_name"
                   class="form-control{{ ($errors->has('first_name'))? ' error':'' }}"
                   placeholder="{{trans('messages.label_first_name', [], null, $adminLang)}}" value="{{Request::old('first_name')}}">
        </div>
        <div class="form-group">
            @if ($errors->has('last_name'))
                <label class="error">{{ $errors->first('last_name') }}</label>
            @endif
            <input data-i18n="[placeholder]admin.last_name" type="text" name="last_name"
                   class="form-control{{ ($errors->has('last_name'))? ' error':'' }}"
                   placeholder="{{trans('messages.label_last_name', [], null, $adminLang)}}" value="{{Request::old('last_name')}}">
        </div>
        <div class="form-group">
            @if ($errors->has('email'))
                <label class="error">{{ $errors->first('email') }}</label>
            @endif
            <input data-i18n="[placeholder]admin.email" type="email" name="email"
                   class="form-control{{ ($errors->has('email'))? ' error':'' }}"
                   placeholder="{{trans('messages.label_email', [], null, $adminLang)}}" value="{{Request::old('email')}}">
        </div>
        <div class="form-group">
            @if ($errors->has('password'))
                <label class="error">{{ $errors->first('password') }}</label>
            @endif
            <input data-i18n="[placeholder]admin.password" id="password" type="password" name="password"
                   class="form-control{{ ($errors->has('password'))? ' error':'' }}"
                   placeholder="{{trans('messages.label_password', [], null, $adminLang)}}">
        </div>
        <div class="form-group">
            <input data-i18n="[placeholder]admin.confirm_password" type="password" name="password_confirm"
                   class="form-control" placeholder="{{trans('messages.label_confirm_password', [], null, $adminLang)}}">
        </div>
        <div class="form-group">
            <select name="gender" class="form-control">
                <option data-i18n="admin.chose_gender" value="N">{{trans('messages.chose_gender', [], null, $adminLang)}}</option>
                <option data-i18n="admin.M" value="M">{{trans('messages.label_M', [], null, $adminLang)}}</option>
                <option data-i18n="admin.W" value="W">{{trans('messages.label_W', [], null, $adminLang)}}</option>
            </select>
        </div>
        {{--<div class="form-group profile">--}}
            {{--<div class="i-checks">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="gender" value="M" checked>{{trans('messages.label_M')}}--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="i-checks">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="gender" value="W">{{trans('messages.label_W')}}--}}
                {{--</label>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="form-group">
            @if ($errors->has('first_name'))
                <label id="country" class="error">{{ $errors->first('country_id') }}</label>
            @endif
            <select class="chosen-select" name="country_id" class="{{ ($errors->has('country_id'))? 'error':'' }}">
                <option data-i18n="admin.country" value="" disabled selected>{{trans('messages.label_country', [], null, $adminLang)}}</option>
                @foreach ($countries as $country)
                    @if (Request::old('country_id') == $country->id)
                        <option data-i18n="admin.{{ $country->name }}" selected value="{{ $country->id }}">
                            {{trans('countries.'.$country->name, [], null, $adminLang)}}
                        </option>
                    @else
                        <option data-i18n="admin.{{ $country->name }}" value="{{ $country->id }}">
                            {{trans('countries.'.$country->name, [], null, $adminLang)}}
                        </option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            @if ($errors->has('terms'))
                <label id="country" class="error">{{ $errors->first('terms') }}</label>
            @endif
            <div class="checkbox i-checks">
                <label>
                    <input id="id" type="checkbox" name="terms">&nbsp;<i></i>  <a data-i18n="admin.terms" href="/terms">
                        {{trans('messages.label_terms', [], null, $adminLang)}}
                    </a>
                </label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary block full-width m-b" disabled>
            <i class="fa fa-spinner hidden"> </i>
            <span data-i18n="admin.registration"><strong>{{trans('messages.label_registration', [], null, $adminLang)}}</strong></span>
        </button>

        <p class="text-muted text-center">
            <a href="/auth/login">
                <small data-i18n="admin.have_account">{{trans('messages.label_have_account', [], null, $adminLang)}}</small>
            </a>
        </p>
    </form>
    @endsection
    @else
        @section('auth_h3_title', trans('messages.label_registration_success', [], null, $adminLang))
        @section('auth_p_title', '')
        @section('content')
            <div data-i18n="admin.check_email_for_activation" class="alert alert-success text-center">
                {{trans('messages.label_check_email_for_activation', [], null, $adminLang)}}
            </div>
            <p class="m-t text-center"> <small>My Village &copy; 2018</small> </p>
        @endsection
    @endif
