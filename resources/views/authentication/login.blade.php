@extends('layouts/authentication.app')

@section('title', trans('message.label_auth', [], null, $adminLang))

@section('auth_h3_title', trans('messages.label_auth', [], null, $adminLang))

@section('auth_p_title', trans('messages.auth_p_title', [], null, $adminLang))

@section('content')
    <form id="loginForm" class="m-t" role="form" action="/auth/login" method="post">
        {{ csrf_field() }}
        @if(session('success'))
            <div class="alert alert-success text-center">
                {{trans(session('success'), [], null, $adminLang)}}
            </div>
        @endif
        <div class="form-group">
            @if(session('error'))
                <label class="error"> {{trans(session('error'), [], null, $adminLang)}}</label>
            @endif
            <input data-i18n="[placeholder]admin.email" type="text" name="email" class="form-control"
                   placeholder="{{trans('messages.label_email', [], null, $adminLang)}}">
        </div>
        <div class="form-group">
            <input data-i18n="[placeholder]admin.password" type="password" name="password" class="form-control"
                   placeholder="{{trans('messages.label_password', [], null, $adminLang)}}">
        </div>
        <div class="form-group">
            <div class="checkbox i-checks">
                <label>
                    <input type="checkbox" name="remember_me">&nbsp;<i></i><span data-i18n="admin.remember_me">
                        {{trans('messages.label_remember_me', [], null, $adminLang)}}</span>
                </label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary block full-width m-b">
            <i class="fa fa-spinner hidden"> </i>
            <span data-i18n="admin.login">{{trans('messages.label_login', [], null, $adminLang)}}</span>
        </button>
        <div class="social-login-btn">
            <a href="/auth/socialite/facebook" class="btn btn-social-icon btn-facebook">
                <span class="fa fa-facebook"></span>
            </a>
            <a href="/auth/socialite/google" class="btn btn-social-icon btn-google">
                <span class="fa fa-google"></span>
            </a>
        </div>
        <p class="text-muted text-center">
            <a href="/auth/forgot-password">
                <small data-i18n="admin.forgot_password">{{trans('messages.label_forgot_password', [], null, $adminLang)}}</small>
            </a>
        </p>
        <p class="text-muted text-center">
            <a href="/auth/registration">
                <small data-i18n="admin.have_no_account">{{trans('messages.label_have_no_account', [], null, $adminLang)}}</small>
            </a>
        </p>
    </form>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        if (window.location.hash && window.location.hash == '#_=_') {
            window.location.href = window.location.origin + window.location.pathname;
        }
    </script>
@endsection