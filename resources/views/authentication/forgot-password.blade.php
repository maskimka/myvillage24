@extends('layouts/authentication.app')

@section('title', trans('messages.label_restore_password', [], null, $adminLang))

@section('auth_h3_title', trans('messages.label_restore_password', [], null, $adminLang)." Myvillage")

@section('auth_p_title', trans('messages.label_restore_password_send_email_info', [], null, $adminLang))

@section('content')
    <form class="m-t" role="form" action="/auth/forgot-password" method="post">
        {{ csrf_field() }}
        @if(session('success'))
            <div class="alert alert-success">
                {{ trans('messages.'.session('success'), [], null, $adminLang) }}
            </div>
        @endif
        <div class="form-group">
            <input data-i18n="admin.have_no_account" type="email" name="email" class="form-control" placeholder="{{trans('messages.label_email', [], null, $adminLang)}}">
        </div>
        <button data-i18n="admin.send" type="submit" class="btn btn-primary block full-width m-b">{{trans('messages.label_send', [], null, $adminLang)}}</button>
    </form>
    <p class="m-t text-center"> <small>My Village &copy; {{ date('Y') }}</small> </p>
@endsection
