<?php $extend_path = 'layouts.'.$namespace.'.app' ?>
@extends($extend_path)

@section('title', trans('messages.architecture_config', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.architecture_config">{{ trans('messages.architecture_config', [], null, $adminLang) }}</h2>
            {!! Breadcrumbs::render('config_architecture_backend', $namespace, $adminLang) !!}
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label data-i18n="admin.chose_country">
                                        {{ trans('messages.chose_country', [], null, $adminLang) }}
                                    </label>
                                    <select id="countryIso" name="country_iso2" class="form-control">
                                        <option data-i18n="admin.not_chosen" value="0">
                                            {{ trans('messages.not_chosen', [], null, $adminLang) }}
                                        </option>
                                        @foreach($countries as $key => $country)
                                            <option data-i18n="admin.{{ $country->name }}" value="{{ $key }}">
                                                {{ trans('countries.'.$country->name, [], null, $adminLang) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <br>
                                <button data-i18n="admin.menu_create" id="addConfig" data-toggle="modal"
                                        data-target="#addConfigAp" class="btn b-btn btn-primary" disabled>
                                    {{ trans('messages.menu_create', [], null, $adminLang) }}</button>
                            </div>
                        </div>
                        <hr>
                        <form action="/admin/config_ap/store" method="post">
                            <div id="settingsContent" class="row"></div>
                            <input type="hidden" name="country_iso2">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="addConfigAp" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/admin/config_ap/create" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 data-i18n="admin.create_config" class="modal-title">
                        {{ trans('messages.create_config', [], null, $adminLang) }}
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label data-i18n="admin.config_name">{{ trans('messages.config_name', [], null, $adminLang) }}
                            <small data-i18n="admin.example_config">
                                {{ trans('messages.example_config', [], null, $adminLang) }}: gray_price
                            </small>
                        </label>
                        <input type="text" name="key" class="form-control">
                    </div>
                    <div class="form-group">
                        <label data-i18n="admin.value">{{ trans('messages.value', [], null, $adminLang) }}</label>
                        <input type="text" name="value" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button data-i18n="admin.button_cancel" type="button" class="btn b-btn btn-secondary" data-dismiss="modal">
                            {{ trans('messages.button_cancel', [], null, $adminLang) }}
                        </button>
                        <button data-i18n="admin.button_send" class="btn b-btn btn-primary">
                            {{ trans('messages.button_send', [], null, $adminLang) }}
                        </button>
                    </div>
                    <input type="hidden" name="country_iso2">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="PUT">
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
