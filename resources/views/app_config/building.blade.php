<?php $extend_path = 'layouts.'.$namespace.'.app' ?>
@extends($extend_path)

@section('title', trans('messages.building_config', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.building_config">{{ trans('messages.building_config', [], null, $adminLang) }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a data-i18n="admin.menu_dashboard" href="/{{$namespace}}/dashboard">
                        {{ trans('messages.menu_dashboard', [], null, $adminLang) }}
                    </a>
                </li>
                <li class="active">
                    <strong data-i18n="admin.building_config">
                        {{ trans('messages.building_config', [], null, $adminLang) }}
                    </strong>
                </li>
            </ol>
        </div>
    </div>

    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
