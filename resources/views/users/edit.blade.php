<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('title', trans('messages.label_profile_edit', [], null, $adminLang))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2 data-i18n="admin.profile_edit">{{ trans('messages.label_profile_edit', [], null, $adminLang) }}</h2>
        {!! Breadcrumbs::render('user_edit_backend', $namespace, $client->id, $adminLang) !!}
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        @include('errors.error_list')
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <form id="wizard_user_form" action="/{{$namespace}}/users/{{ $client->id }}" class="wizard-big edit_user" method="POST">
                        <h1 data-i18n="admin.personal_info">{{trans('messages.label_personal_info', [], null, $adminLang)}}</h1>
                        <fieldset>
                            <h2 data-i18n="admin.personal_info">{{trans('messages.label_personal_info', [], null, $adminLang)}}</h2>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="profile-image">
                                        @if($avatar = $client->avatar)
                                            <img class="img-thumbnail m-b-md" alt="profile"
                                                 src="{{asset(config('custom.PROFILE_AVATAR_PATH').$avatar)}}">
                                        @else
                                            <img class="img-thumbnail m-b-md" alt="profile"
                                                 src="{{asset(config('custom.PROFILE_AVATAR_PATH').'anonym.jpg')}}">
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.first_name" id="userName" value="{{$client->first_name}}"
                                                   name="first_name" type="text" class="form-control required"
                                                   placeholder="{{trans('messages.label_first_name', [], null, $adminLang)}} *">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                            <input data-i18n="[placeholder]admin.last_name" value="{{$client->last_name}}"
                                                   name="last_name" type="text" class="form-control required"
                                                   placeholder="{{trans('messages.label_last_name', [], null, $adminLang)}} *">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group birthday date">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.birthday" id="userBirthday"
                                                   value="{{$client->birthday}}" name="birthday" type="text"
                                                   class="form-control date-mask"
                                                   placeholder="{{trans('messages.label_birthday', [], null, $adminLang)}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <select name="gender" class="form-control">
                                            <option data-i18n="admin.not_chosen" value="N" {{ ($user->gender === 'N')?'selected':'' }}>
                                                {{trans('messages.not_chosen', [], null, $adminLang)}}
                                            </option>
                                            <option data-i18n="admin.man" value="M" {{ ($user->gender === 'M')?'selected':'' }}>
                                                {{trans('messages.man', [], null, $adminLang)}}
                                            </option>
                                            <option data-i18n="admin.woman" value="W" {{ ($user->gender === 'W')?'selected':'' }}>
                                                {{trans('messages.woman', [], null, $adminLang)}}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1 data-i18n="admin.address">{{trans('messages.label_address', [], null, $adminLang)}}</h1>
                        <fieldset>
                            <h2 data-i18n="admin.address">{{trans('messages.label_address', [], null, $adminLang)}}</h2>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <select id="userCountry" class="chosen-select" name="country_id">
                                            <option data-i18n="admin.country" value="" disabled>
                                                {{trans('messages.label_country', [], null, $adminLang)}}
                                            </option>
                                            @foreach ($countries as $country)
                                                @if (Request::old('country_id') == $country->id || $client->country_id == $country->id)
                                                    <option data-i18n="admin.{{ $country->name }}" selected value="{{ $country->id }}"
                                                            data-content="{{$country->phone_mask}}">
                                                        {{ $country->name }}
                                                    </option>
                                                @else
                                                    <option data-i18n="admin.{{ $country->name }}" value="{{ $country->id }}">
                                                        {{ $country->name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.zip" id="userZip" name="zip" type="text"
                                                   class="form-control"
                                                   placeholder="{{trans('messages.label_zip', [], null, $adminLang)}}" value="{{$client->zip}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.state" id="userState" name="state" type="text"
                                                   class="form-control"
                                                   placeholder="{{trans('messages.label_state', [], null, $adminLang)}}" value="{{$client->state}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.region" id="userRegion" name="region"
                                                   type="text" class="form-control"
                                                   placeholder="{{trans('messages.label_region', [], null, $adminLang)}}" value="{{$client->region}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.city" id="userCity" name="city" type="text"
                                                   class="form-control" placeholder="{{trans('messages.label_city', [], null, $adminLang)}}"
                                                   value="{{$client->city}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-address-book-o"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.street" id="userStreet" name="street"
                                                   type="text" class="form-control"
                                                   placeholder="{{trans('messages.label_street', [], null, $adminLang)}}"
                                                   value="{{$client->street}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 nopadding">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input data-i18n="[placeholder]admin.block_long" id="userBlock" name="block"
                                                       type="text" class="form-control"
                                                       placeholder="{{trans('messages.label_block_long', [], null, $adminLang)}}"
                                                       value="{{$client->block}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 nopadding">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-address-book-o"></i>
                                                </span>
                                                <input data-i18n="[placeholder]admin.ap_long" id="userApartment" name="apartment"
                                                       type="text" class="form-control"
                                                       placeholder="{{trans('messages.label_ap_long', [], null, $adminLang)}}"
                                                       value="{{$client->apartment}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1 data-i18n="admin.contacts">{{trans('messages.label_contacts', [], null, $adminLang)}}</h1>
                        <fieldset>
                            <h2 data-i18n="admin.contacts">{{trans('messages.label_contacts', [], null, $adminLang)}}</h2>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.email" id="userEmail" name="email" type="text"
                                                   class="form-control email-mask"
                                                   placeholder="{{trans('messages.label_email', [], null, $adminLang)}}" value="{{$client->email}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-mobile"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.mobile_phone" id="userPersonalPhone"
                                                   name="personal_phone" type="text" class="form-control phone mobile-mask"
                                                   placeholder="{{trans('messages.label_mobile_phone', [], null, $adminLang)}}"
                                                   value="{{$client->personal_phone}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </span>
                                            <input data-i18n="[placeholder]admin.personal_phone" id="userMobilePhone"
                                                   name="mobile_phone" type="text" class="form-control phone phone-mask"
                                                   placeholder="{{trans('messages.label_personal_phone', [], null, $adminLang)}}"
                                                   value="{{$client->mobile_phone}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="PUT">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/inputmask/jquery.inputmask.js') !!}" type="text/javascript"></script>
@stop