<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('title', trans('messages.label_new_user', [], null, $adminLang))

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2 data-i18n="admin.new_user">{{ trans('messages.label_new_user', [], null, $adminLang) }}</h2>
        {!! Breadcrumbs::render('user_create_backend', $namespace, $adminLang) !!}
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        @include('errors.error_list')
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-heading">
                    <div class="ibox-title">
                        <h5 data-i18n="admin.form_new_user">{{trans('messages.form_new_user', [], null, $adminLang)}}</h5>
                    </div>
                </div>
                <div class="ibox-content">
                    <form action="/{{$namespace}}/users" class="wizard-big edit_user" method="POST">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        <input data-i18n="[placeholder]admin.first_name" id="userName" value="{{ old('first_name') }}"
                                               name="first_name" type="text" class="form-control required"
                                               placeholder="{{trans('messages.label_first_name', [], null, $adminLang)}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                        <input data-i18n="[placeholder]admin.last_name" id="" value="{{ old('last_name') }}"
                                               name="last_name" type="text" class="form-control required"
                                               placeholder="{{trans('messages.label_last_name', [], null, $adminLang)}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                        <input data-i18n="[email]admin.email" id="userEmail" name="email" type="text"
                                               class="form-control email-mask" value="{{ old('email') }}"
                                               placeholder="{{trans('messages.label_email', [], null, $adminLang)}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-lock"></i>
                                            </span>
                                            <input data-i18n="[password]admin.password" name="password" type="password"
                                                   class="form-control" value="{{ old('email') }}"
                                                   placeholder="{{trans('messages.label_password', [], null, $adminLang)}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <input data-i18n="[placeholder]admin.password" id="generatePassword" data-size="8"
                                               data-character-set="a-z,A-Z,0-9" type="button" class="form-control"
                                               placeholder="{{trans('messages.label_password', [], null, $adminLang)}}"
                                               value="{{trans('messages.label_generate_password', [], null, $adminLang)}}">
                                    </div>
                                </div>
                                <div class="form-group profile">
                                    <div class="i-checks">
                                        <label>
                                            <input type="radio" name="gender" value="M" checked>
                                            <span data-i18n="admin.M">
                                                {{trans('messages.label_M', [], null, $adminLang)}}
                                            </span>
                                        </label>
                                    </div>
                                    <div class="i-checks">
                                        <label>
                                            <input type="radio" name="gender" value="W">
                                            <span data-i18n="admin.W">
                                                {{trans('messages.label_W', [], null, $adminLang)}}
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select id="userCountry" class="chosen-select" name="country_id">
                                        <option data-i18n="admin.country" value="0" selected>
                                            {{trans('messages.label_country', [], null, $adminLang)}}
                                        </option>
                                        @foreach ($countries as $country)
                                            <option data-i18n="admin.{{ $country->name }}" value="{{ $country->id }}">
                                                {{trans('countries.'.$country->name, [], null, $adminLang)}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button data-i18n="admin.save" role="submit" class="btn btn-primary">
                                        {{trans('messages.label_save', [], null, $adminLang)}}
                                    </button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        {{--<input type="hidden" name="_method" value="PUT">--}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @parent
    <script src="{!! asset('js/plugins/inputmask/jquery.inputmask.js') !!}" type="text/javascript"></script>
@stop