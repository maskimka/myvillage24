<?php $namespace = Request::segment(1)?>
<?php $extend_path = 'layouts.'.$namespace.'.app' ?>

@extends($extend_path)

@section('styles')
    @parent
@stop

@section('title', trans('messages.label_users_list', [], null, $adminLang))

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 data-i18n="admin.user_list">{{trans('messages.label_users_list', [], null, $adminLang)}}</h2>
            {!! Breadcrumbs::render('users_backend', $namespace, $adminLang) !!}
        </div>
    </div>
    {{--CONTENT--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        <table id="userList" class="table table-foo" data-sorting="true" data-filtering="true" data-paging="true">
                            <thead>
                            <tr>
                                <th>
                                    #ID
                                </th>
                                <th data-i18n="admin.user_avatar">
                                    {{trans('messages.label_user_avatar', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.user_name">
                                    {{trans('messages.label_user_name', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.gender" data-breakpoints="xs sm md">
                                    {{trans('messages.label_gender', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.age" data-breakpoints="xs sm md">
                                    {{trans('messages.label_age', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.role" data-breakpoints="xs sm">
                                    {{trans('messages.label_role', [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.email" data-breakpoints="all">
                                    {{trans("messages.label_email", [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.personal_phone" data-breakpoints="all">
                                    {{trans("messages.label_personal_phone", [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.mobile_phone" data-breakpoints="all">
                                    {{trans("messages.label_mobile_phone", [], null, $adminLang)}}
                                </th>
                                <th data-i18n="admin.address" data-breakpoints="all">
                                    {{trans("messages.label_address", [], null, $adminLang)}}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $user)
                                <tr data-content="{{$user->id}}">
                                    <td>
                                        {{ $key + 1 }}
                                    </td>
                                    <td>
                                        <div class="dropdown profile-element">
                                            <a href="#">
                                                <span class="clear" title="{{$user->first_name}} {{$user->last_name}}">
                                                    <span>
                                                        @if($avatar = $user->avatar)
                                                            <img class="img-thumbnail img-md"
                                                                 src="{{asset(config('custom.PROFILE_AVATAR_PATH').$avatar)}}" alt="image">
                                                        @else
                                                            <img class="img-thumbnail img-md"
                                                                 src="{{asset(config('custom.PROFILE_AVATAR_PATH').'anonym.jpg')}}" alt="image">
                                                        @endif
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        {{$user->first_name}}&nbsp;{{$user->last_name}}
                                    </td>
                                    <td>
                                        <span data-i18n="admin.{{ $user->gender }}">
                                            {{ trans(($user->gender == "M"?"messages.label_M":"messages.label_W"), [], null, $adminLang) }}
                                        </span>
                                    </td>
                                    <td>
                                        {{ ($user['age'])?$user['age']:'--' }}
                                    </td>
                                    <td>
                                        <select data-i18n="[placeholder]admin.select_placeholder" id="userRoles"
                                                name="roles" class="chosen-no-search" data-user="{{ $user->id }}"
                                                multiple data-placeholder="{{ trans('messages.label_select_placeholder', [], null, $adminLang) }}">
                                            @foreach($roles as $role)
                                                <option data-i18n="admin.{{ $role->slug }}"
                                                        {{ (in_array($role->id, $user['roles'])?'selected':'') }}
                                                        value="{{ $role->id }}">
                                                    {{trans('messages.'.$role->slug, [], null, $adminLang)}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                       {{ $user->email }}
                                    </td>
                                    <td>
                                       {{ $user->personal_phone }}
                                    </td>
                                    <td>
                                       {{ $user->mobile_phone }}
                                    </td>
                                    <td>
                                       {{ $user->city }}{{ ($user->street)?', '.$user->street:'' }},
                                        <span data-i18n="admin.block">{{trans('messages.label_block', [], null, $adminLang)}}</span> {{ ($user->block)?:'' }},
                                        <span data-i18n="admin.ap">{{trans('messages.label_ap', [], null, $adminLang)}}</span> {{ ($user->apartment)?:'' }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        if(!!window.performance && window.performance.navigation.type === 2)
        {
            window.location.reload();
        }
    </script>
@endsection