<div class="modal fade" id="wishlistOrderModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 data-i18n="admin.send_request">{{ trans('messages.label_send_request', [], null, $adminLang) }}</h2>
                <p data-i18n="admin.fill_fields">{{ trans('messages.fill_fields', [], null, $adminLang) }}</p>
            </div>
            <form id="ptoRequestForm" method="POST" action="/{{ $namespace }}/pto_request">
                <div class="modal-body">
                    @if ($user)
                        <div class="form-group">
                            <input type="text" class="form-control" name="name"
                                   value="{{ $user->first_name .' '. $user->last_name }}" readonly>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <input data-i18n="[placeholder]admin.phone" type="text" class="form-control" name="phone"
                                   value="{{ $user->phone }}" placeholder="{{ trans('messages.label_phone', [], null, $adminLang) }}">
                        </div>
                    @else
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="{{ 'Anonymous Client' }}" readonly>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" value="">
                        </div>
                        <div class="form-group">
                            <input data-i18n="[placeholder]admin.phone" type="text" class="form-control" name="phone"
                                   value="" placeholder="{{ trans('messages.label_phone', [], null, $adminLang) }}">
                        </div>
                    @endif
                    <div class="form-group">
                        <textarea data-i18n="[placeholder]admin.comments" class="form-control" name="comment" rows="6"
                                  placeholder="{{ trans('messages.label_comments', [], null, $adminLang) }}"></textarea>
                    </div>
                    <p>
                        <span data-i18n="admin.attached">{{ trans('messages.attached', [], null, $adminLang) }}</span>
                        <span id="articlesCount"></span> <span data-i18n="admin.articles">
                            {{ trans('messages.articles', [], null, $adminLang) }}
                        </span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button data-i18n="admin.send" class="btn btn-primary">
                        {{ trans('messages.label_send', [], null, $adminLang) }}
                    </button>
                    <button data-i18n="admin.close" type="button" class="btn btn-secondary" data-dismiss="modal">
                        {{ trans('messages.label_close', [], null, $adminLang) }}
                    </button>
                </div>
                <input type="hidden" name="_method" value="POST">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>