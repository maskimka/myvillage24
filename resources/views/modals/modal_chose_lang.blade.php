<div id="modalChoseLang" class="modal inmodal" aria-hidden="true" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 data-i18n="admin.lang_translation" class="modal-title">
                    {{ trans('messages.lang_translation', [], null, $adminLang) }}
                </h4>
                <small data-i18n="admin.lang_translation_explanation">
                    {{ trans('messages.lang_translation_explanation', [], null, $adminLang) }}
                </small>
            </div>
            <div class="modal-body">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="form-group">
                        <select id="locale" class="chosen-select-nosearch">
                            @foreach (config('locales') as $locale => $enabled)
                                @if ($enabled)
                                    <option data-i18n="admin.{{ $locale }}" value="{{ $locale }}">
                                        {{ trans('messages.label_'.$locale, [], null, $adminLang) }}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button data-i18n="admin.button_cancel" class="btn btn-default" data-dismiss="modal" type="button">
                    {{ trans('messages.button_cancel', [], null, $adminLang) }}
                </button>
                <a data-i18n="admin.continue" id="createRegistration" role="button" data-redirect-lang="{{ $adminLang }}"
                   class="btn b-btn b-btn--color-green b-btn--rounded-light">
                    {{ trans('messages.continue', [], null, $adminLang) }}
                </a>
            </div>
        </div>
    </div>
</div>