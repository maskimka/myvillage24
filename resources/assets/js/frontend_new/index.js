import 'js-marker-clusterer/src/markerclusterer_compiled';

class IndexPage {

    constructor()
    {

        let that = this;

        that.objects = window._data_objects;
        that.markers = [];
        that.current_info_window = false;
        that.search_query = {};
        that.card_visible = false;

        that.initHandlers();
        that.initRangeSlider();
    }

    initHandlers()
    {
        let that = this;
    }

    initMap()
    {
        let that = this;

        let coords = {lat: 47.020751, lng: 28.856090};

        that.map = new google.maps.Map(document.getElementById('js-objects-map'), {
            zoom: 12,
            center: coords,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            mapTypeControl: true,
            mapTypeControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            }
        });

        that.objects.forEach(function (object, index) {

            let marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(object.lat),
                    lng: parseFloat(object.lng)
                },
                icon: '/img/pin_active.png',
                map: that.map,
                marker_id: parseInt(object.id),
                //card_template : template
            });

            that.markers.push(
                marker
            );

            //that.attachClickEvent(marker, object);

        });

        that.initMarkerCluster();
    }

    attachClickEvent(marker, object) {
        let that = this;

        marker.addListener('click', function() {

            //that.openInfoWindow(marker);

            console.log(marker);


            $('.b-object-modal-card').html(marker.card_template);

            if (that.card_visible == false) {
                $('.b-object-modal-card').addClass('is-showing');
                that.card_visible = true;
            }else {
                $('.b-object-modal-card').addClass('is-pulsing');
            }

        });

    }

    openInfoWindow(marker) {
        let that = this;

        if (that.current_info_window) {
            that.current_info_window.close();
        }

        that.current_info_window = marker.infowindow;
        marker.infowindow.open(that.map, marker);
    }

    initMarkerCluster() {
        let that = this;

        that.mc_active = new MarkerClusterer(that.map, that.markers, {
            zoom: 10,
            styles: [
                {
                    textColor: 'white',
                    url: '/img/pin-circle.png',
                    height: 50,
                    width: 50,
                    textSize: 16
                }
            ]
        });
    }


}

window._IndexPage = new IndexPage();