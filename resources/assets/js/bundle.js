/*App Components*/
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './components/vuex/store.vue'

Vue.use(VueAxios, axios);

Vue.component('floors', require('./components/floors/floors.vue'));
Vue.component('floor-item', require('./components/floors/floor-item.vue'));
Vue.component('house-zone-list-item', require('./components/floors/house_zone_list_item.vue'));
Vue.component('house-zone-collapse', require('./components/floors/house_zone_collapse.vue'));
Vue.component('modal', require('./components/floors/modal.vue'));
Vue.component('bar-chart', require('./components/dashboard/bar-chart.vue'));

Vue.component('attached-lands', require('./components/attach_lands/attached-land.vue'));
Vue.component('attached-buildings', require('./components/attach_lands/attached-building.vue'));

/*Chosen custome directive*/
Vue.directive('decimal-mask', {

    twoWay: true,

    inserted: function (el, binding) {
        $(el).inputmask({
            alias:"decimal",
            integerDigits:6,
            digits:2,
            allowMinus:false,
            digitsOptional: false,
            placeholder: "0",
            rightAlign: false
        });
    }
});

/* Image uploader custom directive */
Vue.directive('image_uploader', {
    twoWay: true,
    inserted: function (el, binding) {
        var boxHTML = '<div class="fileuploader-items">' +
            '<ul class="fileuploader-items-list">' +
            '<li class="fileuploader-thumbnails-input" style="width: 100%;">'+
            '   <div class="fileuploader-thumbnails-input-inner">' +
            '      <img src="/img/common/placeholder.jpg" style="max-width: 100%;max-height: 100%">'+
            '      <span>+</span>'+
            '   </div>' +
            '</li>' +
            '</ul>' +
            '</div>';
        var itemHTML = '<li class="fileuploader-item" style="width: 100%;">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '<div class="progress-holder">${progressBar}</div>' +
            '</div>' +
            '</li>';
        var item2HTML = '<li class="fileuploader-item" style="width: 100%;">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '</div>' +
            '</li>';
        var files = [];
        
        var image =  binding.value[0];

        if (typeof image !== 'undefined') {

            var fileElement = {};
                fileElement.name = image.name;
                fileElement.type = 'image/jpeg';
                fileElement.file = '/'+image.path+'/thumbnails/'+image.name;

            files.push(fileElement);
        }

        $(el).fileuploader({
            // listInput: null,
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: ' ',
            theme: 'thumbnails',
            enableApi: true,
            addMore: false,
            files:files,
            thumbnails: {
                box: boxHTML,
                item: itemHTML,
                item2: item2HTML,
                startImageRenderer: true,
                canvasImage: false,
                removeConfirmation: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    if(api.getFiles().length >= api.getOptions().limit) {
                        plusInput.hide();
                    }

                    plusInput.insertAfter(item.html);


                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        setTimeout(function() {
                            html.remove();

                            if(api.getFiles().length - 1 < api.getOptions().limit) {
                                plusInput.show();
                            }
                        }, 100);
                    });
                }
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = $.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            }
        });
    }
});

Vue.directive('collapsed', {
    twoWay: true,
    inserted: function(el) {
        // Collapse ibox function
        $(el).on('click', function () {
            var ibox = $(this).closest('div.ibox');
            var button = $(this).find('i');
            var content = ibox.children('.ibox-content');
            content.slideToggle(200);
            button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
            ibox.toggleClass('').toggleClass('border-bottom');
            setTimeout(function () {
                ibox.resize();
                ibox.find('[id^=map-]').resize();
            }, 50);
        });
    }
});


var app = new Vue({

    el: "#wrapper",

    data: {

        message: 'Hello',

        house_zone_items: []
    },

    store,

});