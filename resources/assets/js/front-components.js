/*App Components*/
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import BootstrapVue from 'bootstrap-vue';

import VueTruncate from 'vue-truncate-filter';
import VueGoogleAutocomplete from './components/listing/lands/VueGoogleAutocomplete.vue';
import GoogleMap from './components/googlemap/googleMap.vue';
import googleMarkerPlacePopup from './components/googlemap/googleMarkerPlacePopup.vue';
import wishlist from './components/listing/wishlist/wishlist.vue';
import homePage from './components/listing/homepage/homepage.vue';
import VueRange from 'vue-slider-component/src/vue2-slider.vue';

import store from './components/vuex/store.vue';

Vue.use(VueAxios, axios);
Vue.use(VueTruncate);
Vue.use(BootstrapVue);

Vue.component('land-filter', require('./components/listing/lands/land_filter.vue'));
Vue.component('home-land-filter', require('./components/listing/lands/home/home_land_filter.vue'));
Vue.component('home-building-filter', require('./components/listing/building/home/home_building_filter.vue'));
Vue.component('land-list', require('./components/listing/lands/land_list.vue'));
Vue.component('home-architecture-filter', require('./components/listing/architecture/home/home_architecture_filter.vue'));
Vue.component('architecture-filter', require('./components/listing/architecture/architecture_filter.vue'));
Vue.component('architecture-list', require('./components/listing/architecture/architecture_list.vue'));
Vue.component('building-filter', require('./components/listing/building/building_filter.vue'));
Vue.component('building-list', require('./components/listing/building/building_list.vue'));
Vue.component('vue-google-autocomplete', VueGoogleAutocomplete);
Vue.component('google-map', GoogleMap);
Vue.component('google-place-popup', googleMarkerPlacePopup);
Vue.component('vue-range-slider', VueRange);
Vue.component('wishlist', wishlist);
// Vue.component('homePage', homePage);


//================================
//    Custom directory area
//================================
Vue.directive('chosen', {
    deep: true,
    twoWay: true, // note the two-way binding
    bind: function(){
    },
    inserted: function (el, binding) {
        $(el).chosen({
            inherit_select_classes: false,
            placeholder_text_single: "Выбрать",
            width: '100%',
            disable_search_threshold: 999
        }).change(function(){
            var id = $(el).attr('id');

            if (id == 'region') {
                app.cities[0].parentId = $(el).val();
                app.districts[0].parentId = 0;

            } else if (id == 'city') {
                app.districts[0].parentId = $(el).val();

            } else if (id == 'district') {
                app.districtFilter = parseInt($(el).val());
            }

        });
    },

    update: function(el) {

    },

    componentUpdated: function(el) {

        $(el).trigger("chosen:updated");

    }
});

Vue.directive('key-accepter',{
    deep: true,
    twoWay: true,
    inserted: function(el) {
        $(el).keypress(function(e){

            var regex = new RegExp('^[0-9.]$');
            var id    = $(el).attr('id');

            if(e.key == '.' && $(this).val() == "") {

                $(this).val('0');
            }

            if (!regex.test(e.key)) {

                return false;
            }

        });

    }

});

var app = new Vue({

    el: "#wrapper",

    data: function() {
        return {

            cities: [{
                level: 2,
                parentId: 0
            }],

            districts: [{
                level: 3,
                parentId: 0
            }],

        }
    },

    store,
    
});

global.app = app;