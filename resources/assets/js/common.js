$(document).ready(function(){

    //================= Modals ===============
    $('.video-play').on('click', function() {
        $('#modalMainVideo').modal('show');
    });

    $('#mapView, #mobile-map').on('click', function() {
        $('#modalMap').modal('show');
    });

    $('.land-map').on('click', function() {
        $('#modalLandMap').modal('show');
    });

    $('#modalMap').on('hidden.bs.modal', function () {
        $('.b-object-modal-card').removeClass('is-showing');
    });

    $('#modalMainVideo').on('hidden.bs.modal', function () {
        $("#modalMainVideo iframe").attr("src", $("#modalMainVideo iframe").attr("src"));
    });

    $('.land-video-play').on('click', function() {
        $('#modalLandVideo').modal('show');
    });

    $('#modalLandVideo').on('hidden.bs.modal', function () {
        $("#modalLandVideo iframe").attr("src", $("#modalLandVideo iframe").attr("src"));
    });

    $('.home-architecture-block .price i').on('mouseover', function() {
        $('#priceMeaning').modal('show');
    });

    if ($('.b-object-thumbs__item-link').length > 0) {

        $('.b-object-thumbs__item-link').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });

    }

    if ($('.b-object-thumbs__item-link-construction').length > 0) {

        $('.b-object-thumbs__item-link-construction').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });

    }

    if ($('.b-object-thumbs__item-link-facade').length > 0) {

        $('.b-object-thumbs__item-link-facade').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });

    }

    if ($('.b-object-thumbs__item-img-plan').length > 0) {

        $('.b-object-thumbs__item-img-plan').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });

    }

    if ($('.b-object-thumb__list').length > 0) {
        $('.b-object-thumb__list').each(function () {
            $(this).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        });
    }

    if ($('.b-object-thumb__list-group').length > 0) {
        $('.b-object-thumb__list-group').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });
    }

    if ($('.js-image-cards-gallery').length > 0) {

        $('.js-image-cards-gallery').each(function () {
            $(this).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        });

    }
    if ($('.js-image-cards-gallery-group').length > 0) {

        $('.js-image-cards-gallery-group').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
            }
        });

    }

    //


    if ($('.js-product-carousel').length > 0) {

        $('.js-product-carousel').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3
                    }
                }
            ]
        });

    }

    if ($('.b-object-carousel').length > 0) {


        $('.b-object-carousel').magnificPopup({
           type: 'image',
           delegate: 'a',
           gallery: {
               enabled: true,
               tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
           }
        });
    }

    $('.view-mode-link').on('click', function(e) {

        e.preventDefault();
        e.stopPropagation();
        
        var that = this;
        var mode = $(that).data('cards');
        
        $('.view-mode-link').removeClass('active')
        $(that).addClass('active');
        
        if(mode == 'grid') {
            $('.b-grid-cards').removeClass('b-grid-1').addClass('b-grid-3').find('.b-card').addClass('b-grid-column');
        }
        
        if(mode == 'row') {
            $('.b-grid-cards').removeClass('b-grid-3').addClass('b-grid-1').find('.b-card').removeClass('b-grid-column');
        }
    });

    $('#modalMainVideo').on('hidden.bs.modal', function (e) {
        var src = $('#modalMainVideo').find('iframe').prop('src');
        $('#modalMainVideo').find('iframe').attr('src', '');
        $('#modalMainVideo').find('iframe').attr('src', src);
    });

    $('.js-open-sidebar').on('click', function () {

        $('body').toggleClass('sidebar-open');

        $('.b-sidebar-overlay').addClass('is-visible');
        $('.b-sidebar-overlay').addClass('opening');
    });


    function closeSidebar() {
        $('body').toggleClass('sidebar-open');

        $('.b-sidebar-overlay').removeClass('opening');
        $('.b-sidebar-overlay').addClass('closing');
        setTimeout(function () {
            $('.b-sidebar-overlay').removeClass('is-visible');
            $('.b-sidebar-overlay').removeClass('closing');
        }, 300);
    }
    $('.js-close-sidebar').on('click', function () {

        closeSidebar();
    });

    $('body').on('click', '.b-sidebar-overlay.is-visible', function () {

        closeSidebar();
    });

    /* Slick */
    var myCarousel = $('.b-slick-vertical-container');

    myCarousel.slick({
        infinite: false,
        slidesToShow: 7,
        slidesToScroll: 1,
        swipeToSlide: true,
        vertical: true,
        speed: 400,
        prevArrow: '<button id="prev" class="b-slick-btn b-slick-prev"><i class="fa fa-angle-up"></i></button>',
        nextArrow: '<button id="next" class="b-slick-btn b-slick-next"><i class="fa fa-angle-down"></i></button>',
    });

    myCarousel.mousewheel(function(e) {
        e.preventDefault();

        if (e.deltaY < 0) {
            $(this).slick('slickNext');
        }
        else {
            $(this).slick('slickPrev');
        }
    });

    /*Order form popup*/
    $('#ordersForm').on('show.bs.modal', function(e) {

        var self = this;
        $(this).find('input[name="id"]').val(e.relatedTarget.id);
        clearOrderForm();

        $('#OrderForm').validate({
            errorClass: 'control-label',
            errorPlacement: function (error, element)
            {
                error.insertBefore(element);

                element.parent().addClass('has-error')
            },
            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                name: Lang.get('validation.name_required'),
                phone: {
                    required: Lang.get('validation.phone_required'),
                    minlength: Lang.get('validation.phone_minlength')
                }
            },
            submitHandler: function() {
                var formData = $('#OrderForm').serialize();
                $.ajax({
                    url: e.relatedTarget.value,
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: formData,
                    dataType:'json',
                    success: function(response) {

                        $(self).modal('hide');

                        $('#orderSuccessModal').modal('show');
                    },
                    error: function() {

                    }
                });

                return false;
            }
        });

    });

    function clearOrderForm() {
        $("label.error").hide();
        $(".error").removeClass("error");
        document.getElementById("OrderForm").reset();
    }

    /*SEND MAIL CONTACTS*/
    $('#contactForm').validate({

        errorClass: 'control-label',
        errorPlacement: function (error, element)
        {
            error.insertBefore(element);

            element.parent().addClass('has-error')
        },
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true
            }
        },
        submitHandler: function() {

            var form = $('#contactForm').serialize();

            $.ajax({
                url: 'send_contact',
                type: 'post',
                data: form,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function() {
                    swal({
                        title: Lang.get('messages.text_thanks'),
                        text: Lang.get('messages.text_email_sent'),
                        type: "success",
                        confirmButtonColor: "#1AB394"
                    });
                    $('#contactForm')[0].reset();

                },
                error: function(e) {

                    swal({
                        title: Lang.get('messages.text_error_occurred'),
                        text: Lang.get('messages.text_email_not_sent'),
                        type: "error",
                        confirmButtonColor: "#ED5565",
                    });
                },
                complete: function() {

                }
            });

            return false;
        }
    });

    $('.btn-favorite').on('click', function(event) {
        var self = this;
        var element_id = $(self).data('id');
        var element_type = $(self).data('type');
        var clickedElement = $(event.currentTarget);
        var classes = '';

        var formData = {
            product_type: element_type,
            product_id: element_id
        };

        if ($(clickedElement).hasClass('active')) {

            classes = $(clickedElement).attr('class');

            $('a[class="' + classes + '"]').each(function(index, value) {
                $(value).removeClass('active');
            });

            $('#wishlistCountItems').text(parseInt($('#wishlistCountItems').text()) - 1);

            $.ajax({
                url: "/" + Lang.getLocale() + "/wishlist/" + element_id + '|' + element_type,
                type: 'DELETE',
                data: formData,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function() {
                    $("#wl-success-alert").addClass('active');

                    setTimeout(function() {
                        $("#wl-success-alert").removeClass('active');
                    }, 1000);
                },
                error: function(e) {
                    $("#wl-danger-alert").addClass('active');

                    setTimeout(function() {
                        $("#wl-danger-alert").removeClass('active');
                    }, 1000);
                }
            });
        } else {
            classes = $(clickedElement).attr('class');

            $('a[class="' + classes + '"]').each(function(index, value) {
                $(value).addClass('active');
            });

            $('#wishlistCountItems').text(parseInt($('#wishlistCountItems').text()) + 1);

            $.ajax({
                url: "/" + Lang.getLocale() + "/wishlist",
                type: 'post',
                data: formData,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function() {
                    $("#wl-success-alert").addClass('active');

                    setTimeout(function() {
                        $("#wl-success-alert").removeClass('active');
                    }, 1000);
                },
                error: function(e) {
                    $("#wl-danger-alert").addClass('active');

                    setTimeout(function() {
                        $("#wl-danger-alert").removeClass('active');
                    }, 1000);
                }
            });
        }
    });

    /*Prices popover*/
    $('#priceBuildingPopover').popover({
        toggle: "manual",
        content: "<b>" + Lang.get('pages.prices_title') + ":</b>"+
        "<ol>"+
        "<li>" + Lang.get('pages.prices_subtitle1') + "</li>"+
        "<li>" + Lang.get('pages.prices_subtitle2') + "</li>"+
        "<li>" + Lang.get('pages.prices_subtitle3') + "</li>"+
        "<li>" + Lang.get('pages.prices_subtitle4') + "</li>"+
        "</ol>"+
        "<div class='text-right'>"+
        "<a href='/prices' class='btn b-btn b-btn--color-green b-btn--size-sm popover-more'>" + Lang.get('messages.label_details') + "</a> "+
        "<button class='btn b-btn btn-default b-btn--size-sm popover-close'>" + Lang.get('messages.label_close') + "</button>" +
        "</div>",

        placement: "bottom",
        animation: true
    }).on('shown.bs.popover', function () {
        $('#priceBuildingFacadePopover').popover('hide');
        var $popup = $(this);
        $(this).next('.popover').find('button.popover-close').click(function (e) {
            $popup.popover('hide');
        });
    }).on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 1000);
    });

    $('#priceBuildingFacadePopover').popover({
        content:
        "<b>" + Lang.get('pages.prices_title') + ":</b>"+
        "<ol>"+
        "<li>" + Lang.get('pages.prices_subtitle1') + "</li>"+
        "<li>" + Lang.get('pages.prices_subtitle2') + "</li>"+
        "<li>" + Lang.get('pages.prices_subtitle3') + "</li>"+
        "<li>" + Lang.get('pages.prices_subtitle4') + "</li>"+
        "<li>" + Lang.get('pages.prices_subtitle5') + "</li>"+
        "<li>" + Lang.get('pages.prices_subtitle6') + "</li>"+
        "<li>" + Lang.get('pages.prices_subtitle7') + "</li>"+
        "</ol>"+
        "<div class='text-right'>"+
        "<a href='/prices' class='btn b-btn b-btn--color-green b-btn--size-sm popover-more'>" + Lang.get('messages.label_details') + "</a> "+
        "<button class='btn b-btn btn-default b-btn--size-sm popover-close'>" + Lang.get('messages.label_close') + "</button>"+
        "</div>",

        placement: "bottom",
        animation: true
    }).on('shown.bs.popover', function () {
        var $popup = $(this);

        $(this).next('.popover').addClass('high-price-popover').find('button.popover-close').click(function (e) {
            $popup.popover('hide');
        });
    }).on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 1000);
    });


    /* Share short url*/
    var popoverTemplate =
        '<div ref="popovertemplate" class="share-popover">' +
        '<h4 class="mb-1">' + Lang.get('messages.label_copy_link') +'</h4>' +
        '<p class="mb-2">' + Lang.get('messages.label_link_product') +'</p>' +
            '<div class="input-group b-input-group">' +
                '<input id="shortSharedLink" type="text" class="form-control b-form-control" value="" readonly="">' +
                '<div class="input-group-button input-group-btn">' +
                    '<button class="btn clipboard-copy" data-clipboard-target="#shortSharedLink">' +
                        '<i class="fa fa-clone icon" aria-hidden="true"></i>' +
                    '</button>' +
                '</div>' +
            '</div>' +
            '<div class="input-group ">' +
        '<button class="btn b-btn b-btn--wide b-btn--color-green popover-close">' + Lang.get('messages.label_share_close') +'</button>' +
            '</div>' +
        '</div>';

    var sharePopover =
        $('[data-toggle="popover"]').popover({
            html:true,
            trigger:'click',
            content: popoverTemplate
        });

    $(sharePopover).on('show.bs.popover', function(){
        var host   = document.location.href;
        var country = $('#userCountry').val();

        $.ajax({
            url: "/api/getShortUrl?longUrl="+host+'&country=' + country,
            type: 'get',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                var textElement = $('.share-popover input[type="text"]');
                if (response.short_url) {
                    $(textElement).val(response.short_url);
                } else {
                    $(textElement).val(host+'&country=' + country);
                }
            },
            error: function(e) {

            }
        });
    });

    $(sharePopover).on('shown.bs.popover', function(){
        var elementClipboardButton = document.getElementsByClassName('clipboard-copy');
        var clipboard = new Clipboard(elementClipboardButton);
            clipboard.on('success', function(e) {
                $("#wl-success-alert").addClass('active');

                setTimeout(function() {
                    $("#wl-success-alert").removeClass('active');
                }, 1000);
            });
            clipboard.on('error', function(e) {
                $("#wl-danger-alert").addClass('active');

                setTimeout(function() {
                    $("#wl-danger-alert").removeClass('active');
                }, 1000);
            });
    });

    $(document).on("click", ".popover .popover-close" , function(){
        $(this).parents(".popover").popover('hide');
    });

    let paths = document.querySelectorAll('.world-map .active');
    paths.forEach(function(item, i, paths) {
        $(item).on('click', function(){
            window.location.href = Lang.getLocale() + "/lands?requested_country=" + $(this).data('land');
        });
        setTimeout(function() {svgToggleClass(item, i);}, 100 * i);
    });

    function svgToggleClass(item, i) {
        item.classList.toggle('on');
        setTimeout(function () { item.classList.toggle('on'); }, 700);
    }
    
    $('.world-map .active').hover(function() {
        $('.world-info').css({ 'display': 'inline-block' });
        let country = $(this).attr('id');
        $('.world-info__country').html(country);
    }, function() {
        $('.world-info').css({ 'display': 'none' });
    })
    $('.world-map').mousemove(function(e) {
        let offset = $('.world-map').offset();
        let worldH = $('.world-info').height();
        let worldW = $('.world-info').width();
        let mouseX = e.pageX - offset.left + 13;
        let mouseY = e.pageY - offset.top - worldH - 20;
        $('.world-info').css({
            'top': mouseY,
            'left': mouseX
        });
    })

    $('#wishlistOrderModal').on('show.bs.modal', function(e) {
        var articles = $('input[name="products[]"]');

        $('#articlesCount').html(articles.length);
        console.log(articles.length);
        $(articles).each(function(key, item) {

            var element = '';
            if ($(item).data('type') == 'Land') {
                element = "<input type='hidden' name='models[lands][]' value='" + $(item).val() + "'>";
            }
            if ($(item).data('type') == 'Architecture') {
                element = "<input type='hidden' name='models[architecture][]' value='" + $(item).val() + "'>";
            }
            if ($(item).data('type') == 'Building') {
                element = "<input type='hidden' name='models[buildings][]' value='" + $(item).val() + "'>";
            }
            $('#ptoRequestForm').append(element);
        });

        if (!articles.length) {
            e.preventDefault();
            // swal({
            //     title: "Внимание",
            //     text: "Добавьте пожалуйста минимум 1 артикль",
            //     type: "warning",
            //     confirmButtonColor: "#1AB394",
            //     confirmButtonText: "Продолжить",
            //     closeOnConfirm: true,
            // });
        }
    });

    function resizable(el, factor) {
        var int = Number(factor) || 7.7;
        function resize() { el.style.width = ((el.value.length ) * int) + 'px' }
        var e = 'keyup,keypress,focus,blur,change'.split(',');
        for (var i in e) el.addEventListener(e[i], resize, false);
        resize();
    }
    let filter_elements = document.querySelectorAll('.filter-input');
    filter_elements.forEach(function (item, i, filter_elements) {
        resizable(item, 10)
    });

    //Max scripts
    var mySwiper_lands = new Swiper('.b-list-lands__container', {
        // Optional parameters
        loop: false,
        slidesPerView: 4,
        spaceBetween: 25,

        // Navigation arrows
        navigation: {
            nextEl: '.b-list-lands-next',
            prevEl: '.b-list-lands-prev',
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
        breakpoints: {
            500: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 15
            },
            1200: {
                slidesPerView: 3,
                spaceBetween: 20
            }
        }
    });

    var mySwiper_architecture = new Swiper('.b-list-architecture__container', {
        // Optional parameters
        loop: false,
        slidesPerView: 2,
        slidesPerColumn: 2,
        spaceBetween: 25,

        // Navigation arrows
        navigation: {
            nextEl: '.b-list-architecture-next',
            prevEl: '.b-list-architecture-prev',
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
        breakpoints: {
            // when window width is <= 320px
            480: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            // when window width is <= 480px
            768: {
                slidesPerView: 1,
                spaceBetween: 15
            }
        }
    });
    var mySwiper_buildings = new Swiper('.b-list-buildings__container', {
        // Optional parameters
        loop: false,
        slidesPerView: 1,
        spaceBetween: 0,

        // Navigation arrows
        navigation: {
            nextEl: '.b-list-buildings-next',
            prevEl: '.b-list-buildings-prev',
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        }
    });

    $('.lang-link').on('click', function(e) {
        e.preventDefault();
        let locale = $(this).data('locale');
        let clearPathWithoutDomainAndLocale = window.location.pathname.substring(4);
        let urlParameters = window.location.search;

       window.location = '/' + locale + '/' + clearPathWithoutDomainAndLocale + urlParameters;
    });

    $('.b-list-objects__slider').on('click', function(e) {
        let target = e.target;
        if ($(target).hasClass('b-list-objects-btn')) {
            e.preventDefault();
        }
        console.log(target);
    });
    
    $('#view-change').on('change', function(e) {
        if ($(this).is(':checked')) {
            $('.b-view-inner').removeClass('widescreen');
        }else {
            $('.b-view-inner').addClass('widescreen');
        }
    });

    let header_height = $('.b-topnav').height() + $('.b-navbar').height() + $('.b-view-header').height();
    let map_height = window.innerHeight - header_height;
    $('.b-view-map').height(map_height + 'px');

    $(window).on('resize', function() {
        let header_height = $('.b-topnav').height() + $('.b-navbar').height() + $('.b-view-header').height();
        let map_height = window.innerHeight - header_height;
        $('.b-view-map').height(map_height + 'px');
    });

    var mySwiper_buildings = new Swiper('.b-swiper-lands', {
        // Optional parameters
        loop: false,
        slidesPerView: 1,
        spaceBetween: 0,

        // Navigation arrows
        navigation: {
            nextEl: '.b-swiper-lands-btn-next',
            prevEl: '.b-swiper-lands-btn-prev',
        },

        // And if we need scrollbar
        pagination: {
            el: '.b-swiper-lands-pagination',
            bulletClass: 'b-swiper-lands-bullet',
            bulletActiveClass: 'active'
        }
    });

});