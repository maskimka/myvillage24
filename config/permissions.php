<?php
return [
    'admin' => [
        '/admin/dashboard' => false,
        '/admin/land' => false,
        '/admin/land/*' => false,
        '/admin/land/activate' => false,
        '/admin/land/*/complement' => false,
        '/admin/land/change_sale_status' => false,
        '/admin/architecture' => false,
        '/admin/building' => false,
        '/admin/order' => false,
        '/admin/user' => false,
        '/admin/role' => false,
        '/admin/seo' => false,
    ]
];