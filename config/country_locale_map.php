<?php
return [
    'AT' => 'en',
    'CH' => 'en',
    'RU' => 'ru',
    'UA' => 'ru',
    'MD' => 'ro',
    'RO' => 'ro',
    'ES' => 'es',
    'DE' => 'de',
];