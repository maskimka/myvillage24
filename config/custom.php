<?php
return [
    'PROFILE_AVATAR_PATH'             => 'img/uploads/profile/',
    'LAND_IMAGES_PATH'                => 'img/uploads/lands/',
    'ARCHITECTURE_IMAGES_PATH'        => 'img/uploads/architecture/',
    'BUILDING_IMAGES_PATH'            => 'img/uploads/building/',
    'VILLAGE_IMAGES_PATH'             => 'img/uploads/village/',
    'FLOOR_IMAGES_PATH'               => 'img/uploads/floors/',
    'DOCS_PATH'                       => 'docs',
    'RESIZE_LAND_IMAGE_FIT_W'         => 400,
    'RESIZE_LAND_IMAGE_FIT_H'         => 300,
    'RESIZE_THUMB_WIDTH'              => 400,
    'RESIZE_THUMB_HEIGHT'             => 300,

    'LIVING_ZONE' => [
        'catalog_bedroom',
        'catalog_cabinet',
        'catalog_living_room',
    ],

    'BUILDING_PRICE'        => 300,
    'BUILDING_FACADE_PRICE' => 380,

    'ROOMS'     => 'catalog_bedroom',
    'BATHROOMS' => 'catalog_bathroom',
    'GARAGES'   => 'catalog_garage',
    'RESTROOM'  => 'catalog_restroom',

    'default_country'     => 3,
    'default_country_iso' => 'MD'
];