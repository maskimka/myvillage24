<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVillagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('village', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0);
            $table->unsignedInteger('country_id')->default(0);
            $table->string('article')->nullable();
            $table->string('title')->nullable();
            $table->decimal('general_area', 8, 2)->default(0.00);
            $table->string('general_plan', 255)->nullable();
            $table->decimal('from_city', 8, 2)->default(0.00);
            $table->longText('description')->nullable();
            $table->boolean('is_active')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->engine = 'InnoDB';
        });

        Schema::create('village_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('village_id')->default(0);
            $table->string('locale', 2);
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('enabled')->default(1);

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('village');
        Schema::dropIfExists('village_translations');
    }
}
