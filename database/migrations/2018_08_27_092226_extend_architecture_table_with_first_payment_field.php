<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendArchitectureTableWithFirstPaymentField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('architecture', function (Blueprint $table) {
            $table->decimal('first_payment', 10,2)->default(0.0)->after('full_high_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('architecture', 'first_payment')) {
            Schema::table('architecture', function (Blueprint $table) {
                $table->dropColumn('first_payment');
            });
        }
    }
}
