<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPricesColumnsToArchitectureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('architecture', function (Blueprint $table) {

            $table->decimal('full_low_price', 10,2)->default(0.0)->after('length');
            $table->decimal('full_high_price', 10,2)->default(0.0)->after('full_low_price');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('architecture', 'full_low_price')) {
            Schema::table('architecture', function (Blueprint $table) {
                $table->dropColumn('full_low_price');
            });
        }

        if (Schema::hasColumn('architecture', 'full_high_price')) {
            Schema::table('architecture', function (Blueprint $table) {
                $table->dropColumn('full_high_price');
            });
        }
    }
}
