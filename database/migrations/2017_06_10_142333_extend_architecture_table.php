<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendArchitectureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('architecture', function (Blueprint $table) {
            $table->text('description')->nullable()->after('house_section');
            $table->text('exterior_walls')->nullable()->change();
            $table->text('overlappings')->nullable()->change();
            $table->text('roof')->nullable()->change();
            $table->text('boiler')->nullable()->change();
        });

        Schema::table('architecture_history', function (Blueprint $table) {
            $table->text('description')->nullable()->after('house_section');
            $table->text('exterior_walls')->nullable()->change();
            $table->text('overlappings')->nullable()->change();
            $table->text('roof')->nullable()->change();
            $table->text('boiler')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('architecture', 'description')) {
            Schema::table('architecture', function (Blueprint $table) {
                $table->dropColumn('description');
            });
        }

        if (Schema::hasColumn('architecture_history', 'description')) {
            Schema::table('architecture_history', function (Blueprint $table) {
                $table->dropColumn('description');
            });
        }
    }
}
