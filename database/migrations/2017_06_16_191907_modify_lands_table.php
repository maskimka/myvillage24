<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyLandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lands', function (Blueprint $table) {
            $table->integer('district')->after('state');
            $table->integer('region')->default(0)->change();
            $table->integer('city')->default(0)->change();
        });

        Schema::table('land_history', function (Blueprint $table) {
            $table->integer('district')->after('state');
            $table->integer('region')->default(0)->change();
            $table->integer('city')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
