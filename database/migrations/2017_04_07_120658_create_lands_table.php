<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0);
            $table->string('article')->nullable();
            $table->string('cadastral_number')->nullable();
            $table->unsignedInteger('country_id')->default(0);
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('state')->nullable();
            $table->string('street')->nullable();
            $table->string('street_number', 10)->nullable();
            $table->decimal('area', 8, 2)->default(0.00);
            $table->decimal('from_city', 8, 2)->default(0.00);
            $table->unsignedInteger('purpose_id')->default(1);
            $table->unsignedInteger('sale_status_id')->default(1);
            $table->decimal('first_payment',10,2)->default(0.0);
            $table->decimal('price',10,2)->default(0.0);
            $table->longText('description')->nullable();
            $table->decimal('lat',20,18)->nullable();
            $table->decimal('lng',20,18)->nullable();
            $table->string('size_map')->nullable();
            $table->string('cadastral_map')->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_deleted')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            /* INDEXES */
            $table->unique('article');
            $table->index('user_id');
            $table->index('size_map');
            $table->index('cadastral_number');
            $table->index('city');
            $table->index('region');
            $table->index('state');
            $table->index('street');
            $table->index('street_number');
            $table->index('area');
            $table->index('from_city');
            $table->index('purpose_id');
            $table->index('sale_status_id');
            $table->index('first_payment');
            $table->index('price');
            $table->index('is_active');
            $table->index('is_deleted');
        });

        Schema::create('land_communications', function (Blueprint $table) {
            $table->integer('land_id')->unsigned();
            $table->integer('communication_id')->unsigned();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            $table->primary(['land_id', 'communication_id']);
        });

        Schema::create('land_infrastructure', function (Blueprint $table) {
            $table->integer('land_id')->unsigned();
            $table->integer('infrastructure_id')->unsigned();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            $table->primary(['land_id', 'infrastructure_id']);
        });

        Schema::create('land_transport', function (Blueprint $table) {
            $table->integer('land_id')->unsigned();
            $table->integer('transport_id')->unsigned();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            $table->primary(['land_id', 'transport_id']);
        });

        Schema::create('land_size_map', function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->unsignedInteger('land_id');
            $table->string('name', 45);
            $table->decimal('value',8,2);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            /* INDEXES */
            $table->index('land_id');
            $table->index('value');
            $table->index('created_at');
            $table->index('updated_at');
        });

        Schema::create('land_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('acted_user_id')->default(0);
            $table->unsignedInteger('land_id')->default(0);
            $table->string('article')->nullable();
            $table->string('cadastral_number')->nullable();
            $table->unsignedInteger('country_id')->default(0);
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('state')->nullable();
            $table->string('street')->nullable();
            $table->string('street_number', 10)->nullable();
            $table->decimal('area', 8, 2)->default(0.00);
            $table->decimal('from_city', 8, 2)->default(0.00);
            $table->unsignedInteger('purpose_id')->default(1);
            $table->unsignedInteger('sale_status_id')->default(1);
            $table->decimal('first_payment',8,2)->default(0.00);
            $table->decimal('price', 8,2)->default(0.00);
            $table->longText('description')->nullable();
            $table->decimal('lat',20,18)->nullable();
            $table->decimal('lng',20,18)->nullable();
            $table->string('size_map')->nullable();
            $table->string('cadastral_map')->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_deleted')->default(0);
            $table->string('action', 50)->default('default');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            /* INDEXES */
            $table->index('user_id');
            $table->index('acted_user_id');
            $table->index('land_id');
            $table->index('article');
            $table->index('cadastral_number');
            $table->index('city');
            $table->index('region');
            $table->index('state');
            $table->index('area');
            $table->index('from_city');
            $table->index('purpose_id');
            $table->index('sale_status_id');
            $table->index('first_payment');
            $table->index('price');
            $table->index('is_active');
            $table->index('is_deleted');
            $table->index('action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
        Schema::dropIfExists('land_communications');
        Schema::dropIfExists('land_infrastructure');
        Schema::dropIfExists('land_transport');
        Schema::dropIfExists('land_history');
        Schema::dropIfExists('land_size_map');
    }
}
