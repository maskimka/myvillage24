<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendArchitectureProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('architecture', function (Blueprint $table) {

            $table->decimal('width')->default(0.00)->after('celling_height');
            $table->decimal('length')->default(0.00)->after('width');
        });

        Schema::table('architecture_history', function (Blueprint $table) {

            $table->decimal('width')->default(0.00)->after('celling_height');
            $table->decimal('length')->default(0.00)->after('width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('architecture', 'width')) {
            Schema::table('architecture', function (Blueprint $table) {
                $table->dropColumn('width');
            });
        }

        if (Schema::hasColumn('architecture', 'length')) {
            Schema::table('architecture', function (Blueprint $table) {
                $table->dropColumn('length');
            });
        }

        if (Schema::hasColumn('architecture_history', 'width')) {
            Schema::table('architecture_history', function (Blueprint $table) {
                $table->dropColumn('width');
            });
        }

        if (Schema::hasColumn('architecture_history', 'length')) {
            Schema::table('architecture_history', function (Blueprint $table) {
                $table->dropColumn('length');
            });
        }
    }
}
