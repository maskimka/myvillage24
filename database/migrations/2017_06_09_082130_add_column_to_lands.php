<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToLands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lands', function (Blueprint $table) {
            $table->string('land_video')->nullable()->after('description');
        });

        Schema::table('land_history', function (Blueprint $table) {
            $table->string('land_video')->nullable()->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('lands', 'land_video')) {
            Schema::table('lands', function (Blueprint $table) {
                $table->dropColumn('land_video');
            });
        }

        if (Schema::hasColumn('land_history', 'land_video')) {
            Schema::table('land_history', function (Blueprint $table) {
                $table->dropColumn('land_video');
            });
        }
    }
}
