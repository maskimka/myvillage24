<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyArchitecturePrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('architecture', function (Blueprint $table) {
            $table->renameColumn('price', 'key_price');
        });

        Schema::table('architecture_history', function (Blueprint $table) {
            $table->renameColumn('price', 'key_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('architecture', function (Blueprint $table) {
            $table->renameColumn('key_price', 'price');
        });

        Schema::table('architecture_history', function (Blueprint $table) {
            $table->renameColumn('key_price', 'price');
        });
    }
}
