<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendLandHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('land_history', function (Blueprint $table) {
            $table->decimal('rectangle_width', 8, 2)->default(0.00)->after('area');
            $table->decimal('rectangle_length', 8, 2)->default(0.00)->after('rectangle_width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('land_history', 'rectangle_width')) {
            Schema::table('land_history', function (Blueprint $table) {
                $table->dropColumn('rectangle_width');
            });
        }

        if (Schema::hasColumn('land_history', 'rectangle_length')) {
            Schema::table('land_history', function (Blueprint $table) {
                $table->dropColumn('rectangle_length');
            });
        }
    }
}
