<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyBuildingPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buildings', function (Blueprint $table) {
            $table->renameColumn('price', 'first_payment');
        });

        Schema::table('buildings_history', function (Blueprint $table) {
            $table->renameColumn('price', 'first_payment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buildings', function (Blueprint $table) {
            $table->renameColumn('first_payment', 'price');
        });

        Schema::table('buildings_history', function (Blueprint $table) {
            $table->renameColumn('first_payment', 'price');
        });
    }
}
