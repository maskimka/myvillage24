<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('page_id')->default(0)->index();
            $table->string('name')->nullable()->index();
            $table->string('meta_title')->nullable()->index();
            $table->string('meta_key')->nullable()->index();
            $table->string('meta_description')->nullable()->index();
            $table->boolean('is_active')->default(1);
            $table->boolean('is_deleted')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

//        Schema::create('meta_attributes', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('title')->nullable()->index();
//            $table->string('name')->nullable()->index();
//            $table->string('content')->nullable()->index();
//            $table->boolean('is_active')->default(0);
//            $table->boolean('is_deleted')->default(0);
//            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
//            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
//        });

        Schema::create('meta_opengraph', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->index();
            $table->string('description')->nullable()->index();
            $table->string('url')->nullable()->index();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_deleted')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        Schema::create('seoables', function (Blueprint $table) {
            $table->integer('seo_id');
            $table->morphs('seoables');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
        
        Schema::create('opengraphables', function (Blueprint $table) {
            $table->integer('meta_opengraph_id');
            $table->morphs('opengraphables');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_templates');
        Schema::dropIfExists('meta_opengraph');
        Schema::dropIfExists('opengraphables');
        Schema::dropIfExists('seoables');
    }
}
