<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVillageCommunications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('village_communications', function (Blueprint $table) {
            $table->integer('village_id')->unsigned();
            $table->integer('communication_id')->unsigned();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            $table->primary(['village_id', 'communication_id']);
        });

        Schema::create('village_infrastructure', function (Blueprint $table) {
            $table->integer('village_id')->unsigned();
            $table->integer('infrastructure_id')->unsigned();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            $table->primary(['village_id', 'infrastructure_id']);
        });

        Schema::create('village_transport', function (Blueprint $table) {
            $table->integer('village_id')->unsigned();
            $table->integer('transport_id')->unsigned();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            $table->primary(['village_id', 'transport_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('village_communications');
        Schema::dropIfExists('village_infrastructure');
        Schema::dropIfExists('village_transport');
    }
}
