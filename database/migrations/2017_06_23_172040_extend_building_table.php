<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendBuildingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buildings', function (Blueprint $table) {

            $table->decimal('width')->default(0.00)->after('celling_height');
            $table->decimal('length')->default(0.00)->after('width');
            $table->decimal('construction_area')->default(0.00)->after('builtup_area');
        });

        Schema::table('buildings_history', function (Blueprint $table) {

            $table->decimal('width')->default(0.00)->after('celling_height');
            $table->decimal('length')->default(0.00)->after('width');
            $table->decimal('construction_area')->default(0.00)->after('builtup_area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('buildings', 'width')) {
            Schema::table('buildings', function (Blueprint $table) {
                $table->dropColumn('width');
            });
        }

        if (Schema::hasColumn('buildings', 'length')) {
            Schema::table('buildings', function (Blueprint $table) {
                $table->dropColumn('length');
            });
        }

        if (Schema::hasColumn('buildings', 'construction_area')) {
            Schema::table('buildings', function (Blueprint $table) {
                $table->dropColumn('construction_area');
            });
        }

        if (Schema::hasColumn('buildings_history', 'width')) {
            Schema::table('buildings', function (Blueprint $table) {
                $table->dropColumn('width');
            });
        }

        if (Schema::hasColumn('buildings_history', 'length')) {
            Schema::table('buildings', function (Blueprint $table) {
                $table->dropColumn('length');
            });
        }

        if (Schema::hasColumn('buildings_history', 'construction_area')) {
            Schema::table('buildings', function (Blueprint $table) {
                $table->dropColumn('construction_area');
            });
        }
    }
}
