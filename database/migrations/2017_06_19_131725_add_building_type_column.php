<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBuildingTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('architecture', function (Blueprint $table) {
            $table->unsignedInteger('building_type_id')->default(1)->after('price');
        });

        Schema::table('architecture_history', function (Blueprint $table) {
            $table->unsignedInteger('building_type_id')->default(1)->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('architecture', 'building_type_id')) {
            Schema::table('architecture', function (Blueprint $table) {
                $table->dropColumn('building_type_id');
            });
        }

        if (Schema::hasColumn('architecture_history', 'building_type_id')) {
            Schema::table('architecture_history', function (Blueprint $table) {
                $table->dropColumn('building_type_id');
            });
        }
    }
}
