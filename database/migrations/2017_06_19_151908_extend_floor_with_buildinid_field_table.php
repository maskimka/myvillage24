<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendFloorWithBuildinidFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('floors', function (Blueprint $table) {
            $table->unsignedInteger('building_id')->default(0)->after('architecture_id');
            $table->unsignedInteger('architecture_id')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('buildings', 'building_id')) {
            Schema::table('buildings', function (Blueprint $table) {
                $table->dropColumn('building_id');
            });
        }
    }
}
