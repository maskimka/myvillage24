<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendBuildingTableWithTwoExtraFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buildings', function (Blueprint $table) {

            $table->dateTime('release_date')->nullable()->after('description');
            $table->unsignedInteger('ready_percentage')->default(0)->after('release_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('buildings', 'release_date')) {
            Schema::table('buildings', function (Blueprint $table) {
                $table->dropColumn('release_date');
            });
        }

        if (Schema::hasColumn('buildings', 'ready_percentage')) {
            Schema::table('buildings', function (Blueprint $table) {
                $table->dropColumn('ready_percentage');
            });
        }
    }
}
