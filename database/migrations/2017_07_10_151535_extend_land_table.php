<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendLandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lands', function (Blueprint $table) {
            $table->integer('priority')->default(0)->after('cadastral_map');
        });

        Schema::table('land_history', function (Blueprint $table) {
            $table->integer('priority')->default(0)->after('cadastral_map');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('lands', 'priority')) {
            Schema::table('lands', function (Blueprint $table) {
                $table->dropColumn('priority');
            });
        }

        if (Schema::hasColumn('land_history', 'priority')) {
            Schema::table('land_history', function (Blueprint $table) {
                $table->dropColumn('priority');
            });
        }
    }
}
