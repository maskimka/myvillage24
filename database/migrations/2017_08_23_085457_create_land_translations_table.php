<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('land', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0);
            $table->string('article')->nullable();
            $table->string('cadastral_number')->nullable();
            $table->unsignedInteger('country_id')->default(0);
            $table->unsignedInteger('city')->default(0);
            $table->unsignedInteger('region')->default(0);
            $table->string('state')->nullable();
            $table->unsignedInteger('district')->default(0);
            $table->string('street_number', 10)->nullable();
            $table->decimal('area', 8, 2)->default(0.00);
            $table->decimal('rectangle_width', 8, 2)->default(0.00);
            $table->decimal('rectangle_length', 8, 2)->default(0.00);
            $table->decimal('from_city', 8, 2)->default(0.00);
            $table->unsignedInteger('purpose_id')->default(1);
            $table->unsignedInteger('sale_status_id')->default(1);
            $table->decimal('first_payment', 10, 2)->default(0.0);
            $table->decimal('price', 10, 2)->default(0.0);
            $table->string('land_video')->nullable();
            $table->decimal('lat', 20, 18)->nullable();
            $table->decimal('lng', 20, 18)->nullable();
            $table->string('size_map')->nullable();
            $table->string('cadastral_map')->nullable();
            $table->boolean('priority')->default(0);
            $table->boolean('is_active')->default(0);
            $table->boolean('is_deleted')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            /* INDEXES */
            $table->unique('article');
            $table->index('user_id');
            $table->index('size_map');
            $table->index('cadastral_number');
            $table->index('city');
            $table->index('region');
            $table->index('street_number');
            $table->index('area');
            $table->index('rectangle_width');
            $table->index('rectangle_length');
            $table->index('from_city');
            $table->index('purpose_id');
            $table->index('sale_status_id');
            $table->index('first_payment');
            $table->index('price');
            $table->index('priority');
            $table->index('is_active');
            $table->index('is_deleted');
        });

        Schema::create('land_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('land_id');
            $table->string('locale', 2);
            $table->string('street')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('enabled')->default(1);

            $table->engine = 'InnoDB';
            /* INDEXES */
            $table->unique(['land_id', 'locale']);
            $table->foreign('land_id')->references('id')->on('land')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('land_translations');
        Schema::dropIfExists('land');
    }
}
