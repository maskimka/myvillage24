<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendLandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lands', function (Blueprint $table) {
            $table->decimal('rectangle_width', 8, 2)->default(0.00)->after('area');
            $table->decimal('rectangle_length', 8, 2)->default(0.00)->after('rectangle_width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('lands', 'rectangle_width')) {
            Schema::table('lands', function (Blueprint $table) {
                $table->dropColumn('rectangle_width');
            });
        }

        if (Schema::hasColumn('lands', 'rectangle_length')) {
            Schema::table('lands', function (Blueprint $table) {
                $table->dropColumn('rectangle_length');
            });
        }
    }
}
