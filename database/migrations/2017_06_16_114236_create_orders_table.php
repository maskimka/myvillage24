<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone', 100);
            $table->ipAddress('ip');
            $table->text('comments');
            $table->unsignedInteger('status')->default(1);
            $table->boolean('is_active')->default(1);
            $table->boolean('is_deleted')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            /*INDEXES*/
            $table->index('name');
            $table->index('phone');
            $table->index('ip');
            $table->index('status');
            $table->index('created_at');
            $table->index('updated_at');
        });

        Schema::create('order_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 150)->nullable();
            $table->string('name', 100);
            $table->string('color', 45)->default('#f8ac59');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        Schema::create('orderables', function (Blueprint $table) {
            $table->integer('order_id');
            $table->morphs('orderables');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        Schema::create('order_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('acted_user_id');
            $table->string('name');
            $table->string('phone', 100);
            $table->ipAddress('ip');
            $table->text('comments');
            $table->unsignedInteger('status')->default(1);
            $table->boolean('is_active')->default(1);
            $table->boolean('is_deleted')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            /*INDEXES*/
            $table->index('order_id');
            $table->index('acted_user_id');
            $table->index('name');
            $table->index('phone');
            $table->index('ip');
            $table->index('status');
            $table->index('created_at');
            $table->index('updated_at');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_statuses');
        Schema::dropIfExists('orderables');
        Schema::dropIfExists('order_history');
    }
}
