<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0);
            $table->unsignedInteger('building_type_id')->default(1);
            $table->string('article')->nullable();
            $table->string('title')->nullable();
            $table->decimal('general_area', 8, 2)->default(0.00);
            $table->decimal('living_area', 8, 2)->default(0.00);
            $table->decimal('floors_area', 8, 2)->default(0.00);
            $table->decimal('roof_area', 8, 2)->default(0.00);
            $table->decimal('builtup_area', 8, 2)->default(0.00);
            $table->unsignedInteger('floors_number')->default(1);
            $table->unsignedInteger('bathrooms_number')->default(0);
            $table->unsignedInteger('rooms_number')->default(1);
            $table->unsignedInteger('garages_number')->default(0);
            $table->decimal('celling_height',4,2)->default(0.00);
            $table->string('exterior_walls')->nullable();
            $table->string('overlappings')->nullable();
            $table->string('roof')->nullable();
            $table->string('boiler')->nullable();
            $table->decimal('building_price', 8,2)->default(0.0);
            $table->decimal('price', 10,2)->default(0.0);
            $table->string('general_plan')->nullable();
            $table->string('house_section')->nullable();
            $table->text('description')->nullable();
            $table->string('project_files')->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_deleted')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            /* INDEXES */
            $table->index('article');
            $table->unique('title');
            $table->index('user_id');
            $table->index('building_type_id');
            $table->index('living_area');
            $table->index('floors_area');
            $table->index('roof_area');
            $table->index('builtup_area');
            $table->index('floors_number');
            $table->index('bathrooms_number');
            $table->index('rooms_number');
            $table->index('garages_number');
            $table->index('celling_height');
            $table->index('price');
            $table->index('general_plan');
            $table->index('house_section');
            $table->index('is_active');
            $table->index('is_deleted');
        });

        Schema::create('buildings_acc_kit', function (Blueprint $table) {
            $table->integer('building_id')->unsigned();
            $table->integer('aac_kit_id')->unsigned();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            $table->primary(['building_id', 'aac_kit_id'], 'buildings_kit_primary');
        });

        Schema::create('buildings_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0);
            $table->unsignedInteger('building_type_id')->default(1);
            $table->unsignedInteger('acted_user_id')->default(0);
            $table->unsignedInteger('building_id')->default(0);
            $table->string('article');
            $table->string('title')->nullable();
            $table->unsignedInteger('construction_term')->defaul(0); //Срок строительства
            $table->decimal('general_area', 8,2)->default(0);
            $table->decimal('living_area', 8,2)->default(0);
            $table->decimal('floors_area', 8,2)->default(0);
            $table->decimal('roof_area', 8,2)->default(0);
            $table->decimal('builtup_area', 8,2)->default(0);
            $table->unsignedInteger('floors_number')->default(1);
            $table->unsignedInteger('bathrooms_number')->default(0);
            $table->unsignedInteger('rooms_number')->default(1);
            $table->unsignedInteger('garages_number')->default(0);
            $table->decimal('celling_height',4,2)->default(0.0);
            $table->string('exterior_walls')->nullable();
            $table->string('overlappings')->nullable();
            $table->string('roof')->nullable();
            $table->string('boiler')->nullable();
            $table->decimal('building_price', 8,2)->default(0.0);
            $table->decimal('price', 8,2)->default(0.0);
            $table->string('general_plan')->nullable();
            $table->string('house_section')->nullable();
            $table->text('description')->nullable();
            $table->string('project_files')->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_deleted')->default(0);
            $table->string('action');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            /* INDEXES */
            $table->index('article');
            $table->unique('title');
            $table->index('user_id');
            $table->index('building_type_id');
            $table->index('acted_user_id');
            $table->index('building_id');
            $table->index('living_area');
            $table->index('floors_area');
            $table->index('roof_area');
            $table->index('builtup_area');
            $table->index('floors_number');
            $table->index('bathrooms_number');
            $table->index('rooms_number');
            $table->index('garages_number');
            $table->index('celling_height');
            $table->index('general_plan');
            $table->index('house_section');
            $table->index('price');
            $table->index('action');
            $table->index('is_active');
            $table->index('is_deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buildings');
        Schema::dropIfExists('buildings_acc_kit');
        Schema::dropIfExists('buildings_history');
    }
}
