<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendBuildingTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_translations', function (Blueprint $table) {
            $table->longText('facade')->nullable()->after('boiler');
            $table->longText('windows')->nullable()->after('boiler');
            $table->longText('reparation')->nullable()->after('boiler');
        });

        Schema::table('buildings_history', function (Blueprint $table) {
            $table->longText('facade')->nullable()->after('boiler');
            $table->longText('windows')->nullable()->after('boiler');
            $table->longText('reparation')->nullable()->after('boiler');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('building_translations', 'facade')) {
            Schema::table('building_translations', function (Blueprint $table) {
                $table->dropColumn('facade');
            });
        }

        if (Schema::hasColumn('building_translations', 'windows')) {
            Schema::table('building_translations', function (Blueprint $table) {
                $table->dropColumn('windows');
            });
        }

        if (Schema::hasColumn('building_translations', 'reparation')) {
            Schema::table('building_translations', function (Blueprint $table) {
                $table->dropColumn('reparation');
            });
        }

        if (Schema::hasColumn('buildings_history', 'facade')) {
            Schema::table('buildings_history', function (Blueprint $table) {
                $table->dropColumn('facade');
            });
        }

        if (Schema::hasColumn('buildings_history', 'windows')) {
            Schema::table('buildings_history', function (Blueprint $table) {
                $table->dropColumn('windows');
            });
        }

        if (Schema::hasColumn('building_history', 'reparation')) {
            Schema::table('building_history', function (Blueprint $table) {
                $table->dropColumn('reparation');
            });
        }
    }
}
