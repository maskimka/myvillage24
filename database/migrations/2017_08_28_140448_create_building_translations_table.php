<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0);
            $table->string('article')->nullable();
            $table->decimal('general_area', 8, 2)->default(0.00);
            $table->decimal('living_area', 8, 2)->default(0.00);
            $table->decimal('floors_area', 8, 2)->default(0.00);
            $table->decimal('roof_area', 8, 2)->default(0.00);
            $table->decimal('builtup_area', 8, 2)->default(0.00);
            $table->decimal('construction_area', 8, 2)->default(0.00);
            $table->unsignedInteger('floors_number')->default(1);
            $table->unsignedInteger('bathrooms_number')->default(0);
            $table->unsignedInteger('rooms_number')->default(1);
            $table->unsignedInteger('garages_number')->default(0);
            $table->decimal('celling_height',4,2)->default(0.00);
            $table->decimal('width', 8, 2)->default(0.00);
            $table->decimal('length', 8, 2)->default(0.00);
            $table->decimal('building_price', 8, 2)->default(0.0);
            $table->decimal('first_payment', 10, 2)->default(0.0);
            $table->unsignedInteger('building_type_id')->default(1);
            $table->string('general_plan')->nullable();
            $table->string('house_section')->nullable();
            $table->datetime('release_date')->nullable();
            $table->unsignedInteger('ready_percentage')->default(0);
            $table->string('project_files')->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_deleted')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->engine = 'InnoDB';
            /* INDEXES */
            $table->index('article');
            $table->index('user_id');
            $table->index('general_area');
            $table->index('living_area');
            $table->index('floors_area');
            $table->index('roof_area');
            $table->index('builtup_area');
            $table->index('construction_area');
            $table->index('floors_number');
            $table->index('bathrooms_number');
            $table->index('rooms_number');
            $table->index('garages_number');
            $table->index('celling_height');
            $table->index('width');
            $table->index('length');
            $table->index('building_price');
            $table->index('first_payment');
            $table->index('ready_percentage');
            $table->index('is_active');
            $table->index('is_deleted');
        });

        Schema::create('building_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('building_id');
            $table->string('locale', 2);
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('exterior_walls')->nullable();
            $table->longText('overlappings')->nullable();
            $table->longText('roof')->nullable();
            $table->longText('boiler')->nullable();
            $table->boolean('enabled')->default(1);

            $table->engine = 'InnoDB';
            /* INDEXES */
            $table->unique(['building_id', 'locale']);
            $table->foreign('building_id')->references('id')->on('building')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('building_translations');
        Schema::dropIfExists('building');
    }
}
