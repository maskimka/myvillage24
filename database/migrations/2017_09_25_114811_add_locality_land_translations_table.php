<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalityLandTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('land_translations', function (Blueprint $table) {

            $table->string('street_number')->nullable()->after('locale');
            $table->string('sublocality')->nullable()->after('street');
            $table->string('locality')->nullable()->after('sublocality');
            $table->string('administrative')->nullable()->after('locality');
            $table->string('country')->nullable()->after('administrative');

        });

        Schema::table('land_history', function (Blueprint $table) {

            $table->string('sublocality')->nullable()->after('street_number');
            $table->string('locality')->nullable()->after('sublocality');
            $table->string('administrative')->nullable()->after('locality');
            $table->string('country')->nullable()->after('administrative');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('land_translations', 'street_number')) {
            Schema::table('land_translations', function (Blueprint $table) {
                $table->dropColumn('street_number');
            });
        }

        if (Schema::hasColumn('land_translations', 'sublocality')) {
            Schema::table('land_translations', function (Blueprint $table) {
                $table->dropColumn('sublocality');
            });
        }

        if (Schema::hasColumn('land_translations', 'locality')) {
            Schema::table('land_translations', function (Blueprint $table) {
                $table->dropColumn('locality');
            });
        }

        if (Schema::hasColumn('land_translations', 'administrative')) {
            Schema::table('land_translations', function (Blueprint $table) {
                $table->dropColumn('administrative');
            });
        }

        if (Schema::hasColumn('land_translations', 'country')) {
            Schema::table('land_translations', function (Blueprint $table) {
                $table->dropColumn('country');
            });
        }

        if (Schema::hasColumn('land_history', 'sublocality')) {
            Schema::table('land_history', function (Blueprint $table) {
                $table->dropColumn('sublocality');
            });
        }

        if (Schema::hasColumn('land_history', 'locality')) {
            Schema::table('land_history', function (Blueprint $table) {
                $table->dropColumn('locality');
            });
        }

        if (Schema::hasColumn('land_history', 'administrative')) {
            Schema::table('land_history', function (Blueprint $table) {
                $table->dropColumn('administrative');
            });
        }

        if (Schema::hasColumn('land_history', 'country')) {
            Schema::table('land_history', function (Blueprint $table) {
                $table->dropColumn('country');
            });
        }
    }
}
