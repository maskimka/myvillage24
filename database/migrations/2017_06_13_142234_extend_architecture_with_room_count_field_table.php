<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendArchitectureWithRoomCountFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('architecture', function (Blueprint $table) {
            $table->unsignedInteger('rooms_number')->default(1)->after('bathrooms_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('architecture', 'rooms_number')) {
            Schema::table('architecture', function (Blueprint $table) {
                $table->dropColumn('rooms_number');
            });
        }
    }
}
