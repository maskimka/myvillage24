<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatLocalityTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locality_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('locality_id');
            $table->string('locale', 2);
            $table->string('name')->nullable();
            $table->boolean('enabled')->default(1);

            $table->engine = 'InnoDB';
            /* INDEXES */
            $table->unique(['locality_id', 'locale']);
            $table->foreign('locality_id')->references('id')->on('localities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locality_translations');
    }
}
