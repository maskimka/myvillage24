<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RolesTableSeeder::class);
         $this->call(CountriesTableSeeder::class);
         $this->call(CatalogsSeeder::class);
         $this->call(MasksTableSeeder::class);
         $this->call(BuildingTypesTableSeeder::class);
    }
}
