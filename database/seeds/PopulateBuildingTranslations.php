<?php

use Illuminate\Database\Seeder;

class PopulateBuildingTranslations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $buildings = DB::table('buildings')->get();

        foreach ($buildings as $building) {
            DB::table('building')->insert(array(
                'id' => $building->id,
                'user_id' => $building->user_id,
                'article' => $building->article,
                'general_area' => $building->general_area,
                'living_area' => $building->living_area,
                'floors_area' => $building->floors_area,
                'roof_area' => $building->roof_area,
                'builtup_area' => $building->builtup_area,
                'construction_area' => $building->construction_area,
                'floors_number' => $building->floors_number,
                'bathrooms_number' => $building->bathrooms_number,
                'rooms_number' => $building->rooms_number,
                'garages_number' => $building->garages_number,
                'celling_height' => $building->celling_height,
                'width' => $building->width,
                'length' => $building->length,
                'building_price' => $building->building_price,
                'first_payment' => $building->first_payment,
                'building_type_id' => $building->building_type_id,
                'general_plan' => $building->general_plan,
                'house_section' => $building->house_section,
                'release_date' => $building->release_date,
                'ready_percentage' => $building->ready_percentage,
                'project_files' => $building->project_files,
                'is_active' => $building->is_active,
                'is_deleted' => $building->is_deleted,
                'created_at' => $building->created_at,
                'updated_at' => $building->updated_at
            ));

            DB::table('building_translations')->insert(array(
                'building_id' => $building->id,
                'locale' => 'ru',
                'title' => $building->title,
                'description' => $building->description,
                'exterior_walls' => $building->exterior_walls,
                'overlappings' => $building->overlappings,
                'roof' => $building->roof,
                'boiler' => $building->boiler,
                'enabled' => 1
            ));
        }
    }
}
