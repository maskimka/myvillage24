<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->truncate();

        DB::table('countries')->insert([
            'name' => 'Austria',
            'iso2code' => 'AT',
            'iso3code' => 'AUT',
            'latitude' => 47.716231,
            'longitude' => 14.550072,
            'map_zoom' => 7,
        ]);

        DB::table('countries')->insert([
            'name' => 'Germany',
            'iso2code' => 'DE',
            'iso3code' => 'DEU',
            'latitude' => 51.165691,
            'longitude' => 10.451526000000058,
            'map_zoom' => 5,
        ]);

        DB::table('countries')->insert([
            'name' => 'Moldova',
            'iso2code' => 'MD',
            'iso3code' => 'MDA',
            'latitude' => 47.00367,
            'longitude' => 28.907089000000042,
            'map_zoom' => 6,
        ]);

        DB::table('countries')->insert([
            'name' => 'Romain',
            'iso2code' => 'RO',
            'iso3code' => 'ROU',
            'latitude' => 46.018901159049356,
            'longitude' => 25.283714578125,
            'map_zoom' => 6,
        ]);

        DB::table('countries')->insert([
            'name' => 'Russian',
            'iso2code' => 'RU',
            'iso3code' => 'RUS',
            'latitude' => 55.751244,
            'longitude' => 37.618423,
            'map_zoom' => 4,
        ]);

        DB::table('countries')->insert([
            'name' => 'Spain',
            'iso2code' => 'ES',
            'iso3code' => 'ESP',
            'latitude' => 40.5,
            'longitude' => -3.7,
            'map_zoom' => 5,
        ]);

        DB::table('countries')->insert([
            'name' => 'Switzerland',
            'iso2code' => 'CH',
            'iso3code' => 'CHE',
            'latitude' => 46.91181423464224,
            'longitude' => 7.8703845,
            'map_zoom' => 7,
        ]);

        DB::table('countries')->insert([
            'name' => 'Ukraine',
            'iso2code' => 'UA',
            'iso3code' => 'UKR',
            'latitude' => 49.29020169510767,
            'longitude' => 32.33482337500004,
            'map_zoom' => 5,
        ]);
    }
}
