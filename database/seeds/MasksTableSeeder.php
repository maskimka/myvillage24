<?php

use Illuminate\Database\Seeder;

class MasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('masks')->truncate();

        DB::table('masks')->insert([
            'country_id' => '1',
            'phone'      => '+999 99 999-99-99',
            'mobile'     => '+999 99 999-99-99',
            'cadastral'  => '9999999999:99:999:9999'
        ]);

        DB::table('masks')->insert([
            'country_id' => '2',
            'phone'      => '+9 999 999-99-99',
            'mobile'     => '+9 999 999-99-99',
            'cadastral'  => '99:99:9999999:999'
        ]);

        DB::table('masks')->insert([
            'country_id' => '3',
            'phone'      => '+999 (99) 999-999',
            'mobile'     => '+999 99-99-99-99',
            'cadastral'  => '9999999[.]999[9]'
        ]);

        DB::table('masks')->insert([
            'country_id' => '4',
            'phone'      => '+999 (99) 999-999',
            'mobile'     => '+999-99-99-99-99',
            'cadastral'  => '9999999[.]999[9]'
        ]);

        DB::table('masks')->insert([
            'country_id' => '5',
            'phone'      => '+999 (99) 999-999',
            'mobile'     => '+999-99-99-99-99',
            'cadastral'  => '9999999[.]999[9]'
        ]);

        DB::table('masks')->insert([
            'country_id' => '6',
            'phone'      => '+999 (99) 999-999',
            'mobile'     => '+999-99-99-99-99',
            'cadastral'  => '9999999[.]999[9]'
        ]);

        DB::table('masks')->insert([
            'country_id' => '7',
            'phone'      => '+999 (99) 999-999',
            'mobile'     => '+999-99-99-99-99',
            'cadastral'  => '9999999[.]999[9]'
        ]);

        DB::table('masks')->insert([
            'country_id' => '8',
            'phone'      => '+999 (99) 999-999',
            'mobile'     => '+999-99-99-99-99',
            'cadastral'  => '9999999[.]999[9]'
        ]);
    }
}
