<?php

use Illuminate\Database\Seeder;
use App\Architecture;

class CalculateArchitectureFullPrices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $architectures = Architecture::all();

        foreach ($architectures as $architecture) {

            $fullLowPrice  = $architecture->general_area * $architecture->building_price;
            $fullHighPrice = $architecture->general_area * $architecture->key_price;

            $architecture->update([
                'full_low_price' => $fullLowPrice
            ]);

            $architecture->update([
                'full_high_price' => $fullHighPrice
            ]);
        }
    }
}
