<?php

use Illuminate\Database\Seeder;

class CatalogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*purposes*/
        DB::table('purposes')->insert([
            'slug' => 'catalog_for_construction',
            'name' => 'Под строительство'
        ]);

        DB::table('purposes')->insert([
            'slug' => 'catalog_for_agriculture',
            'name' => 'Под агрокультуру'
        ]);

        /*sale_statuses*/
        DB::table('sale_status')->insert([
            'slug' => 'status_for_sale',
            'name' => 'Продается'
        ]);

        DB::table('sale_status')->insert([
            'slug' => 'status_sold',
            'name' => 'Продано'
        ]);

        DB::table('sale_status')->insert([
            'slug' => 'status_reserved',
            'name' => 'Зарезервировано'
        ]);

        /*infrastructure*/
        DB::table('infrastructure')->insert([
            'slug' => 'catalog_school',
            'name' => 'Школы'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_kindergarden',
            'name' => 'Детские сад'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_hospital',
            'name' => 'Поликлиника'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_drugstore',
            'name' => 'Аптеки'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_beauty_salon',
            'name' => 'Салон красоты'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_gym',
            'name' => 'Спортзал'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_swimming_pool',
            'name' => 'Бассейн'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_store',
            'name' => 'Магазины'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_cafe',
            'name' => 'Кафе'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_bank',
            'name' => 'Банки'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_shopping_center',
            'name' => 'Торговые центры'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_play_ground',
            'name' => 'Игровая площадка'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_lakes',
            'name' => 'Рядом с водоёмом'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_wood',
            'name' => 'Рядом с лесом'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_parks',
            'name' => 'Парк'
        ]);

        DB::table('infrastructure')->insert([
            'slug' => 'catalog_gaz_station',
            'name' => 'АЗС'
        ]);

        /*Comminications*/
        DB::table('communications')->insert([
            'slug' => 'catalog_gas',
            'name' => 'Газопровод'
        ]);

        DB::table('communications')->insert([
            'slug' => 'catalog_sewerage',
            'name' => 'Канализация'
        ]);

        DB::table('communications')->insert([
            'slug' => 'catalog_electricity',
            'name' => 'Электроснабжение'
        ]);

        DB::table('communications')->insert([
            'slug' => 'catalog_water',
            'name' => 'Вода'
        ]);

        DB::table('communications')->insert([
            'slug' => 'catalog_security',
            'name' => 'Охрана'
        ]);

        DB::table('communications')->insert([
            'slug' => 'catalog_phone_line',
            'name' => 'Телефонная линия'
        ]);

        DB::table('communications')->insert([
            'slug' => 'catalog_internet',
            'name' => 'Интернет'
        ]);

        DB::table('communications')->insert([
            'slug' => 'catalog_asphalt_road',
            'name' => 'Асфальтированная трасса'
        ]);

        /*Transport*/
        DB::table('transport')->insert([
            'slug' => 'catalog_tram',
            'name' => 'Трамвай'
        ]);

        DB::table('transport')->insert([
            'slug' => 'catalog_metropolitan',
            'name' => 'Метрополитен'
        ]);

        DB::table('transport')->insert([
            'slug' => 'catalog_trolleybus',
            'name' => 'Троллейбус'
        ]);

        DB::table('transport')->insert([
            'slug' => 'catalog_shuttle_bus',
            'name' => 'Маршрутное такси'
        ]);

        DB::table('transport')->insert([
            'slug' => 'catalog_bus',
            'name' => 'Автобус'
        ]);

        DB::table('transport')->insert([
            'slug' => 'catalog_train',
            'name' => 'Электричка'
        ]);

        DB::table('transport')->insert([
            'slug' => 'catalog_express_train',
            'name' => 'Экспресс-поезд'
        ]);

        /*Architectural and construction kit AAC_KIT table*/
        DB::table('aac_kit')->insert([
            'slug' => 'catalog_architecture',
            'name' => 'Архитектура'
        ]);

        DB::table('aac_kit')->insert([
            'slug' => 'catalog_constructive',
            'name' => 'Конструктив'
        ]);

        DB::table('aac_kit')->insert([
            'slug' => 'catalog_network_engineering',
            'name' => 'Инженерные сети'
        ]);

        DB::table('aac_kit')->insert([
            'slug' => 'catalog_project_passport',
            'name' => 'Паспорт проекта'
        ]);

        DB::table('aac_kit')->insert([
            'slug' => 'catalog_interior_design',
            'name' => 'Дизайн интерьера'
        ]);
        /* House Zone */
        DB::table('house_zone')->insert([
            'slug' => 'catalog_balcony',
            'name' => 'Балкон'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_pool',
            'name' => 'Бассейн'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_library',
            'name' => 'Библиотека'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_billiard_room',
            'name' => 'Бильярдная'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_garage',
            'name' => 'Гараж'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_wardrobe',
            'name' => 'Гардероб'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_living_room',
            'name' => 'Гостиная'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_home_theater',
            'name' => 'Домашний кинотеатр'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_bathroom',
            'name' => 'Ванная комната'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_cabinet',
            'name' => 'Кабинет'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_pantry',
            'name' => 'Кладовка'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_bedroom',
            'name' => 'Спальная комната'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_сorridor',
            'name' => 'Коридор'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_boiler_room',
            'name' => 'Котельная'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_porch',
            'name' => 'Крыльцо'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_kitchen',
            'name' => 'Кухня'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_kitchen-dining_room',
            'name' => 'Кухня-столовая'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_elevator',
            'name' => 'Лифт'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_entrance_hall',
            'name' => 'Прихожая'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_restroom',
            'name' => 'Санузел'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_sauna',
            'name' => 'Сауна'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_canteen',
            'name' => 'Столовая'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_terrace',
            'name' => 'Терраса'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_gym',
            'name' => 'Тренажерный зал'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_hall',
            'name' => 'Холл'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_stairs',
            'name' => 'Лесница'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_laundry',
            'name' => 'Прачечная'
        ]);

        DB::table('house_zone')->insert([
            'slug' => 'catalog_still_room',
            'name' => 'Буфет'
        ]);
    }
}
