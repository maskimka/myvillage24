<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currency')->truncate();

        DB::table('currency')->insert([
            'symbol' => '&euro;',
            'name' => 'EUR',
            'rate' => 1,
        ]);

        DB::table('currency')->insert([
            'symbol' => '&dollar;',
            'name' => 'USD',
            'rate' => 1,
        ]);

        DB::table('currency')->insert([
            'symbol' => '&#8369;',
            'name' => 'RUB',
            'rate' => 1,
        ]);

        DB::table('currency')->insert([
            'symbol' => 'RON',
            'name' => 'RON',
            'rate' => 1,
        ]);

        DB::table('currency')->insert([
            'symbol' => 'CHF',
            'name' => 'CHF',
            'rate' => 1,
        ]);
    }
}
