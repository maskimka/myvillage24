<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'slug' => 'admin',
            'permissions' => '{"/admin":true,"/admin/profile":true,"/admin/profile/*":true,"/admin/land":true, "/admin/land/*":true, "/admin/architecture":true, "/admin/architecture/*":true}',
            'name' => 'Project Administartor'
        ]);

        DB::table('roles')->insert([
            'slug' => 'manager',
            'permissions' => '{"/panel":true,"/panel/profile":true,"/panel/profile/*":true}',
            'name' => 'Project Manager'
        ]);

        DB::table('roles')->insert([
            'slug' => 'realtor',
            'permissions' => '{"/panel":true,"/panel/profile":true,"/panel/profile/*":true,"/panel/land":true, "/panel/land/*":true}',
            'name' => 'Realtor'
        ]);

        DB::table('roles')->insert([
            'slug' => 'architect',
            'permissions' => '{"/panel":true,"/panel/profile":true,"/panel/profile/*":true,"/panel/architecture":true, "/panel/architecture/*":true}',
            'name' => 'Architect'
        ]);

        DB::table('roles')->insert([
            'slug' => 'lawyer',
            'permissions' => '',
            'name' => 'Lawyer'
        ]);

        DB::table('roles')->insert([
            'slug' => 'client',
            'permissions' => '{"/panel":true,"/panel/profile":true,"/panel/profile/*":true,"/panel/projects":true}',
            'name' => 'Client'
        ]);
    }
}
