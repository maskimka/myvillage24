<?php

use Illuminate\Database\Seeder;
use App\Locality;

class PopulateLocalityTranslation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $localities = DB::table('localities')->get();

        foreach($localities as $locality)
        {
            DB::table('locality_translations')->insert(array(
                'locality_id' => $locality->id,
                'locale' => 'ru',
                'name' => $locality->name,
                'enabled' => 1
            ));
        }
    }
}
