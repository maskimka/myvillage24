<?php

use Illuminate\Database\Seeder;
use App\Land;

class SetIsoCountryCodeToLands extends Seeder
{
    /**
     * Run the database seeds.
     *  ALTER TABLE `land` ADD `country_code` VARCHAR(3) NULL AFTER `country_id`;
     *  ALTER TABLE `land_history` ADD `country_code` VARCHAR(3) NULL AFTER `country_id`;
     * ALTER TABLE `land_translations` ADD `country_code` VARCHAR(3) NULL AFTER `country`
     * @return void
     */
    public function run()
    {
        $lands = Land::all()->where('is_active',1)->where('country_code', null);
        foreach ($lands as $land) {
            var_dump($land->id);
            var_dump($land->lat);
            var_dump($land->lng);
            $iso2code = $this->getIso2CodeByCoordinats($land->lat, $land->lng);
            var_dump($iso2code);
            if ($iso2code) {
                $this->updateLand($land->id, $iso2code);
            }
            var_dump('======================================');
        }
    }

    /**
     * @param $lat
     * @param $lng
     * @return string|null
     */
    private function getIso2CodeByCoordinats($lat, $lng)
    {
        $iso2code = null;
        $deal_lat = $lat;
        $deal_long = $lng;

        $geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$deal_lat.','.$deal_long.'&sensor=false');
        $output= json_decode($geocode);

        if (isset($output->results[0])) {
            for ($j = 0; $j < count($output->results[0]->address_components); $j++){
                $cn=array($output->results[0]->address_components[$j]->types[0]);
                if(in_array("country", $cn)){
//                $country  = $output->results[0]->address_components[$j]->long_name;
                    $iso2code = $output->results[0]->address_components[$j]->short_name;
                }
            }
        }

        return $iso2code;
    }

    /**
     * @param $landId
     * @param $iso2code
     * @return mixed
     */
    private function updateLand($landId, $iso2code)
    {
       return DB::table('land')
            ->where('id', $landId)
            ->update(array('country_code' => strtolower($iso2code)));
    }
}
