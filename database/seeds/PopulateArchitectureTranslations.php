<?php

use Illuminate\Database\Seeder;

class PopulateArchitectureTranslations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $architectures = DB::table('architectures')->get();

        foreach ($architectures as $architecture) {
            DB::table('architecture')->insert(array(
                'id' => $architecture->id,
                'user_id' => $architecture->user_id,
                'article' => $architecture->article,
                'title' => $architecture->title,
                'general_area' => $architecture->general_area,
                'living_area' => $architecture->living_area,
                'floors_area' => $architecture->floors_area,
                'roof_area' => $architecture->roof_area,
                'builtup_area' => $architecture->builtup_area,
                'floors_number' => $architecture->floors_number,
                'bathrooms_number' => $architecture->bathrooms_number,
                'rooms_number' => $architecture->rooms_number,
                'garages_number' => $architecture->garages_number,
                'celling_height' => $architecture->celling_height,
                'width' => $architecture->width,
                'length' => $architecture->length,
                'building_price' => $architecture->building_price,
                'key_price' => $architecture->key_price,
                'building_type_id' => $architecture->building_type_id,
                'general_plan' => $architecture->general_plan,
                'house_section' => $architecture->house_section,
                'project_files' => $architecture->project_files,
                'is_active' => $architecture->is_active,
                'is_deleted' => $architecture->is_deleted,
                'created_at' => $architecture->created_at,
                'updated_at' => $architecture->updated_at
            ));

            DB::table('architecture_translations')->insert(array(
                'architecture_id' => $architecture->id,
                'locale' => 'ru',
                'description' => $architecture->description,
                'exterior_walls' => $architecture->exterior_walls,
                'overlappings' => $architecture->overlappings,
                'roof' => $architecture->roof,
                'boiler' => $architecture->boiler,
                'enabled' => 1
            ));
        }
    }
}
