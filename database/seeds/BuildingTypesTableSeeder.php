<?php

use Illuminate\Database\Seeder;

class BuildingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('building_types')->insert([
            'slug' => 'cottage',
            'name' => 'Коттедж'
        ]);

        DB::table('building_types')->insert([
            'slug' => 'duplex',
            'name' => 'Дуплекс'
        ]);

        DB::table('building_types')->insert([
            'slug' => 'town_house',
            'name' => 'Таунхаус'
        ]);

        DB::table('building_types')->insert([
            'slug' => 'villa',
            'name' => 'Вилла'
        ]);
    }
}
