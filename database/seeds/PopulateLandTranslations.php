<?php

use Illuminate\Database\Seeder;

class PopulateLandTranslations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lands = DB::table('lands')->get();

        foreach ($lands as $land) {
            DB::table('land')->insert(array(
                'id' => $land->id,
                'user_id' => $land->user_id,
                'article' => $land->article,
                'cadastral_number' => $land->cadastral_number,
                'country_id' => $land->country_id,
                'city' => $land->city,
                'region' => $land->region,
                'state' => $land->state,
                'district' => $land->district,
                'street_number' => $land->street_number,
                'area' => $land->area,
                'rectangle_width' => $land->rectangle_width,
                'rectangle_length' => $land->rectangle_length,
                'from_city' => $land->from_city,
                'purpose_id' => $land->purpose_id,
                'sale_status_id' => $land->sale_status_id,
                'first_payment' => $land->first_payment,
                'price' => $land->price,
                'land_video' => $land->land_video,
                'lat' => $land->lat,
                'lng' => $land->lng,
                'size_map' => $land->size_map,
                'cadastral_map' => $land->cadastral_map,
                'priority' => $land->priority,
                'is_active' => $land->is_active,
                'is_deleted' => $land->is_deleted,
                'created_at' => $land->created_at,
                'updated_at' => $land->updated_at
            ));

            DB::table('land_translations')->insert(array(
                'land_id' => $land->id,
                'locale' => 'ru',
                'street' => $land->street,
                'description' => $land->description,
                'enabled' => 1
            ));
        }
    }
}
