<?php

use Illuminate\Database\Seeder;
use App\Country;

class AppConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = Country::all();

        DB::table('app_configs')->truncate();

        foreach ($countries as $country) {

            DB::table('app_configs')->insert([
                'country_iso2' => $country->iso2code,
                'key'          => 'arch_gray_price',
                'value'        => 300
            ]);

            DB::table('app_configs')->insert([
                'country_iso2' => $country->iso2code,
                'key'          => 'arch_gray_facade_price',
                'value'        => 380
            ]);
        }
    }
}
