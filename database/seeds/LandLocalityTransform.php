<?php

use Illuminate\Database\Seeder;
use App\Land;

class LandLocalityTransform extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lands = Land::all();
        $localyISO2 = config('locales');

        foreach ($lands as $item) {

            foreach ($localyISO2 as $locale => $enable) {

                $landTranslation = [];

                if ($item->country) {

                    $tmpCountry = $item->country->translations->where('locale', $locale)->toArray();

                    if (count($tmpCountry)) {

                        $tmpCountry = array_pop($tmpCountry);
                        $landTranslation['country'] = $tmpCountry['name'];
                    }
                }
                if ($item->regions) {

                    $tmpRegion = $item->regions->translations->where('locale', $locale)->toArray();

                    if (count($tmpRegion)) {

                        $tmpRegion = array_pop($tmpRegion);
                        $landTranslation['administrative'] = $tmpRegion['name'];
                    }
                }
                if ($item->cities) {

                    $tmpCities = $item->cities->translations->where('locale', $locale)->toArray();

                    if (count($tmpCities)) {

                        $tmpCities = array_pop($tmpCities);

                        $landTranslation['locality'] = $tmpCities['name'];
                    }
                }
                if ($item->districts) {

                    $tmpDistrict = $item->districts->translations->where('locale', $locale)->toArray();

                    if (count($tmpDistrict)) {

                        $tmpDistrict = array_pop($tmpDistrict);
                        $landTranslation['sublocality'] = $tmpDistrict['name'];
                    }

                }

                $landTranslation['street_number'] = $item->street_number;

                DB::table('land_translations')
                    ->where('land_id', $item->id)
                    ->where('locale', $locale)
                    ->update($landTranslation);

            }
        }
    }
}
