<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
/*Admin Routes*/
Route::group(['prefix' => 'admin','middleware'=>['admin', 'admin.language']], function(){
    $locales = config('locales');
    $locales[''] = true;
    foreach($locales as $loc => $item){
        $locale = ($loc)? $loc : '/'.$loc;
        Route::get($locale.'/dashboard', 'Admin\DashboardController@index');
        /*Profile*/
        Route::get($locale.'/profile', 'ProfileController@index');
        Route::get($locale.'/profile/{id}/edit', 'ProfileController@edit');
        Route::delete($locale.'/profile/{id}/delete', 'ProfileController@destroy');
        Route::post($locale.'/profile/change_avatar', 'ProfileController@change_avatar');
        Route::post($locale.'/profile/update', 'ProfileController@update');
        Route::get($locale.'/profile/roles', 'ProfileController@roles');
        Route::post($locale.'/profile/role/realtor', 'ProfileController@requestRoleRealtor');
        Route::post($locale.'/profile/role/architect', 'ProfileController@requestRoleArchitect');
        /*Land*/
        Route::resource($locale.'/land', 'LandController', ['except' => ['show', 'store']]);
        Route::get($locale.'/land/{id}/complement', "LandController@complement");
        Route::post($locale.'/land/{id}/complement_save', "LandController@complement_save");
        Route::post($locale.'/land/activate', "LandController@activate");
        Route::post($locale.'/land/change_sale_status', "LandController@change_sale_status");
        Route::post($locale.'/getLocalities', "LandController@getLocalities");
        /*Architect project*/
        Route::resource($locale.'/architecture', 'ArchitectureController', ['except' => ['show', 'store']]);
        Route::post($locale.'/architecture/{id}/save_galleries', "ArchitectureController@save_galleries");
        Route::post($locale.'/architecture/activate', "ArchitectureController@activate");
        Route::post($locale.'/architecture/attach_land', "ArchitectureController@attach_land");
        Route::post($locale.'/architecture/detach_land', "ArchitectureController@detach_land");
        /*Building*/
        Route::resource($locale.'/building', 'BuildingController', ['except' => ['show', 'store']]);
        Route::post($locale.'/building/{id}/save_galleries', "BuildingController@save_galleries");
        Route::post($locale.'/building/activate', "BuildingController@activate");
        Route::post($locale.'/building/attach_land', "BuildingController@attach_land");
        Route::post($locale.'/building/detach_land', "BuildingController@detach_land");
        /*Village*/
        Route::resource($locale.'/village', 'VillageController', ['except' => ['show', 'store']]);
        Route::post($locale.'/village/activate', "VillageController@activate");
        Route::post($locale.'/village/attach_land', "VillageController@attach_land");
        Route::post($locale.'/village/detach_land', "VillageController@detach_land");
        Route::post($locale.'/village/attach_building', "VillageController@attach_building");
        Route::post($locale.'/village/detach_building', "VillageController@detach_building");
        /*Users*/
        Route::resource($locale.'/users', 'UsersController',['except' => ['show']]);
        Route::post($locale.'/users/updateRole', 'UsersController@updateRole');
        /*Roles*/
        Route::resource($locale.'/roles', 'RolesController',['except' => ['show']]);
        /*Orders*/
        Route::resource($locale.'/order', 'Admin\OrderController',['except' => ['show', 'store', 'update']]);
        Route::post($locale.'/order/change_status', 'Admin\OrderController@change_order_status');
        /*SEO*/
        Route::resource($locale.'/seo', 'Admin\SeoController',['except' => ['show', 'store']]);
        Route::resource($locale.'/seo/land', 'Admin\SeoLandController',['except' => ['create', 'show', 'store', 'destroy']]);
        Route::resource($locale.'/seo/architecture', 'Admin\SeoArchitectureController',['except' => ['create', 'show', 'store', 'destroy']]);
        Route::resource($locale.'/seo/building', 'Admin\SeoBuildingController',['except' => ['create', 'show', 'store', 'destroy']]);
        /*PAGES*/
        Route::resource($locale.'/pages', 'Admin\PageController',['except' => ['show', 'store']]);
        /*Statistics*/
        Route::post($locale.'/getStatistics', 'Admin\DashboardController@getStatistics');
        /* AppConfig */
        Route::get($locale.'/config_general', 'Admin\AppConfigController@generalConfig');
        Route::get($locale.'/config/land', 'Admin\AppConfigController@landConfig');
        Route::get($locale.'/config/architecture', 'Admin\AppConfigController@architectureConfig');
        Route::put($locale.'/config_ap/store', 'Admin\AppConfigController@architectureConfigStore');
        Route::put($locale.'/config_ap/create', 'Admin\AppConfigController@architectureConfigCreate');
        Route::post($locale.'/getArchConfigByCountry', 'Admin\AppConfigController@architectureConfigByCountry');
        Route::get($locale.'/config/building', 'Admin\AppConfigController@buildingConfig');
        /* Wishlist */
        Route::resource($locale.'/wishlist', 'WishlistController', ['except' => ['create', 'show', 'store', 'edit', 'update']]);
        /* PTORequest */
        Route::resource($locale.'/pto_request', 'PtoRequestController', ['except' => ['create', 'show', 'edit', 'update']]);
        Route::post($locale.'/pto_request/change_status', 'PtoRequestController@changePtoRequestStatus');
    }
});
/* ===== PANEL =====*/
Route::group([/*'namespace' => 'Panel',*/'prefix' => 'panel', 'middleware'=>['panel', 'admin.language']], function(){
    $locales = config('locales');
    $locales[''] = true;

    foreach($locales as $loc => $item) {
        $locale = ($loc)? $loc : '/'.$loc;
        Route::get($locale.'/dashboard', 'Panel\DashboardController@index');
        Route::get($locale.'/profile', 'Panel\DashboardController@profile');
        /*Profile*/
        Route::get($locale.'/profile', 'ProfileController@index');
        Route::get($locale.'/profile/{id}/edit', 'ProfileController@edit');
        Route::delete($locale.'/profile/{id}/delete', 'ProfileController@destroy');
        Route::post($locale.'/profile/change_avatar', 'ProfileController@change_avatar');
        Route::post($locale.'/profile/update', 'ProfileController@update');
        Route::get($locale.'/profile/roles', 'ProfileController@roles');
        Route::post($locale.'/profile/role/realtor', 'ProfileController@requestRoleRealtor');
        Route::post($locale.'/profile/role/architect', 'ProfileController@requestRoleArchitect');
        /*Land*/
        Route::resource($locale.'/land', 'LandController', ['except' => ['show', 'store']]);
        Route::post($locale.'/land/change_sale_status', 'LandController@change_sale_status');
        Route::post($locale.'/getLocalities', "LandController@getLocalities");
        /*Architect Project*/
        Route::resource($locale.'/architecture', 'ArchitectureController', ['except' => ['show', 'store']]);
        Route::post($locale.'/architecture/{id}/save_galleries', "ArchitectureController@save_galleries");
        /* Wishlist */
        Route::resource($locale.'/wishlist', 'WishlistController', ['except' => ['create', 'show', 'store', 'edit', 'update']]);
        /* PTORequest */
        Route::resource($locale.'/pto_request', 'PtoRequestController', ['except' => ['index', 'create', 'show', 'edit', 'update', 'destroy']]);
    }

});
/* ===== Authentication =====*/
Route::group(['namespace' => 'Authentication','prefix' => 'auth', 'middleware' => 'admin.language'], function(){
    $locales = config('locales');
    $locales[''] = true;
    foreach($locales as $loc => $item) {
        $locale = ($loc)? $loc : '/'.$loc;
        Route::get($locale.'/registration', 'RegistrationController@showRegistrationForm');
        Route::post($locale.'/registration', 'RegistrationController@register');

        Route::get($locale.'/login', 'LoginController@showLoginForm');
        Route::post($locale.'/login', 'LoginController@login');
        Route::post($locale.'/logout', 'LoginController@logout');

        Route::get($locale.'/activation/{user_id}/{ACC}/', 'ActivationController@activate');

        Route::get($locale.'/forgot-password', 'ForgotPasswordController@showForgotPasswordForm');
        Route::post($locale.'/forgot-password', 'ForgotPasswordController@restorePassword');
        Route::get($locale.'/reset/{user_id}/{reset_code}', 'ForgotPasswordController@resetPassword');
        Route::post($locale.'/reset', 'ForgotPasswordController@postResetPassword');
    }
});
/* ===== WEB SITE =====*/
Route::group(['namespace'=>'Frontend', 'middleware'=>['language', 'visitor', 'frontend']], function(){

    $locales = config('locales');
    $locales[''] = true;

    foreach($locales as $locale=>$item)
    {
        Route::get($locale, 'PageController@home')->name('home');
        Route::get($locale.'/services', 'PageController@services');
        Route::get($locale.'/about', 'PageController@about');
        Route::get($locale.'/partners', 'PageController@partners');
        Route::get($locale.'/hypothec', 'PageController@hypothec');
        Route::get($locale.'/contacts', 'PageController@contacts');
        Route::get($locale.'/prices', 'PageController@prices');
        Route::get($locale.'/terms/{model}', 'PageController@terms');
        Route::get($locale.'/realtor', 'PageController@realtor');
        Route::get($locale.'/architect', 'PageController@architect');
        Route::get($locale.'/construction', 'PageController@construction');
        Route::get($locale. '/representative', 'PageController@representative');
        Route::get($locale. '/finance', 'PageController@finance');
        Route::get($locale. '/modularstructures', 'PageController@modularstructures');

        Route::get($locale.'/lands', 'LandController@index')->name('lands');
        Route::get($locale.'/land/{id}', 'LandController@show')->name('land');
        Route::get($locale.'/ru/land/{id}', 'LandController@show');

        Route::post($locale.'/lands/order', 'LandController@order');
        Route::get($locale.'/related_lands/{id}', 'LandController@related');

        Route::get($locale.'/architecture', 'ArchitectureController@index')->name('projects');
        Route::get($locale.'/architecture/{id}', 'ArchitectureController@show')->name('project');
        Route::post($locale.'/architecture/order', 'ArchitectureController@order');
        Route::get($locale.'/related_architecture/{id}', 'ArchitectureController@related');

        Route::get($locale.'/buildings', 'BuildingController@index')->name('buildings');
        Route::get($locale.'/building/{id}', 'BuildingController@show')->name('building');
        Route::post($locale.'/buildings/order', 'BuildingController@order');

        Route::post($locale.'/getBuildingList', 'BuildingController@getBuildingList');
        Route::post($locale.'/getBuildingCount', 'BuildingController@getBuildingCount');

        Route::get($locale.'/villages', 'VillageController@index')->name('villages');
        Route::get($locale.'/village/{id}', 'VillageController@show')->name('village');
        Route::post($locale.'/villages/order', 'VillageController@order');

        Route::post($locale.'/getVillageList', 'VillageController@getBuildingList');
        Route::post($locale.'/getVillageCount', 'VillageController@getBuildingCount');

        Route::post($locale.'/getLandList', 'LandController@getLandList');
        Route::get($locale.'/getLocalities', 'LandController@getLocalities');
        Route::get($locale.'/getMatchingLocalityPlaces/{locality}', 'LandController@getMatchingLocalityPlaces');
        Route::post($locale.'/getLandCount', 'LandController@getLandCount');

        Route::post($locale.'/getArchitectureList', 'ArchitectureController@getArchitectureList');
        Route::post($locale.'/getArchitectureCount', 'ArchitectureController@getArchitectureCount');
        Route::get($locale.'/getBuildingTypes', 'ArchitectureController@getBuildingTypes');

        //Contact email form
        Route::post($locale.'/send_contact', 'ContactController@sendEmailContacts');

        Route::resource($locale.'/wishlist', 'WishlistController');
        Route::post($locale.'/getWishlist', 'WishlistController@getWishlist');
        Route::post($locale.'/pto_request', 'PtoRequestController@store');
        Route::get($locale. '/cookie-policy', 'PageController@cookie');
    }
});

// Language route
//Route::post('/language', array(
//    'before' => 'csrf',
//    'uses'   => 'LanguageController@changeLanguage'
//));

Route::get('auth/socialite/{provider}', 'Authentication\LoginController@redirectToProvider');
Route::get('auth/socialite/callback/{provider}', 'Authentication\LoginController@handleProviderCallback');
Route::get('/_debugbar/assets/stylesheets', [
    'as' => 'debugbar-css',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@css'
]);

Route::get('/_debugbar/assets/javascript', [
    'as' => 'debugbar-js',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@js'
]);








