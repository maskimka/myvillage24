<?php

    /*===============================================================================*/
    /*============================= SITE'S LINKS ====================================*/
    /*===============================================================================*/
    Breadcrumbs::register('home', function($breadcrumbs)
    {
        $breadcrumbs->push(trans('messages.menu_home'), route('home'));
    });

    Breadcrumbs::register('lands', function($breadcrumbs)
    {
        $breadcrumbs->parent('home');
        $breadcrumbs->push(trans('messages.menu_lands_f'), route('lands'));
    });

    Breadcrumbs::register('projects', function($breadcrumbs)
    {
        $breadcrumbs->parent('home');
        $breadcrumbs->push(trans('messages.menu_projects_f'), route('projects'));
    });

    Breadcrumbs::register('buildings', function($breadcrumbs)
    {
        $breadcrumbs->parent('home');
        $breadcrumbs->push(trans('messages.menu_buildings_f'), route('buildings'));
    });

    Breadcrumbs::register('land', function($breadcrumbs, $item)
    {
        $breadcrumbs->parent('lands');
        $breadcrumbs->push($item->article, route('land', $item->id));
    });

    Breadcrumbs::register('project', function($breadcrumbs, $item)
    {
        $breadcrumbs->parent('projects');
        $breadcrumbs->push($item->article, route('project', $item->id));
    });

    Breadcrumbs::register('building', function($breadcrumbs, $item)
    {
        $breadcrumbs->parent('buildings');
        $breadcrumbs->push($item->article, route('building', $item->id));
    });

    Breadcrumbs::register('wishlist', function($breadcrumbs)
    {
        $breadcrumbs->parent('home');
        $breadcrumbs->push(trans('messages.menu_favorite'), url('wishlist'));
    });

    Breadcrumbs::register('termsland', function($breadcrumbs)
    {
        $breadcrumbs->parent('home');
        $breadcrumbs->push(trans('messages.label_terms_conditions'), url('terms/land'));
    });

    Breadcrumbs::register('prices', function($breadcrumbs)
    {
        $breadcrumbs->parent('home');
        $breadcrumbs->push(trans('messages.menu_prices'), url('prices'));
    });

    /*===============================================================================*/
    /*========================== ADMIN AND PANEL LINKS ==============================*/
    /*===============================================================================*/
    Breadcrumbs::register('dashboard', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->push(trans('messages.menu_home', [], null, $adminLang), url($namespace.'/dashboard'));
    });

    /* Profile */
    Breadcrumbs::register('profile_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.menu_profile', [], null, $adminLang), url($namespace.'/profile'));
    });

    Breadcrumbs::register('profile_edit_backend', function($breadcrumbs, $namespace, $item, $adminLang)
    {
        $breadcrumbs->parent('profile_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_profile_edit', [], null, $adminLang), url($namespace.'/profile/'.$item.'/edit'));
    });

    Breadcrumbs::register('profile_roles_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('profile_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_start_your_work', [], null, $adminLang), url($namespace.'/profile/roles'));
    });

    /* Lands */
    Breadcrumbs::register('lands_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_land_list', [], null, $adminLang), url($namespace.'/land'));
    });

    Breadcrumbs::register('lands_edit_backend', function($breadcrumbs, $namespace, $item, $adminLang)
    {
        $breadcrumbs->parent('lands_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_edit_land', [], null, $adminLang), url($namespace.'/land/'.$item.'/edit'));
    });

    Breadcrumbs::register('lands_seo_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.menu_seoland', [], null, $adminLang), url($namespace.'/seo/land/'));
    });

    /* Architecture */
    Breadcrumbs::register('projects_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.menu_architecture', [], null, $adminLang), url($namespace.'/architecture'));
    });

    Breadcrumbs::register('projects_edit_backend', function($breadcrumbs, $namespace, $item, $adminLang)
    {
        $breadcrumbs->parent('projects_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_add_architect_project', [], null, $adminLang), url($namespace.'/architecture/'.$item.'/edit'));
    });

    /* Buildings */
    Breadcrumbs::register('buildings_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.menu_buildings', [], null, $adminLang), url($namespace.'/building'));
    });

    Breadcrumbs::register('buildings_edit_backend', function($breadcrumbs, $namespace, $item, $adminLang)
    {
        $breadcrumbs->parent('buildings_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.add_building', [], null, $adminLang), url($namespace.'/building/'.$item.'/edit'));
    });

    /* Villages */
    Breadcrumbs::register('villages_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.menu_villages', [], null, $adminLang), url($namespace.'/village'));
    });

    Breadcrumbs::register('villages_edit_backend', function($breadcrumbs, $namespace, $item, $adminLang)
    {
        $breadcrumbs->parent('villages_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.add_village', [], null, $adminLang), url($namespace.'/village/'.$item.'/edit'));
    });

    /* Wishlist */
    Breadcrumbs::register('wishlist_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_favorite', [], null, $adminLang), url($namespace.'/wishlist'));
    });

    /* Orders */
    Breadcrumbs::register('order_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.menu_orders', [], null, $adminLang), url($namespace.'/order'));
    });

    /* PTO requests */
    Breadcrumbs::register('pto_request_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.pto_requests', [], null, $adminLang), url($namespace.'/pto_Request'));
    });

    /* Users */
    Breadcrumbs::register('users_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_users_list', [], null, $adminLang), url($namespace.'/users'));
    });

    Breadcrumbs::register('user_create_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('users_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_new_user', [], null, $adminLang), url($namespace.'/users/create'));
    });

    Breadcrumbs::register('user_edit_backend', function($breadcrumbs, $namespace, $item, $adminLang)
    {
        $breadcrumbs->parent('users_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_profile_edit', [], null, $adminLang), url($namespace.'/users/'.$item.'/edit'));
    });

    /* Roles */
    Breadcrumbs::register('roles_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.menu_roles', [], null, $adminLang), url($namespace.'/roles'));
    });

    Breadcrumbs::register('role_create_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('roles_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_permissions', [], null, $adminLang), url($namespace.'/roles/create'));
    });

    Breadcrumbs::register('role_edit_backend', function($breadcrumbs, $namespace, $item, $adminLang)
    {
        $breadcrumbs->parent('roles_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.label_permissions', [], null, $adminLang), url($namespace.'/roles/'.$item.'/edit'));
    });

    /* Seo templates */
    Breadcrumbs::register('seo_templates_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.seo_templates', [], null, $adminLang), url($namespace.'/seo'));
    });

    Breadcrumbs::register('seo_template_edit_backend', function($breadcrumbs, $namespace, $item, $adminLang)
    {
        $breadcrumbs->parent('seo_templates_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.edit_seo_template', [], null, $adminLang), url($namespace.'/seo/'.$item.'/edit'));
    });

    /* Seo pages */
    Breadcrumbs::register('seo_pages_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.seo_pages', [], null, $adminLang), url($namespace.'/pages'));
    });

    Breadcrumbs::register('seo_page_edit_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('seo_pages_backend', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.edit_seo_page', [], null, $adminLang), url($namespace.'/seo'));
    });

    /* App Config */
    Breadcrumbs::register('config_architecture_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.architecture_config', [], null, $adminLang), url($namespace.'/config/architecture'));
    });

    Breadcrumbs::register('config_land_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.land_config', [], null, $adminLang), url($namespace.'/config/land'));
    });

    Breadcrumbs::register('config_building_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.building_config', [], null, $adminLang), url($namespace.'/config/building'));
    });

    Breadcrumbs::register('config_general_backend', function($breadcrumbs, $namespace, $adminLang)
    {
        $breadcrumbs->parent('dashboard', $namespace, $adminLang);
        $breadcrumbs->push(trans('messages.common_config', [], null, $adminLang), url($namespace.'/config/general'));
    });