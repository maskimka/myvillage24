<?php

use Illuminate\Http\Request;
use App\Land;
use App\ArchitectureFloor;
use App\HouseZone;
use App\Http\Requests\UpdateLandRequest;
use Shorty as Shorty;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {

    return $request->user();

})->middleware('auth:api');

Route::group([],function(){

    Route::post('/image/remove', function(Request $request) {

        return response()->json(['name' => 'test']);
    });

    Route::get('/house_zone', function (Request $request) {

        $house_zone = HouseZone::all('id','name');

        return response()->json(['house_zone' => $house_zone]);
    });

    Route::get('/floors', function(Request $request) {

        $floors = ArchitectureFloor::all()
            ->where($request['f_key'], $request['model_id'])
            ->where('is_deleted', 0);

        foreach($floors as $key => $floor) {

            $floors[$key]->setAttribute('image', $floor->images()->get());
            $floors[$key]->setAttribute('house_zone_items', $floor->house_zone()->get());
        }

        return response()->json(['floors' => $floors]);
    });

    Route::get('/floor/create', function(Request $request) {

        $request['number'] = 1;

        $highestFloor = ArchitectureFloor::where($request['f_key'], $request['model_id'])
            ->where('is_deleted', 0)
            ->orderBy('id','desc')
            ->first(['number']);

        $request[$request['f_key']] = $request['model_id'];

        unset($request['f_key']);
        unset($request['model_id']);

        if ($highestFloor) {

            $request['number'] = $highestFloor->number + 1;
        }

        $floor = ArchitectureFloor::create($request->all());

        $floor->setAttribute('image', []);
        $floor->setAttribute('area', 0.00);

        return response()->json(['floor' => $floor]);
    });

    Route::post('/floor/update', function(Request $request) {
        
        parse_str($request->data, $requestData); // !!

        ArchitectureFloor::updateFloor($requestData, $request->file('image'));

        return response()->json(['status' => 'OK']);
        
    });

    Route::post('/floor/remove', function(Request $request) {

        ArchitectureFloor::removeFloor($request);

        return response()->json(['status' => 'OK']);

    });

    Route::get('/getShortUrl', function(Request $request){
        
        $shortUrl = null;

        if ($request->query('longUrl')){
            try {
                $shortUrl = Shorty::shorten($request->query('longUrl'));
            } catch(Exception $ex) {

            }

            return response()->json(['short_url' => $shortUrl]);
        }

    });

});

