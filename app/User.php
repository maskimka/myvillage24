<?php

namespace App;

use App\Traits\Imageable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Cartalyst\Sentinel\Users\EloquentUser;
use Cookie;

class User extends EloquentUser
{
    use Notifiable;
    
    use Imageable;

    use SoftDeletes;
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
//    protected $guarded = ['id', 'permissions', 'is_deleted'];

    protected $dates = ['birthday', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'facebook_id',
        'email',
        'password',
        'last_name',
        'first_name',
        'country_id',
        'permissions',
        'birthday',
        'zip',
        'state',
        'region',
        'city',
        'street',
        'block',
        'apartment',
        'avatar',
        'gender',
        'personal_phone',
        'mobile_phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lands()
    {
        return $this->hasMany('App\Land');
    }

    public function architecture()
    {
        return $this->hasMany('App\Architecture');
    }

    public function buildings()
    {
        return $this->hasMany('App\Building');
    }
    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_users');
    }

    /**
     * The country that belong to the user.
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * The docs that belong to the user.
     */
    public function docs()
    {
        return $this->belongsTo('App\UserDoc', 'user_docs');
    }

    /**
     * Route notifications for the mail channel.
     *
     * @return string
     */
    public function routeNotificationForMail()
    {
        return $this->email;
    }

    public function hasRole($role_slug)
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->slug == $role_slug)
            {
                return true;
            }
        }

        return false;
    }

    public function wishlist() {

        return $this->morphToMany('App\Wishlist', 'wishlistables');
    }

    public function setCountry($request, $countries)
    {
        if (Cookie::has('__mv_client_country')) {
            $countryIsoCode = $request->cookie('__mv_client_country');
            $countryId =  isset($countries[$countryIsoCode]) ? $countries[$countryIsoCode]->id : null;
        }
        $this->country_id = isset($countryId)?$countryId:config('custom.default_country');

        return $this->save();
    }

}
