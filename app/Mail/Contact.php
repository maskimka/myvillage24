<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    public $formData = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->formData = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('layout.mail.contact')->subject('MyVillage24 Contact Form')->with([
            'name'    => $this->formData['name'],
            'email'   => $this->formData['email'],
            'text'    => $this->formData['message'],
        ]);
    }
}
