<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RoleAssignmentNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $formData = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->formData = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('layouts.mail.role_assignment')->subject('MyVillage24 Role Assignment')->with([
            'id'   => $this->formData['id'],
            'name' => $this->formData['name'],
            'role' => $this->formData['role'],
        ]);
    }
}
