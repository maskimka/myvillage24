<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoTemplate extends Model
{
    protected $table = 'seo_templates';

    protected $guarded = [
        'id',
        '_method',
    ];

    public function page()
    {
        return $this->belongsTo('App\Page', 'page_id');
    }
}
