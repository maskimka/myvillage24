<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EloquentEventServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \App\User::observe(\App\Observers\UserProfileObserver::class);
        \App\Land::observe(\App\Observers\LandObserver::class);
        \App\Architecture::observe(\App\Observers\ArchitectureObserver::class);
        \App\ArchitectureFloor::observe(\App\Observers\ArchitectureFloorObserver::class);
        \App\Order::observe(\App\Observers\OrderObserver::class);
        \App\Building::observe(\App\Observers\BuildingObserver::class);
        \App\Village::observe(\App\Observers\VillageObserver::class);
        \App\PtoRequest::observe(\App\Observers\PtoRequestObserver::class);
        \App\Currency::observe(\App\Observers\CurrencyObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
