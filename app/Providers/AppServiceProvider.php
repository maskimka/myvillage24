<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation as Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'LandGallery' => \App\Land::class,
            'ArchitectureGallery' => \App\Architecture::class,
            'ArchitectureFloor' => \App\ArchitectureFloor::class,
            'BuildingGallery' => \App\Building::class,
            'VillageGallery' => \App\Village::class,
        ]);

        view()->composer('*', 'App\Http\ViewComposers\AppComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        require_once __DIR__ . '/../Http/Helpers/Navigation.php';
        require_once __DIR__ . '/../Http/Helpers/MetaHelper.php';
    }
}
