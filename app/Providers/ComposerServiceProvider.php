<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
//        View::composer(
//            ['frontend.lands.index', 'frontend.lands.show'],
//            'App\Http\ViewComposers\TranslationComposer'
//        );
        $views = [
            'admin.*',
            'app_config.*',
            'architecture.*',
            'authentication.*',
            'building.*',
            'village.*',
            'errors.*',
            'floors.*',
            'land.*',
            'layouts.admin.*',
            'layouts.authentication.*',
            'layouts.errors.*',
            'layouts.panel.*',
            'modals.*',
            'orders.*',
            'panel.*',
            'profile.*',
            'pto_request.*',
            'roles.*',
            'users.*',
            'wishlist.*',
        ];
        view()->composer($views, 'App\Http\ViewComposers\TranslationComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
