<?php

namespace App\Console\Commands;

use App\Currency;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class LoadECBCurrency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:currency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load currency from ECB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Currency::loadCurrency($this->signature);
        Log::info("Currency Loaded");
    }
}
