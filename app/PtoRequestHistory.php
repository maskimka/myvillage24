<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PtoRequestHistory extends Model
{
    protected $table = 'pto_request_history';

    protected $guarded = ['id'];
}
