<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:150',
            'last_name'  => 'required|max:150',
            'email'      => 'required|email|max:150|unique:users',
            'password'   => 'required|min:8|regex:/[A-Za-z0-9]/',
            'country_id' => 'required',
            'terms'      => 'required',
        ];
    }
}
