<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateLand extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'purpose_id'       => 'nullable|numeric',
                    'area'             => 'nullable|numeric',
                    'from_city'        => 'nullable|numeric',
                    'description'      => 'nullable',
//                    'land_video'       => 'nullable|url'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'purpose_id'       => 'nullable|numeric',
                    'area'             => 'nullable|numeric',
                    'from_city'        => 'nullable|numeric',
                    'description'      => 'nullable',
//                    'land_video'       => 'nullable|url'
                ];
            default: break;
        }
    }
}
