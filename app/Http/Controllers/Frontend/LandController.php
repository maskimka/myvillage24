<?php

namespace App\Http\Controllers\Frontend;

use App\Communication;
use App\Http\Controllers\Controller;
use App\Land;
use App\Locality;
use App\Architecture;
use App\Building;
use App\Traits\Translatable;
use App\User;
use App\Visitor;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LandController extends Controller
{
    protected $landInstance;

    public function __construct(Land $land)
    {
        $this->landInstance = $land;
    }

    public function index(Request $request)
    {
        $initParams = $this->landInstance->getFormInitialParams($request);
        $landCount = $this->landInstance
            ->where(['is_deleted' => 0, 'is_active' => 1])
            ->count();

        $architectureCount = Architecture::where(['is_deleted' => 0, 'is_active' => 1])
            ->count();

        $buildingCount = Building::where(['is_deleted' => 0, 'is_active' => 1])
            ->count();

        return view('frontend.lands.index', compact('initParams', 'landCount', 'architectureCount', 'buildingCount'));
    }

    public function show($id, Wishlist $wishlistInstance, Request $request)
    {
        $item = $this->landInstance
            ->with([
                'images',
                'sale_status',
                'translations',
            ])
            ->where(['is_deleted'=> 0, 'is_active'=> 1, 'id' => $id])
            ->first();

        if(is_null($item)) {
            abort(404);
        }

        $item['relatedArchitecture'] = $item->getRelatedArchitecture();

        $item['per_are'] = ($item->area > 0)?number_format((float)($item->price / $item->area), 2, '.', ' '):0;

        $Meta = new \MetaHelper('Land', 'show', $item);

        $item->translate(collect([$item]));
        $item->prepareLandLongAddressForShow(collect([$item]), true);

        $user = $wishlistInstance->getUserOrVisitor($request);

        $wishlist = $user->wishlist()
            ->where([
                'product_type' => 'Land',
                'product_id' => $item->id
            ])
            ->get();

        $item['is_favorite'] = $wishlist->isEmpty()?false:true;
        // dd($item);

        return view('frontend.lands.show', compact('item', 'Meta'));
    }

    public function related($related_to)
    {
        return view('frontend.lands.related', compact('related_to'));
    }

    public function getLandList(Request $request, Land $landInstance, Wishlist $wishlistInstance)
    {
        $related_lands_id_list = [];
        if ($request->related_to) {
            $architecture = Architecture::find($request->related_to);
            $related_lands = $architecture->getRelatedLands()->get();

            foreach($related_lands as $item) {
                array_push($related_lands_id_list, $item->id);
            }
        }

        $lands = $landInstance->getLandList(null, $related_lands_id_list, $request, true);
        $landCount = $landInstance->getLandListCount(null, $related_lands_id_list, $request);

        $landInstance->translate($lands);

        $user = $wishlistInstance->getUserOrVisitor($request);

        $wishlistLandIds = $user->wishlist()
            ->where('product_type', 'Land')
            ->pluck('product_id')
            ->toArray();

        foreach($lands as $key => $land) {
            $lands[$key]['per_are'] = ($land->area > 0)?number_format((float)($land->price / $land->area), 2, '.', ' '):0;
            $lands[$key]['is_favorite'] = in_array($land->id, $wishlistLandIds)?true:false;
        }

        return new JsonResponse(['lands' => $lands, 'landCount' => $landCount], 200);
    }

    public function order(Request $request)
    {
        if ($request->ajax()) {

            $architecture  = $this->landInstance->find($request->id);
            $request['ip'] = $request->ip();

            $architecture->orders()->create($request->all());

            return new JsonResponse(['status'=>'OK'],200);
        }
    }
}
