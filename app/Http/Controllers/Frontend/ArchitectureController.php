<?php

namespace App\Http\Controllers\Frontend;

use App\AppConfig;
use App\Http\Controllers\Controller;
use App\Architecture;
use App\Land;
use App\Building;
use App\BuildingType;
use App\HouseZone;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class ArchitectureController extends Controller
{
    protected $architectureInstance;

    public function __construct(Architecture $architecture)
    {
        $this->architectureInstance = $architecture;
    }

    public function index(Request $request)
    {
        $architectureInitParams = $this->architectureInstance->getFormInitialParams($request);
        $architectureCount = $this->architectureInstance
            ->where(['is_deleted' => 0, 'is_active' => 1])
            ->count();
        $landCount = Land::where(['is_deleted' => 0, 'is_active' => 1])
            ->count();
        $buildingCount = Building::where(['is_deleted' => 0, 'is_active' => 1])
            ->count();

        return view('frontend.architecture.index', compact('architectureInitParams', 'architectureCount', 'buildingCount', 'landCount'));
    }

    public function show($id, Wishlist $wishlistInstance, Request $request, AppConfig $appConfigInstance)
    {
        $countryIso2 = $this->getCountryFromCookie($request);
        $item = $this->architectureInstance
            ->with([
                'images',
                'floors',
                'aac_kit',
                'floors.images',
                'floors.house_zone',
                'facadeGalleryImages',
                'interiorGalleryImages'
            ])
            ->where(['is_deleted'=> 0, 'is_active'=> 1, 'id' => $id])
            ->first();

        if(is_null($item)) {
            abort(404);
        }

        foreach ($item->floors as $key => $floor) {
            $item['floors'][$key]['image_thumbnail'] = isset($floor->images[0])?$floor->images[0]->path.'/thumbnails/'.$floor->images[0]->name:'img/common/no-image-b.png';
            $item['floors'][$key]['image_original']  = isset($floor->images[0])?$floor->images[0]->path.'/original/'.$floor->images[0]->name:'img/common/no-image-b.png';
            $item['floors'][$key]['is_placeholder']  = isset($floor->images[0])?false:true;
        }

        $item['getRelatedLands'] = $item->getRelatedLands(4)->get();
        $relatedLandsCount       = $item->getRelatedLands()->count();

        foreach ($item['getRelatedLands'] as $key => $land) {
            $item['getRelatedLands'][$key]['per_are'] = ($land->area > 0)?number_format((float)($land->price / $land->area), 0, '.', ' '):0;
        }

        $prices = $appConfigInstance
            ->where('country_iso2', $countryIso2)
            ->where('key', 'arch_gray_price')
            ->orWhere('key', 'arch_gray_facade_price')
            ->where('country_iso2', $countryIso2)
            ->get()
            ->keyBy('key');

        $Meta = new \MetaHelper('Architecture', 'show', $item);
        $this->architectureInstance->translate(collect([$item]));
        $user = $wishlistInstance->getUserOrVisitor($request);
        $wishlist = $user->wishlist()
            ->where([
                'product_type' => 'Architecture',
                'product_id' => $item->id
            ])
            ->get();

        $item['is_favorite'] = $wishlist->isEmpty()?false:true;

        return view('frontend.architecture.show', compact('item', 'Meta', 'relatedLandsCount', 'prices'));
    }

    public function related($related_to)
    {
        $architectureInitParams = $this->architectureInstance->getFormInitialParams();

        return view('frontend.architecture.related', compact('related_to', 'architectureInitParams'));
    }

    public function getArchitectureList(Request $request, Land $landInstance, Wishlist $wishlistInstance, AppConfig $appConfigInstance)
    {
        $countryIso2 = $this->getCountryFromCookie($request);

        $related_architecture_id_list = [];

        if ($request->related_to) {

            $lands = $landInstance->find($request->related_to);
            $related_architecture = $lands->getRelatedArchitecture();

            foreach($related_architecture as $item) {

                array_push($related_architecture_id_list, $item['id']);
            }
        }

        $architecture = $this->architectureInstance->getArchitectureList(null, $related_architecture_id_list, $request ,true);
        $architectureCount = $this->architectureInstance->getArchitectureList(null, $related_architecture_id_list, $request, true, true);

        $user = $wishlistInstance->getUserOrVisitor($request);

        $wishlistLandIds = $user->wishlist()
            ->where('product_type', 'Architecture')
            ->pluck('product_id')
            ->toArray();

        $prices = $appConfigInstance
            ->where('country_iso2', $countryIso2)
            ->where('key', 'arch_gray_price')
            ->orWhere('key', 'arch_gray_facade_price')
            ->where('country_iso2', $countryIso2)
            ->get()
            ->keyBy('key');

        foreach($architecture as $key => $item) {

            $architecture[$key]['is_favorite']     = in_array($item->id, $wishlistLandIds)?true:false;
            $architecture[$key]['full_low_price']  = $prices['arch_gray_price']->value * $item->general_area;
//            $architecture[$key]['full_high_price'] = $prices['arch_gray_facade_price']->value * $item->general_area;
            $architecture[$key]['currency'] = $prices['arch_gray_price']->currency_symbol;
        }

        return new JsonResponse(['architecture' => $architecture, 'architectureCount'=>$architectureCount], 200);
    }

    public function getArchitectureCount()
    {
        $architectureCount = $this->architectureInstance
            ->where(['is_deleted'=> 0,'is_active'=> 1])
            ->count();

        return new JsonResponse(['architectureCount' => $architectureCount], 200);
    }

    public function getBuildingTypes(Request $request)
    {
        $response = BuildingType::all();

        return new JsonResponse(['buildingTypes' => $response], 200);

    }

    public function order(Request $request)
    {
        if ($request->ajax()) {

            $architecture = $this->architectureInstance->find($request->id);
            $request['ip'] = $request->ip();

            $architecture->orders()->create($request->all());

            return new JsonResponse(['status'=>'OK'],200);
        }
    }

    private function getCountryFromCookie($request) {

        return $request->cookie('__mv_client_country');
    }
}
