<?php

namespace App\Http\Controllers\Frontend;

use App\AppConfig;
use App\Architecture;
use App\Building;
use App\Http\Controllers\Controller;
use App\User;
use App\Visitor;
use App\Wishlist;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Sentinel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Land;

class WishlistController extends Controller
{
    protected $wishlistInstance;

    public function __construct(Wishlist $wishlist)
    {
        $this->wishlistInstance = $wishlist;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.wishlist.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $wishlist = new Wishlist();
        $wishlist->fill($request->all());
        $user = $this->getUserOrVisitor($request);
        try
        {
            $user->wishlist()->save($wishlist);
        }
        catch(Exception $ex)
        {
            return new JsonResponse($ex, 500);
        }

        return new JsonResponse(null, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $params)
    {
        $params = explode('|', $params);
        $user = $this->getUserOrVisitor($request);
        try
        {
            $wishlistIds = $user->wishlist()
                ->where([
                    'product_id'   => $params[0],
                    'product_type' => $params[1],
                ])
                ->pluck('id')->toArray();

            $user->wishlist()
                ->where([
                    'product_id'   => $params[0],
                    'product_type' => $params[1],
                ])
                ->delete();

            DB::table('wishlistables')
                ->where('wishlistables_type', get_class($user))
                ->where('wishlistables_id', $user->id)
                ->whereIn('wishlist_id', $wishlistIds)
                ->delete();

        }
        catch(Exception $ex)
        {
            return new JsonResponse($ex, 500);
        }

        return new JsonResponse(null, 200);
    }

    public function getWishlist(Request $request, Land $land, Architecture $architecture, Building $building, AppConfig $appConfigInstance)
    {
        $countryIso2 = $this->getCountryFromCookie($request);

        $wishlist = $this->wishlistInstance->getWishlist($request);

        $land->translate($wishlist['lands']);
        $building->translate($wishlist['buildings']);
        $architecture->translate($wishlist['architectures']);
        $land->prepareLandShortAddress($wishlist['lands']);

        $prices = $appConfigInstance
            ->where('country_iso2', $countryIso2)
            ->where('key', 'arch_gray_price')
            ->orWhere('key', 'arch_gray_facade_price')
            ->where('country_iso2', $countryIso2)
            ->get()
            ->keyBy('key');

        foreach($wishlist['lands'] as $key => $land) {

            $wishlist['lands'][$key]['per_are'] = ($land->area > 0)?number_format((float)($land->price / $land->area), 0, '.', ' '):0;
        }

        foreach($wishlist['architectures'] as $key => $architecture) {

            $wishlist['architectures'][$key]['full_low_price']  = $prices['arch_gray_price']->value * $architecture->general_area;
            $wishlist['architectures'][$key]['currency'] = $prices['arch_gray_price']->currency_symbol;
        }

        return new JsonResponse($wishlist, 200);
    }

    private function getUserOrVisitor($request)
    {
        $user = Sentinel::getUser();

        if (!$user) {

            $cookie = $request->cookie('__mv_cid', null);

            if($cookie) {

                $user = Visitor::where('client_key', $cookie)->first();
            }
        }
        return $user;
    }

    private function getCountryFromCookie($request) {

        return $request->cookie('__mv_client_country');
    }
}
