<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Land;
use App\Architecture;
use App\Building;
use App\Traits\Translatable;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;

class PageController extends Controller
{
    public function home(Land $landInstance, Architecture $architectureInstance, Building $buildingInstance, Request $request)
    {
        $landCount = $landInstance
            ->where(['is_deleted' => 0,'is_active' => 1])
            ->count();
        $architectureCount = $architectureInstance
            ->where(['is_deleted' => 0,'is_active' => 1])
            ->count();
        $buildingCount = $buildingInstance
            ->where(['is_deleted' => 0,'is_active' => 1])
            ->count();
        $currentCountryCode = $this->getUserCountryIsoCode($request);

        $landInitParams = $landInstance->getFormInitialParams($request);
        $lands = $landInstance->where(['is_deleted' => 0, 'is_active' => 1])
            ->where('country_code', $currentCountryCode)
            ->take(5)
            ->get();
        $landInstance->translate($lands);
        foreach ($lands as $key => $land) {
            $lands[$key]['per_are'] = ($land->area > 0) ? number_format((float)($land->price / $land->area), 2, '.', ' ') : 0;
        }

        $architectureInitParams = $architectureInstance->getFormInitialParams($request);
        $architectures = $architectureInstance->where(['is_deleted' => 0, 'is_active' => 1])->take(6)->get();
        $architectureInstance->translate($architectures);
        foreach ($architectures as $key => $architecture) {
            $architectures[$key]['per_are'] = ($architecture->area > 0) ? number_format((float)($architecture->price / $architecture->area), 0, '.', ' ') : 0;
        }

        $buildingInitParams = $buildingInstance->getFormInitialParams($request);
        $buildings = $buildingInstance->with([
            'translations',
            'images',
            'lands'
        ])->where(['is_deleted' => 0, 'is_active' => 1])->take(4)->get();
        foreach ($buildings as $key => $building) {
            $buildings[$key]['release_date'] = Date::parse($building->release_date)->format('F Y');
        }
        $buildingInstance->translate($buildings);

        $projectCount = $architectureInstance
            ->where(['is_deleted' => 0,'is_active' => 1])
            ->count();

        return view('frontend.pages.home',
            compact(
                'landCount',
                'architectureCount',
                'buildingCount',
                'projectCount',
                'landInitParams',
                'architectureInitParams',
                'buildingInitParams',
                'lands',
                'architectures',
                'buildings'
            ));
    }

    public function minor()
    {
        return view('frontend.pages.minor');
    }
    
    public function services()
    {
        $Meta = new \MetaHelper('Page', 'services');

        return view('frontend.pages.services')->with('Meta',$Meta);
    }

    public function contacts()
    {
        $Meta = new \MetaHelper('Page', 'contacts');

        return view('frontend.pages.contacts')->with('Meta', $Meta);
    }

    public function partners()
    {
        $Meta = new \MetaHelper('Page', 'partners');

        return view('frontend.pages.partners')->with('Meta',$Meta);
    }

    public function hypothec()
    {
        return view('frontend.pages.hypothec');
    }

    public function about()
    {
        $Meta = new \MetaHelper('Page', 'about');

        return view('frontend.pages.about')->with('Meta',$Meta);
    }

    public function terms($model)
    {
        $Meta = new \MetaHelper('Page', 'terms');
        
        return view('frontend.pages.terms'.$model)->with('Meta',$Meta);
    }

    public function prices()
    {
        $Meta = new \MetaHelper('Page', 'prices');

        return view('frontend.pages.prices')->with('Meta',$Meta);
    }

    public function realtor()
    {
        $Meta = new \MetaHelper('Page', 'realtor');

        return view('frontend.pages.realtor')->with('Meta',$Meta);
    }

    public function architect()
    {
        $Meta = new \MetaHelper('Page', 'architect');

        return view('frontend.pages.architect')->with('Meta',$Meta);
    }

    public function construction()
    {
        $Meta = new \MetaHelper('Page', 'construction');

        return view('frontend.pages.construction')->with('Meta',$Meta);
    }
    
    public function representative()
    {
        $Meta = new \MetaHelper('Page', 'representative');

        return view('frontend.pages.representative')->with('Meta',$Meta);
    }

    public function finance()
    {
        $Meta = new \MetaHelper('Page', 'finance');

        return view('frontend.pages.finance')->with('Meta',$Meta);
    }

    public function modularstructures()
    {
        $Meta = new \MetaHelper('Page', 'modularstructures');

        return view('frontend.pages.modularstructures')->with('Meta',$Meta);
    }

    private function getUserCountryIsoCode($request)
    {
        $countryIsoCode = config('country_iso_code');
        if (!empty($request->country_code)) {
            $countryIsoCode = $request->country_code;
        } else {
            $countryIsoCode = $request->cookie('__mv_client_country');
        }

        return strtolower($countryIsoCode);
    }
    public function cookie()
    {
        return view('frontend.pages.cookie');
    } 
}
