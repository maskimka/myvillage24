<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Architecture;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Building;
use App\Land;
use Jenssegers\Date\Date;

class BuildingController extends Controller
{
    protected $buildingInstance;

    public function __construct(Building $building)
    {
        $this->buildingInstance = $building;
    }

    public function index(Request $request)
    {
        $initParams = $this->buildingInstance->getFormInitialParams($request);
        $buildingCount = $this->buildingInstance
            ->where(['is_deleted' => 0, 'is_active' => 1])
            ->count();
        $landCount = Land::where(['is_deleted' => 0, 'is_active' => 1])
            ->count();
        $architectureCount = Architecture::where(['is_deleted' => 0, 'is_active' => 1])
            ->count();

        return view('frontend.buildings.index',  compact('initParams', 'buildingCount', 'landCount', 'architectureCount'));
    }

    public function show($id, Land $landInstance, Wishlist $wishlistInstance, Request $request)
    {
        $item = $this->buildingInstance
            ->with([
                'lands',
                'lands.translations',
                'floors',
                'images',
                'floors.images'
            ])
            ->where(['is_deleted'=> 0, 'is_active'=> 1, 'id' => $id])
            ->first();

        if(is_null($item)) {
            abort(404);
        }

        foreach ($item->floors as $key => $floor) {

            $item['floors'][$key]['image_thumbnail'] = isset($floor->images[0])?$floor->images[0]->path.'/thumbnails/'.$floor->images[0]->name:'img/common/no-image-b.png';
            $item['floors'][$key]['image_original']  = isset($floor->images[0])?$floor->images[0]->path.'/original/'.$floor->images[0]->name:'img/common/no-image-b.png';
            $item['floors'][$key]['is_placeholder']  = isset($floor->images[0])?false:true;
        }

        $item['release_date'] = Date::parse($item->release_date)->format('F Y');

        $this->buildingInstance->translate(collect([$item]));
        $landInstance->prepareLandLongAddress($item->lands);

        $user = $wishlistInstance->getUserOrVisitor($request);

        $wishlist = $user->wishlist()
            ->where([
                'product_type' => 'Building',
                'product_id' => $item->id
            ])
            ->get();

        $item['is_favorite'] = $wishlist->isEmpty()?false:true;

        return view('frontend.buildings.show', compact('item'));
    }

    public function getBuildingList(Request $request, Land $landInstance, Wishlist $wishlistInstance)
    {
        $buildings =
            $this->buildingInstance->getBuildingList(null, true, $request);

        foreach($buildings as $key => $building) {
            $landInstance->prepareLandAddressForCards($building->lands, null, true);
        }

        $this->buildingInstance->translate($buildings);

        $user = $wishlistInstance->getUserOrVisitor($request);

        $wishlistLandIds = $user->wishlist()
            ->where('product_type', 'Building')
            ->pluck('product_id')
            ->toArray();

        foreach($buildings as $key => $building) {

            $buildings[$key]['is_favorite'] = in_array($building->id, $wishlistLandIds)?true:false;
        }

        return new JsonResponse(['buildings' => $buildings, 'buildingCount' => count($buildings)], 200);
    }

    public function getBuildingCount()
    {
        $buildingCount = $this->buildingInstance
            ->where(['is_deleted'=> 0,'is_active'=> 1])
            ->count();

        return new JsonResponse(['buildingCount' => $buildingCount], 200);
    }

    public function order(Request $request)
    {
        if ($request->ajax()) {

            $building = $this->buildingInstance->find($request->id);
            $request['ip'] = $request->ip();

            $building->orders()->create($request->all());

            return new JsonResponse(['status'=>'OK'],200);
        }
    }
}
