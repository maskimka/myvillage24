<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\SendContact;
use App\Mail\Contact;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function sendEmailContacts(SendContact $request)
    {
        Mail::to(config('mail.contact_email'))
            ->send(new Contact($request->all()));

        return new JsonResponse(null, 200);
    }
}
