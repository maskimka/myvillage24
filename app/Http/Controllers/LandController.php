<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\LandTranslation;
use App\SaleStatus;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use Mockery\CountValidator\Exception;
use Sentinel;
use App\Country;
use App\Purpose;
use App\Communication;
use App\Infrastructure;
use App\Transport;
use App\Land;
use App\LandSizeMap;
use Image;
use App\Locality;
use DB;

use App\Http\Requests\CreateLand;

class LandController extends Controller
{
    protected $landInstance;

    public function __construct(Land $land)
    {
        $this->landInstance = $land;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Sentinel::getUser();

        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');

        if(
            $user->inRole('admin') ||
            $user->inRole('manager') ||
            $user->inRole('moderator')
        )
        {
            $lands = $this->landInstance->getLandsByLocale($locale);
        } else {
            $lands = $this->landInstance->getLandsByLocale($locale, $user->id);
        }

        foreach ($lands as &$land) {
            $land['translation'] = collect($land->translations->reduce(function ($translateCollection, $item) {
                $translateCollection[$item['locale']] = $item;
                return $translateCollection;
            }, []));
        }

        $statuses = SaleStatus::all();

        return view('land.index', compact('lands', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');
        $adminLocale = session('admin_locale', config('app.admin_locale'));

        $land = $this->landInstance->create();

        LandTranslation::updateOrCreate([
                'land_id' => $land->id,
                'locale'  => $locale,
            ],
            [
                'land_id' => $land->id,
                'locale'  => $locale,
            ]);

        return redirect($request->segment(1).'/'.$adminLocale.'/land/'.$land->id.'/edit?lang='.$locale);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateLand  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLand $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');

        $land = $this->landInstance
            ->where('id', $id)
            ->first();

        if ($land->translations()->where('locale', $locale)->count() == 0) {
            LandTranslation::create([
                'land_id' => $land->id,
                'locale'  => $locale,
            ]);

            $land = $this->landInstance
                ->with(['infrastructure', 'communications', 'transport', 'translations'])
                ->where('id', $id)
                ->first();
        }

        $land['translation'] = collect($land->translations->reduce(function ($translateCollection, $item) {
            $translateCollection[$item['locale']] = $item;
            return $translateCollection;
        }, []));

        $land_images = $land->images()->get();

        return view("land.edit",[
            'land'           => $land,
            'land_images'    => $land_images,
            'purposes'       => Purpose::all()->sortBy('id'),
            'communications' => Communication::all()->sortBy('id'),
            'infrastructure' => Infrastructure::all()->sortBy('id'),
            'transport'      => Transport::all()->sortBy('id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CreateLand  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateLand $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $land = Land::findOrFail($id);

            $land->communications()->sync($request->get('communications',[]));
            $land->infrastructure()->sync($request->get('infrastructure',[]));
            $land->transport()->sync($request->get('transport', []));

            $files = $request->file('images');
            $land->multipleFileUpload($files, config('custom.LAND_IMAGES_PATH'), config('custom.RESIZE_LAND_IMAGE_FIT_W'), config('custom.RESIZE_LAND_IMAGE_FIT_H'), $request['fileuploader-list-images']);
            $land->translations()->updateOrCreate(
                array('land_id' => $land->id, 'locale' => $request->data_lang),
                [
                    'land_id'        => $land->id,
                    'locale'         => $request->data_lang,
                    'street_number'  => $request->street_number,
                    'street'         => $request->street,
                    'sublocality'    => $request->sublocality,
                    'locality'       => $request->locality,
                    'administrative' => $request->administrative,
                    'country'        => $request->country,
                    'country_code'   => $request->country_code,
                    'description'    => $request->description
                ]
            );

            if ($land->update($request->all())) {
                DB::commit();
            };
        }
        catch(Exception $e) {
            DB::rollback();
            throw new Exception(500);
        }

        return redirect($request->segment(1).'/land?lang='.$request->data_lang);
    }

    /**
     * Show the form for complement the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function complement($id)
    {
        return view("land.complement",[
            'land' => $this->landInstance->find($id),
        ]);
    }

    /**
     * Show the form for complement the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function complement_save(Request $request, $id)
    {
        $land = $this->landInstance->find($id);

        $landSizeMapFile = ($request->file('imageSizeMap'))?:null;
        $landCadastralMapFile = ($request->file('imageCadastralMap'))?:null;
        $imageSizeMapCurrentFile = $request['fileuploader-list-imageSizeMap'];
        $imageCadastralMapCurrentFile = $request['fileuploader-list-imageCadastralMap'];

        $request['size_map'] =
            $land->oneFileFitUpload($landSizeMapFile, config('custom.LAND_IMAGES_PATH'), config('custom.RESIZE_THUMB_WIDTH'), config('custom.RESIZE_THUMB_HEIGHT'), $imageSizeMapCurrentFile, $land->size_map);

        $request['cadastral_map'] =
            $land->oneFileFitUpload($landCadastralMapFile, config('custom.LAND_IMAGES_PATH'),config('custom.RESIZE_THUMB_WIDTH'), config('custom.RESIZE_THUMB_HEIGHT'), $imageCadastralMapCurrentFile, $land->cadastral_map);

        $land->land_size_map()->delete();

        if (!empty($request->parties)) {
            foreach($request->parties as $key=>$value)
            {
                $landSizeMap = new LandSizeMap();
                $landSizeMap->name = $key;
                $landSizeMap->value = $value;

                $land->land_size_map()->save($landSizeMap);
            }
        }

        $land->update($request->all());

        return redirect('/'.$request->segment(1).'/land');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $land = $this->landInstance->find($id);
            $land->is_deleted = 1;

            if ($land->save()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }

    public function activate(Request $request)
    {
        if ($request->ajax()) {

            $land = $this->landInstance->find($request->land_id);

            $land->is_active = $request->activate;

            if ($land->save()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }

    public function change_sale_status(Request $request)
    {
        if ($request->ajax()) {

            $land = $this->landInstance->find($request->land_id);

            $land->sale_status_id = $request->status_id;

            if ($land->save()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }

    public function getLocalities(Request $request)
    {
        if ($request->ajax()) {

            $response = Locality::where('level', intval($request['level']))
                ->where('parent_id', intval($request['locality_id']))->get();

            return new JsonResponse($response, 200);
        }
    }
}
