<?php

namespace App\Http\Controllers;

use App\Building;
use App\Communication;
use App\Infrastructure;
use App\Transport;
use App\Village;
use App\VillageTranslation;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\SaleStatus;
use App\Land;
use Sentinel;
use DB;

class VillageController extends Controller
{
    protected $villageInstance;

    public function __construct(Village $villageInstance)
    {
        $this->villageInstance = $villageInstance;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Sentinel::getUser();
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');
        if ($user->inRole('admin')
            || $user->inRole('manager')
            || $user->inRole('moderator')
        ) {
            $village = $this->villageInstance->getVillagesByLocale($locale);
        } else {
            $village = $this->villageInstance->getVillagesByLocale($locale, $user->id);
        }

        $statuses = SaleStatus::all();

        return view('village.index', compact('village', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');
        $adminLocale = session('admin_locale', config('app.admin_locale'));

        $village = $this->villageInstance->create();
        VillageTranslation::updateOrCreate([
                'village_id' => $village->id,
                'locale'  => $locale,
            ],[
                'village_id' => $village->id,
                'locale'  => $locale,
            ]
        );

        return redirect($request->segment(1).'/'.$adminLocale.'/village/'.$village->id.'/edit?lang='.$locale);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');

        $village = $this->villageInstance->findOrFail($id);
        if ($village->translations()->where('locale', $locale)->count() == 0) {
            VillageTranslation::create([
                'village_id' => $village->id,
                'locale'  => $locale,
            ]);

            $village = $this->landInstance
                ->with(['infrastructure', 'communications', 'transport', 'translations'])
                ->where('id', $id)
                ->first();
        }

        $village['translation'] = collect($village->translations->reduce(function ($translateCollection, $item) {
            $translateCollection[$item['locale']] = $item;
            return $translateCollection;
        }, []));

        return view('village.update', [
            'village' => $village,
            'communications' => Communication::all()->sortBy('id'),
            'infrastructure' => Infrastructure::all()->sortBy('id'),
            'transport'      => Transport::all()->sortBy('id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $village = $this->villageInstance->find($id);

            $general_plan_image = $request->file('general_plan');
            $village->general_plan =
                $village->oneFileFitUpload($general_plan_image,
                    config('custom.VILLAGE_IMAGES_PATH'),
                    config('custom.RESIZE_THUMB_WIDTH'),
                    config('custom.RESIZE_THUMB_HEIGHT'),
                    $request['fileuploader-list-general_plan'],
                    $village->general_plan);

            $village->communications()->sync($request->get('communications',[]));
            $village->infrastructure()->sync($request->get('infrastructure',[]));
            $village->transport()->sync($request->get('transport', []));

            $village->translations()->updateOrCreate(
                array('village_id' => $village->id, 'locale' => $request->data_lang),
                [
                    'village_id' => $village->id,
                    'locale' => $request->data_lang,
                    'title' => $request->title
                ]
            );

            if ($village->update($request->all())) {
                DB::commit();
            }
        }
        catch(\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return redirect($request->segment(1).'/village?lang='.$request->data_lang);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $village = $this->villageInstance->find($id);
            if ($village->delete()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }

    public function activate(Request $request)
    {
        if ($request->ajax()) {
            $village = $this->villageInstance->find($request->building_id);
            $village->is_active = $request->activate;
            if ($village->save()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }

    public function attach_land(Request $request)
    {
        $land = Land::where('article', strtoupper($request->article))->get();
        $village = $this->villageInstance->find($request->id);

        $existing_lands = [];
        foreach ($village->lands()->get() as $item) {
            $existing_lands[] = $item->id;
        }

        if (!in_array($land[0]->id, $existing_lands)) {
            $village->lands()->attach($land[0]->id);
        } else {
            return new JsonResponse(null, 501);
        }

        return new JsonResponse(['result'=>$land[0]], 200);
    }

    public function detach_land(Request $request)
    {
        $village = $this->villageInstance->find($request->id);
        $village->lands()->detach($request->land_id);

        return new JsonResponse(['status'=>'OK'], 200);
    }

    public function attach_building(Request $request)
    {
        $building = Building::where('article', trim(strtoupper($request->article)))->get();
        $village = $this->villageInstance->find($request->id);

        $existing_buildings = [];
        foreach ($village->buildings()->get() as $item) {
            $existing_buildings[] = $item->id;
        }

        if (!in_array($building[0]->id, $existing_buildings)) {
            $village->buildings()->attach($building[0]->id);
        } else {
            return new JsonResponse(null, 501);
        }

        return new JsonResponse(['result'=>$building[0]], 200);
    }

    public function detach_building(Request $request)
    {
        $village = $this->villageInstance->find($request->id);
        $village->buildings()->detach($request->land_id);

        return new JsonResponse(['status'=>'OK'], 200);
    }
}
