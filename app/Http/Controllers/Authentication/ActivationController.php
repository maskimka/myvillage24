<?php

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Activation;
use Sentinel;
use Carbon\Carbon;

class ActivationController extends Controller
{
    protected $toPath = 'auth/login';

    public function activate($user_id, $ACC)
    {
        $user = Sentinel::findUserById($user_id);

        if (Activation::complete($user, $ACC)) {
            // set notification complete
            $user->unreadNotifications()
                ->where(['type' => 'App\Notifications\ActivationUser'])
                ->update(['read_at' => Carbon::now()]);
            return redirect($this->toPath)->with(['success' => "note_account_activation_success"]);

        }
        
        return redirect($this->toPath)->with(['error' => "note_account_activation_wrong"]);
    }
}
