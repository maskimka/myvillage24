<?php

namespace App\Http\Controllers\Authentication;


use App\Country;
use Illuminate\Support\Facades\Cookie;
use Validator;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Socialite;
use App\User;


class LoginController extends Controller
{
    protected $valid;

    protected $sentinel;

    protected $errors;
    
    protected $remember_me = false;

    public function showLoginForm()
    {
        if ($user = Sentinel::check()) {

            if (
                $user->inRole('admin') || 
                $user->inRole('seo_manager') || 
                $user->inRole('moderator') ||
                $user->inRole('manager')  
            ) 
            {

                return redirect('/admin/dashboard');
            }

            return redirect('/panel/dashboard');
        }

        return view('authentication.login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email'      => 'required|email|max:150',
            'password'   => 'required|min:8',
        ]);
    }

    public function login(Request $request)
    {
        try {
            $this->valid = $this->validator($request->all());

            if (isset($request->remember_me)) {
                $this->remember_me = true;
            }

            if (!$this->valid->fails() && $user = Sentinel::authenticate($request->all(), $this->remember_me))
            {
                if(
                    $user->inRole('admin') ||
                    $user->inRole('seo_manager') ||
                    $user->inRole('manager') ||
                    $user->inRole('moderator')
                ) {
                    $redirectPath = "/admin/dashboard";
                } else {
                    $redirectPath = "/panel/dashboard";
                }
                return redirect($redirectPath);
            }
            else
            {
                $this->errors = trans("messages.note_wrong_credentials");
            }
        } catch (ThrottlingException $e) {

            $this->errors = trans('messages.note_security_wait', ['seconds' => $e->getDelay()]);
        } catch (NotActivatedException $e) {

            $this->errors = trans("messages.note_account_activation_wrong");
        }
        return redirect()->back()->with(['error' => $this->errors]);
    }

    public function logout()
    {
        Sentinel::logout();

        return redirect('/auth/login');
    }

    function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    function handleProviderCallback(Request $request, $provider)
    {
        if (!$request->has('code') || $request->has('denied')) {

            return redirect('/auth/login');
        }

        try {

            $user = Socialite::driver($provider)->user();

        } catch (Exception $e) {

            return redirect('auth/socialite/' . $provider);

        }
        $authUser = $this->findOrCreateUser($user, $request);

        Sentinel::authenticate($authUser, true);

        return redirect('/auth/login');
    }

    private function findOrCreateUser($socialiteUser, $request)
    {
        $user = User::where('socialite_id', $socialiteUser->id)->where('is_deleted', 0)->first();

        if (!$user) {

            if ($user = User::where('email', $socialiteUser->email)->where('is_deleted', 0)->first()) {

                $user->socialite_id = $socialiteUser->id;
                $user->save();
            }
        }

        if ($user){

            $sentinelUser = User::where('id', $user->id)->where('is_deleted', 0)->first();
            return $sentinelUser;
        }

        $name = preg_replace('/[\s]{2,}/', ' ', $socialiteUser->name);

        $name = explode(' ', $name);

        $gender = 'N';

        if (isset($socialiteUser->user['gender'])) {

            $gender = ($socialiteUser->user['gender'] == 'female')?'W':'M';
        }

        $newUser = Sentinel::register([
            'first_name'   => $name[0],
            'last_name'    => $name[1],
            'email'        => $socialiteUser->email,
            'socialite_id' => $socialiteUser->id,
            'password'     => $socialiteUser->id,
            'gender'       => $gender,
            'country_id'   => $this->getClientCountryId($request)
        ]);

        if ($newUser->avatar = $newUser->saveSocialAvatar($socialiteUser->avatar)) {
            $newUser->save();
        }

        $role = Sentinel::findRoleBySlug('client');
        $role->users()->attach($newUser);

        return $newUser;
    }

    private function getClientCountryId($request)
    {
        $countryIso2Code = Cookie::get('__mv_client_country', null);
        if (! $countryIso2Code) {
            $countryInformation = geoip()->getLocation($request->server('HTTP_X_REAL_IP'));
            $countryIso2Code = $countryInformation->iso_code;
        }

        $countryId = Country::where('iso2code', $countryIso2Code)->pluck('id')->first();

        return $countryId?:config('custom.default_country');
    }
}
