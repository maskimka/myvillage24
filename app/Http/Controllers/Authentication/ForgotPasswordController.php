<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Reminder;
use Sentinel;
use Carbon\Carbon;
use App\User;
use App\Http\Requests\ResetPassword;
use App\Notifications\RestorePassword;

class ForgotPasswordController extends Controller
{
    public function showForgotPasswordForm()
    {
        return view('authentication.forgot-password');
    }

    public function restorePassword(Request $request)
    {
        $user = User::whereEmail($request->email)->first();

        if (count($user) > 0) {
            $reminder = Reminder::exists($user)?:Reminder::create($user);
            $user->notify(new RestorePassword($reminder->code));
        }

        return redirect()->back()->with(['success' => "note_sent_code_reset_password_on_email"]);
    }
    
    public function resetPassword($user_id, $code)
    {
        $user     = Sentinel::findById($user_id);
        $reminder = Reminder::exists($user);

        if (!count($user) || !$reminder || $reminder->code !== $code) {
            abort(498);
        }

        return view('authentication.reset-password',
            [
                'user_id' => $user->id,
                'code'    => $code
            ]
        );
    }

    public function postResetPassword(ResetPassword $request)
    {
        $user = Sentinel::findUserById($request->user_id);

        if (count($user) == 0) {
            return redirect('/auth/login')
                ->with('error', 'note_error_try_again');
        }

        $user->unreadNotifications()
            ->where(['type' => 'App\Notifications\RestorePassword'])
            ->update(['read_at' => Carbon::now()]);

        Reminder::complete($user, $request->code, $request->password);

        return redirect('/auth/login')
            ->with('success', 'note_login_with_new_password');
    }
}
