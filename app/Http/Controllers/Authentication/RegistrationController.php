<?php

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use App\Http\Requests\RegistrateUser;
use App\Http\Controllers\Controller;
use App\Notifications\ActivationUser;
use Sentinel;
use Activation;
use App\Country;

class RegistrationController extends Controller
{
    protected $valid;

    protected $sentinel;

    protected $baseRole    = 'client';

    protected $isCompleted = false;

    public function showRegistrationForm()
    {
        return view(
            'authentication.registration',
            [
                'countries' => Country::all()->sortBy('name'),
                'isCompleted' => $this->isCompleted
            ]
        );
    }

    public function register(RegistrateUser $request)
    {
        $user = Sentinel::register($request->all());

        //$activation = Activation::create($user);

        $role = Sentinel::findRoleBySlug($this->baseRole);
        $role->users()->attach($user);

//        if (isset($user)) {
//            $user->notify(new ActivationUser($activation->code));
//            $this->isCompleted = true;
//        }

//        return view(
//            'authentication.registration',
//            [
//                'isCompleted' => $this->isCompleted
//            ]
//        );

        return redirect('/auth/login')->with(['success' => trans("messages.note_account_registration_success")]);

    }
}
