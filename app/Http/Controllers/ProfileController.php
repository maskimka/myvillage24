<?php

namespace App\Http\Controllers;

use App\Architecture;
use App\Building;
use App\Currency;
use App\Land;
use App\Mail\RoleAssignmentNotification;
use App\Role;
use App\UserDoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mockery\CountValidator\Exception;
use Sentinel;
use Image;
use App\Country;
use App\Http\Requests\ProfileUpdate;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\JsonResponse;
use Validator;
use DB;

class ProfileController extends Controller
{
    protected $pf_avatar_path;
    
    protected $status;
    
    protected $message;

    public function __construct()
    {
        $this->pf_avatar_path = config('custom.PROFILE_AVATAR_PATH');
        $this->status = 'success';
        $this->message = trans('messages.label_success');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserDoc $userDoc)
    {
        $user = Sentinel::getUser();

        $age = null;

        if ($user->birthday) {
            $age = $user->birthday->diff(Carbon::now())->format('%y');
        }

        $requestedRoles = $userDoc
            ->where([
                'user_id'     => $user->id,
                'approved'   => 0,
                'is_deleted' => 0
            ])
            ->groupBy('role_id')
            ->get(['role_id']);

        $roles = $requestedRoles->map(function ($role) {
            return $role['role_id'];
        });

        return view('profile.profile', compact("age", "roles"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('profile.edit', [
            'countries' => Country::all()->sortBy('name')
        ]);
    }

    public function update(ProfileUpdate $request)
    {
        try {

            $user = Sentinel::getUser();

            foreach($request->all() as $column => $fields) {

                if (!in_array($column, array('password','password_confirmation','terms', '_token')) && !empty($fields)) {

                    $user->{$column} = trim($fields, '_-() ');
                }
            }

            if ( !empty($request['password']) ) {

                $user->password  = bcrypt($request['password']);
            }

            $user->save();

        } catch(Exception $e) {

            throw new Exception(500);
        }

        return redirect($request->segment(1).'/profile');
    }

    public function change_avatar(Request $request)
    {
        try {

            if ($request->hasFile('avatar')) {

                $user = Sentinel::getUser();
                
                $resize = ["width" => 128, "height" => 128 ];

                $user->oneFileUploadAvatar($request->file('avatar'), $this->pf_avatar_path, $resize, $request['cropperParams']);

                $user->save();
            } else {

                $this->status = 'error';
                $this->message = trans('messages.label_error');
            }
        } catch( Exception $e ) {

            $this->status = 'error';
            $this->message = trans('messages.label_error');
        }

        return redirect()->back()->with(array($this->status=>$this->message));
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @param Request $request
     * @param Land $landInstance
     * @return JsonResponse
     */
    public function destroy($id, Request $request, Land $landInstance, Architecture $architectureInstance, Building $buildingInstance)
    {
        if ($request->ajax()) {
            $user = Sentinel::findById($id);
            DB::beginTransaction();
            try {
                if ($user->hasRole('realtor')) {
                    $lands = $user->lands();
                    if ($lands->count()) {
                        $landInstance->hardDelete($lands);
                    }
                }

                if ($user->hasRole('architect')) {
                    $architecture = $user->architecture();
                    if ( $architecture->count()) {
                        $architectureInstance->hardDelete($architecture);
                    }

                    $buildings = $user->buildings();
                    if ($buildings->count()) {
                        $buildingInstance->hardDelete($buildings);
                    }
                }

                if (!$user->forceDelete()) {
                    throw new \Exception('Can\'t delete user');
                }
                DB::commit();
                Sentinel::logout();
            }
            catch (\Exception $e) {
                DB::rollback();
                return new JsonResponse(null, 500);
            }
        }
        return new JsonResponse(null, 200);
    }

    public function roles()
    {
        return view('profile.roles');
    }

    public function requestRoleRealtor(Request $request, Role $roleInstance)
    {
        if ($request->ajax()) {

            $validator = Validator::make($request->all(), [
                'roleId' => 'required',
            ]);

            if ($validator->passes()) {

                $user = Sentinel::getUser();

                $roles = $user->roles()->pluck('id');

                if (!$roles->contains($request->roleId)) {

                    $roles[] = $request->roleId;
                    $user->roles()->sync($roles);
                }
                $roleName = $roleInstance->where('id', 3)->pluck('name')->first();
                $this->roleAssignmentNotify($user, $roleName);

                return new JsonResponse(null, 200);

            } else {
                return new JsonResponse($validator->errors(), 500);
            }
        }
    }

    public function requestRoleArchitect(Request $request, Role $roleInstance)
    {
        if ($request->ajax()) {

            $validator = Validator::make($request->all(), [
                'roleId' => 'required',
                'archive' => 'bail|required|max:7000|mimes:zip,rar',
            ]);

            if ($validator->passes()) {

                $user = Sentinel::getUser();
                $archiveName = $user->archiveUpload($request->archive, config('custom.DOCS_PATH'));

                $result = UserDoc::create([
                    'user_id' => $user->id,
                    'role_id' => $request->roleId,
                    'path' => config('custom.DOCS_PATH'),
                    'name' => $archiveName,
                ]);

                if ($result) {

                    $roles = $user->roles()->pluck('id');

                    if (!$roles->contains($request->roleId)) {

                        $roles[] = $request->roleId;
                        $user->roles()->sync($roles);
                    }

                    $roleName = $roleInstance->where('id', 3)->pluck('name')->first();
                    $this->roleAssignmentNotify($user, $roleName);

                    return new JsonResponse(null, 200);
                } else {
                    return new JsonResponse(null, 500);
                }
            } else {
                return new JsonResponse($validator->errors(), 200);
            }
        }
    }

    private function roleAssignmentNotify($user, $role)
    {
        Mail::to(config('mail.contact_email'))
            ->send(new RoleAssignmentNotification([
                'id' => $user->id,
                'name' => $user->first_name.' '.$user->last_name,
                'role' => $role
            ]));
    }
}
