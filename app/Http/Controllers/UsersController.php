<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Sentinel;
use Carbon\Carbon;
use App\Role;
use App\Country;
use Illuminate\Http\JsonResponse;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('is_deleted', 0)->where('id', '!=', Sentinel::getUser()->id)->get();

        foreach($users as $key =>$user) {

            $users[$key]['age'] = ($user->birthday)?$user->birthday->diff(Carbon::now())->format('%y'):null;
            $userRoles = $user->roles()->get();

            $userRolesId = [];

            foreach($userRoles as $i=>$userRole) {

                $userRolesId[] = $userRole->id;
            }

            $users[$key]['roles'] = $userRolesId;
        }

        $roles = Role::all();

        return view('users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();

        return view('users.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Sentinel::register($request->all());

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Sentinel::findById($id);
        $countries = Country::all();

        return view('users.edit', compact('client', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        foreach($request->all() as $column => $fields) {

            if (!in_array($column, array('password','password_confirmation','terms', '_token', '_method')) && !empty($fields)) {

                $user->{$column} = $fields;
            }
        }

        if ( !empty($request['password']) ) {

            $user->password  = bcrypt($request['password']);
        }

        $user->save();

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $user = Sentinel::findById($id);
            $user->is_deleted = 1;

            if ($user->save()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }

    public function updateRole(Request $request)
    {
        if ($request->ajax()) {

            $user = Sentinel::findById(intval($request['user_id']));

            if (!is_null($request['roles'])) {
                $user->roles()->sync($request['roles']);
            }

            return new JsonResponse(null, 200);
        }
    }
}
