<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permissions['/admin/roles']     = true;
        $permissions['/admin/roles/*']   = true;
        $permissions['/admin/dashboard'] = true;
        $permissions['/admin/orders']    = true;
        $permissions['/admin/getLocalities']    = true;

        $input = $request->except(['_token', '_method']);
        $input['permissions'] = json_decode($input['permissions'], true);
        $role  = Role::create($input);

//        if (isset($input['landAll'])) {
//
//            $permissions[$input['landAll']] = true;
//            $permissions['/admin/land'] = true;
//
//        } elseif (isset($input['land'])) {
//
//            $permissions = array_merge($permissions, $input['land']);
//        }
//
//        if (isset($input['architectureAll'])) {
//
//            $permissions[$input['architectureAll']] = true;
//            $permissions['/admin/architecture'] = true;
//
//        } elseif (isset($input['architecture'])) {
//
//            $permissions = array_merge($permissions, $input['architecture']);
//        }
//
//        if (isset($input['buildingAll'])) {
//
//            $permissions[$input['buildingAll']] = true;
//            $permissions['/admin/building'] = true;
//
//        } elseif (isset($input['building'])) {
//
//            $permissions = array_merge($permissions, $input['building']);
//        }
//
//        if (isset($input['usersAll'])) {
//
//            $permissions[$input['usersAll']] = true;
//            $permissions['/admin/users'] = true;
//
//        } elseif (isset($input['users'])) {
//
//            $permissions = array_merge($permissions, $input['users']);
//        }
//
//        $role->permissions = $permissions;

        $role->save();

        return redirect('/admin/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);

        return view("roles.edit", compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
//        $permissions['/admin/roles']     = true;
//        $permissions['/admin/roles/*']   = true;
//        $permissions['/admin/dashboard'] = true;
//        $permissions['/admin/orders']    = true;
//        $permissions['/admin/getLocalities']    = true;

        $input = $request->except(['_token', '_method']);
        $role  = Role::find($input['id']);

        $input['permissions'] = json_decode($input['permissions'], true);
        $role->fill($input);
//dd($role->permissions);
//        $role->name = $input['name'];
//        $role->slug = $input['slug'];
//        $role->description = $input['description'];
//        dd(json_decode($input['permissions'], true));

//        if (isset($input['landAll'])) {
//
//            $permissions[$input['landAll']] = true;
//            $permissions['/admin/land'] = true;
//
//        } elseif (isset($input['land'])) {
//
//            $permissions = array_merge($permissions, $input['land']);
//        }
//
//        if (isset($input['architectureAll'])) {
//
//            $permissions[$input['architectureAll']] = true;
//            $permissions['/admin/architecture'] = true;
//
//        } elseif (isset($input['architecture'])) {
//
//            $permissions = array_merge($permissions, $input['architecture']);
//        }
//
//        if (isset($input['buildingAll'])) {
//
//            $permissions[$input['buildingAll']] = true;
//            $permissions['/admin/building'] = true;
//
//        } elseif (isset($input['building'])) {
//
//            $permissions = array_merge($permissions, $input['building']);
//        }
//
//        if (isset($input['usersAll'])) {
//
//            $permissions[$input['usersAll']] = true;
//            $permissions['/admin/users'] = true;
//
//        } elseif (isset($input['users'])) {
//
//            $permissions = array_merge($permissions, $input['users']);
//        }
//
//        $role->permissions = $permissions;

        $role->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
