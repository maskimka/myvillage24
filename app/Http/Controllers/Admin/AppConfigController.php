<?php

namespace App\Http\Controllers\Admin;

use App\AppConfig;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppConfigController extends Controller
{
    private $appConfigInstance;

    public function __construct(AppConfig $appConfig)
    {
        $this->appConfigInstance = $appConfig;
    }

    public function generalConfig()
    {
        return view('app_config.general');
    }

    public function landConfig()
    {
        return view('app_config.land');
    }

    public function architectureConfig()
    {
        return view('app_config.architecture');
    }

    public function buildingConfig()
    {
        return view('app_config.building');
    }

    public function architectureConfigByCountry(Request $request)
    {
        if ($request->ajax()) {

            $configs = $this->appConfigInstance
                ->where('key', 'LIKE', 'arch_%')
                ->where('country_iso2', $request->iso2)
                ->get();

            return new JsonResponse($configs, 200);
        }
    }

    public function architectureConfigStore(Request $request)
    {
        $configKeys = $request->except(['_token', 'country_iso2']);

        foreach ($configKeys as $key => $value) {

            $this->appConfigInstance->updateOrCreate([
                'country_iso2' => $request->country_iso2,
                'key'  => $key,
            ],
                [
                    'key'  => $key,
                    'value' => $value,
                ]);
        }

        return redirect($request->segment(1).'/config/architecture');
    }

    public function architectureConfigCreate(Request $request)
    {
        $data = $request->except(['_method', '_token']);

        $data['key'] = 'arch_'.$data['key'];

        if (!empty($data['country_iso2'])) {

            try {

                $this->appConfigInstance->create($data);
            }
            catch (\Exception $ex) {
                //throw exception
            }
        }

        return redirect($request->segment(1).'/config/architecture');
    }
}
