<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Land;
use App\Architecture;
use App\Building;
use DB;

class DashboardController extends Controller
{

    public function index(Land $land, Architecture $architecture, Building $building)
    {
        $landsDailyCount = $land
            ->whereYear('created_at', '=', date('Y'))
            ->whereMonth('created_at', '=', date('m'))
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->count();

        $landsMonthlyCount = $land
            ->whereYear('created_at', '=', date('Y'))
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->count();

        $landsAnnualCount = $land
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->count();

        $projectsDailyCount = $architecture
            ->whereYear('created_at', '=', date('Y'))
            ->whereMonth('created_at', '=', date('m'))
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->count();

        $projectsMonthlyCount = $architecture
            ->whereYear('created_at', '=', date('Y'))
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->count();

        $projectsAnnualCount = $architecture
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->count();

        $buildingsDailyCount = $building
            ->whereYear('created_at', '=', date('Y'))
            ->whereMonth('created_at', '=', date('m'))
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->count();

        $buildingsMonthlyCount = $building
            ->whereYear('created_at', '=', date('Y'))
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->count();

        $buildingsAnnualCount = $building
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->count();

        return view('admin/dashboard', compact(
            'landsDailyCount',
            'landsMonthlyCount',
            'landsAnnualCount',
            'projectsDailyCount',
            'projectsMonthlyCount',
            'projectsAnnualCount',
            'buildingsDailyCount',
            'buildingsMonthlyCount',
            'buildingsAnnualCount'
        ));
    }
    
    public function getStatistics(Request $request, Land $land, Architecture $architecture, Building $building)
    {
        $lands         = $land->getLandsStatistics($request->period);
        $landsData     = $this->prepareStatisticsArray($lands, $request->period);

        $projects      = $architecture->getArchitectureStatistics($request->period);
        $projectsData  = $this->prepareStatisticsArray($projects, $request->period);

        $buildings     = $building->getBuildingsStatistics($request->period);
        $buildingsData = $this->prepareStatisticsArray($buildings, $request->period);

        return new JsonResponse(compact('landsData', 'projectsData', 'buildingsData'), 200);
    }

    private function prepareStatisticsArray($modelData, $period)
    {
        $modelArray  = [];
        $resultArray = [];

        foreach ($modelData as $row) {

            $modelArray[$row->date] = $row->count;
        }

        if ($period != 'Y') {

            if ($period == 'd') {

                $countLabels = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

            } else {

                $countLabels = count(config('custom.months'));
            }

            for ($i = 1; $i <= $countLabels; $i++) {

                if (array_key_exists($i, $modelArray)) {
                    $resultArray[$i] = $modelArray[$i];
                } else {
                    $resultArray[$i] = 0;
                }
            }
        }

        return !empty($resultArray)?$resultArray:$modelArray;
    }
}
