<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Order;
use App\OrderStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with(['status', 'lands', 'architecture', 'buildings'])
            ->where('is_deleted', 0)
            ->orderBy('created_at', 'DESC')
            ->get();

        $statuses = OrderStatus::all();

        return view('orders.index', compact('orders', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $order = Order::find($id);
            $order->is_deleted = 1;

            if ($order->save()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }

    public function change_order_status(Request $request)
    {
        if ($request->ajax()) {

            $order = Order::find($request->order_id);

            $order->status_id = $request->status_id;

            if ($order->save()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }
}
