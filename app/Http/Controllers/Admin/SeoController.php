<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SeoTemplate;
use App\Page;
use App\Land;

class SeoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = SeoTemplate::where('is_deleted', 0)
            ->where('is_active', 1)
            ->get();

        return view('admin.seo.templates.index', compact('templates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $template = new SeoTemplate();
        $template = $template->create();

        return redirect('admin/seo/'.$template->id.'/edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pages = Page::all();
        $template =
            SeoTemplate::with(
                array(
                    'page' => function($query) {
                        $query->select('id','title');
                    })
                )
                ->findOrFail($id);

        return view('admin.seo.templates.update', compact('template', 'pages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $template = SeoTemplate::findOrFail($id);
        $template->name = $request['name'];
        $template->meta_title = $request['meta_title'];
        $template->meta_key = $request['meta_key'];
        $template->meta_description = $request['meta_description'];

        $template->update($request->all());

        return redirect('/admin/seo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
