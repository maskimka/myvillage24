<?php

namespace App\Http\Controllers;

use App\Architecture;
use App\ArchitectureTranslation;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\AacKit;
use App\HouseZone;
use App\ArchitectureFloor;
use App\SaleStatus;
use App\Land;
use Sentinel;
use App\BuildingType;
use Mockery\CountValidator\Exception;
use DB;

class ArchitectureController extends Controller
{
    protected $architectureInstance;

    public function __construct(Architecture $architecture)
    {
        $this->architectureInstance = $architecture;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Sentinel::getUser();

        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');

        if(
            $user->inRole('admin') ||
            $user->inRole('manager') ||
            $user->inRole('moderator')
        ) {

            $architecture = $this->architectureInstance->getArchitectureByLocale($locale);
        } else {

            $architecture = $this->architectureInstance->getArchitectureByLocale($locale, $user->id);
        }

        $statuses = SaleStatus::all();

        return view('architecture.index', compact('architecture', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');
        $adminLocale = session('admin_locale', config('app.admin_locale'));

        $architecture = $this->architectureInstance->create();

        ArchitectureTranslation::updateOrCreate(
            [
                'architecture_id' => $architecture->id,
                'locale'  => $locale,
            ],
            [
                'architecture_id' => $architecture->id,
                'locale'  => $locale,
            ]
        );

        $floor = new ArchitectureFloor();
        $floor['architecture_id'] = $architecture->id;
        $floor['number'] = 1;
        $floor->save();

        return redirect($request->segment(1).'/'.$adminLocale.'/architecture/'.$architecture->id.'/edit?lang='.$locale);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');

        $architecture = $this->architectureInstance->find($id);

        $floors = ArchitectureFloor::all()->where('architecture_id', $architecture->id);
        $acc_selected = [];
        foreach($architecture->aac_kit()->get() as $selectedItem) {

            $acc_selected[] = $selectedItem->id;
        }

        if ($architecture->translations()->where('locale', $locale)->count() == 0) {

            ArchitectureTranslation::create([
                'architecture_id' => $architecture->id,
                'locale'  => $locale,
            ]);
        }

        $architecture['translation'] = collect($architecture->translations->reduce(function ($translateCollection, $item) {

            $translateCollection[$item['locale']] = $item;

            return $translateCollection;
        }, []));

        return view('architecture.update',[
            'architecture' => $architecture,
            'floors' => $floors,
            'aac_kit' => AacKit::all()->sortBy('name'),
            'acc_selected' => $acc_selected,
            'house_zone' => HouseZone::all()->sortBy('name'),
            'building_types' => BuildingType::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $architecture = $this->architectureInstance->findOrFail($id);
            $architecture->aac_kit()->sync($request->get('aac_kit',[]));

            // image upload
            $files = $request->file('images');
            $architecture->multipleFileUpload($files,
                config('custom.ARCHITECTURE_IMAGES_PATH'),
                config('custom.RESIZE_THUMB_WIDTH'),
                config('custom.RESIZE_THUMB_HEIGHT'),
                $request['fileuploader-list-images']);

            $general_plan_image = $request->file('general_plan');

            $architecture->general_plan =
                $architecture->oneFileFitUpload($general_plan_image,
                    config('custom.ARCHITECTURE_IMAGES_PATH'),
                    config('custom.RESIZE_THUMB_WIDTH'),
                    config('custom.RESIZE_THUMB_HEIGHT'),
                    $request['fileuploader-list-general_plan'],
                    $architecture->general_plan);

            $house_section_image = $request->file('house_section');

            $architecture->house_section =
                $architecture->oneFileFitUpload($house_section_image,
                    config('custom.ARCHITECTURE_IMAGES_PATH'),
                    config('custom.RESIZE_THUMB_WIDTH'),
                    config('custom.RESIZE_THUMB_HEIGHT'),
                    $request['fileuploader-list-house_section'],
                    $architecture->house_section);

            $architecture->translations()->updateOrCreate(
                array('architecture_id' => $architecture->id, 'locale' => $request->data_lang),
                [
                    'architecture_id' => $architecture->id,
                    'locale' => $request->data_lang,
                    'exterior_walls' => $request->exterior_walls,
                    'overlappings' => $request->overlappings,
                    'roof' => $request->roof,
                    'boiler' => $request->boiler,
                    'description' => $request->description
                ]
            );

            if($architecture->update($request->all())) {
                DB::commit();
            }
        }
        catch(Exception $e) {
            DB::rollback();
            throw new Exception(500);
        }

        return redirect($request->segment(1).'/architecture?lang='.$request->data_lang);
    }

    public function save_galleries(Request $request, $id)
    {
        $architecture = Architecture::find($id);

        if (isset($request['fileuploader-list-facade_gallery'])) {
            $files = $request->file('facade_gallery');
            $architecture->multipleFileUpload($files,
                config('custom.ARCHITECTURE_IMAGES_PATH'),
                config('custom.RESIZE_THUMB_WIDTH'),
                config('custom.RESIZE_THUMB_HEIGHT'),
                $request['fileuploader-list-facade_gallery'],
                'facadeGallery');
        }

        if (isset($request['fileuploader-list-interior_gallery'])) {

            $files = $request->file('interior_gallery');
            $architecture->multipleFileUpload($files,
                config('custom.ARCHITECTURE_IMAGES_PATH'),
                config('custom.RESIZE_THUMB_WIDTH'),
                config('custom.RESIZE_THUMB_HEIGHT'),
                $request['fileuploader-list-interior_gallery'],
                'interiorGallery');
        }

        return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $architecture = $this->architectureInstance->find($id);
            $architecture->is_deleted = 1;

            if ($architecture->save()) {
                
                return new JsonResponse(null, 200);
            } else {
                
                return new JsonResponse(null, 500);
            }
        }
    }

    public function activate(Request $request)
    {
        if ($request->ajax()) {

            $architecture = $this->architectureInstance->find($request->architecture_id);

            $architecture->is_active = $request->activate;

            if ($architecture->save()) {

                return new JsonResponse(null, 200);
            } else {

                return new JsonResponse(null, 500);
            }
        }
    }

    public function attach_land(Request $request) {

        $land = Land::where('article', strtoupper($request->article))->get();

        $architecture = $this->architectureInstance->find($request->id);

        $existing_lands = [];

        foreach($architecture->lands()->get() as $item) {

            $existing_lands[] = $item->id;
        }

        if (!in_array($land[0]->id, $existing_lands)) {

            $architecture->lands()->attach($land[0]->id);
        } else {

            return new JsonResponse(null, 501);
        }

        return new JsonResponse(['result'=>$land[0]], 200);
    }

    public function detach_land(Request $request) {

        $architecture = $this->architectureInstance->find($request->id);

        $architecture->lands()->detach($request->land_id);

        return new JsonResponse(['status'=>'OK'], 200);
    }
}
