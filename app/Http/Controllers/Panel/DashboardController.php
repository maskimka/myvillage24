<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;

class DashboardController extends Controller
{
    public function index()
    {
        return view('panel/dashboard');
    }
}
