<?php

namespace App\Http\Controllers;

use App\Building;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\AacKit;
use App\HouseZone;
use App\ArchitectureFloor;
use App\SaleStatus;
use App\Land;
use Sentinel;
use DB;
use App\BuildingTranslation;

class BuildingController extends Controller
{
    protected $buildingInstance;

    public function __construct(Building $building)
    {
        $this->buildingInstance = $building;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Sentinel::getUser();
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');
        if ($user->inRole('admin')
            || $user->inRole('manager')
            || $user->inRole('moderator')
        ) {
            $building = $this->buildingInstance->getBuildingsByLocale($locale);
        } else {
            $building = $this->buildingInstance->getBuildingsByLocale($locale, $user->id);
        }

        $statuses = SaleStatus::all();

        return view('building.index', compact('building', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');
        $adminLocale = session('admin_locale', config('app.admin_locale'));

        $building = $this->buildingInstance->create();
        BuildingTranslation::updateOrCreate([
                'building_id' => $building->id,
                'locale'  => $locale,
            ],[
                'building_id' => $building->id,
                'locale'  => $locale,
            ]
        );

        $floor = new ArchitectureFloor();
        $floor['building_id'] = $building->id;
        $floor['number'] = 1;
        $floor->save();

        return redirect($request->segment(1).'/'.$adminLocale.'/building/'.$building->id.'/edit?lang='.$locale);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locale = (request()->lang != null)?request()->lang:config('app.fallback_locale');

        $building = $this->buildingInstance->findOrFail($id);
        $floors = ArchitectureFloor::all()->where('building_id', $building->id);
        $acc_selected = [];

        foreach($building->aac_kit()->get() as $selectedItem) {
            $acc_selected[] = $selectedItem->id;
        }

        if ($building->translations()->where('locale', $locale)->count() == 0) {
            BuildingTranslation::create([
                'building_id' => $building->id,
                'locale'  => $locale,
            ]);
        }

        $building['translation'] = collect($building->translations->reduce(function ($translateCollection, $item) {
            $translateCollection[$item['locale']] = $item;
            return $translateCollection;
        }, []));

        return view('building.update', [
            'building' => $building,
            'floors' => $floors,
            'aac_kit' => AacKit::all()->sortBy('name'),
            'acc_selected' => $acc_selected,
            'house_zone' => HouseZone::all()->sortBy('name'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $building = $this->buildingInstance->find($id);
            $building->aac_kit()->sync($request->get('aac_kit',[]));

            // image upload
            $files = $request->file('images');
            $building->multipleFileUpload($files,
                config('custom.BUILDING_IMAGES_PATH'),
                config('custom.RESIZE_THUMB_WIDTH'),
                config('custom.RESIZE_THUMB_HEIGHT'),
                $request['fileuploader-list-images']);

            $general_plan_image = $request->file('general_plan');

            $building->general_plan =
                $building->oneFileFitUpload($general_plan_image,
                    config('custom.BUILDING_IMAGES_PATH'),
                    config('custom.RESIZE_THUMB_WIDTH'),
                    config('custom.RESIZE_THUMB_HEIGHT'),
                    $request['fileuploader-list-general_plan'],
                    $building->general_plan);

            $house_section_image = $request->file('house_section');

            $building->house_section =
                $building->oneFileFitUpload($house_section_image,
                    config('custom.BUILDING_IMAGES_PATH'),
                    config('custom.RESIZE_THUMB_WIDTH'),
                    config('custom.RESIZE_THUMB_HEIGHT'),
                    $request['fileuploader-list-house_section'],
                    $building->house_section);

            $building->translations()->updateOrCreate(
                array('building_id' => $building->id, 'locale' => $request->data_lang),
                [
                    'building_id' => $building->id,
                    'locale' => $request->data_lang,
                    'exterior_walls' => $request->exterior_walls,
                    'overlappings' => $request->overlappings,
                    'roof' => $request->roof,
                    'boiler' => $request->boiler,
                    'facade' => $request->facade,
                    'windows' => $request->windows,
                    'reparation' => $request->reparation,
                    'description' => $request->description,
                    'title' => $request->title
                ]
            );

            if($building->update($request->all())) {
                DB::commit();
            }
        }
        catch(Exception $e) {
            DB::rollback();
            throw new Exception(500);
        }

        return redirect($request->segment(1).'/building?lang='.$request->data_lang);
    }

    public function save_galleries(Request $request, $id)
    {
        $building = $this->buildingInstance->find($id);

        if (isset($request['fileuploader-list-facade_gallery'])) {
            $files = $request->file('facade_gallery');
            $building->multipleFileUpload($files,
                config('custom.BUILDING_IMAGES_PATH'),
                config('custom.RESIZE_THUMB_WIDTH'),
                config('custom.RESIZE_THUMB_HEIGHT'),
                $request['fileuploader-list-facade_gallery'],
                'facadeBuildingGallery');
        }

        if (isset($request['fileuploader-list-interior_gallery'])) {

            $files = $request->file('interior_gallery');
            $building->multipleFileUpload($files,
                config('custom.BUILDING_IMAGES_PATH'),
                config('custom.RESIZE_THUMB_WIDTH'),
                config('custom.RESIZE_THUMB_HEIGHT'),
                $request['fileuploader-list-interior_gallery'],
                'interiorBuildingGallery');
        } 
        
        if (isset($request['fileuploader-list-underconstruct_gallery'])) {

            $files = $request->file('underconstruct_gallery');
            $building->multipleFileUpload($files,
                config('custom.BUILDING_IMAGES_PATH'),
                config('custom.RESIZE_THUMB_WIDTH'),
                config('custom.RESIZE_THUMB_HEIGHT'),
                $request['fileuploader-list-underconstruct_gallery'],
                'underconstructBuildingGallery');
        }

        return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $building = $this->buildingInstance->find($id);
            $building->is_deleted = 1;

            if ($building->save()) {
                
                return new JsonResponse(null, 200);
            } else {
                
                return new JsonResponse(null, 500);
            }
        }
    }

    public function activate(Request $request)
    {
        if ($request->ajax()) {

            $building = Building::find($request->building_id);

            $building->is_active = $request->activate;

            if ($building->save()) {

                return new JsonResponse(null, 200);
            } else {

                return new JsonResponse(null, 500);
            }
        }
    }

    public function attach_land(Request $request)
    {
        $land = Land::where('article', strtoupper($request->article))->get();
        $building = Building::find($request->id);

        $existing_lands = [];
        foreach ($building->lands()->get() as $item) {
            $existing_lands[] = $item->id;
        }

        if (!in_array($land[0]->id, $existing_lands)) {
            $building->lands()->attach($land[0]->id);
        } else {
            return new JsonResponse(null, 501);
        }

        return new JsonResponse(['result'=>$land[0]], 200);
    }

    public function detach_land(Request $request) {

        $building = Building::find($request->id);

        $building->lands()->detach($request->land_id);

        return new JsonResponse(['status'=>'OK'], 200);
    }
}
