<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App;
use Lang;


class LanguageController extends Controller
{
    /**
     * @desc To change the current language
     * @request Ajax
     */
    public function changeLanguage(Request $request)
    {
        if ($request->ajax()) {
            session()->set('locale', $request['locale']);
            App::setLocale(session()->get('locale'));
        }
    }
}
