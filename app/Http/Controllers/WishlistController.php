<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Sentinel::getUser();
        $wishlist = $user->wishlist()->get();

        foreach ($wishlist as $key => $item) {

            $type = $item->product_type;

            $wishlist[$key]['products'] = $item->{$type}()->with('images')->first();
        }

        return view('wishlist.index', compact('wishlist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = Sentinel::getUser();
        try
        {
            $wishlistIds = $user->wishlist()
                ->where([
                    'product_id'   => $request->id,
                    'product_type' => $request->type,
                ])
                ->pluck('id')->toArray();

            $user->wishlist()
                ->where([
                    'product_id'   => $request->id,
                    'product_type' => $request->type,
                ])
                ->delete();

            DB::table('wishlistables')
                ->where('wishlistables_type', get_class($user))
                ->where('wishlistables_id', $user->id)
                ->whereIn('wishlist_id', $wishlistIds)
                ->delete();

        }
        catch(Exception $ex)
        {
            return new JsonResponse($ex, 500);
        }

        return new JsonResponse(null, 200);
    }
}
