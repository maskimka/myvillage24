<?php

namespace App\Http\Controllers;

use App\Land;
use App\OrderStatus;
use App\PtoRequest;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PtoRequestController extends Controller
{
    private $ptoRequestInstance;

    public function __construct(PTORequest $ptoRequest)
    {
        $this->ptoRequestInstance = $ptoRequest;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = $this->ptoRequestInstance
            ->with(['status', 'lands', 'architecture', 'buildings'])
            ->where('is_deleted', 0)
            ->orderBy('created_at', 'DESC')
            ->get();

        $statuses = OrderStatus::all();


        return view('pto_request.index', compact('requests', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->except(['_token', '_method', 'models']);

        $requestData['user_id'] = Sentinel::getUser()->id;

        $requestModel =  $this->ptoRequestInstance->create($requestData);

        foreach($request->models as $model => $modelIds) {

            $requestModel->{$model}()->sync($modelIds);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $order = $this->ptoRequestInstance->find($id);
            $order->is_deleted = 1;

            if ($order->save()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }

    public function changePtoRequestStatus(Request $request)
    {
        if ($request->ajax()) {

            $ptoRequest = $this->ptoRequestInstance->find($request->pto_request_id);

            $ptoRequest->status_id = $request->status_id;

            if ($ptoRequest->save()) {
                return new JsonResponse(null, 200);
            } else {
                return new JsonResponse(null, 500);
            }
        }
    }
}
