<?php

use App\SeoTemplate;
use App\Page;
use App\Land;
use App\Architecture;

class MetaHelper
{
    private $page = null;
    private $template = null;
    public static $has_template = true;

    protected $variables = [
        'Name' => '',
        'Price' => ''
    ];

    public function __construct($controller, $action, $obj=null)
    {
        $this->page = Page::where('controller', $controller)
            ->where('action', $action)
            ->first();

        if (isset($this->page->id)) {
            $this->template = SeoTemplate::where('page_id', $this->page->id)
                ->where('is_active', 1)
                ->where('is_deleted', 0)
                ->first();
        }

        if (!$this->template) {
            self::$has_template = false;
        }

        if ($obj instanceof Land)
        {
//            $region = ($obj->regions)?$obj->region:'';
//            $city = ($obj->cities)?', '.$obj->city:'';
//            $district = ($obj->district)?', '.$obj->districts->name:'';
//            $street = ($obj->street)?', '.$obj->street:'';

            $region = '';
            $city = '';
            $district = '';
            $street = '';

            $this->variables['Name'] = $region . $city . $district . $street;
            $this->variables['Price'] = $obj->price;
        }

        if ($obj instanceof Architecture)
        {
            $this->variables['Name'] = $obj->title;
            $this->variables['Price'] = $obj->building_price * $obj->general_area;
        }

    }

    public function getTitle()
    {
        $this->template->meta_title = str_replace('%%Name%%', $this->variables['Name'], $this->template->meta_title);
        $this->template->meta_title = str_replace('%%Price%%', $this->variables['Price'], $this->template->meta_title);

        return $this->template->meta_title;
    }

    public function getDescription()
    {
        $this->template->meta_description = str_replace('%%Name%%', $this->variables['Name'], $this->template->meta_description);
        $this->template->meta_description = str_replace('%%Price%%', $this->variables['Price'], $this->template->meta_description);

        return $this->template->meta_description;
    }

    public function getKeyWords()
    {
        return $this->template->meta_key;
    }
    
}