<?php

    function isActiveRoute($route, $output = 'active')
    {
        if (Request::capture()->getRequestUri() === $route) {
            return $output;
        }
    }
