<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Session;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Notifications\WelcomeUser;

class PanelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = Sentinel::check()) {

            if (!$user->is_deleted) {

                if (!Sentinel::hasAccess($request->getRequestUri())) {

                    throw new HttpException(403);
                }
            } else {

                Sentinel::logout();
                return redirect('/auth/login')->with(['error' => 'Ваша учетная запись не активна. Обратитесь пожалуйста к администратору.']);
            }

            if (!count($this->getUnreadNotificationsByType('App\Notifications\WelcomeUser'))) {

                $user->notify(new WelcomeUser());
            }
        } else {

            return redirect('/auth/login');
        }

        return $next($request);
    }

    private function getUnreadNotificationsByType($type)
    {
        return Sentinel::getUser()->unreadNotifications
            ->whereIn('type', $type);
    }
}
