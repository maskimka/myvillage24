<?php

namespace App\Http\Middleware;

use Closure;
use App\Visitor;
use Cookie;

class VisitorMiddleware
{
    protected $clientHashKey;
    protected $client;
    protected $request;
    protected $cookie;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->request = $request;

        if (!$this->hasCookie('__mv_cid')) {
            $this->setVisitor();
        }

        return $next($request);
    }

    protected function identifyVisitor()
    {
        $this->client = Visitor::where('client_key', $this->clientHashKey)->first();

        return $this->client?:false;
    }

    protected function setVisitor()
    {
        $this->clientHashKey = sha1($this->generateUUID());
        $visitor = new Visitor();
        $visitor->client_key = $this->clientHashKey;
        $visitor->client_ip  = $this->request->server('HTTP_X_REAL_IP');
        $visitor->user_agent = $this->request->server('HTTP_USER_AGENT');

        if ($visitor->save()) {
            $this->markVisitor();

            return true;
        }

        return false;
    }

    protected function hasCookie($cookie_name)
    {
        $cookie_exist = Cookie::get($cookie_name);

        return ($cookie_exist) ? true : false;
    }

    protected function markVisitor()
    {
        Cookie::queue(Cookie::forever('__mv_cid', $this->clientHashKey, '/', env('APP_DOMAIN')));
    }

    protected function generateUUID()
    {
        return uniqid();
    }
}
