<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use App;
use Config;
use Cookie;

class AdminLanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->segment(2);

        if (array_key_exists($locale, (array)config('locales')) && config('locales.'.$locale)) {
            session(['admin_locale' => $locale]);
        } else {
            $locale = session('admin_locale', config('app.admin_locale'));

            if($request->method() === 'GET') {

                return redirect($this->getLocalizedUrl($request, $locale));
            }
        }

        return $next($request);
    }

    public function getLocalizedUrl(Request &$request, $locale)
    {
        $segments = $request->segments();
        array_splice($segments, 1, 0, $locale);
        return '/'.implode('/', $segments);
    }
}
