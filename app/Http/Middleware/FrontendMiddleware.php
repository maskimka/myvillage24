<?php

namespace App\Http\Middleware;

use Closure;
use App;
use App\Visitor;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Cookie;
use Shorty;

class FrontendMiddleware
{
    protected $request;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->request = $request;

        $wishlistCountItems = '';
        $visitor = $this->getUserOrVisitor();

        if ($visitor) {

            $wishlistCountItems = $visitor->wishlist()->count();
        }

        if ($request->isMethod('GET')) {
            $request['wishlistCountItems'] = $wishlistCountItems;

            if ($request->query('country')){
                $request['clientCountry'] = $this->markClientCountry($request->query('country'));
                return redirect($request->getPathInfo(), 301);
            } elseif($clientCountry = $this->getClientCountry()) {
                $request['clientCountry'] = $this->markClientCountry($clientCountry);
            }
        }

        return $next($request);
    }

    public function getUserOrVisitor()
    {
        $user = Sentinel::getUser();

        if (!$user) {

            $cookie = Cookie::get('__mv_cid', null);

            if($cookie) {

                $user = Visitor::where('client_key', $cookie)->first();
            }
        }
        return $user;
    }

    protected function getClientCountry()
    {
        $countryIso2Code = Cookie::get('__mv_client_country', null);
        if (! $countryIso2Code) {
            $countryInformation = geoip()->getLocation($this->request->server('HTTP_X_REAL_IP'));
            $countryIso2Code = $countryInformation->iso_code;
            $this->markClientCountry($countryIso2Code);
        }

        return $countryIso2Code;
    }

    protected function markClientCountry($countryIso2Code)
    {
        Cookie::queue(Cookie::forever('__mv_client_country', $countryIso2Code, '/', env('APP_DOMAIN')));

        return $countryIso2Code;
    }
}
