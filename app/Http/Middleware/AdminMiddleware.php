<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AdminMiddleware
{
    protected $accessAllowRoles = ['admin'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = Sentinel::check()) {

            if (!$user->is_deleted) {

                if (!Sentinel::hasAccess($request->getRequestUri())) {

                    throw new HttpException(403);
                }
            } else {

                Sentinel::logout();
                return redirect('/auth/login')->with(['error' => 'Ваша учетная запись не активна. Обратитесь пожалуйста к администратору.']);
            }

        } else {

            return redirect('/auth/login');
        }

        return $next($request);
    }
}
