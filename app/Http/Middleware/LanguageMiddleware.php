<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use Config;
use Illuminate\Support\Facades\Cookie;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->segment(1);

        if (array_key_exists($locale, config('locales')) && config('locales.'.$locale)) {

            App::setLocale($locale);
            session(['locale' => $locale]);

        } else {
            if (!$clientCountryCode = $this->getClientCountry($request)) {
                $clientCountryCode = config('app.locale');
            }

            $locale = session('locale', $clientCountryCode);
            App::setLocale($locale);
            return redirect($locale.$request->getPathInfo());
        }

        return $next($request);
    }

    protected function getClientCountry($request)
    {
        $locale = Cookie::get('__mv_client_country', null);
        $countryLocaleMap = config('country_locale_map');
        $localeList = config('locales');
        if (!$locale) {
            $countryInformation = geoip()->getLocation($request->server('HTTP_X_REAL_IP'));
            $locale = key_exists($countryInformation->iso_code, $countryLocaleMap)?$countryLocaleMap[$countryInformation->iso_code]:'';
        }

        return key_exists($locale, $localeList)?$locale:null;
    }
}
