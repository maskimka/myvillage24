<?php
/**
 * Created by PhpStorm.
 * User: alexk
 * Date: 5/19/17
 * Time: 2:38 PM
 */

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use View as ViewFacade;
use App\ArchitectureFloor;
use App\HouseZone;

class FloorComposer
{
    public function compose(View $view)
    {
        $architecture_id = app('request')->architecture;

        $floors= ArchitectureFloor::all()->where('architecture_id', $architecture_id);

        $template = ViewFacade::make('floors.floor')->with([
            'floors' => $floors,
            'house_zone' => HouseZone::all(),
        ]);

        return $view->with('', $template);
    }

}