<?php

namespace App\Http\ViewComposers;

use Sentinel;
use Illuminate\View\View;
use App\Mask;
use App\Country;

class AppComposer
{
    public function compose(View $view)
    {
        if (!app('request')->ajax()) {

            $user = Sentinel::getUser();
            $controller = null;
            $action = '404';

            if (app('request')->route()) {

                $action = app('request')->route()->getAction();
            }

            if (isset($action['controller'])) {

                $controller = class_basename($action['controller']);
            }

            $namespace = app('request')->segment(1);

            if ($controller) {

                list($controller, $action) = explode('@', $controller);
            }

            if (!$maskCollection = session()->get('mask_collection')) {
                $maskCollection = Mask::all();
                session()->set('mask_collection', $maskCollection);
            }

            $masks = $maskCollection->keyBy('country_id');

            if (!$countryCollection = session()->get('country_collection')) {
                $countryCollection = Country::all();
                session()->set('country_collection', $countryCollection);
            }

            $countries = $countryCollection->keyBy('iso2code');

            foreach ($countries as $key => $country) {
                $countries[$key]['center'] = [
                    'lat' => (float)$country->latitude,
                    'lng' => (float)$country->longitude,
                ];
                $countries[$key]['zoom'] = (int)$country->map_zoom;
            }

            $currentCountry = strtoupper((new Country)->getUserCountryIsoCode(app('request')));

            if ($user && !$user->country) {
                $user->setCountry(app('request'), $countries);
            }

            $view->with(compact(
                'controller',
                'action',
                'user',
                'namespace',
                'masks',
                'countries',
                'currentCountry'
            ));

        }
    }
}