<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class TranslationComposer
{
    public function compose(View $view)
    {
        $adminLang = session('admin_locale') ?: config('app.admin_locale');

        $view->with(compact(
            'adminLang'
        ));
    }

}