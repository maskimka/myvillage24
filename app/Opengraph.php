<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opengraph extends Model
{
    protected $table = "meta_opengraph";
}
