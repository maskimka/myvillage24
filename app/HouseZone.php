<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseZone extends Model
{
    protected $table = "house_zone";
}
