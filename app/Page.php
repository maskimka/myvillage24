<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = [
        "id",
        "op_title",
        "op_description",
        "op_url",
    ];

    public function opengraphs()
    {
        return $this->morphToMany('App\Opengraph', 'opengraphables');
    }
}
