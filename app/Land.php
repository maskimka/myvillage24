<?php

namespace App;

use App\Traits\Imageable;
use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
use Sentinel;
use DB;

class Land extends Model
{
    use Imageable, Translatable, SearchableTrait;

    protected $table = 'land';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'communications',
        'infrastructure',
        'transport',
        'images',
        'imageCroppedCoords',
        'imageSizeMapCroppedCoords',
        'imageCadastralMapCroppedCoords',
        'imageSizeMap',
        'imageCadastralMap',
        'fileuploader-list-images',
        'fileuploader-list-imageSizeMap',
        'fileuploader-list-imageCadastralMap',
        'parties',
        'is_active',
        'is_deleted',
        'terms',
        'data_lang',
        'description',
        'street',
        'street_number',
        'sublocality',
        'locality',
        'administrative',
        'country',
    ];

    protected $searchable = [
        'columns' => [
            'land_translations.street' => 10,
            'land_translations.sublocality' => 8,
            'land_translations.locality' => 6,
            'land_translations.administrative' => 4,
            'land_translations.country_code' => 4,
        ],
        'joins' => [
            'land_translations' => ['land.id','land_translations.land_id'],
        ],
        'groupBy' => ['land.id']
    ];

    public function translations()
    {
        return $this->hasMany('App\LandTranslation');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function communications()
    {
        return $this->belongsToMany('App\Communication', 'land_communications');
    }

    public function infrastructure()
    {
        return $this->belongsToMany('App\Infrastructure', 'land_infrastructure');
    }

    public function transport()
    {
        return $this->belongsToMany('App\Transport', 'land_transport');
    }

    public function images()
    {
        return $this->morphToMany('App\Image', 'imageables');
    }

    public function orders()
    {
        return $this->morphToMany('App\Order', 'orderables');
    }

    public function pto_requests()
    {
        return $this->morphToMany('App\PtoRequest', 'pto_requestables');
    }

    public function opengraphs()
    {
        return $this->morphToMany('App\Opengraph', 'opengraphables');
    }

    public function purpose()
    {
        return $this->belongsTo('App\Purpose');
    }

    public function sale_status()
    {
        return $this->belongsTo('App\SaleStatus', 'sale_status_id');
    }

    public function land_size_map()
    {
        return $this->hasMany('App\LandSizeMap');
    }

    public function buildings()
    {
        return $this->morphedByMany('App\Building', 'landables');
    }

    public function villages()
    {
        return $this->morphedByMany('App\Building', 'landables');
    }

    public function wishlist()
    {
        return $this->hasMany('App\Wishlist', 'product_id')
            ->where('product_type', 'Land');
    }

    public function getRelatedArchitecture() {

        $architecture =
            Architecture::with(['translations','images'])
                ->where(function($query) {
                $query->where('width', '<=', $this->rectangle_width - 3)
                    ->where('length', '<=', $this->rectangle_length - 3)
                    ->where(['is_deleted' => 0,'is_active' => 1]);
                })->orWhere(function($query) {
                $query->where('width', '<=', $this->rectangle_length - 3)
                    ->where('length', '<=', $this->rectangle_width - 3)
                    ->where(['is_deleted' => 0,'is_active' => 1]);
                })->latest()->get();

        $this->translate($architecture);

        return $architecture;
    }

    public function getLandList($userId = null, $relatedLandsIdList = null, $request, $isFrontend = false, $justCount = false)
    {
        $queryA = $this
            ->with([
                'translations',
                'images',
                'sale_status',
                'buildings',
                'buildings.images',
                'purpose',
            ])
            ->search($request['requested_country'].' '.$this->getUserCountryIsoCode($request))
            ->where(function($query ) use ($userId, $relatedLandsIdList, $request, $isFrontend) {
                if ($userId) {
                    $query->where('user_id', $userId);
                }

                if (count($relatedLandsIdList) > 0) {
                    $query->whereIn('id', $relatedLandsIdList);
                }

                if ($request->price_min) {
                    $query->where('price', '>=', $request->price_min);
                }

                if ($request->price_max) {
                    $query->where('price', '<=', $request->price_max);
                }

                if ($request->area_min) {
                    $query->where('area', '>=', $request->area_min);
                }

                if ($request->area_max) {
                    $query->where('area', '<=', $request->area_max);
                }

                if ($request->min_lat) {
                    $query->where('lat', '>=', $request->min_lat);
                }

                if ($request->max_lat) {
                    $query->where('lat', '<=', $request->max_lat);
                }

                if ($request->min_lng) {
                    $query->where('lng', '>=', $request->min_lng);
                }

                if ($request->max_lng) {
                    $query->where('lng', '<=', $request->max_lng);
                }

                if ($request->land_id) {
                    $query->where('id', $request->land_id);
                }

                if ($isFrontend) {
                    $query->where('is_active', 1);
                }

                $query->where('is_deleted', 0);

            });

        $queryB = $this->select(['*', 'id as relevance'])
            ->with([
                'translations',
                'images',
                'sale_status',
                'buildings',
                'buildings.images',
                'purpose',
            ])
            ->where(function($query ) use ($userId, $relatedLandsIdList, $request, $isFrontend) {
//                        if (!$request->country_code) {
                            $query->where('country_code', '!=' , $this->getUserCountryIsoCode($request));
//                        }

                        if ($userId) {
                            $query->where('user_id', $userId);
                        }

                        if (count($relatedLandsIdList) > 0) {
                            $query->whereIn('id', $relatedLandsIdList);
                        }

                        if ($request->price_min) {
                            $query->where('price', '>=', $request->price_min);
                        }

                        if ($request->price_max) {
                            $query->where('price', '<=', $request->price_max);
                        }

                        if ($request->area_min) {
                            $query->where('area', '>=', $request->area_min);
                        }

                        if ($request->area_max) {
                            $query->where('area', '<=', $request->area_max);
                        }

                        if ($request->min_lat) {
                            $query->where('lat', '>=', $request->min_lat);
                        }

                        if ($request->max_lat) {
                            $query->where('lat', '<=', $request->max_lat);
                        }

                        if ($request->min_lng) {
                            $query->where('lng', '>=', $request->min_lng);
                        }

                        if ($request->max_lng) {
                            $query->where('lng', '<=', $request->max_lng);
                        }

                        if ($request->land_id) {
                            $query->where('id', $request->land_id);
                        }

                        if ($isFrontend) {
                            $query->where('is_active', 1);
                        }

                        $query->where('is_deleted', 0);

                    });

        if (!$request->country_code) {
            $query = $queryA->union($queryB);
        } else {
            $query = $queryA;
        }

        if (isset($request['sortby']) && !empty($request['sortby'])) {
            $explodedSort = explode('.',$request['sortby']);
            $query->orderBy($explodedSort[0], strtoupper($explodedSort[1]));
        }


        $lands = $query
            ->offset(isset($request->offsetData)?$request->offsetData:0)
            ->limit(isset($request->limit)?$request->limit:20)
            ->get();
        $this->translate($lands);
        $this->prepareLandShortAddress($lands);

        return $lands;
    }

    public function getLandListCount($userId = null, $relatedLandsIdList = null, $request)
    {
        $queryA = $this
            ->with([
                'translations',
                'images',
                'sale_status',
                'buildings',
                'buildings.images',
                'purpose',
            ])
            ->search($request['requested_country'].' '.$this->getUserCountryIsoCode($request))
            ->where(function($query ) use ($userId, $relatedLandsIdList, $request) {
                if ($userId) {
                    $query->where('user_id', $userId);
                }

                if (count($relatedLandsIdList) > 0) {
                    $query->whereIn('id', $relatedLandsIdList);
                }

                if ($request->price_min) {
                    $query->where('price', '>=', $request->price_min);
                }

                if ($request->price_max) {
                    $query->where('price', '<=', $request->price_max);
                }

                if ($request->area_min) {
                    $query->where('area', '>=', $request->area_min);
                }

                if ($request->area_max) {
                    $query->where('area', '<=', $request->area_max);
                }

                if ($request->min_lat) {
                    $query->where('lat', '>=', $request->min_lat);
                }

                if ($request->max_lat) {
                    $query->where('lat', '<=', $request->max_lat);
                }

                if ($request->min_lng) {
                    $query->where('lng', '>=', $request->min_lng);
                }

                if ($request->max_lng) {
                    $query->where('lng', '<=', $request->max_lng);
                }

                if ($request->land_id) {
                    $query->where('id', $request->land_id);
                }

                $query->where('is_active', 1);

                $query->where('is_deleted', 0);

            });

        $queryB = $this->select(DB::raw('COUNT(*) as `aggregate`'))
            ->with([
                'translations',
                'images',
                'sale_status',
                'buildings',
                'buildings.images',
                'purpose',
            ])
            ->where(function($query ) use ($userId, $relatedLandsIdList, $request) {

                $query->where('country_code', '!=' , $this->getUserCountryIsoCode($request));

                if ($userId) {
                    $query->where('user_id', $userId);
                }

                if (count($relatedLandsIdList) > 0) {
                    $query->whereIn('id', $relatedLandsIdList);
                }

                if ($request->price_min) {
                    $query->where('price', '>=', $request->price_min);
                }

                if ($request->price_max) {
                    $query->where('price', '<=', $request->price_max);
                }

                if ($request->area_min) {
                    $query->where('area', '>=', $request->area_min);
                }

                if ($request->area_max) {
                    $query->where('area', '<=', $request->area_max);
                }

                if ($request->min_lat) {
                    $query->where('lat', '>=', $request->min_lat);
                }

                if ($request->max_lat) {
                    $query->where('lat', '<=', $request->max_lat);
                }

                if ($request->min_lng) {
                    $query->where('lng', '>=', $request->min_lng);
                }

                if ($request->max_lng) {
                    $query->where('lng', '<=', $request->max_lng);
                }

                if ($request->land_id) {
                    $query->where('id', $request->land_id);
                }

                $query->where('is_active', 1);

                $query->where('is_deleted', 0);

            });

        if (!$request->requested_country) {
            $landsCount = $queryA->count() + $queryB->count();
        } else {
            $landsCount = $queryA->count();
        }

        return $landsCount;
    }
//    public function getLandList($userId = null, $relatedLandsIdList = null, $request, $isFrontend = false, $justCount = false)
//    {
//        $query =
//            $this->search($request['requested_country'])
//                ->with([
//                    'translations',
//                    'images',
//                    'sale_status',
//                    'buildings',
//                    'buildings.images',
//                    'purpose',
//                ])
//                ->where(function($query ) use ($userId, $relatedLandsIdList, $request, $isFrontend) {
//                    if ($userId) {
//
//                        $query->where('user_id', $userId);
//                    }
//                    if (count($relatedLandsIdList) > 0) {
//
//                        $query->whereIn('id', $relatedLandsIdList);
//                    }
//                    if ($request->price_min) {
//
//                        $query->where('price', '>=', $request->price_min);
//                    }
//                    if ($request->price_max) {
//
//                        $query->where('price', '<=', $request->price_max);
//                    }
//                    if ($request->area_min) {
//
//                        $query->where('area', '>=', $request->area_min);
//                    }
//                    if ($request->area_max) {
//
//                        $query->where('area', '<=', $request->area_max);
//                    }
//                    if ($request->min_lat) {
//
//                        $query->where('lat', '>=', $request->min_lat);
//                    }
//                    if ($request->max_lat) {
//
//                        $query->where('lat', '<=', $request->max_lat);
//                    }
//                    if ($request->min_lng) {
//
//                        $query->where('lng', '>=', $request->min_lng);
//                    }
//                    if ($request->max_lng) {
//
//                        $query->where('lng', '<=', $request->max_lng);
//                    }
//                    if ($request->land_id) {
//
//                        $query->where('id', $request->land_id);
//                    }
//                    if ($isFrontend) {
//
//                        $query->where('is_active', 1);
//                    }
//
//                    $query->where('is_deleted', 0);
//                });
//
//        if ($isFrontend) {
//            if (isset($request['sortby']) && !empty($request['sortby'])) {
//                $explodedSort = explode('.',$request['sortby']);
//                $query->orderBy($explodedSort[0], strtoupper($explodedSort[1]));
//            } elseif(empty($request['requested_country'])) {
//                $query->orderBy('priority', 'DESC');
//            }
//        }
//// original code
////        $lands = $query
////            ->offset(isset($request->offsetData)?$request->offsetData:0)
////            ->limit(isset($request->limit)?$request->limit:20)
////            ->get();
////
////        $this->translate($lands);
////        $this->prepareLandShortAddress($lands);
//
//        if (!$justCount) {
//            $lands = $query
//                ->offset(isset($request->offsetData)?$request->offsetData:0)
//                ->limit(isset($request->limit)?$request->limit:20)
//                ->get();
//
//            $this->translate($lands);
//            $this->prepareLandShortAddress($lands);
//
//        } else {
//
//            $lands = $query->count();
//        }
//
//
//        return $lands;
//    }

    public function getLandsByLocale($locale, $userId = null)
    {
        $lands = self::select('land.*', 'land_translations.street', 'land_translations.description')
            ->join('land_translations', 'land_translations.land_id', '=', 'land.id')
            ->with(['images', 'sale_status', 'purpose', 'user', 'translations'])
            ->where('land_translations.locale', $locale)
            ->where(function($query) use ($userId) {
                if($userId != null) {
                    $query->where('user_id', $userId);
                }
            })
            ->where('is_deleted', 0)
            ->orderBy('created_at', 'DESC')
            ->get();

        $this->translate($lands);

        return $lands;
    }

    public function getLandsStatistics($period = 'd')
    {
        $groupDate = 'day(created_at) as date';

        if ($period == 'm') {
            $groupDate = 'month(created_at) as date';
        }

        if ($period == 'Y') {
            $groupDate = 'year(created_at) as date';
        }

        $query = $this->select(DB::raw('count(user_id) as count'), DB::raw($groupDate))
            ->where('user_id', Sentinel::getUser()->id)
            ->where(['is_deleted' => 0,'is_active' => 1])
            ->whereYear('created_at', '=', date('Y'));

        if ($period == "d") {
            $query->whereMonth('created_at', '=', date('m'));
        }

        $lands = $query
            ->groupBy('date')
            ->get();


        return $lands;
    }

    public function prepareLandLongAddress($landsCollection)
    {
        foreach ($landsCollection as $key => $land) {

            $address = '';

            if (!empty($land->country)) {

                $address .= $land->country;
            }
            if (!empty($land->administrative)) {

                $address .= ', '.$land->administrative;
            }
            if (!empty($land->locality)) {

                $address .= ', '.$land->locality;
            }
            if (!empty($land->sublocality)) {

                $address .= ', '.$land->sublocality;
            }
            if (!empty($land->street)) {

                $address .= ', '.$land->street;
            }
            if (!empty($land->street_number)) {

                $address .= ', '.$land->street_number;
            }

            $landsCollection[$key]['address'] = $address;
        }
    }

    public function prepareLandShortAddress($landsCollection)
    {
        foreach ($landsCollection as $key => $land) {

            $address = '';

            if (!empty($land->country)) {

                $address .= $land->country;
            }
            if (!empty($land->locality)) {

                $address .= ', '.$land->locality;
            }
            if (!empty($land->street)) {

                $address .= ', '.$land->street;
            }
            if (!empty($land->street_number)) {

                $address .= ', '.$land->street_number;
            }

            $landsCollection[$key]['address'] = $address;
        }
    }

    public function prepareLandLongAddressForShow($landsCollection)
    {
        foreach ($landsCollection as $key => $land) {

            $address = '';

            if (!empty($land->administrative)) {

                $address .= $land->administrative;
            }
            if (!empty($land->locality)) {

                $address .= ', ' . $land->locality;
            }
            if (!empty($land->sublocality)) {

                $address .= ', ' . $land->sublocality;
            }
            if (!empty($land->street)) {

                $address .= ', ' . $land->street;
            }
            if (!empty($land->street_number)) {

                $address .= ', ' . $land->street_number;
            }

            $landsCollection[$key]['address'] = $address;
        }
    }

    public function prepareLandAddressForCards($landsCollection)
    {
        foreach ($landsCollection as $key => $land) {

            $address = '';

            if (!empty($land->country)) {

                $address .= $land->country;
            }
            if (!empty($land->locality)) {

                $address .= ', '.$land->locality;
            }

            $landsCollection[$key]['address'] = $address;
        }
    }

    public function getFormInitialParams(Request $request)
    {
        $currentCountryCode = $this->getUserCountryIsoCode($request);
        $initParams = [];
        $initParams['priceRange']['maximalPrice'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->where('country_code', $currentCountryCode)
                ->max('price');

        $initParams['priceRange']['minimalPrice'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->where('country_code', $currentCountryCode)
                ->min('price');

        $initParams['areaRange']['maximalArea'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->where('country_code', $currentCountryCode)
                ->max('area');

        $initParams['areaRange']['minimalArea'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->where('country_code', $currentCountryCode)
                ->min('area');

        if ($request->get('sortby')) {
            $initParams['sortBy'] = $request->get('sortby');
        }

        $initParams['limit'] = $request->get('limit')?(int)$request->get('limit'):10;
        $initParams['page'] = $request->get('page')?(int)$request->get('page'):1;
        $initParams['requestedCountry']  = $request->get('requested_country')?:'';
        $initParams['priceRange']['min'] = $request->get('price_min')?:$initParams['priceRange']['minimalPrice'];
        $initParams['priceRange']['max'] = $request->get('price_max')?:$initParams['priceRange']['maximalPrice'];
        $initParams['areaRange']['min']  = $request->get('area_min')?:$initParams['areaRange']['minimalArea'];
        $initParams['areaRange']['max']  = $request->get('area_max')?:$initParams['areaRange']['maximalArea'];

        return $initParams;
    }

    public function hardDelete(HasMany $lands)
    {
        return $lands->delete();
    }

    private function getUserCountryIsoCode($request)
    {
        $countryIsoCode = config('country_iso_code');
        if (!empty($request->country_code)) {
            $countryIsoCode = $request->country_code;
        } else {
            $countryIsoCode = $request->cookie('__mv_client_country');
        }

        return strtolower($countryIsoCode);
    }
}
