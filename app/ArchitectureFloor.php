<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Imageable;

class ArchitectureFloor extends Model
{
    use Imageable;

    protected $table = 'floors';

    protected $guarded = [
        'id',
        'house_zone',
        'image',
        'fileuploader-list-image',
    ];

    public function images()
    {
        return $this->morphToMany('App\Image', 'imageables');
    }

    public function house_zone()
    {
        return $this->belongsToMany('App\HouseZone', 'floors_house_zone')->withPivot('house_zone_value');
    }

    public function architecture()
    {
        return $this->belongsTo('App\Architecture');
    }

    public function building()
    {
        return $this->belongsTo('App\Building');
    }

    public static function updateFloor($floor, $image = null)
    {
        $nFloor = self::find(intval($floor['id']));
        $nFloor['area'] = floatval($floor['area']);
        $nFloor['number'] = intval($floor['number']);

        $nFloor->save();

        $nFloor->house_zone()->sync([]);

        if (isset($floor['house_zone'])) {

            foreach($floor['house_zone'] as $key => $item) {

                foreach($item as $value) {

                    $tmp = array(
                        $key => array(
                            'house_zone_value' => floatval($value['house_zone_value'])
                        )
                    );

                    $nFloor->house_zone()->attach($tmp);

                }
            }
        }


        if ($image) {
            $imagesList[0] = $image;
        } else {
            $imagesList = null;
        }

        $nFloor->multipleFileUpload($imagesList,
            config('custom.FLOOR_IMAGES_PATH'),
            config('custom.RESIZE_THUMB_WIDTH'),
            config('custom.RESIZE_THUMB_HEIGHT'),
            $floor['fileuploader-list-image']
        );

        $nFloor->updateAutomaticallyArchitecture();

    }

    public static function createItems($floors, $architecture_id)
    {
        foreach($floors as $item) {

            $item['architecture_id'] = $architecture_id;

            $nFloor = self::updateOrCreate(
                [
                    'architecture_id'=>$item['architecture_id'],
                    'number'=>$item['number'],
                ],
                $item
            );

            if (isset($item['image'])) {

                $imagesList[0] = $item['image'];

                $nFloor->multipleFileUpload($imagesList, config('custom.ARCHITECTURE_IMAGES_PATH')."/floors", config('custom.RESIZE_ARCHITECTURE_IMAGE_FIT_W'), config('custom.RESIZE_ARCHITECTURE_IMAGE_FIT_H'));
            }

            $nFloor->house_zone()->sync((isset($item['house_zone']))?:[]);
        }

    }
    
    public static function removeFloor($request) {

        $floor = self::find($request['floor_id']);
        $floor['is_deleted'] = 1;
        $floor->save();

        $floors = self::all()
            ->where('architecture_id', $request['architecture_id'])
            ->where('is_deleted', 0);

        foreach($floors as $key => $item) {

              if ($item['number'] > 1) {

                  $floors[$key]['number'] -= 1;

                  $floors[$key]->save();
              }
        }

    }

    private function updateAutomaticallyArchitecture()
    {
        $generalArea     = 0;
        $livingArea      = 0;
        $roomsNumber     = 0;
        $bathroomsNumber = 0;
        $garagesNumber   = 0;
        $model  = ($this->architecture_id)?"architecture":"building";

        foreach ($this->{$model}->floors as $floor) {

            foreach ($floor->house_zone as $zone) {

                if  (in_array($zone->slug, config('custom.LIVING_ZONE'))) {

                    $livingArea += $zone->pivot->house_zone_value;
                }

                if  ($zone->slug == config('custom.ROOMS')) {

                    $roomsNumber++;
                }

                if  ($zone->slug == config('custom.BATHROOMS') || $zone->slug == config('custom.RESTROOM')) {

                    $bathroomsNumber++;
                }

                if  ($zone->slug == config('custom.GARAGES')) {

                    $garagesNumber++;
                }
            }
            $generalArea += $floor->area;
        }

        $this->{$model}->rooms_number     = $roomsNumber;
        $this->{$model}->bathrooms_number = $bathroomsNumber;
        $this->{$model}->garages_number   = $garagesNumber;
        $this->{$model}->floors_number    = $this->{$model}->floors()->count();
        $this->{$model}->living_area      = $livingArea;
        $this->{$model}->general_area     = $generalArea;

        $this->{$model}->save();
    }
}
