<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchitectureHistory extends Model
{
    protected $table = "architecture_history";
}
