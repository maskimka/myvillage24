<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandTranslation extends Model
{
    public $timestamps = false;

    protected $guarded = [];
}
