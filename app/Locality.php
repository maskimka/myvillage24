<?php

namespace App;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
    use Translatable;

    public function translations()
    {
        return $this->hasMany('App\LocalityTranslation');
    }
}
