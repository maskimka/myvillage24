<?php

namespace App;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Imageable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Sentinel;
use DB;

class Architecture extends Model
{
    use Imageable, Translatable;
    
    protected $table = "architecture";

    protected $guarded = [
        'id',
        'aac_kit',
        'images',
        'floors',
        'general_plan',
        'house_section',
        'facade_gallery',
        'interior_gallery',
        'fileuploader-list-images',
        'fileuploader-list-facade_gallery',
        'fileuploader-list-interior_gallery',
        'fileuploader-list-general_plan',
        'fileuploader-list-house_section',
        'description',
        'exterior_walls',
        'overlappings',
        'roof',
        'boiler',
        'data_lang',
    ];

    public function translations()
    {
        return $this->hasMany('App\ArchitectureTranslation');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function aac_kit()
    {
        return $this->belongsToMany('App\AacKit', 'architecture_acc_kit');
    }
    
    public function images()
    {
        return $this->morphToMany('App\Image', 'imageables');
    }

    public function orders()
    {
        return $this->morphToMany('App\Order', 'orderables');
    }

    public function lands()
    {
        return $this->morphToMany('App\Land', 'landables');
    }

    public function facadeGalleryImages() {

        return $this->belongsToMany('App\Image', 'imageables', 'imageables_id')
            ->wherePivot('imageables_type', 'facadeGallery');
    }

    public function interiorGalleryImages() {

        return $this->belongsToMany('App\Image', 'imageables', 'imageables_id')
            ->wherePivot('imageables_type','interiorGallery');
    }

    public function floors()
    {
        return $this->hasMany('App\ArchitectureFloor')->where('is_deleted', 0);
    }

    public function building_type()
    {
        return $this->belongsTo('App\BuildingType');
    }

    public function getRelatedLands($limit = null) {

        $mainQuery =
            Land::with(['sale_status', 'images'])
                ->where(function($query) {
                    $query->where('rectangle_width', '>=', $this->width + 3)
                        ->where('rectangle_length', '>=', $this->length + 3)
                        ->where(['is_deleted' => 0,'is_active' => 1]);
                })->orWhere(function($query) {

                    $query->where('rectangle_width', '>=', $this->width + 3)
                        ->where('rectangle_length', '>=', $this->length + 3)
                        ->where(['is_deleted' => 0,'is_active' => 1]);
                })->orderBy('priority', 'DESC');

        if ($limit) {
            $mainQuery->take($limit);
        }

        return $mainQuery;
    }

    public function getArchitectureList($userId = null, $relatedArchitectureIdList = null, $request, $isFrontend = false, $justCount = false)
    {
        $query =
            $this->with(['translations', 'images', 'building_type', 'aac_kit', 'floors'])
                ->where(function($query) use ($userId, $relatedArchitectureIdList, $request, $isFrontend) {
                    if ($userId) {
                        $query->where('user_id', $userId);
                    }
                    if (count($relatedArchitectureIdList) > 0) {

                        $query->whereIn('id', $relatedArchitectureIdList);
                    }
                    if ($request->building_type) {

                        $query->where('building_type_id', $request->building_type);
                    }
                    if ($request->floors_min) {

                        $query->where('floors_number', '>=', $request->floors_min);
                    }
                    if ($request->floors_max) {

                        $query->where('floors_number', '<=', $request->floors_max);
                    }
                    if ($request->rooms_min) {

                        $query->where('rooms_number', '>=', $request->rooms_min);
                    }
                    if ($request->rooms_max) {

                        $query->where('rooms_number', '<=', $request->rooms_max);
                    }
                    if ($request->price_min) {

                        $query->where('full_low_price', '>=', $request->price_min);
                    }
                    if ($request->price_max) {

                        $query->where('full_low_price', '<=', $request->price_max);
                    }
                    if ($request->area_min) {

                        $query->where('general_area', '>=', $request->area_min);
                    }
                    if ($request->area_max) {

                        $query->where('general_area', '<=', $request->area_max);
                    }
                    if ($isFrontend) {

                        $query->where('is_active', 1);
                    }
                    $query->where('is_deleted', 0);
                });

        if (!$justCount) {
            if ($isFrontend) {
                if (isset($request['sortby']) && !empty($request['sortby'])) {
                    $explodedSort = explode('.',$request['sortby']);
                    $query->orderBy($explodedSort[0], strtoupper($explodedSort[1]));
                } else {
                    $query->orderBy('created_at', 'DESC');
                }
            }

            $architecture = $query
                ->offset(isset($request->offsetData)?$request->offsetData:0)
                ->limit(isset($request->limit)?$request->limit:20)->get();

            $this->translate($architecture);

        } else {

            $architecture = $query->count();

        }
// original
//        $architecture = $query
//            ->offset(isset($request->offsetData)?$request->offsetData:0)
//            ->limit(isset($request->limit)?$request->limit:20)
//            ->orderBy('created_at', 'DESC')->get();
//
//        $this->translate($architecture);

        return $architecture;
    }

    public function getArchitectureByLocale($locale, $userId = null)
    {
        $architecture = self::select(
                'architecture.*',
                'architecture_translations.exterior_walls',
                'architecture_translations.overlappings',
                'architecture_translations.roof',
                'architecture_translations.boiler',
                'architecture_translations.description')
            ->join('architecture_translations', 'architecture_translations.architecture_id', '=', 'architecture.id')
            ->with(['images', 'building_type', 'aac_kit', 'floors', 'translations', 'user'])
            ->where('architecture_translations.locale', $locale)
            ->where(function($query) use ($userId) {
                if($userId != null) {
                    $query->where('user_id', $userId);
                }
            })
            ->where('is_deleted', 0)
            ->orderBy('created_at', 'DESC')
            ->get();

        $this->translate($architecture);

        return $architecture;
    }

    public function getArchitectureStatistics($period = 'd')
    {
        $groupDate = 'day(created_at) as date';

        if ($period == 'm') {
            $groupDate = 'month(created_at) as date';
        }

        if ($period == 'Y') {
            $groupDate = 'year(created_at) as date';
        }

        $query = $this->select(DB::raw('count(user_id) as count'), DB::raw($groupDate))
            ->where('user_id', Sentinel::getUser()->id)
            ->where(['is_active' => 1, 'is_deleted' => 0])
            ->whereYear('created_at', '=', date('Y'));

        if ($period == "d") {
            $query->whereMonth('created_at', '=', date('m'));
        }

        $architecture = $query
            ->groupBy('date')
            ->get();

        return $architecture;
    }

    public function getFormInitialParams(Request $request) {

        $initParams = [];
        $initParams['priceRange']['maximalPrice'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->max('full_low_price');

        $initParams['priceRange']['minimalPrice'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->min('full_low_price');

        $initParams['areaRange']['maximalArea'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->max('general_area');

        $initParams['areaRange']['minimalArea'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->min('general_area');

        if ($request->get('sortby')) {
            $initParams['sortBy'] = $request->get('sortby');
        }

        $initParams['limit'] = $request->get('limit')?(int)$request->get('limit'):10;
        $initParams['page'] = $request->get('page')?(int)$request->get('page'):1;

        $initParams['buildingType'] = $request->get('building_type')?(int)$request->get('building_type'):0;
        $initParams['floorsRange']['min'] = $request->get('floors_min')?(int)$request->get('floors_min'):'';
        $initParams['floorsRange']['max'] = $request->get('floors_max')?(int)$request->get('floors_max'):'';
        $initParams['roomsRange']['min']  = $request->get('rooms_min')?(int)$request->get('rooms_min'):'';
        $initParams['roomsRange']['max']  = $request->get('rooms_max')?(int)$request->get('rooms_max'):'';
        $initParams['priceRange']['min'] = $request->get('price_min')?:$initParams['priceRange']['minimalPrice'];
        $initParams['priceRange']['max'] = $request->get('price_max')?:$initParams['priceRange']['maximalPrice'];
        $initParams['areaRange']['min']  = $request->get('area_min')?:$initParams['areaRange']['minimalArea'];
        $initParams['areaRange']['max']  = $request->get('area_max')?:$initParams['areaRange']['maximalArea'];

        return $initParams;
    }

    public function hardDelete(HasMany $architecture)
    {
        return $architecture->delete();
    }
}
