<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Currency extends Model
{
    protected $table = 'currency';

    protected $guarded = ['acted_by'];

    protected static $urlECBApi = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

    public static function loadCurrency($actedBy = null)
    {
        $XMLContent = file(self::$urlECBApi);

        $currency = self::pluck('name')->toArray();

        foreach($XMLContent as $line){
            if(preg_match("/currency='([[:alpha:]]+)'/", $line, $currencyCode)){
                if(preg_match("/rate='([[:graph:]]+)'/", $line, $rate) && in_array($currencyCode[1], $currency)){

                    $data = [
                        'rate' => $rate[1],
                        'acted_by' => $actedBy,
                    ];
                    $currencyOne = self::where('name', $currencyCode[1])->first();
                    $currencyOne->update($data);
                }
            }
        }
    }
}
