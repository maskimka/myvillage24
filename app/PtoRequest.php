<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PtoRequest extends Model
{

    protected $guarded = ['id'];

    public function lands()
    {
        return $this->morphedByMany('App\Land', 'pto_requestables');
    }

    public function architecture()
    {
        return $this->morphedByMany('App\Architecture', 'pto_requestables');
    }

    public function buildings()
    {
        return $this->morphedByMany('App\Building', 'pto_requestables');
    }

    public function status() {

        return $this->belongsTo('App\OrderStatus');
    }
}
