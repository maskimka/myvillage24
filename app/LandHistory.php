<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandHistory extends Model
{
    protected $table = 'land_history';

    protected $guarded = ['id', 'acted_user_id', 'action'];
}
