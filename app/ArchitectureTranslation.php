<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchitectureTranslation extends Model
{
    public $timestamps = false;

    protected $guarded = [];
}
