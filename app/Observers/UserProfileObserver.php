<?php

namespace App\Observers;

use App\User;

class UserProfileObserver
{
    /**
     * firing when before user creating
     */
    public function creating(User $user)
    {
        if (!empty($user->first_name)) {
            $user->row_completed += 5;
        }

        if (!empty($user->last_name)) {
            $user->row_completed += 5;
        }

        if ($user->email) {
            $user->row_completed += 10;
        }

        if ($user->gender) {
            $user->row_completed += 5;
        }

        if ($user->birthday) {
            $user->row_completed += 5;
        }

        if ($user->personal_phone) {
            $user->row_completed += 5;
        }

        if ($user->mobile_phone) {
            $user->row_completed += 5;
        }

        if (!empty($user->avatar)) {
            $user->row_completed += 15;
        }

        if ($user->country_id > 0) {
            $user->row_completed += 10;
        }

        if ($user->zip) {
            $user->row_completed += 10;
        }

        if ($user->city) {
            $user->row_completed += 1;
        }

        if ($user->region) {
            $user->row_completed += 1;
        }

        if ($user->state) {
            $user->row_completed += 1;
        }

        if ($user->street) {
            $user->row_completed += 1;
        }

        if ($user->block) {
            $user->row_completed += 0.5;
        }

        if ($user->apartment) {
            $user->row_completed += 0.5;
        }
    }

    /**
     * firing when before user updating
     */
    public function updating(User $user)
    {
        $user->row_completed = 0;

        if (!empty($user->first_name)) {
            $user->row_completed += 5;
        }

        if (!empty($user->last_name)) {
            $user->row_completed += 5;
        }

        if ($user->email) {
            $user->row_completed += 10;
        }

        if ($user->gender) {
            $user->row_completed += 5;
        }

        if ($user->birthday) {
            $user->row_completed += 5;
        }

        if ($user->personal_phone) {
            $user->row_completed += 5;
        }

        if ($user->mobile_phone) {
            $user->row_completed += 5;
        }

        if (!empty($user->avatar)) {
            $user->row_completed += 15;
        }

        if ($user->country_id > 0) {
            $user->row_completed += 10;
        }

        if ($user->zip) {
            $user->row_completed += 10;
        }

        if ($user->city) {
            $user->row_completed += 1;
        }

        if ($user->region) {
            $user->row_completed += 1;
        }

        if ($user->state) {
            $user->row_completed += 1;
        }

        if ($user->street) {
            $user->row_completed += 1;
        }

        if ($user->block) {
            $user->row_completed += 0.5;
        }

        if ($user->apartment) {
            $user->row_completed += 0.5;
        }
    }
}