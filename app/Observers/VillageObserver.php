<?php

namespace App\Observers;

use App\Village;
use Sentinel;

class VillageObserver
{

    public function creating(Village $village)
    {
        $village->user_id = Sentinel::getUser()->id;
    }
    /**
     * firing when before architecture creating
     */
    public function created(Village $village)
    {

        $village->article = "VL-".$village->id;
        $village->save();
    }

    /**
     * firing when before architecture updating
     */
    public function updated(Village $village)
    {

    }
}