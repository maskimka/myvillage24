<?php

namespace App\Observers;

use App\PtoRequest;
use App\PtoRequestHistory;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class PtoRequestObserver
{
    public function created(PtoRequest $ptoRequest)
    {
        $ptoRequestHistory = new PtoRequestHistory();
        $ptoRequestHistory->acted_user_id = (isset(Sentinel::getUser()->id))?:0;
        $ptoRequestHistory->action = "created";
        $ptoRequestHistory->pto_request_id = $ptoRequest->id;
        $ptoRequestHistory->fill($ptoRequest['attributes']);

        $ptoRequestHistory->save();
    }

    public function updating(PtoRequest $ptoRequest)
    {
        $ptoRequestHistory = new PtoRequestHistory();
        $ptoRequestHistory->acted_user_id = Sentinel::getUser()->id;
        $ptoRequestHistory->action = "updated";
        $ptoRequestHistory->pto_request_id = $ptoRequest->id;
        $ptoRequestHistory->fill($ptoRequest['attributes']);

        $ptoRequestHistory->save();
    }
}