<?php

namespace App\Observers;

use App\Building;
use App\BuildingHistory;
use Sentinel;

class BuildingObserver
{

    public function creating(Building $building)
    {
        $building->user_id = Sentinel::getUser()->id;
    }
    /**
     * firing when before architecture creating
     */
    public function created(Building $building)
    {

        $building->article = "BLD-".$building->id;
        $building->save();
    }

    /**
     * firing when before architecture updating
     */
    public function updated(Building $building)
    {

    }
}