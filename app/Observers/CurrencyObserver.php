<?php

namespace App\Observers;

use App\Currency;
use App\CurrencyHistory;
use Illuminate\Support\Facades\Log;

class CurrencyObserver
{
    /**
     * firing when before land updating
     *
     * @param $currency Currency
     */
    public function updated(Currency $currency)
    {
        $data = $currency['attributes'];
        $data['action'] = 'update';
        $data['currency_id'] = $currency->id;
//        $currencyHistory->create($data);
        Log::info('observer');
    }
}