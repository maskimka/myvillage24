<?php

namespace App\Observers;

use App\Architecture;
use App\ArchitectureHistory;
use Sentinel;

class ArchitectureObserver
{

    public function creating(Architecture $architecture)
    {
        $architecture->user_id = Sentinel::getUser()->id;
    }
    /**
     * firing when before architecture creating
     */
    public function created(Architecture $architecture)
    {

        $architecture->article = "AP-".$architecture->id;
        $architecture->save();
    }

    /**
     * firing when before architecture updating
     */
    public function updating(Architecture $architecture)
    {
        $architecture->full_low_price = $architecture->general_area * $architecture->building_price;
        $architecture->full_high_price = $architecture->general_area * $architecture->key_price;
    }
}