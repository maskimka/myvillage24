<?php

namespace App\Observers;

use App\Order;
use App\OrderHistory;
use Sentinel;

class OrderObserver
{
    public function created(Order $order)
    {
        $order_history = new OrderHistory();
        $order_history->acted_user_id = (isset(Sentinel::getUser()->id))?:0;
        $order_history->action = "created";
        $order_history->order_id = $order->id;
        $order_history->fill($order['attributes']);

        $order_history->save();

    }
    /**
     * firing when before land updating
     */
    public function updating(Order $order)
    {
        $order_history = new OrderHistory();
        $order_history->acted_user_id = Sentinel::getUser()->id;
        $order_history->action = "updated";
        $order_history->order_id = $order->id;
        $order_history->fill($order['attributes']);

        $order_history->save();
    }
}