<?php

namespace App\Observers;

use App\Land;
use App\LandHistory;
use Sentinel;

class LandObserver
{
    /**
     * firing when before land creating
     */
    public function creating(Land $land)
    {
        $land->user_id    = Sentinel::getUser()->id;
        $land->country_id = Sentinel::getUser()->country_id;
    }

    public function created(Land $land)
    {
        $land->article = "LN-".$land->id;

        $land_history = new LandHistory();

        $land_history->acted_user_id = Sentinel::getUser()->id;
        $land_history->action = "created";
        $land_history->land_id = $land->id;
        $land_history->fill($land['attributes']);

        $land_history->save();
        $land->save();
    }
    /**
     * firing when before land updating
     */
    public function updating(Land $land)
    {
        $land_history = new LandHistory();
        $land_history->acted_user_id = Sentinel::getUser()->id;
        $land_history->action = "updated";
        $land_history->land_id = $land->id;
        $land_history->fill($land['attributes']);

        $land_history->save();
    }
}