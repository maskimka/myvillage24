<?php

namespace App\Observers;

use App\Architecture;
use App\ArchitectureFloor;

class ArchitectureFloorObserver {

    public function creating ($architecture_floor) {

//        if (!isset($architecture_floor['area']) && empty($architecture_floor['area'])) {
//
//            unset($architecture_floor['area']);
//        }
//
//        if (isset($architecture_floor['number']) && empty($architecture_floor['number'])) {
//
//            unset($architecture_floor['number']);
//        }
//
//        if (!isset($architecture_floor['image'])) {
//
//            $architecture_floor['image'] = [];
//        }
        if (isset($architecture_floor['general_area']) && empty($architecture_floor['general_area'])) {

            unset($architecture_floor['general_area']);
        }
    }

    public function updated(ArchitectureFloor $architectureFloor)
    {

    }
}

