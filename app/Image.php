<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function lands()
    {
        return $this->morphedByMany('App\Land', 'imageables');
    }

    public function architecture()
    {
        return $this->morphedByMany('App\Architecture', 'imageables');
    }

    public function architecture_floor()
    {
        return $this->morphedByMany('App\ArchitectureFloor', 'imageables');
    }
}
