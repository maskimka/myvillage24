<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function getUserCountryIsoCode($request)
    {
        $countryIsoCode = config('country_iso_code');
        if (!empty($request->country_code)) {
            $countryIsoCode = $request->country_code;
        } else {
            $countryIsoCode = $request->cookie('__mv_client_country');
        }

        return strtolower($countryIsoCode);
    }
}
