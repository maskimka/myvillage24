<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Sentinel;

class Wishlist extends Model
{
    protected $guarded = ['wishlistCountItems'];

    public function land()
    {
        return $this->belongsTo('App\Land', 'product_id')
            ->with(['images', 'translations', 'purpose']);
    }

    public function architecture()
    {
        return $this->belongsTo('App\Architecture', 'product_id')
            ->with(['images', 'translations']);
    }

    public function building()
    {
        return $this->belongsTo('App\Building', 'product_id')
            ->with(['images', 'translations', 'lands']);
    }

    public function users()
    {
        return $this->morphedByMany('App\User', 'wishlistables')
            ->where('wishlistables_id', 1);
    }

    public function visitors()
    {
        return $this->morphedByMany('App\Visitor', 'wishlistables')
            ->where('wishlistables_id', 1);
    }

    public function getUserOrVisitor($request)
    {
        $user = Sentinel::getUser();

        if (!$user) {

            $cookie = $request->cookie('__mv_cid', null);

            if($cookie) {

                $user = Visitor::where('client_key', $cookie)->first();
            }
        }
        return $user;
    }

    public function getWishlist($request)
    {
        $lands         = collect();
        $buildings     = collect();
        $architectures = collect();

        $user = $this->getUserOrVisitor($request);
        $wishlist = $user->wishlist()->get();

        foreach ($wishlist as $item) {

            if($item->product_type == 'Land') {
                $lands->push($item->land()->first());
            }

            if($item->product_type == 'Architecture') {
                $architectures->push($item->architecture()->first());
            }

            if($item->product_type == 'Building') {
                $buildings->push($item->building()->first());
            }
        }

        return [
            'lands' => $lands,
            'buildings' => $buildings,
            'architectures' => $architectures
        ];
    }
}
