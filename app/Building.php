<?php

namespace App;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Imageable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Sentinel;
use DB;

class Building extends Model
{
    use Imageable, Translatable;

    protected $table = 'building';

    protected $guarded = [
        'id',
        'aac_kit',
        'images',
        'floors',
        'general_plan',
        'house_section',
        'facade_gallery',
        'interior_gallery',
        'fileuploader-list-images',
        'fileuploader-list-facade_gallery',
        'fileuploader-list-interior_gallery',
        'fileuploader-list-general_plan',
        'fileuploader-list-house_section',
        'title',
        'description',
        'exterior_walls',
        'overlappings',
        'roof',
        'boiler',
        'data_lang',
        'facade',
        'windows',
        'reparation',
    ];

    public function translations()
    {
        return $this->hasMany('App\BuildingTranslation');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function aac_kit()
    {
        return $this->belongsToMany('App\AacKit', 'buildings_acc_kit');
    }

    public function images()
    {
        return $this->morphToMany('App\Image', 'imageables');
    }

    public function lands()
    {
        return $this->morphToMany('App\Land', 'landables');
    }

    public function orders()
    {
        return $this->morphToMany('App\Order', 'orderables');
    }

    public function facadeBuildingGalleryImages() {

        return $this->belongsToMany('App\Image', 'imageables', 'imageables_id')
            ->wherePivot('imageables_type', 'facadeBuildingGallery');
    }

    public function interiorBuildingGalleryImages() {

        return $this->belongsToMany('App\Image', 'imageables', 'imageables_id')
            ->wherePivot('imageables_type','interiorBuildingGallery');
    }
    
    public function underconstructBuildingGalleryImages() {

        return $this->belongsToMany('App\Image', 'imageables', 'imageables_id')
            ->wherePivot('imageables_type','underconstructBuildingGallery');
    }

    public function floors()
    {
        return $this->hasMany('App\ArchitectureFloor')->where('is_deleted', 0);
    }

    public function building_type()
    {
        return $this->belongsTo('App\BuildingType');
    }

    public function getBuildingList($userId = null, $isFrontend = false, $request)
    {
        $query =
            $this->with(['images', 'building_type', 'aac_kit', 'floors', 'lands'])
                ->where(function($query) use ($userId, $isFrontend, $request) {
                    if ($userId) {
                        $query->where('user_id', $userId);
                    }
                    if ($isFrontend) {
                        if ($request->building_type_id) {
                            $query->where('building_type_id', '=', $request->building_type_id);
                        }
                        if ($request->floors_number) {
                            $query->where('floors_number', '=', $request->floors_number);
                        }
                        if ($request->price_min) {
                            $query->where('building_price', '>=', $request->price_min);
                        }
                        if ($request->price_max) {
                            $query->where('building_price', '<=', $request->price_max);
                        }
                        if ($request->area_min) {
                            $query->where('general_area', '>=', $request->area_min);
                        }
                        if ($request->area_max) {
                            $query->where('general_area', '<=', $request->area_max);
                        }
                        $query->where('is_active', 1);
                    }
                    $query->where('is_deleted', 0);
                }
            );

        if (isset($request['sortby']) && !empty($request['sortby'])) {
            $explodedSort = explode('.',$request['sortby']);
            $query->orderBy($explodedSort[0], strtoupper($explodedSort[1]));
        } else {
            $query->orderBy('created_at', 'DESC');
        }

        $buildings = $query->get();

        $this->translate($buildings);

        return $buildings;
    }

    public function getBuildingsByLocale($locale, $userId = null)
    {
        $buildings = self::select(
            'building.*',
            'building_translations.title',
            'building_translations.exterior_walls',
            'building_translations.overlappings',
            'building_translations.roof',
            'building_translations.boiler',
            'building_translations.facade',
            'building_translations.windows',
            'building_translations.reparation',
            'building_translations.description')
            ->join('building_translations', 'building_translations.building_id', '=', 'building.id')
            ->with(['images', 'building_type', 'aac_kit', 'floors', 'lands', 'translations', 'user'])
            ->where('building_translations.locale', $locale)
            ->where(function($query) use ($userId) {
                if($userId != null) {
                    $query->where('user_id', $userId);
                }
            })
            ->where('is_deleted', 0)
            ->orderBy('created_at', 'DESC')
            ->get();

        $this->translate($buildings);

        foreach ($buildings as $building) {

            $this->translate($building->lands);
        }

        return $buildings;
    }

    public function getBuildingsStatistics($period = 'd')
    {
        $groupDate = 'day(created_at) as date';

        if ($period == 'm') {
            $groupDate = 'month(created_at) as date';
        }

        if ($period == 'Y') {
            $groupDate = 'year(created_at) as date';
        }

        $query = $this->select(DB::raw('count(user_id) as count'), DB::raw($groupDate))
            ->where('user_id', Sentinel::getUser()->id)
            ->where(['is_active' => 1, 'is_deleted' => 0])
            ->whereYear('created_at', '=', date('Y'));

        if ($period == "d") {
            $query->whereMonth('created_at', '=', date('m'));
        }

        $buildings = $query
            ->groupBy('date')
            ->get();

        return $buildings;
    }

    public function getFormInitialParams(Request $request) {

        $initParams = [];
        $initParams['priceRange']['maximalPrice'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->max('building_price');

        $initParams['priceRange']['minimalPrice'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->min('building_price');

        $initParams['areaRange']['maximalArea'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->max('general_area');

        $initParams['areaRange']['minimalArea'] =
            self::where('is_active', 1)
                ->where('is_deleted', 0)
                ->min('general_area');

        if ($request->get('sortby')) {
            $initParams['sortBy'] = $request->get('sortby');
        }

        $initParams['limit'] = $request->get('limit')?(int)$request->get('limit'):10;
        $initParams['page'] = $request->get('page')?(int)$request->get('page'):1;

        $initParams['buildingType'] = $request->get('building_type')?(int)$request->get('building_type'):0;
        $initParams['floorsRange']['min'] = $request->get('floors_min')?(int)$request->get('floors_min'):'';
        $initParams['floorsRange']['max'] = $request->get('floors_max')?(int)$request->get('floors_max'):'';
        $initParams['priceRange']['min'] = $request->get('price_min')?:$initParams['priceRange']['minimalPrice'];
        $initParams['priceRange']['max'] = $request->get('price_max')?:$initParams['priceRange']['maximalPrice'];
        $initParams['areaRange']['min']  = $request->get('area_min')?:$initParams['areaRange']['minimalArea'];
        $initParams['areaRange']['max']  = $request->get('area_max')?:$initParams['areaRange']['maximalArea'];

        return $initParams;

    }

    public function hardDelete(HasMany $buildings)
    {
        return $buildings->delete();
    }
}
