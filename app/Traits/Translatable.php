<?php

namespace App\Traits;

use \Illuminate\Database\Eloquent\Model;
use Dedicated\GoogleTranslate\Translator;
use App\Locality;

trait Translatable
{
    protected $translatableFields;

    protected $translator;

    public function init($locale) {

//        $this->translator = new Translator();
//        $this->translator->setTargetLang($locale);
        $this->translatableFields = [
            'title'          => '',
            'description'    => '',
            'street_number'  => '',
            'street'         => '',
            'sublocality'    => '',
            'locality'       => '',
            'administrative' => '',
            'country'        => '',
            'exterior_walls' => '',
            'overlappings'   => '',
            'roof'           => '',
            'boiler'         => '',
            'name'           => '',
        ];
    }

    public function translate($models)
    {
        $this->init(\Lang::getLocale());

        foreach ($models as $key => $model) {
            $models[$key]['translation'] = collect($model->translations->reduce(function ($translateCollection, $item) {
                $translateCollection[$item['locale']] = $item;
                return $translateCollection;
            }, []));
            /**
             * Check if related classes instance of Model
             * Check if Model use App\Traits\Translatable trait
             * If it does then make recursive translation
             */
            foreach ($model->relations as $relatedModelClassName => $relatedModel) {
                if ($relatedModel instanceof Model) {
                    $relatedModel = collect([$relatedModel]);
                }

                if (isset($relatedModel[0])) {
                    if (in_array('App\Traits\Translatable', class_uses($relatedModel[0]))) {
                        $model->translate($relatedModel);
                    }
                }
            }

            foreach ($this->translatableFields as $field => $value) {
                if (array_key_exists($field, $model->translations[0]->toArray())) {
                    if (isset($model->translation[config('app.locale')]) && !empty($model->translation[config('app.locale')][$field])) {
                        $models[$key][$field] = $model->translation[config('app.locale')][$field];
                    }
                    elseif (isset($model->translation[config('app.fallback_locale')]) && !empty($model->translation[config('app.fallback_locale')][$field])) {
//                        $models[$key][$field] = $this->translator->setTargetLang('ru')->translate($value);
                        $models[$key][$field] = $model->translation[config('app.fallback_locale')][$field];
                    } else {
                        $models[$key][$field] = $value;
                    }
                }
            }

            unset($model['translation']);
        }
    }
}