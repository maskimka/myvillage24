<?php

namespace App\Traits;

use App\Image as ImageModel;
use Image;
use Mockery\CountValidator\Exception;
use ZipArchive;

trait Imageable
{
    public function oneFileUploadAvatar($file, $path, $resizeParams = null, $cropParams = null)
    {
        $old_image = $this->avatar;

        $avatar = $file;
        $filename = md5($file->getClientOriginalName().time()).'.'.$avatar->getClientOriginalExtension();

        $image = Image::make($avatar);

        if ($cropParams) {
            $cropParams = json_decode($cropParams, true);
            $image->crop(intval($cropParams['width']), intval($cropParams['height']), intval($cropParams['x']), intval($cropParams['y']));
        }

        if ($resizeParams) {

            $image->resize($resizeParams['width'], $resizeParams['height']);
        }

        $image->save(public_path($path.$filename));

        $this->avatar = $filename;
        $obj = $this->save();

        if ($obj) {
            if(!empty($old_image) && file_exists(public_path($path.$old_image))) {
                // remove old avatar image
                unlink(public_path($path.$old_image));
            }
        } else {

           throw new Exception(501);
        }
    }

    public function oneFileUpload($file, $path, $options)
    {
        if (is_array($options)) {

            $filename = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();

            $image = Image::make($file);
            $image->backup();

            if ($options['cropParams']) {

                $cropParams = json_decode($options['cropParams'], true);
                $image->crop(intval($cropParams['width']), intval($cropParams['height']), intval($cropParams['x']), intval($cropParams['y']));
            }

            if (isset($options['resizeParams'])) {

                $image->resize($options['resizeParams'][0], $options['resizeParams'][0]);
            }

            // save thumbnail image
            $image->save(public_path($path.'/thumbnails/'.$filename));
            // reset image (return to backup state) and save original image
            $image->reset();
            // save original image
            $image->save(public_path($path.'/original/'.$filename));

            if ($options['old_image']) {
                // remove old avatar image
                if (file_exists($path."/original/".$options['old_image']))
                {
                    unlink(public_path($path."/original/".$options['old_image']));
                }
                if (file_exists($path."/thumbnails/".$options['old_image']))
                {
                    unlink(public_path($path."/thumbnails/".$options['old_image']));
                }
            }

            return $filename;

        } else {

            throw new Exception(501);
        }
    }

    public function oneFileFitUpload($file, $path, $fitW = 400, $fitH = 300, $fileListMap, $old_image = null)
    {
        $fileUploaderImageMap = json_decode($fileListMap);

        if ($old_image) {

            if (isset($fileUploaderImageMap[0])) {
                
                $tmpArray = explode(':', $fileUploaderImageMap[0]);

                $fileUploaderImageMap[0] = substr($tmpArray[0], strrpos($tmpArray[0], '/') + 1);

                if ($fileUploaderImageMap[0] !== $old_image) {

                    $this->onFileUnlink($path, $old_image);
                    $old_image = null;
                }

            } else {

                $this->onFileUnlink($path, $old_image);
                $old_image = null;
            }
        }

        $filename = $old_image;

        if ( $file ) {
            
            $filename = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();

            $image = Image::make($file);

            $image->backup();

            $image->fit($fitW, $fitH);

            // save thumbnail image
            $image->save(public_path($path.'/thumbnails/'.$filename));
            // reset image (return to backup state) and save original image
            $image->reset();
            // save original image
            $image->save(public_path($path.'/original/'.$filename));

        }

        return $filename;

    }

    public function multipleFileUpload($files, $path, $fitWidth = 400, $fitHeight = 270, $fileUploaderImageMap = null, $morphyType=null)
    {
        $file_id_list = array();
        $current_files = $this->images()->get();

        if ($morphyType) {

            $method = $morphyType.'Images';
            $current_files = $this->{$method}()->get();

        }

        $fileUploaderImageMap = json_decode($fileUploaderImageMap);

        if (count($current_files) > 0) {

            foreach($fileUploaderImageMap as $key => $item)
            {
                $tmpArray = explode(':', $item);

                if ( $tmpArray[0] ) {

                    $fileUploaderImageMap[$key] = substr($tmpArray[0], strrpos($tmpArray[0], '/') + 1);

                } else {

                    unset($fileUploaderImageMap[$key]);
                }
            }

            foreach($current_files as $item) {

                if (in_array($item->name, $fileUploaderImageMap)) {

                    array_push($file_id_list, $item->id);

                } else {

                    if (file_exists(public_path($path.'/thumbnails/'.$item->name))) {

                        unlink(public_path($path.'/thumbnails/'.$item->name));
                    }

                    if (file_exists(public_path($path.'/original/'.$item->name))) {

                        unlink(public_path($path.'/original/'.$item->name));
                    }
                }
            }

        }

        if ( count($files) )
        {
            foreach($files as $file) {

                $filename = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();

                $image = Image::make($file);

                $modelImage = new ImageModel();
                $modelImage->name     = $filename;
                $modelImage->bytesize = $image->filesize();
                $modelImage->path     = $path;

                if ($modelImage->save()) {

                    // backup status
                    $image->backup();

                    $image->fit($fitWidth, $fitHeight);

                    // save thumbnail image
                    $image->save(public_path($path.'/thumbnails/'.$filename));

                    // reset image (return to backup state) and save original image
                    $image->reset();

                    if ($image->save(public_path($path.'/original/'.$filename))) {

                        array_push($file_id_list, $modelImage->id);
                    }
                }
            }
        }

        $file_id_final_list = [];

        if ($morphyType) {

            if (count($file_id_list)) {

                foreach($file_id_list as $key => $item) {

                    $file_id_final_list[$item] = ['imageables_type' => $morphyType];
                }
            }

            $obj = $this->{$method}()->sync($file_id_final_list, true);

        } else {

            $file_id_final_list = $file_id_list;

            $obj = $this->images()->sync($file_id_final_list, true);
        }

        if (!$obj) {

            throw new Exception(501);
        }
    }

    public function saveSocialAvatar($link)
    {

        $filename = md5($link) . '.jpg';

        Image::make($link)->save(public_path('img/uploads/profile/' . $filename));

        return $filename;
    }

    public function archiveUpload($file, $path = 'docs')
    {
        $filename = md5($file->getClientOriginalName() . time()) . '.' . $file->getClientOriginalExtension();
        move_uploaded_file($file->getPathName(), public_path($path)."/" . $filename);

        return $filename;
    }


    private function onFileUnlink($path, $old_image)
    {
        if (file_exists($path."/original/".$old_image))
        {
            unlink(public_path($path."/original/".$old_image));
        }
        if (file_exists($path."/thumbnails/".$old_image))
        {
            unlink(public_path($path."/thumbnails/".$old_image));
        }
    }
}