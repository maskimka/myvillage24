<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandSizeMap extends Model
{
    protected $table = 'land_size_map';
}
