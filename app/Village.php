<?php

namespace App;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Imageable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Sentinel;
use DB;

class Village extends Model
{
    use Imageable, Translatable, SoftDeletes;

    protected $table = 'village';

    protected $guarded = [
        'id',
        'images',
        'general_plan',
        'title',
        'communications',
        'infrastructure',
        'transport',
        'fileuploader-list-general_plan',
        'data_lang',
    ];

    protected $dates = ['deleted_at'];

    public function translations()
    {
        return $this->hasMany('App\VillageTranslation');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function lands()
    {
        return $this->morphToMany('App\Land', 'landables');
    }

    public function buildings()
    {
        return $this->morphToMany('App\Building', 'buildingables');
    }

    public function images()
    {
        return $this->morphToMany('App\Image', 'imageables');
    }

    public function communications()
    {
        return $this->belongsToMany('App\Communication', 'village_communications');
    }

    public function infrastructure()
    {
        return $this->belongsToMany('App\Infrastructure', 'village_infrastructure');
    }

    public function transport()
    {
        return $this->belongsToMany('App\Transport', 'village_transport');
    }

    public function getVillageList($userId = null, $isFrontend = false, $request)
    {
        $query =
            $this->with(['images', 'lands', 'buildings'])
                ->where(function($query) use ($userId, $isFrontend, $request) {
                    if ($userId) {
                        $query->where('user_id', $userId);
                    }
                    if ($isFrontend) {
                        if ($request->area_min) {
                            $query->where('general_area', '>=', $request->area_min);
                        }
                        if ($request->area_max) {
                            $query->where('general_area', '<=', $request->area_max);
                        }
                        $query->where('is_active', 1);
                    }
                }
            );

        if (isset($request['sortby']) && !empty($request['sortby'])) {
            $explodedSort = explode('.',$request['sortby']);
            $query->orderBy($explodedSort[0], strtoupper($explodedSort[1]));
        } else {
            $query->orderBy('created_at', 'DESC');
        }

        $villages = $query->get();
        $this->translate($villages);

        return $villages;
    }

    public function getVillagesByLocale($locale, $userId = null)
    {
        $villages = self::select(
            'village.*',
            'village_translations.title',
            'village_translations.description')
            ->join('village_translations', 'village_translations.village_id', '=', 'village.id')
            ->with(['images', 'lands', 'buildings', 'translations', 'user'])
            ->where('village_translations.locale', $locale)
            ->where(function($query) use ($userId) {
                if ($userId != null) {
                    $query->where('user_id', $userId);
                }
            })
            ->orderBy('created_at', 'DESC')
            ->get();

        $this->translate($villages);
        foreach ($villages as $village) {
            $this->translate($village->lands);
//            $this->translate($village->buildings);
        }

        return $villages;
    }

    public function getFormInitialParams(Request $request)
    {
        $initParams = [];
        $initParams['areaRange']['maximalArea'] =
            self::where('is_active', 1)
                ->max('general_area');

        $initParams['areaRange']['minimalArea'] =
            self::where('is_active', 1)
                ->min('general_area');

        if ($request->get('sortby')) {
            $initParams['sortBy'] = $request->get('sortby');
        }

        $initParams['limit'] = $request->get('limit')?(int)$request->get('limit'):10;
        $initParams['page'] = $request->get('page')?(int)$request->get('page'):1;

        $initParams['areaRange']['min']  = $request->get('area_min')?:$initParams['areaRange']['minimalArea'];
        $initParams['areaRange']['max']  = $request->get('area_max')?:$initParams['areaRange']['maximalArea'];

        return $initParams;

    }

    public function hardDelete(HasMany $villages)
    {
        return $villages->delete();
    }
}
