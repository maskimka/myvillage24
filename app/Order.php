<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [
        'id'
    ];

    public function lands()
    {
        return $this->morphedByMany('App\Land', 'orderables');
    }

    public function architecture()
    {
        return $this->morphedByMany('App\Architecture', 'orderables');
    }

    public function buildings()
    {
        return $this->morphedByMany('App\Building', 'orderables');
    }

    public function getRelatedModelArticle() {

        if ($this->lands->count()) {

            return $this->lands[0]->article;

        } elseif ($this->architecture->count()) {

            return $this->architecture[0]->article;

        } else if($this->buildings->count()){

            return $this->buildings[0]->article;

        }
    }

    public function status() {

        return $this->belongsTo('App\OrderStatus');
    }
}
