<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Cookie\CookieJar as Cookie;

class Visitor extends Model
{
    protected $guarded = [ ];

    public function wishlist() {

        return $this->morphToMany('App\Wishlist', 'wishlistables');
    }
}
