const elixir = require('laravel-elixir');


var nib = require('nib');
var stylus = require('laravel-elixir-stylus');

require('laravel-elixir-vueify');

// require("laravel-elixir-react");
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss');
    mix.copy('resources/assets/vendor/bootstrap/fonts', 'public/fonts');
    mix.copy('resources/assets/vendor/font-awesome/fonts', 'public/fonts')
    mix.styles([
        'resources/assets/vendor/bootstrap/css/bootstrap.css',
        'resources/assets/vendor/animate/animate.css',
        'resources/assets/vendor/font-awesome/css/font-awesome.css',
    ], 'public/css/vendor.css', './');
    mix.scripts([
        'resources/assets/vendor/jquery/jquery-3.1.1.min.js',
        'resources/assets/vendor/jquery/jquery-ui.min.js',
        'resources/assets/vendor/bootstrap/js/bootstrap.js',
        'resources/assets/vendor/metisMenu/jquery.metisMenu.js',
        'resources/assets/vendor/slimscroll/jquery.slimscroll.min.js',
        'resources/assets/vendor/pace/pace.min.js',
        'resources/assets/js/app.js'
    ], 'public/js/app.js', './');
    mix.browserify([
        'resources/assets/js/common.js'
    ], 'public/js/frontend/common.js', './');
    mix.browserify([
       'resources/assets/js/admin.js'
    ], 'public/js/admin/common.js', './');
    mix.browserify('bundle.js');
    mix.browserify("front-components.js", "public/js/frontend/");
    mix.version(['js/frontend/common.js']);
    mix
        .stylus('main.styl', 'public/css/frontend/new/styles.css', {
            use: [nib()]
        });
});
